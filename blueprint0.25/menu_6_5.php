<?php
    $select = 'open_menu_1';
    $select2 = 'menu_6';
    $select3 = 'menu_6';
    $select4 = 'menu_6_5';
	
?>
<!--  -->
<? include('inc.header.php');?>
<!--  -->
<? include('inc.navbar.php');?>
<!--  -->
<? include('inc.menu.php');?>
<!-- <div class="layout-main"> -->
<!--  -->
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <h4 class="m-t-0">
                <!-- <span class="d-ib text-danger">--</span> <span class="icon icon-angle-double-right"></span> -->
                <!-- <span class="d-ib">ข้อมูลหลัก</span> -->
                <span class="d-ib text-primary">ข้อมูลหลัก</span> <span class="icon icon-angle-double-right"></span> ซ่อมบำรุง <span class="icon icon-angle-double-right"></span> รายละเอียดงานซ่อมบำรุง
                <!-- <span class="d-ib">
                    <a class="title-bar-shortcut" href="#" title="Add to shortcut list" data-container="body" data-toggle-text="Remove from shortcut list" data-trigger="hover" data-placement="right" data-toggle="tooltip">
                        <span class="sr-only">Add to shortcut list</span>
                    </a>
                </span> -->
            </h4>
            <!-- <p class="title-bar-description">
                <small>Latest update on 01/01/2017 - 10.57 am.</small>
            </p> -->
        </div>

        <div class="row gutter-xs">
            <div class="col-xs-12">
                <!--  -->
                <div class="card">
                    <!-- <div class="card-header">
                        <a href="javascript:window.history.back(-1);" class="btn btn-sm btn-outline-primary"><span class="icon icon-long-arrow-left"></span>&nbsp;&nbsp;Back</a>&nbsp;&nbsp;
                        <strong class="">Latest update on 01/01/2017 - 10.57 am.</strong>
                    </div> -->
                    <div class="card-body">
                        <!--  -->
                        <? include('inc.home_top_filter.php');?>
                        <!--  -->
                        <div class="table-responsive">
                             <table id="demo-datatables-2" class="table table-striped table-bordered table-nowrap dataTable text-center no-footer" cellspacing="0" width="100%">
                                <thead>
                                    <tr class="at_bg_table_blue">
                                        <th class="text-center">รหัสรายละเอียดงาน</th>
                                        <th class="text-center">รายละเอียดงาน</th>
                                        <th class="text-center">เริ่มใช้งาน</th>
                                        <th class="text-center">ปรับแก้ไข</th>
                                    </tr>
                                </thead>
                                <tbody>
                                
                                    <tr>
                                        <td>001</td>
                                        <td>เปลี่ยนตะขอล่าง</td>
                                        <td>27/05/2010 14:20 น.</td>
                                        <td class="text-center" style="display: table-cell;">
                                           <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_6_5.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                                            <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                                                <span class="icon icon-lg icon-close"></span>
                                            </button>
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td>002</td>
                                        <td>เปลี่ยนผ้ากรองกระสวย</td>
                                        <td>27/05/2010 15:05 น.</td>
                                        <td class="text-center" style="display: table-cell;">
                                           <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_6_5.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                                            <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                                                <span class="icon icon-lg icon-close"></span>
                                            </button>
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td>003</td>
                                        <td></td>
                                        <td>27/05/2010 17:07 น.</td>
                                        <td class="text-center" style="display: table-cell;">
                                           <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_6_5.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                                            <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                                                <span class="icon icon-lg icon-close"></span>
                                            </button>
                                        </td>
                                    </tr>
                                
                                    <tr>
                                        <td>004</td>
                                        <td></td>
                                        <td>12/06/2010 9:56 น.</td>
                                        <td class="text-center" style="display: table-cell;">
                                           <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_6_5.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                                            <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                                                <span class="icon icon-lg icon-close"></span>
                                            </button>
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td>005</td>
                                        <td></td>
                                        <td>16/06/2010 17:32 น.</td>
                                        <td class="text-center" style="display: table-cell;">
                                           <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_6_5.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                                            <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                                                <span class="icon icon-lg icon-close"></span>
                                            </button>
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td>006</td>
                                        <td></td>
                                        <td>27/05/2010 14:20 น.</td>
                                        <td class="text-center" style="display: table-cell;">
                                           <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_6_5.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                                            <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                                                <span class="icon icon-lg icon-close"></span>
                                            </button>
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td>007</td>
                                        <td></td>
                                        <td>27/05/2010 15:05 น.</td>
                                        <td class="text-center" style="display: table-cell;">
                                           <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_6_5.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                                            <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                                                <span class="icon icon-lg icon-close"></span>
                                            </button>
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td>008</td>
                                        <td></td>
                                        <td>227/05/2010 17:07 น.</td>
                                        <td class="text-center" style="display: table-cell;">
                                           <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_6_5.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                                            <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                                                <span class="icon icon-lg icon-close"></span>
                                            </button>
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td>009</td>
                                        <td></td>
                                        <td>12/06/2010 9:56 น.</td>
                                        <td class="text-center" style="display: table-cell;">
                                           <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_6_5.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                                            <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                                                <span class="icon icon-lg icon-close"></span>
                                            </button>
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td>010</td>
                                        <td></td>
                                        <td>16/06/2010 17:32 น.</td>
                                        <td class="text-center" style="display: table-cell;">
                                           <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_6_5.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                                            <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                                                <span class="icon icon-lg icon-close"></span>
                                            </button>
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td>011</td>
                                        <td></td>
                                        <td>27/05/2010 15:05 น.</td>
                                        <td class="text-center" style="display: table-cell;">
                                           <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_6_5.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                                            <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                                                <span class="icon icon-lg icon-close"></span>
                                            </button>
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td>012</td>
                                        <td></td>
                                        <td>27/05/2010 17:07 น.</td>
                                        <td class="text-center" style="display: table-cell;">
                                           <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_6_5.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                                            <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                                                <span class="icon icon-lg icon-close"></span>
                                            </button>
                                        </td>
                                    </tr>


                            
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
            </div>
        </div>










    </div>
</div>
<!--  -->
<? include('inc.footer.php');?>
<!--  -->
<? include('inc.footer.script.php');?>
<!--  -->
