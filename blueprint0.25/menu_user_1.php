<?php
    $select = 'open_menu_9';
    $select2 = 'menu_9_1';
	$select3 = 'menu_9_1';
	$select4 = 'menu_9_1';
?>
<!--  -->
<? include('inc.header.php');?>
<!--  -->
<? include('inc.navbar.php');?>
<!--  -->
<? include('inc.menu.php');?>
<!-- <div class="layout-main"> -->
<!--  -->
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <h4 class="m-t-0">
                <!-- <span class="d-ib text-danger">--</span> <span class="icon icon-angle-double-right"></span> -->
                <!-- <span class="d-ib">ข้อมูลหลัก</span> -->
                 <span class="d-ib text-primary"> ผู้ดูแลระบบ</span> <span class="icon icon-angle-double-right"> กำหนดสิทธิผู้ใช้งานระบบ</span>  
                <!-- <span class="d-ib">
                    <a class="title-bar-shortcut" href="#" title="Add to shortcut list" data-container="body" data-toggle-text="Remove from shortcut list" data-trigger="hover" data-placement="right" data-toggle="tooltip">
                        <span class="sr-only">Add to shortcut list</span>
                    </a>
                </span> -->
            </h4>
            <!-- <p class="title-bar-description">
                <small>Latest update on 01/01/2017 - 10.57 am.</small>
            </p> -->
        </div>

        <div class="row gutter-xs">
            <div class="col-xs-12">
                <!--  -->
                <div class="card">
                    <!-- <div class="card-header">
                        <a href="javascript:window.history.back(-1);" class="btn btn-sm btn-outline-primary"><span class="icon icon-long-arrow-left"></span>&nbsp;&nbsp;Back</a>&nbsp;&nbsp;
                        <strong class="">Latest update on 01/01/2017 - 10.57 am.</strong>
                    </div> -->
                    <div class="card-body">
                        <!--  -->
<div class="at_add_box">
    <div class="row">
        <div class="col-sm-12 text-right">
            <button class="btn btn-success" type="button"><span class="icon icon-lg icon-plus"></span>&nbsp;&nbsp;เพิ่มใหม่</button>
            <button onClick="window.print()" class="btn btn-default" type="button"><span class="icon icon-lg icon-print"></span>&nbsp;&nbsp;พิมพ์</button>
            <a href="images/SampleXLSFile_212kb.xls" class="btn btn-warning" type="button"><span class="icon icon-lg icon-file-text-o"></span>&nbsp;&nbsp;ส่งออกไฟล์</a>
        </div>
    </div>
</div>
                        <!--  -->
                        <div class="table-responsive">
                            <table id="demo-datatables-2" class="table table-striped table-bordered table-nowrap dataTable text-center no-footer" cellspacing="0" width="100%">
                                <thead>
                                    <tr class="at_bg_table_blue">
                                        <th class="text-center">ชื่อสิทธิ</th>
                                        <th class="text-center">คำอธิบาย</th>
										<th class="text-center">สถานะการใช้งาน</th>
										<th class="text-center">วันหมดอายุ</th>
										<th class="text-center">ปรับแก้ไข</th>
                                    </tr>
                                </thead>
                                <tbody>
								<?php	
								for($i = 0;$i<=4;$i++){
								?>
								<tr>
    <td>Administrator</td>
    <td>Administrator Role</td>
	<td>ใช้งานอยู่</td>
	<td>ไม่มีวันหมดอายุ</td>
	<td class="text-center" style="display: table-cell;">
	 <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="menu_user_1_1.php" type="button">
	 <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
<tr>
    <td>Marketing Manager</td>
    <td>Manager Role</td>
	<td>ไม่ใช้งาน</td>
	<td>20/07/2018</td>
	<td class="text-center" style="display: table-cell;">
	 <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="menu_user_1_1.php" type="button">
	 <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
								<?php
								}
								?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
            </div>
        </div>










    </div>
</div>
<!--  -->
<? include('inc.footer.php');?>
<!--  -->
<? include('inc.footer.script.php');?>
<!--  -->