<?php
    $select = 'open_menu_1';
    $select2 = 'menu_9';
    $select3 = 'menu_9';
    $select4 = 'menu_2_3';
?>
<!--  -->
<? include('inc.header.php');?>
<!--  -->
<? include('inc.navbar.php');?>
<!--  -->
<? include('inc.menu.php');?>
<!-- <div class="layout-main"> -->
<!--  -->
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <h4 class="m-t-0">
                <!-- <span class="d-ib text-danger">--</span> <span class="icon icon-angle-double-right"></span> -->
                <!-- <span class="d-ib">ข้อมูลหลัก</span> -->
                <span class="d-ib text-primary">การตั้งค่า</span> <span class="icon icon-angle-double-right"></span> ข้อมูลอ้างอิง <span class="icon icon-angle-double-right"></span> ระดับคุณภาพ
                <!-- <span class="d-ib">
                    <a class="title-bar-shortcut" href="#" title="Add to shortcut list" data-container="body" data-toggle-text="Remove from shortcut list" data-trigger="hover" data-placement="right" data-toggle="tooltip">
                        <span class="sr-only">Add to shortcut list</span>
                    </a>
                </span> -->
            </h4>
            <!-- <p class="title-bar-description">
                <small>Latest update on 01/01/2017 - 10.57 am.</small>
            </p> -->
        </div>

        <div class="row gutter-xs">
            <div class="col-xs-12">
                <!--  -->
                <div class="card">
                    <!-- <div class="card-header">
                        <a href="javascript:window.history.back(-1);" class="btn btn-sm btn-outline-primary"><span class="icon icon-long-arrow-left"></span>&nbsp;&nbsp;Back</a>&nbsp;&nbsp;
                        <strong class="">Latest update on 01/01/2017 - 10.57 am.</strong>
                    </div> -->
                    <div class="card-body">
                        <!--  -->
                        <? include('inc.home_top_filter.php');?>
                        <!--  -->
                        <div class="table-responsive">
                              <table id="demo-datatables-2" class="table table-striped table-bordered table-nowrap dataTable text-center no-footer" cellspacing="0" width="100%">
                                <thead>
<tr class="at_bg_table_blue">
     <td class="text-center">รหัสระดับคุณภาพ</td>
    <td class="text-center">ชื่อระดับคุณภาพ</td>
    <td class="text-center">รหัส Outsource</td>
    <td class="text-center">ชื่อ OutSource</td>
    <td class="text-center">Flag OutSource</td>
    <td class="text-center">(ShortDesc)</td>
    <td class="text-center">(plan_Flag)</td>
    <td class="text-center">เริ่มใช้งาน</td>
    <th class="text-center">ปรับแก้ไข</th>
</tr>
                                </thead>
                                <tbody>

<tr height=19 style='height:14.25pt'>
  <td height=19  style='height:14.25pt'>A</td>
  <td >Outsource<span style='mso-spacerun:yes'>&nbsp; </span>เกรด A
  (ใยอันฮุย/ซานตง)</td>
  <td >CHN</td>
  <td >CHINA</td>
  <td >Y</td>
  <td >Outsource<span style='mso-spacerun:yes'>&nbsp; </span>เกรด A</td>
  <td >N</td>
  <td >28/02/2556 17:00 น.</td>
  <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_2_3.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
 </tr>
 <tr height=19 style='height:14.25pt'>
  <td>C</td>
  <td >Outsouce<span style='mso-spacerun:yes'>&nbsp; </span>เกรด B
  (ใยอันฮุย/ซานตง)</td>
  <td >CHN</td>
  <td >CHINA</td>
  <td >Y</td>
  <td >Outsouce<span style='mso-spacerun:yes'>&nbsp; </span>เกรด B</td>
  <td >N</td>
  <td >28/02/2556 16:59 น.</td>
  <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_2_3.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
 </tr>
 <tr height=19 style='height:14.25pt'>
  <td>F</td>
  <td >FIGHTING(คุณภาพสินค้าต่ำกว่ามาตฐานเล็กน้อยในเรื่องความนิ่ม
  ความใสเงา/ขนาดใย)</td>
  <td >KKF</td>
  <td >KKF</td>
  <td >N</td>
  <td >FIGHTING</td>
  <td >Y</td>
  <td >06/11/2556 12:05 น.</td>
  <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_2_3.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
 </tr>
 <tr height=19 style='height:14.25pt'>
  <td>G</td>
  <td >อวน New Process (Premium)</td>
  <td >KKF</td>
  <td >KKF</td>
  <td >N</td>
  <td></td>
  <td >Y</td>
  <td >20/04/2561 12:18 น.</td>
  <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_2_3.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
 </tr>
 <tr height=19 style='height:14.25pt'>
  <td>I</td>
  <td >อวน New Process (Super Low Cost)</td>
  <td >KKF</td>
  <td >KKF</td>
  <td >N</td>
  <td></td>
  <td >Y</td>
  <td >23/04/2556 16:13 น.</td>
   <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_2_3.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
 </tr>
 <tr height=19 style='height:14.25pt'>
  <td>K</td>
  <td >อวนดำ Type<span style='mso-spacerun:yes'>&nbsp; </span>B
  (อวนดำ ร้อยหู ไม่ปะดำ) Low Cost</td>
  <td >MYM</td>
  <td >KKF/MYENMAR</td>
  <td >N</td>
  <td></td>
  <td >Y</td>
  <td >28/01/2558 17:29 น.</td>
   <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_2_3.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
 </tr>
 <tr height=19 style='height:14.25pt'>
  <td>L</td>
  <td >อวนดำ Type C (อวนดำ ร้อยหู ปะดำ) Low Cost</td>
  <td >MYM</td>
  <td >KKF/MYENMAR</td>
  <td >N</td>
  <td></td>
  <td >Y</td>
  <td >28/01/2558 17:30 น.</td>
  <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_2_3.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td></tr>
 <tr height=19 style='height:14.25pt'>
  <td>M</td>
  <td >อวนดำ Type A<span style='mso-spacerun:yes'>&nbsp;
  </span>(อวนดำ ไม่ร้อยหู ไม่ปะดำ)</td>
  <td >MYM</td>
  <td >KKF/MYENMAR</td>
  <td >N</td>
  <td >อวนดำ Type A</td>
  <td >Y</td>
  <td >28/11/2556 10:54 น.</td>
  <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_2_3.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td></tr>
 <tr height=19 style='height:14.25pt'>
  <td>N</td>
  <td >Non Standard Goods - อวนไม่ได้มาตรฐาน</td>
  <td >KKF</td>
  <td >KKF</td>
  <td >N</td>
  <td >Non Standard Goods</td>
  <td >Y</td>
  <td >14/09/2560 12:00 น.</td>
 <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_2_3.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td> </tr>
 <tr height=19 style='height:14.25pt'>
  <td>O</td>
  <td >อวนดำ Type C (อวนดำ ร้อยหู ปะดำ)</td>
  <td >MYM</td>
  <td >KKF/MYENMAR</td>
  <td >N</td>
  <td >อวนดำ Type C</td>
  <td >Y</td>
  <td >28/11/2556 10:54 น.</td>
   <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_2_3.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
 </tr>
 <tr height=19 style='height:14.25pt'>
  <td>P</td>
  <td >PREMIUM(คุณภาพสินค้ามาตรฐาน)</td>
  <td >KKF</td>
  <td >KKF</td>
  <td >N</td>
  <td >PREMIUM</td>
  <td >Y</td>
  <td >28/02/2556 16:59 น.</td>
   <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_2_3.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
 </tr>
 <tr height=19 style='height:14.25pt'>
  <td>Q</td>
  <td >อวนดำ Type<span style='mso-spacerun:yes'>&nbsp; </span>B
  (อวนดำ ร้อยหู ไม่ปะดำ)</td>
  <td >MYM</td>
  <td >KKF/MYENMAR</td>
  <td >N</td>
  <td >อวนดำ Type<span style='mso-spacerun:yes'>&nbsp; </span>B</td>
  <td >Y</td>
  <td >06/12/2557 09:41 น.</td>
   <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_2_3.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
 </tr>
 <tr height=19 style='height:14.25pt'>
  <td>R</td>
  <td >Recycle - คุณภาพอวนหรือด้ายโพลีที่ใช้เม็ดหลอม</td>
  <td >KKF</td>
  <td >KKF/KC</td>
  <td >N</td>
  <td >Recycle</td>
  <td >N</td>
  <td >23/04/2561 16:12 น.</td>
   <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_2_3.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
 </tr>
 <tr height=19 style='height:14.25pt'>
  <td>S</td>
  <td >SUPER LOW COST</td>
  <td >KKF</td>
  <td >KKF</td>
  <td >N</td>
  <td >LOW COST</td>
  <td >Y</td>
  <td >28/02/2556 16:59 น.</td>
   <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_2_3.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
 </tr>
 <tr height=19 style='height:14.25pt'>
  <td>T</td>
  <td >อวนดำ Type A (อวนดำ ไม่ร้อยหู ไม่ปะดำ)<span
  style='mso-spacerun:yes'>&nbsp; </span>low cost</td>
  <td >MYM</td>
  <td >KKF/MYENMAR</td>
  <td >N</td>
  <td></td>
  <td >Y</td>
  <td >28/01/2558 17:24 น.</td>
   <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_2_3.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
 </tr>
 <tr height=19 style='height:14.25pt'>
  <td>U</td>
  <td >คุณภาพอวนหรือด้ายโพลีที่ใช้เม็ดหลอม + เม็ดเชือก</td>
  <td >KKF</td>
  <td >KKF/KC</td>
  <td >N</td>
  <td></td>
  <td >Y</td>
  <td >09/02/2558 17:09 น.</td>
   <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_2_3.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
 </tr>
 <tr height=19 style='height:14.25pt'>
  <td>X</td>
  <td >Outsouce<span style='mso-spacerun:yes'>&nbsp; </span>เกรด A
  (ใย XFF)</td>
  <td >CHN</td>
  <td >CHINA</td>
  <td >Y</td>
  <td >Outsouce<span style='mso-spacerun:yes'>&nbsp; </span>เกรด A</td>
  <td >N</td>
  <td >28/02/2556 17:00 น.</td>
   <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_2_3.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
 </tr>
 <tr height=19 style='height:14.25pt'>
  <td>Z</td>
  <td >Outsouce<span style='mso-spacerun:yes'>&nbsp; </span>เกรด C
  (ใย XFF)</td>
  <td >CHN</td>
  <td >CHINA</td>
  <td >Y</td>
  <td >Outsouce<span style='mso-spacerun:yes'>&nbsp; </span>เกรด C</td>
  <td >N</td>
  <td >28/02/2556 17:00 น.</td>
   <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_2_3.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
 </tr>








                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
            </div>
        </div>










    </div>
</div>
<!--  -->
<? include('inc.footer.php');?>
<!--  -->
<? include('inc.footer.script.php');?>
<!--  -->
