<?php
    $select = 'open_menu_1';
    $select2 = 'menu_6';
    $select3 = 'menu_6';
    $select4 = 'menu_6_6';
	
?>
<!--  -->
<? include('inc.header.php');?>
<!--  -->
<? include('inc.navbar.php');?>
<!--  -->
<? include('inc.menu.php');?>
<!-- <div class="layout-main"> -->
<!--  -->
<div class="layout-content">
    <div class="layout-content-body">
		<div class="title-bar">
            <h1 class="title-bar-title">
              <span class="d-ib">กิจกรรมงานซ่อมบำรุง</span>
            </h1>
            <div class="title-bar-description">
				  <span class="d-ib text-primary">การตั้งค่า</span> <span class="icon icon-angle-double-right"></span> ซ่อมบำรุง <span class="icon icon-angle-double-right"></span> กิจกรรมงานซ่อมบำรุง <span class="icon icon-angle-double-right"></span> แก้ไข
			</div>
		</div>

		 <div class="row">
		
            <div class="col-md-12">
				 
                <!--  -->
                <div class="demo-form-wrapper">
                        <!--  -->
					<form class="form form-horizontal">
						<div class="panel m-b-lg">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#information" data-toggle="tab" aria-expanded="true">ข้อมูลทั่วไป</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane fade active in" id="information">
								<div class="card" aria-expanded="true">
                            <div class="card-header at_bg_table_light_gray">
                               <div class="card-actions">
                                    <button type="button" class="card-action card-toggler" title="Collapse" aria-expanded="true"></button>
                                    <!-- <button type="button" class="card-action card-reload" title="Reload"></button>
                                    <button type="button" class="card-action card-remove" title="Remove"></button> -->
                                </div>
                                <strong>กิจกรรมงานซ่อมบำรุง</strong>
                            </div>
                            <div class="card-body" style="display: block;">
								<form class="form horizontal">
									<div class="form-group">
										<label class="col-sm-2 control-label" for="form-control-1">รหัสกิจกรรมงาน</label>
										<div class="col-sm-4">
											<input id="form-control-1" class="form-control" type="text" value="A01">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="form-control-1">กิจกรรมงาน</label>
										<div class="col-sm-4">
											<input id="form-control-1" class="form-control" type="text" value="การเปลี่ยนอุปกรณ์รอบ 1 เดือน">
										</div>
									</div>

									<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">เครื่องทอ</label>
												<div class="col-sm-4">
													<select id="form-control-6" class="form-control">
														<option value="1">A001 : ED  7.5 * 130 * 785</option>
														<option value="2">A002 : ED  7.5 * 130 * 810</option>
														<option value="3">C1 : FM  10.0*200*430</option>
														<option value="4">C10 : DK  12.7*270*480</option>
													</select>
												</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="form-control-1">กำลังคนมาตรฐาน</label>
										<div class="col-sm-4">
											<input id="form-control-1" class="form-control" type="text" value="1">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="form-control-1">ต้องการจอดเครื่อด</label>
										<div class="col-sm-4">
											<label class="custom-control custom-control-primary custom-checkbox">
													<input class="custom-control-input" type="checkbox" name="mode" checked="checked">
													<span class="custom-control-indicator"></span>
												 </label>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="form-control-1">เริ่มใช้งาน</label>
										<div class="col-sm-4">
											 <div class=" input-with-icon">
											  <input class="form-control" type="text" data-provide="datepicker" value="11/13/2009  5:09:08 PM">
												<span class="icon icon-calendar input-icon"></span>
											 </div>
										</div>
									</div>
								</form>
                            </div>
							</div>

								<div class="card" aria-expanded="true">
                            <div class="card-header at_bg_table_light_gray">
                               <div class="card-actions">
                                    <button type="button" class="card-action card-toggler" title="Collapse" aria-expanded="true"></button>
                                    <!-- <button type="button" class="card-action card-reload" title="Reload"></button>
                                    <button type="button" class="card-action card-remove" title="Remove"></button> -->
                                </div>
                                <strong>เวลามาตรฐาน</strong>
                            </div>
                            <div class="card-body" style="display: block;">
								<form class="form horizontal">
									
									<div class="form-group">
										<label class="col-sm-2 control-label" for="form-control-1">ระยะเวลา</label>
										<div class="col-sm-4">
											<input id="form-control-1" class="form-control" type="text" value="0:30">
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-sm-2 control-label" for="form-control-1">หน่วย</label>
										<div class="col-sm-4">
											<input id="form-control-1" class="form-control" type="text" value="ชั่วโมง">
										</div>
									</div>
								</form>
                            </div>
							</div>

							</div>
							</div>
						</div>
						<div class="m-t text-center">
						<a class="btn btn-lg btn-primary" href="menu_6_6.php" type="submit">บันทึก</a>
                        <a class="btn btn-lg btn-default" href="menu_6_6.php" type="button>">ยกเลิก</a>
						</div>
					</form>
                    
                        <!--  -->
                    </div>
                </div>
                <!--  -->
            </div>
        </div>

    </div>
</div>
<!--  -->
<? include('inc.footer.php');?>
<!--  -->
<? include('inc.footer.script.php');?>
<!--  -->