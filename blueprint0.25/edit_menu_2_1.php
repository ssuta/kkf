<?php
    $select = 'open_menu_1';
    $select2 = 'menu_2';
    $select3 = 'menu_2';
    $select4 = 'menu_2_1';
?>
<!--  -->
<? include('inc.header.php');?>
<!--  -->
<? include('inc.navbar.php');?>
<!--  -->
<? include('inc.menu.php');?>
<!-- <div class="layout-main"> -->
<!--  -->
<div class="layout-content">
    <div class="layout-content-body">
		<div class="title-bar">
            <h1 class="title-bar-title">
              <span class="d-ib">สินค้า</span>
            </h1>
            <div class="title-bar-description">
				<span class="d-ib text-primary">การตั้งค่า</span> <span class="icon icon-angle-double-right"></span> สินค้า <span class="icon icon-angle-double-right"></span> แก้ไข
			</div>
		</div>
        <div class="row">
            <div class="col-md-12">
                <!--  -->
				 <div class="demo-form-wrapper">
					<form class="form form-horizontal">
					<div class="panel m-b-lg">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#goods-1" data-toggle="tab" aria-expanded="true">ข้อมุลทั่วไป</a></li>
                                <li class=""><a href="#goods-2" data-toggle="tab" aria-expanded="false">ข้อมูลการผลิต</a></li>
								<li class=""><a href="#goods-3" data-toggle="tab" aria-expanded="false">ต้นทุน</a></li>
                            </ul>
                            <div class="tab-content">
							<!-- -->
                                <div class="tab-pane fade active in" id="goods-1">
									<div class="card" aria-expanded="true">
										<div class="card-header at_bg_table_light_gray">
											<div class="card-actions">
												 <button type="button" class="card-action card-toggler" title="Collapse" aria-expanded="true"></button>
												 <button type="button" class="card-action card-reload" title="Reload"></button>
												 <button type="button" class="card-action card-remove" title="Remove"></button>
											</div>
											<strong>ข้อมุลสินค้า</strong>
										</div>
										<div class="card-body" style="display: block;">
										<form class="form horizontal">
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">รหัสสินค้า</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="1119P/013010400">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">ชื่อสินค้า</label>
												<div class="col-sm-5 textbox-lang">
													<span class="textbox-lang-wrapper th">
														<input id="form-control-1" class="form-control" type="text" value="ข่ายเอ็น 0.09 SK 1.3*40*180A. สีง SK00 YOKO เรือใบเงิน">
													</span>
													<span class="textbox-lang-wrapper eng">
														<input id="form-control-1" class="form-control" type="text" value="ข่ายเอ็น 0.09 SK 1.3*40*180A. สีง SK00 YOKO เรือใบเงิน">
													</span>
													<span class="textbox-lang-wrapper cn">
														<input id="form-control-1" class="form-control" type="text" value="ข่ายเอ็น 0.09 SK 1.3*40*180A. สีง SK00 YOKO เรือใบเงิน">
													</span>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">ประเภทสินค้า</label>
												<div class="col-sm-4">
													<select id="form-control-6" class="form-control">
														<option value="1">ข่ายเอ็น</option>
														<option value="2">อวนปั๊ม</option>
														<option value="3">อวนปั๊ม</option>
														<option value="4">ด้ายโปลี</option>
														<option value="5">ข่ายรุม</option>
													</select>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">ประเภทเครื่องทอ</label>
												<div class="col-sm-4">
													<select id="form-control-6" class="form-control">
														<option value="1">โรงทอ M</option>
														<option value="2">โรงทอ S</option>
														<option value="3">โรงทอ C</option>
													</select>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">เงื่อน</label>
												<div class="col-sm-4">
													<select id="form-control-6" class="form-control">
														<option value="1">03 : 0.90 kg / spool</option>
														<option value="2">DK : DK</option>
														<option value="3">SK : SK</option>
													</select>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">อบ</label>
												<div class="col-sm-4">
													<select id="form-control-6" class="form-control">
														<option value="1">Y : YOKO</option>
														<option value="2">T : TATE</option>
													</select>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">รหัสรุ่น</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="29">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">ชื่อรุ่น</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="SK YOKO เรือใบเงิน">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">กลุ่มตรวจนับ</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="03">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">จำนวนต่อห่อ</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="0.00">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">ผืนทอต่อวัน</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="0.00">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">จำนวนผืนเนื้อ</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="0.00">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">จำนวนผืนสำเร็จ</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="0.00">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">%กำไรมาตรฐาน</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="0.00">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">Barcode 13 หลัก</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="8854445691345">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">สาขา</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="B&S1">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">มีกระดูก</label>
												<div class="col-sm-4">
												<label class="custom-control custom-control-primary custom-checkbox">
													<input class="custom-control-input" type="checkbox" name="mode" checked="">
													<span class="custom-control-indicator"></span>
												 </label>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">มีกระดูกทุกๆกี่ตา</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="0.00">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">จำนวนห่วงมาตรฐาน</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="0.00">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">รหัสปัญหาในการหาต้นทุน</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">จำนวนเชื่อมต่อ</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="0.00">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">รหัสหูผลิตภัณฑ์</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="G">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">เน้นคุณภาพ</label>
												<div class="col-sm-4">
												<label class="custom-control custom-control-primary custom-checkbox">
													<input class="custom-control-input" type="checkbox" name="mode" checked="">
													<span class="custom-control-indicator"></span>
												 </label>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">แฟลกสั่งผลิต</label>
												<div class="col-sm-4">
												<label class="custom-control custom-control-primary custom-checkbox">
													<input class="custom-control-input" type="checkbox" name="mode" checked="">
													<span class="custom-control-indicator"></span>
												 </label>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">สถานะ</label>
												<div class="col-sm-4">
												<label class="custom-control custom-control-primary custom-checkbox">
													<input class="custom-control-input" type="checkbox" name="mode" checked="">
													<span class="custom-control-indicator"></span>
												 </label>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">เริ่มใช้งาน</label>
												<div class="col-sm-4">
													<div class=" input-with-icon">
														<input class="form-control" type="text" data-provide="datepicker" value="11/13/2009  5:09:08 PM">
														<span class="icon icon-calendar input-icon"></span>
													</div>
												</div>
											</div>
										</form>
										</div>
									</div>

									<div class="card" aria-expanded="true">
										<div class="card-header at_bg_table_light_gray">
											<div class="card-actions">
												 <button type="button" class="card-action card-toggler" title="Collapse" aria-expanded="true"></button>
												 <button type="button" class="card-action card-reload" title="Reload"></button>
												 <button type="button" class="card-action card-remove" title="Remove"></button>
											</div>
											<strong>ปริมาตรและน้ำหนัก</strong>
										</div>
										<div class="card-body" style="display: block;">
										<form class="form horizontal">
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">น้ำหนัก/ผืน(กรัม)</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="1,000">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">น้ำหนักขาย</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="1,000">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">น้ำหนักเฉลี่ย</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="0.00">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">น้ำหนักวัสดุประกอบ</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="0.00">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">ปริมาตร</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="0.00">
												</div>
											</div>
										</form>
										</div>
									</div>

									<div class="card" aria-expanded="true">
										<div class="card-header at_bg_table_light_gray">
											<div class="card-actions">
												 <button type="button" class="card-action card-toggler" title="Collapse" aria-expanded="true"></button>
												 <button type="button" class="card-action card-reload" title="Reload"></button>
												 <button type="button" class="card-action card-remove" title="Remove"></button>
											</div>
											<strong>หน่วย</strong>
										</div>
										<div class="card-body" style="display: block;">
										<form class="form horizontal">
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">หน่วยคลัง</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="KG">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">หน่วยต้นทุน</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="KG">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">หน่วยผลิต</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="KG">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">หน่วยขาย</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="KG">
												</div>
											</div>
										</form>
										</div>
									</div>

									<div class="card" aria-expanded="true">
										<div class="card-header at_bg_table_light_gray">
											<div class="card-actions">
												 <button type="button" class="card-action card-toggler" title="Collapse" aria-expanded="true"></button>
												 <button type="button" class="card-action card-reload" title="Reload"></button>
												 <button type="button" class="card-action card-remove" title="Remove"></button>
											</div>
											<strong>อวน</strong>
										</div>
										<div class="card-body" style="display: block;">
										<form class="form horizontal">
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">สีอวน</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="SK00">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">สีอวนติดป้าย</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="สีง SK00">
												</div>
											</div>
										</form>
										</div>
									</div>

									<div class="card" aria-expanded="true">
										<div class="card-header at_bg_table_light_gray">
											<div class="card-actions">
												 <button type="button" class="card-action card-toggler" title="Collapse" aria-expanded="true"></button>
												 <button type="button" class="card-action card-reload" title="Reload"></button>
												 <button type="button" class="card-action card-remove" title="Remove"></button>
											</div>
											<strong>ป้าย</strong>
										</div>
										<div class="card-body" style="display: block;">
										<form class="form horizontal">
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">รหัสป้าย</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="74">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">ชื่อป้าย</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="เรือใบเงิน">
												</div>
											</div>
										</form>
										</div>
									</div>
								</div>
								<!-- -->
								<div class="tab-pane fade" id="goods-2">
									<div class="card" aria-expanded="true">
										<div class="card-header at_bg_table_light_gray">
											<div class="card-actions">
												 <button type="button" class="card-action card-toggler" title="Collapse" aria-expanded="true"></button>
												 <button type="button" class="card-action card-reload" title="Reload"></button>
												 <button type="button" class="card-action card-remove" title="Remove"></button>
											</div>
											<strong>การผลิต</strong>
										</div>
										<div class="card-body" style="display: block;">
										<form class="form horizontal">
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">รหัสผลิต</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">อักขระพิเศษ</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="">
												</div>
											</div>
										</form>
										</div>
									</div>

									<div class="card" aria-expanded="true">
										<div class="card-header at_bg_table_light_gray">
											<div class="card-actions">
												 <button type="button" class="card-action card-toggler" title="Collapse" aria-expanded="true"></button>
												 <button type="button" class="card-action card-reload" title="Reload"></button>
												 <button type="button" class="card-action card-remove" title="Remove"></button>
											</div>
											<strong>ตา</strong>
										</div>
										<div class="card-body" style="display: block;">
										<form class="form horizontal">
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">รหัสขนาดตา</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="H820">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">ขนาดตาในรายละเอียดสินค้า</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="3.70 cm">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">ขนาดตา(cm)</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="3.7">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">จำนวนตาผลิต</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="40">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">ปภ.หาจน.ตา</label>
												<div class="col-sm-4">
													<select id="form-control-6" class="form-control">
														<option value="1">MD : B</option>
													</select>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">รหัสจน.ตา</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="025">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">จน.ตาในรายละเอียดสินค้า</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="25 md">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">จำนวนตาติดป้าย</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="40 md">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">จน.ตา(md)</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="25">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">จำนวนตาเชื่อมต่อ</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="0.00">
												</div>
											</div>
										</form>
										</div>
									</div>

									<div class="card" aria-expanded="true">
										<div class="card-header at_bg_table_light_gray">
											<div class="card-actions">
												 <button type="button" class="card-action card-toggler" title="Collapse" aria-expanded="true"></button>
												 <button type="button" class="card-action card-reload" title="Reload"></button>
												 <button type="button" class="card-action card-remove" title="Remove"></button>
											</div>
											<strong>ความยาว</strong>
										</div>
										<div class="card-body" style="display: block;">
										<form class="form horizontal">
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">สูตรหาความยาว</label>
												<div class="col-sm-4">
													<select id="form-control-6" class="form-control">
														<option value="1">Y : B*91.44/100.00</option>
														<option value="1">M : B</option>
														<option value="1">KG : B*0</option>
														<option value="1">ML : B*A/100.00</option>
													</select>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">รหัสความยาว</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="0">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">ความยาวในรายละเอียดสินค้า</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="20yd">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">ความยาวติดป้าย</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="180A.">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">ความยาว</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="18.29">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">แฟลกความยาวเป็นนน.</label>
												<div class="col-sm-4">
													<label class="custom-control custom-control-primary custom-checkbox">
													<input class="custom-control-input" type="checkbox" name="mode" checked="">
													<span class="custom-control-indicator"></span>
												 </label>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">ความยาวคำนวณ</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="0.00">
												</div>
											</div>
										</form>
										</div>
									</div>

									<div class="card" aria-expanded="true">
										<div class="card-header at_bg_table_light_gray">
											<div class="card-actions">
												 <button type="button" class="card-action card-toggler" title="Collapse" aria-expanded="true"></button>
												 <button type="button" class="card-action card-reload" title="Reload"></button>
												 <button type="button" class="card-action card-remove" title="Remove"></button>
											</div>
											<strong>หู</strong>
										</div>
										<div class="card-body" style="display: block;">
										<form class="form horizontal">
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">หูทอ</label>
												<div class="col-sm-4">
													<select id="form-control-6" class="form-control">
														<option value="1">G : หูปกติ</option>
														<option value="2">N : หูไนล่อน</option>
														<option value="3">S : หูพิเศษ</option>
													</select>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">หูอบ</label>
												<div class="col-sm-4">
													<select id="form-control-6" class="form-control">
														<option value="1">A : Single(หูเดี่ยวอบ)</option>
														<option value="2">B : Double(หูคู่อบ)</option>
														<option value="3">E : หูไนล่อน</option>
													</select>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">สีหู</label>
												<div class="col-sm-4">
													<select id="form-control-6" class="form-control">
														<option value="1">SAME : สีหูเหมือนตัวอวน</option>
														<option value="2">M206 : NATURAL WHITE M206</option>
													</select>
												</div>
											</div>
										</form>
										</div>
									</div>

									<div class="card" aria-expanded="true">
										<div class="card-header at_bg_table_light_gray">
											<div class="card-actions">
												 <button type="button" class="card-action card-toggler" title="Collapse" aria-expanded="true"></button>
												 <button type="button" class="card-action card-reload" title="Reload"></button>
												 <button type="button" class="card-action card-remove" title="Remove"></button>
											</div>
											<strong>สี</strong>
										</div>
										<div class="card-body" style="display: block;">
										<form class="form horizontal">
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">รหัสสีมาตรฐาน</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="L026">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">รหัสสี</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="1">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">รายละเอียดสีในรายละเอียดสินค้า</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="RED L026">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">รายละเอียดสี</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="RED L026">
												</div>
											</div>
										</form>
										</div>
									</div>

									<div class="card" aria-expanded="true">
										<div class="card-header at_bg_table_light_gray">
											<div class="card-actions">
												 <button type="button" class="card-action card-toggler" title="Collapse" aria-expanded="true"></button>
												 <button type="button" class="card-action card-reload" title="Reload"></button>
												 <button type="button" class="card-action card-remove" title="Remove"></button>
											</div>
											<strong>ใย</strong>
										</div>
										<div class="card-body" style="display: block;">
										<form class="form horizontal">
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">ประเภทใย</label>
												<div class="col-sm-4">
													<select id="form-control-6" class="form-control">
														<option value="1">M : MONO</option>
														<option value="2">N : NYLON</option>
														<option value="3">P : POLY</option>
													</select>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">รหัสเบอร์ใย</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="0100">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">เบอร์ใยผลิต</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="500/40">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">เบอร์ใยติดป้าย</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="0.09 mm">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">ขนาดใย</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="500">
												</div>
											</div>
										</form>
										</div>
									</div>
                                </div>
								<!-- -->
								<div class="tab-pane fade" id="goods-3">
									<div class="card" aria-expanded="true">
										<div class="card-header at_bg_table_light_gray">
											<div class="card-actions">
												 <button type="button" class="card-action card-toggler" title="Collapse" aria-expanded="true"></button>
												 <button type="button" class="card-action card-reload" title="Reload"></button>
												 <button type="button" class="card-action card-remove" title="Remove"></button>
											</div>
											<strong>ต้นทุน</strong>
										</div>
										<div class="card-body" style="display: block;">
										<form class="form horizontal">
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">ต้นทุนมาตรฐาน</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="0.00">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">ต้นทุนเฉลี่ย</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="0.00">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">ต้นทุน</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="0.00">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">ต้นทุน/กก.</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="0.00">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">ต้นทุน/กก. ไม่รวมบริหาร</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="0.00">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">ต้นทุน/ผืน ไม่รวมบริหาร</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="0.00">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">วันที่ต้นทุน</label>
												<div class="col-sm-4">
													<div class=" input-with-icon">
														<input class="form-control" type="text" data-provide="datepicker" value="04/08/2010  8:48:39 AM">
														<span class="icon icon-calendar input-icon"></span>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">ต้นทุนค่ารุม</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="0.00">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">ต้นทุนค่ารุม/กก.</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="0.00">
												</div>
											</div>
										</form>
										</div>
									</div>

									<div class="card" aria-expanded="true">
										<div class="card-header at_bg_table_light_gray">
											<div class="card-actions">
												 <button type="button" class="card-action card-toggler" title="Collapse" aria-expanded="true"></button>
												 <button type="button" class="card-action card-reload" title="Reload"></button>
												 <button type="button" class="card-action card-remove" title="Remove"></button>
											</div>
											<strong>วัสดุประกอบ</strong>
										</div>
										<div class="card-body" style="display: block;">
										<form class="form horizontal">
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">น้ำหนักวัสดุประกอบ</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="0.00">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">ต้นทุนวัสดุประกอบ</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="0.00">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">ต้นทุนวัสดุประกอบ/กก.</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="0.00">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">ต้นทุนวัสดุประกอบ/กก.</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="0.00">
												</div>
											</div>
										</form>
										</div>
									</div>

									<div class="card" aria-expanded="true">
										<div class="card-header at_bg_table_light_gray">
											<div class="card-actions">
												 <button type="button" class="card-action card-toggler" title="Collapse" aria-expanded="true"></button>
												 <button type="button" class="card-action card-reload" title="Reload"></button>
												 <button type="button" class="card-action card-remove" title="Remove"></button>
											</div>
											<strong>เนื้ออวน</strong>
										</div>
										<div class="card-body" style="display: block;">
										<form class="form horizontal">
										<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">รหัสเนื้ออวน</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="1414A381C025/">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">น้ำหนักเนื้ออวน</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="0.00">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">ต้นทุนเนื้ออวน</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="0.00">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">ต้นทุนเนื้ออวน/กก.</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="0.00">
												</div>
											</div>
										</form>
										</div>
									</div>
                                </div>
						    <!-- -->
							</div>
					</div>
					<div class="m-t text-center">
							<a class="btn btn-lg btn-primary" href="menu_2_1.php" type="submit">บันทึก</a>
							<a class="btn btn-lg btn-default" href="menu_2_1.php" type="button>">ยกเลิก</a>
					</div>
					</form>
				</div>
				<!--  -->
            </div>
        </div>
    </div>
</div>
<!--  -->
<? include('inc.footer.php');?>
<!--  -->
<? include('inc.footer.script.php');?>
<!--  -->