<?php
    $select = 'open_menu_1';
    $select2 = 'menu_9';
    $select3 = 'menu_9';
    $select4 = 'menu_3_2';
?>
<!--  -->
<? include('inc.header.php');?>
<!--  -->
<? include('inc.navbar.php');?>
<!--  -->
<? include('inc.menu.php');?>
<!-- <div class="layout-main"> -->
<!--  -->
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <h4 class="m-t-0">
                <!-- <span class="d-ib text-danger">--</span> <span class="icon icon-angle-double-right"></span> -->
                <!-- <span class="d-ib">ข้อมูลหลัก</span> -->
                <span class="d-ib text-primary">การตั้งค่า</span> <span class="icon icon-angle-double-right"></span> ข้อมูลอ้างอิง <span class="icon icon-angle-double-right"></span> กำหนดชักใย
                <!-- <span class="d-ib">
                    <a class="title-bar-shortcut" href="#" title="Add to shortcut list" data-container="body" data-toggle-text="Remove from shortcut list" data-trigger="hover" data-placement="right" data-toggle="tooltip">
                        <span class="sr-only">Add to shortcut list</span>
                    </a>
                </span> -->
            </h4>
            <!-- <p class="title-bar-description">
                <small>Latest update on 01/01/2017 - 10.57 am.</small>
            </p> -->
        </div>

        <div class="row gutter-xs">
            <div class="col-xs-12">
                <!--  -->
                <div class="card">
                    <!-- <div class="card-header">
                        <a href="javascript:window.history.back(-1);" class="btn btn-sm btn-outline-primary"><span class="icon icon-long-arrow-left"></span>&nbsp;&nbsp;Back</a>&nbsp;&nbsp;
                        <strong class="">Latest update on 01/01/2017 - 10.57 am.</strong>
                    </div> -->
                    <div class="card-body">
                        <!--  -->
                        <? include('inc.home_top_filter.php');?>
                        <!--  -->
                        <div class="table-responsive">
                               <table id="demo-datatables-2" class="table table-striped table-bordered table-nowrap dataTable text-center no-footer" cellspacing="0" width="100%">
                                                    <thead>
                                                        <tr class="at_bg_table_blue">
                                                            <th rowspan="2" class="text-center">กลุ่มเบอร์ใย</th>
                                                            <th rowspan="2" class="text-center">เม็ดวัตถุดิบ</th>
                                                            <th rowspan="2" class="text-center">สถานะใย/อวน</th>
                                                            <th rowspan="2" class="text-center">รหัสสาขา</th>
                                                            <th rowspan="2" class="text-center">ประเภทวัสดุพิเศษ<br>ต้องสั่งซื้อล่วงหน้าก่อนฉีดใย (วัน)</th>
                                                            <th rowspan="2" class="text-center">วางแผนชักใยล่วงหน้าก่อนทอ (วัน)</th>
                                                            <th rowspan="2" class="text-center">ฝ่ายการตลาด<br>นน.อวนที่สั่งทอขั้นต่ำ / ใย 1 รายการ  (กก.)</th>
                                                            <th colspan="2" class="text-center">ฝ่ายวางแผนการผลิต<br></th>
                                                            <th rowspan="2" class="text-center">เริ่มต้นใช้งาน</th>
                                                            <th rowspan="2" class="text-center">ปรับแก้ไข</th>
                                                        </tr>
                                                        <tr class="at_bg_table_blue">
                        <th class="text-center">นน.อวนที่สั่งทอขั้นต่ำ / ใย 1 รายการ  (กก.)</th>
                        <th class="text-center">สั่งผลิตใยล่วงหน้าก่อรทอ (วัน)</th>
                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    
                                                    <tr>
                        <td>0.20 - 1.20</td>  <!-- กลุ่มเบอร์ใย  -->
                        <td>Homo / Copo</td> <!--เม็ดวัตถุดิบ -->
                        <td>หูอวน : ใยฉีดสีต่าง ๆ</td> <!-- สถานะใย/อวน  -->
                        <td>B&S</td> <!-- รหัสสาขา  -->
                        <td>15</td> <!-- ประเภทวัสดุพิเศษ  -->
                        <td>3</td> <!-- วางแผนชักใยล่วงหน้าก่อนทอ (วัน)  -->
                        <td>200</td> <!-- ฝ่ายการตลาด  -->
                        <td>200</td> <!-- นน.อวนที่สั่งทอขั้นต่ำ / ใย 1 รายการ  (กก.)  -->
                        <td>5</td> <!-- สั่งผลิตใยล่วงหน้าก่อรทอ (วัน)  -->
                         <td>11/13/2009  5:09:08 น.</td> <!-- เริ่มใช้งาน -->
                        <td class="text-center" style="display: table-cell;">
                             <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_3_2.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                            <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                                <span class="icon icon-lg icon-close"></span>
                            </button>
                        </td>
                    </tr>

                    <tr>
                        <td>0.20 - 0.40</td>  <!-- กลุ่มเบอร์ใย  -->
                        <td>Homo / Copo</td> <!--เม็ดวัตถุดิบ -->
                        <td>ตัวอวน : ใยฉีด / ใยย้อม</td> <!-- สถานะใย/อวน  -->
                        <td>B&S</td> <!-- รหัสสาขา  -->
                        <td> </td> <!-- ประเภทวัสดุพิเศษ  -->
                        <td>3</td> <!-- วางแผนชักใยล่วงหน้าก่อนทอ (วัน)  -->
                        <td>2200</td> <!-- ฝ่ายการตลาด  -->
                        <td>2200</td> <!-- นน.อวนที่สั่งทอขั้นต่ำ / ใย 1 รายการ  (กก.)  -->
                        <td>5</td> <!-- สั่งผลิตใยล่วงหน้าก่อรทอ (วัน)  -->
                         <td>28/11/2011  11:47:21 น.</td> <!-- เริ่มใช้งาน -->

                        <td class="text-center" style="display: table-cell;">
                             <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_3_2.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                            <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                                <span class="icon icon-lg icon-close"></span>
                            </button>
                        </td>
                    </tr>

                     <tr>
                        <td>0.41 - 0.60 </td>  <!-- กลุ่มเบอร์ใย  -->
                        <td>Homo / Copo</td> <!--เม็ดวัตถุดิบ -->
                        <td>ตัวอวน : ใยฉีด / ใยย้อม</td> <!-- สถานะใย/อวน  -->
                        <td>B&S</td> <!-- รหัสสาขา  -->
                        <td> </td> <!-- ประเภทวัสดุพิเศษ  -->
                        <td>3</td> <!-- วางแผนชักใยล่วงหน้าก่อนทอ (วัน)  -->
                        <td>1800</td> <!-- ฝ่ายการตลาด  -->
                        <td>1800</td> <!-- นน.อวนที่สั่งทอขั้นต่ำ / ใย 1 รายการ  (กก.)  -->
                        <td>5</td> <!-- สั่งผลิตใยล่วงหน้าก่อรทอ (วัน)  -->
                         <td>7/12/2016 12:24 น.</td> <!-- เริ่มใช้งาน -->

                        <td class="text-center" style="display: table-cell;">
                             <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_3_2.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                            <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                                <span class="icon icon-lg icon-close"></span>
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <td>0.61 - 0.84 </td>  <!-- กลุ่มเบอร์ใย  -->
                        <td>Homo / Copo</td> <!--เม็ดวัตถุดิบ -->
                        <td>ตัวอวน : ใยฉีด / ใยย้อม</td> <!-- สถานะใย/อวน  -->
                        <td>B&S</td> <!-- รหัสสาขา  -->
                        <td>15</td> <!-- ประเภทวัสดุพิเศษ  -->
                        <td>5</td> <!-- วางแผนชักใยล่วงหน้าก่อนทอ (วัน)  -->
                        <td>1500</td> <!-- ฝ่ายการตลาด  -->
                        <td>1500</td> <!-- นน.อวนที่สั่งทอขั้นต่ำ / ใย 1 รายการ  (กก.)  -->
                        <td>10</td> <!-- สั่งผลิตใยล่วงหน้าก่อรทอ (วัน)  -->
                         <td>18/09/2017 11:36 น.</td> <!-- เริ่มใช้งาน -->

                        <td class="text-center" style="display: table-cell;">
                             <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_3_2.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                            <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                                <span class="icon icon-lg icon-close"></span>
                            </button>
                        </td>
                    </tr>

                    <tr>
                        <td>0.85 - 1.20 </td>  <!-- กลุ่มเบอร์ใย  -->
                        <td>Homo / Copo</td> <!--เม็ดวัตถุดิบ -->
                        <td>ตัวอวน : ใยฉีด / ใยย้อม</td> <!-- สถานะใย/อวน  -->
                        <td>B&S</td> <!-- รหัสสาขา  -->
                        <td> </td> <!-- ประเภทวัสดุพิเศษ  -->
                        <td>5</td> <!-- วางแผนชักใยล่วงหน้าก่อนทอ (วัน)  -->
                        <td>1400</td> <!-- ฝ่ายการตลาด  -->
                        <td>1400</td> <!-- นน.อวนที่สั่งทอขั้นต่ำ / ใย 1 รายการ  (กก.)  -->
                        <td>10</td> <!-- สั่งผลิตใยล่วงหน้าก่อรทอ (วัน)  -->
                         <td>26/03/2014 9:29 น.</td> <!-- เริ่มใช้งาน -->

                        <td class="text-center" style="display: table-cell;">
                             <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_3_2.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                            <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                                <span class="icon icon-lg icon-close"></span>
                            </button>
                        </td>
                    </tr>
                    

                        
                                                
                                                
                                                

                                                    </tbody>
                                                </table>
                        </div>
                    </div>
                </div>                
            </div>
        </div>










    </div>
</div>
<!--  -->
<? include('inc.footer.php');?>
<!--  -->
<? include('inc.footer.script.php');?>
<!--  -->
