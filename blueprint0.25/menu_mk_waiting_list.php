<?php
    $select = 'open_menu_2';
    $select2 = 'menu_mk_waiting_list';
    $select3 = 'menu_mk_3';
    $select4 = 'menu_mk_3';
	
	$typeSale = array('ขายใน', 'ขายต่าง');
    $orders = array('ออเดอร์', 'สอบถาม');
    
    $numberOrder = 0;
    $numberEnquiry = 0;
	
	$statusText = array('ร่างแผนแล้ว<br>รออนุมัติ', 'ร่างแผนแล้ว<br>รอยืนยันสั่งซื้อ', 'ปรับปรุงแผน<br>รออนุมัติ');
    $statusClass = array('warning', 'warning tone3', 'warning tone2');
    
	$productsName = array('1119P/013010400', '1ฑ090/013000402', '11190/013010400', '19090/013000402', '1ฌ094/013000502');
?>
<!--  -->
<? include('inc.header.php');?>
<!--  -->
<? include('inc.navbar.php');?>
<!--  -->
<? include('inc.menu.php');?>
<!-- <div class="layout-main"> -->
<!--  -->
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <h4 class="m-t-0">
                <!-- <span class="d-ib text-danger">--</span> <span class="icon icon-angle-double-right"></span> -->
                <!-- <span class="d-ib">ข้อมูลหลัก</span> -->
                <span class="d-ib text-primary">การตลาด</span> <span class="icon icon-angle-double-right"></span> รายการรออนุมัติ
                <!-- <span class="d-ib">
                    <a class="title-bar-shortcut" href="#" title="Add to shortcut list" data-container="body" data-toggle-text="Remove from shortcut list" data-trigger="hover" data-placement="right" data-toggle="tooltip">
                        <span class="sr-only">Add to shortcut list</span>
                    </a>
                </span> -->
            </h4>
            <!-- <p class="title-bar-description">
                <small>Latest update on 01/01/2017 - 10.57 am.</small>
            </p> -->
        </div>

        <div class="row gutter-xs">
            <div class="col-xs-12">
                <!--  -->
                <div class="card">
                    <!-- <div class="card-header">
                        <a href="javascript:window.history.back(-1);" class="btn btn-sm btn-outline-primary"><span class="icon icon-long-arrow-left"></span>&nbsp;&nbsp;Back</a>&nbsp;&nbsp;
                        <strong class="">Latest update on 01/01/2017 - 10.57 am.</strong>
                    </div> -->
                    <div class="card-body">
                        <!--  -->

						
                        <? include('inc.home_top_filter_extra_order.php');?>
						
                        <!--  -->
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered text-center table-items" cellspacing="0" width="100%">
                                <thead>
									<tr class="at_bg_table_blue">
                                        <th class="text-center middle" rowspan="3">เลขที่</th>
                                        <th class="text-center middle" rowspan="3">ประเภท<br>รายการ</th>
                                        <th class="text-center middle" rowspan="3">ชื่อลูกค้า</th>
                                        <th class="text-center middle" rowspan="3">ประเภทขาย</th>
                                        <th class="text-center middle" rowspan="3">วันที่สั่งซื้อ</th>
                                        <th class="text-center middle" colspan="4">วันที่ผลิต</th>
                                        <th class="text-center middle" colspan="2" rowspan="2">วันที่ส่งคลังสินค้า</th>
                                        <th class="text-center middle" rowspan="3">วันที่<br>จัดส่งสินค้า</th>
                                        <th class="text-center middle" rowspan="3">สถานะ</th>
                                        <th class="text-center middle" rowspan="3">ปรับแก้ไข</th>
                                    </tr>
                                    <tr class="at_bg_table_blue">
                                        <th class="text-center" colspan="2">ทอ</th>
                                        <th class="text-center" colspan="2">วงจรส่วนหลัง</th>
                                    </tr>
                                    <tr class="at_bg_table_blue">
                                        <th class="text-center">เริ่ม</th>
                                        <th class="text-center">เสร็จ</th>
                                        <th class="text-center">เริ่ม</th>
                                        <th class="text-center">เสร็จ</th>
                                        <th class="text-center">เริ่ม</th>
                                        <th class="text-center">เสร็จ</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php for($i=0; $i<10; $i++){ ?>
										<?php 
											$statusRand = rand(0,2);
											$typeOrderRand = rand(0,1);
										?>
										<tr>
											<td data-toggle="collapse" data-target="#item-<?=$i+1?>"><?=$typeOrderRand==0?"ORDER":"EQR"?>000<?=$typeOrderRand==0?++$numberOrder:++$numberEnquiry?></td>
                                            <td data-toggle="collapse" data-target="#item-<?=$i+1?>"><?=$orders[$typeOrderRand]?></td>
                                            <td data-toggle="collapse" data-target="#item-<?=$i+1?>">Customer00<?=$i+1?></td>
											<td data-toggle="collapse" data-target="#item-<?=$i+1?>"><?=$typeSale[rand(0,1)]?></td>
											<td data-toggle="collapse" data-target="#item-<?=$i+1?>">20/07/2555</td>
											<td data-toggle="collapse" data-target="#item-<?=$i+1?>">20/07/2555</td>
											<td data-toggle="collapse" data-target="#item-<?=$i+1?>">20/07/2555</td>
											<td data-toggle="collapse" data-target="#item-<?=$i+1?>">20/07/2555</td>
											<td data-toggle="collapse" data-target="#item-<?=$i+1?>">20/07/2555</td>
											<td data-toggle="collapse" data-target="#item-<?=$i+1?>">20/07/2555</td>
											<td data-toggle="collapse" data-target="#item-<?=$i+1?>">20/07/2555</td>
											<td data-toggle="collapse" data-target="#item-<?=$i+1?>">20/07/2555</td>
											<?php
												$elementDiv = '';
												if($statusRand==0 || $statusRand==2){
													$elementDiv = "data-toggle=\"modal\" data-target=\"#approve-plan\"";
												}else if($statusRand==1){
													$elementDiv = "data-toggle=\"modal\" data-target=\"#modalConfirmInquiry\"";
												}
											?>
											<td class="left"><button class="btn btn-<?=$statusClass[$statusRand]?> status " type="button" <?=$elementDiv?>><?=$statusText[$statusRand]?></button></td>
											<td class="text-center" style="display: table-cell;">
												<a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="menu_mk_3.php" target="_blank" type="button">
													<span class="icon icon-lg icon-file-text-o"></span>
												</a>
												<a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="javascript:void(0)" type="button">
													<span class="icon icon-lg icon-edit"></span>
												</a>
												<button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
													<span class="icon icon-lg icon-close"></span>
												</button>
											</td>
										</tr>

										<tr class="cl">
                                            <td colspan="14" class="bg_item_in n-p-i">
                                                <div class="accordian-body collapse" id="item-<?=$i+1?>"> 
                                                    <table class="table table-bordered text-center table-hover" cellspacing="0" width="100%">
                                                        <thead>
                                                        <tr class="bg_table_in">
                                                            <th class="text-center middle" rowspan="3">หมายเลขร่างแผน</th>
                                                            <th class="text-center middle" rowspan="3">รหัสสินค้า</th>
                                                            <th class="text-center middle" rowspan="3">ประเภท<br>สินค้า</th>
                                                            <th class="text-center middle" colspan="2" rowspan="2">สั่งซื้อ</th>
                                                            <th class="text-center middle" colspan="4">วันที่ผลิต</th>
                                                            <th class="text-center middle" rowspan="3">จำนวน<br>ชุดทอ</th>
                                                            <th class="text-center middle" rowspan="3">จำนวน<br>ผืนทอ</th>
                                                            <th class="text-center middle" colspan="2" rowspan="2">วันที่ส่งคลังสินค้า</th>
                                                            <th class="text-center middle" rowspan="3">วันที่<br>จัดส่งสินค้า</th>
                                                            <th class="text-center middle" rowspan="3">สถานะในคลัง<br>สินค้าปัจจุบัน</th>
                                                        </tr>
                                                        <tr class="bg_table_in">
                                                            <th class="text-center" colspan="2">ทอ</th>
                                                            <th class="text-center" colspan="2">วงจรส่วนหลัง</th>
                                                        </tr>
                                                        <tr class="bg_table_in">
                                                            <th class="text-center">จำนวน</th>
                                                            <th class="text-center">หน่วย</th>
                                                            <th class="text-center">เริ่ม</th>
                                                            <th class="text-center">เสร็จ</th>
                                                            <th class="text-center">เริ่ม</th>
                                                            <th class="text-center">เสร็จ</th>
                                                            <th class="text-center">เริ่ม</th>
                                                            <th class="text-center">เสร็จ</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php for($j=0; $j<count($productsName); $j++){ ?>
                                                                    <?php 
                                                                        $amountItem = rand(100, 300);
                                                                        $amountWovenDress = rand(10,20);
                                                                        $amountWeave = rand(300, 500);
                                                                    ?>
                                                                    <tr>
                                                                        <td class="<?=$j!=0?'f-white':''?>">DRAFT.ENQUIRE001R1</td>
                                                                        <td><?=$productsName[$j]?></td>
                                                                        <td>ข่ายเอ็น</td>
                                                                        <td><?=$amountItem?></td>
                                                                        <td>PC</td>
                                                                        <td>05/08/2556</td>
                                                                        <td>09/08/2556</td>
                                                                        <td>10/08/2556</td>
                                                                        <td>13/08/2556</td>
                                                                        <td><?=$amountWovenDress?></td>
                                                                        <td><?=$amountWeave?></td>
                                                                        <td>17/08/2556</td>
                                                                        <td>20/08/2556</td>
                                                                        <td>24/08/2556</td>
                                                                        <td><?=$amountWeave?></td>
                                                                    </tr>
                                                                <?php } ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
									<?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
            </div>
        </div>
    </div>
</div>


<!--  -->
<? include('inc.footer.php');?>
<!--  -->
<? include('inc.footer.script.php');?>
<!--  -->
