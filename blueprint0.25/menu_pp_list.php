<?php
    $select = 'open_menu_5';
    $select2 = 'menu_pp_list';

    $index = array();
    $data = array();
    // $type = $_GET['type'];
    // $amount = $_GET['amount'];
    $type = 'all';
    $amount = 3;
    $hexColors = array(0x922B21, 0x6C3483, 0x1F618D, 0x0E6655, 0x0E6655, 0x9C640C, 0x797D7F, 0x212F3C);
    $bgColors = array('FADBD8', 'E8DAEF', 'D6EAF8', 'D1F2EB', 'FCF3CF', 'EAEDED', 'ffdfff');
    $fontColors = array();
    $bgTableFirst = array();
    $bgTableSecond = array();

    // echo 'value: ' . dechex($hexColors[0] + 1);
    
    foreach($hexColors as $item){
        $bgTableFirst[] = dechex($hexColors[0] + 1);
    }

    $statusText = array(
        'หมดอายุ', // 0
        'ถูกยกเลิกเพื่อ<br>ร่างแผนใหม่', // 1
        'ร่างแผนแล้ว<br>รออนุมัติ', // 2
        'ร่างแผนแล้ว<br>รอยืนยันสั่งซื้อ', // 3
        'สั่งซื้อแล้ว<br>รอยืนยันแผน',  // 4
        'วางแผนแล้ว<br>รอผลิต', // 5
        'ปรับปรุงแผน<br>รออนุมัติ', // 6
        'กำลังผลิต', // 7
        'ผลิตเสร็จแล้ว' // 8
    );
    $statusClass = array(
        'default', 
        'default tone4', //
        'warning', 
        'warning tone3', 
        'warning tone4', // 4
        'info tone3', 
        'warning tone2', 
        'info tone2', 
        'success'
    );

    $data = array(
        array('DRAFT.ENQUIRE002R2', 'ORDER007', 'ออเดอร์', '12/07/2556', '05/07/2556', '28/07/2556', $statusText[2], $statusClass[2]),
        array('DRAFT.ENQUIRE002R1', 'ORDER009', 'สอบถาม', '27/06/2556', '02/07/2556', '06/07/2556', $statusText[4], $statusClass[4]),
        array('DRAFT.ENQUIRE002R1', 'ORDER008', 'ออเดอร์', '28/06/2556', '02/07/2556', '06/07/2556', $statusText[3], $statusClass[3]),
        array('DRAFT.ENQUIRE002R1', 'ORDER007', 'ออเดอร์', '12/07/2556', '05/07/2556', '28/07/2556', $statusText[1], $statusClass[1]),
        array('DRAFT.ENQUIRE002R1', 'ORDER006', 'สอบถาม', '27/06/2556', '02/07/2556', '06/07/2556', $statusText[8], $statusClass[8]),
        array('DRAFT.ENQUIRE002R1', 'ORDER005', 'ออเดอร์', '28/06/2556', '02/07/2556', '06/07/2556', $statusText[5], $statusClass[5]),
        array('DRAFT.ENQUIRE002R1', 'ORDER004', 'ออเดอร์', '22/06/2556', '02/07/2556', '06/07/2556', $statusText[7], $statusClass[7]),
        array('DRAFT.ENQUIRE001R2', 'ORDER002', 'ออเดอร์', '12/07/2556', '05/07/2556', '28/07/2556', $statusText[6], $statusClass[6]),
        array('DRAFT.ENQUIRE001R1', 'ORDER003', 'สอบถาม', '27/06/2556', '02/07/2556', '06/07/2556', $statusText[8], $statusClass[8]),
        array('DRAFT.ENQUIRE001R1', 'ORDER002', 'ออเดอร์', '28/06/2556', '02/07/2556', '06/07/2556', $statusText[1], $statusClass[1]),
        array('DRAFT.ENQUIRE001R1', 'ORDER001', 'ออเดอร์', '22/06/2556', '02/07/2556', '06/07/2556', $statusText[0], $statusClass[0]),
    );

?>                                  
<!--  -->
<? include('inc.header.php');?>
<!--  -->
<? include('inc.navbar.php');?>
<!--  -->
<? include('inc.menu.php');?>
<!-- <div class="layout-main"> -->
<style>
.ui-draggable-dragging, .cell-highlight {
    z-index: 99;
    background-color: yellow;
}
</style>
<!--  -->
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <h4 class="m-t-0">
                <!-- <span class="d-ib text-danger">--</span> <span class="icon icon-angle-double-right"></span> -->
                <!-- <span class="d-ib">ข้อมูลหลัก</span> -->
                <span class="d-ib text-primary">การวางแผนการผลิต</span> <span class="icon icon-angle-double-right"></span> ร่างแผนการผลิต
                <!-- <span class="d-ib">
                    <a class="title-bar-shortcut" href="#" title="Add to shortcut list" data-container="body" data-toggle-text="Remove from shortcut list" data-trigger="hover" data-placement="right" data-toggle="tooltip">
                        <span class="sr-only">Add to shortcut list</span>
                    </a>
                </span> -->
            </h4>
            <!-- <p class="title-bar-description">
                <small>Latest update on 01/01/2017 - 10.57 am.</small>
            </p> -->
        </div>
        <div class="row gutter-xs">
            <div class="col-xs-12">
                <div class="card">
                    <div class="card-body">

                        <div class="at_add_box">

                            <div class="card filter" aria-expanded="true">
                                <div class="card-header at_bg_table_blue">
                                    <div class="card-actions">
                                            <button type="button" class="card-action card-toggler" title="Collapse" aria-expanded="true"></button>
                                    </div>
                                    <strong>เลือกรายการ</strong>
                                </div>
                                <div class="card-body" style="display: block;">

                                    <form id="form-filter" method="get" class="form form-horizontal">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label text-right" for="form-control-14">สาขา : </label>
                                                    <div class="col-sm-9 has-feedback">
                                                        <select id="demo-select2-1" class="form-control" name="branch">
                                                            <option value="all">ทั้งหมด</option>
                                                            <option value="1">ขอนแก่นแหอวน</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-8 text-right">
                                                <div class="form-group">
                                                    <div class="col-sm-12">
                                                        <button id="save-filter" class="btn btn-outline-primary btn-sm buttons-collection buttons-colvis width-fit hide" type="button">
                                                            <span class="icon icon-save"></span>
                                                            &nbsp;&nbsp;บันทึก
                                                        </button>

                                                        <button class="btn btn-sm btn-primary btn-thick width-fit" type="submit">
                                                            <span class="icon icon-search"></span>
                                                            &nbsp;&nbsp;ค้นหา
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label text-right" for="form-control-14">สถานะ</label>
                                                    <div class="col-sm-9 has-feedback">
                                                        <select id="demo-select2-3" class="form-control">
                                                            <option value="0">ทั้งหมด</option>
                                                            <option value="0">รออนุมัติ</option>
                                                            <option value="0">อนุมัติแล้ว</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label text-right" for="form-control-14">ตามช่วงเวลา</label>
                                                    <div class="col-sm-8 has-feedback">
                                                        <div class="input-group input-daterange" data-provide="datepicker" data-date-autoclose="true" data-date-format="dd-M-yyyy">
                                                            <input class="form-control" type="text" value="เริ่มต้น">
                                                            <span class="input-group-addon">ถึง</span>
                                                            <input class="form-control" type="text" value="สิ้นสุด">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>

                            <section id="section-list" data-type="order">
                                <form id="form-order" method="get" class="form form-horizontal">
                                    <!-- <div class="row top-header">
                                        <div class="col-sm-4">
                                            <input type="text" class="form-control" placeholder="ค้นหา...">
                                        </div>
                                        <div class="col-sm-4 text-right text-fade">
                                            <span class="icon icon-lg icon-hourglass-half" title="รอร่างแผน"></span> = รอร่างแผน,
                                            <span class="icon icon-lg icon-edit" style="margin-left:15px;"></span> = มีการเปลี่ยนแปลง รอร่างแผนใหม่
                                        </div>
                                        <div class="col-sm-12 text-right">
                                            <button id="start-plan-order" class="btn btn-sm btn-primary btn-thick" type="submit" onclick="openToPlaning('order')">
                                                <span class="icon icon-download"></span>
                                                &nbsp;&nbsp;เริ่มวางแผน
                                            </button>
                                        </div>
                                    </div> -->

                                    <button id="start-plan-order" class="btn btn-sm btn-primary btn-thick start-plan width-fit" type="submit" onclick="openToPlaning('order')">
                                        <span class="icon icon-calendar"></span>
                                        &nbsp;&nbsp;เริ่มวางแผน
                                    </button>
                                    
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="table-responsive">
                                                <table class="table table-hover table-bordered table-nowrap dataTable table-items text-center no-footer" cellspacing="0" width="100%" aria-describedby="demo-datatables-2_info" role="grid" style="width: 100%;">
                                                    <thead>
                                                        <tr class="at_bg_table_blue">
                                                            <th class="text-center">หมายเลขร่างแผน</th>
                                                            <th class="text-center">รายการ</th>
                                                            <th class="text-center">ประเภทรายการ</th>
                                                            <th class="text-center">วันที่สอบถาม/สั่งซื้อ</th>
                                                            <th class="text-center">วันที่ร่างแผน/แก้ไขแผน</th>
                                                            <th class="text-center">วันที่ตอบกลับ</th>
                                                            <th class="text-center">สถานะ</th>
                                                        </tr>
                                                    </thead>
                                                    <!-- <tfoot>
                                                        <tr>
                                                            <th class="text-center" >ค้นหา</th>
                                                            <th class="text-center" >ค้นหา</th>
                                                            <th class="text-center" >ค้นหา</th>
                                                            <th class="text-center" >ค้นหา</th>
                                                            <th class="text-center" >ค้นหา</th>
                                                            <th class="text-center" >ค้นหา</th>
                                                            <th class="text-center" >ค้นหา</th>
                                                            <th class="text-center" >ค้นหา</th>
                                                            <th class="text-center" >ค้นหา</th>
                                                            <th class="text-center" >ค้นหา</th>
                                                            <th class="text-center" >ค้นหา</th>
                                                            <th class="text-center no-sort" >ค้นหา</th>
                                                        </tr>
                                                    </tfoot> -->
                                                    <tbody class="text-center">
                                                        <?php for($i=0; $i<count($data); $i++){ ?>
                                                            <?php $statusRand = rand(0,2); ?>
                                                            <tr>
                                                                <td data-toggle="collapse" data-target="#item-<?=$i+1?>" class="accordion-toggle">
                                                                   <?=$data[$i][0]?>
                                                                </td>
                                                                <td data-toggle="collapse" data-target="#item-<?=$i+1?>" class="accordion-toggle">
                                                                   <?=$data[$i][1]?>
                                                                </td>
                                                                <td data-toggle="collapse" data-target="#item-<?=$i+1?>" class="accordion-toggle">
                                                                   <?=$data[$i][2]?>
                                                                </td>
                                                                <td data-toggle="collapse" data-target="#item-<?=$i+1?>" class="accordion-toggle">
                                                                   <?=$data[$i][3]?>
                                                                </td>
                                                                <td data-toggle="collapse" data-target="#item-<?=$i+1?>" class="accordion-toggle">
                                                                   <?=$data[$i][4]?>
                                                                </td>
                                                                <td data-toggle="collapse" data-target="#item-<?=$i+1?>" class="accordion-toggle">
                                                                   <?=$data[$i][5]?>
                                                                </td>
                                                                <td class="text-center" style="display: table-cell;">
                                                                    <button class="btn btn-<?=$data[$i][7]?> status " type="button"><?=$data[$i][6]?></button>
                                                                </td>
                                                            </tr>
                                                            
                                                            
                                                            <tr class="cl">
                                                                <td colspan="14" class="bg_item_in n-p-i">
                                                                    <div class="accordian-body collapse" id="item-<?=$i+1?>"> 
                                                                        <table class="table table-bordered text-center" cellspacing="0" width="100%">
                                                                            <thead>
                                                                            <tr class="bg_table_in">
                                                                                <th class="text-center middle" rowspan="3">รหัสสินค้า</th>
                                                                                <th class="text-center middle" rowspan="3">ประเภทสินค้า</th>
                                                                                <th class="text-center middle" colspan="2" rowspan="2">สั่งซื้อ</th>
                                                                                <th class="text-center middle" colspan="4">วันที่ผลิต</th>
                                                                                <th class="text-center middle" rowspan="3">จำนวนชุดทอ</th>
                                                                                <th class="text-center middle" rowspan="3">จำนวนผืนทอ</th>
                                                                                <th class="text-center middle" colspan="2" rowspan="2">วันที่ส่งคลังสินค้า</th>
                                                                                <th class="text-center middle" rowspan="3">วันที่<br>จัดส่งสินค้า</th>
                                                                                <th class="text-center middle" rowspan="3">สถานะในคลัง<br>สินค้าปัจจุบัน</th>
                                                                            </tr>
                                                                            <tr class="bg_table_in">
                                                                                <th class="text-center" colspan="2">ทอ</th>
                                                                                <th class="text-center" colspan="2">วงจรส่วนหลัง</th>
                                                                            </tr>
                                                                            <tr class="bg_table_in">
                                                                                <th class="text-center">จำนวน</th>
                                                                                <th class="text-center">หน่วย</th>
                                                                                <th class="text-center">เริ่ม</th>
                                                                                <th class="text-center">เสร็จ</th>
                                                                                <th class="text-center">เริ่ม</th>
                                                                                <th class="text-center">เสร็จ</th>
                                                                                <th class="text-center">เริ่ม</th>
                                                                                <th class="text-center">เสร็จ</th>
                                                                            </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <?php $itemRand = rand(4,7) ?>
                                                                                <?php for($j=0; $j<$itemRand; $j++){ ?>
                                                                                    <?php 
                                                                                        $amountItem = rand(100, 300);
                                                                                        $amountWovenDress = rand(10,20);
                                                                                        $amountWeave = rand(300, 500);
                                                                                    ?>
                                                                                    <tr>
                                                                                        <td>P000<?=$j+1?></td>
                                                                                        <td>ข่ายเอ็น</td>
                                                                                        <td><?=$amountItem?></td>
                                                                                        <td>PC</td>
                                                                                        <td>05/08/2556</td>
                                                                                        <td>09/08/2556</td>
                                                                                        <td>10/08/2556</td>
                                                                                        <td>13/08/2556</td>
                                                                                        <td><?=$amountWovenDress?></td>
                                                                                        <td><?=$amountWeave?></td>
                                                                                        <td>17/08/2556</td>
                                                                                        <td>20/08/2556</td>
                                                                                        <td>24/08/2556</td>
                                                                                        <td><?=$amountWeave?></td>
                                                                                    </tr>
                                                                                <?php } ?>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<? include('inc.footer.php');?>
<!--  -->
<? include('inc.footer.script.php');?>
