<?php
$select = 'open_menu_1';
$select2 = 'menu_5';
$select3 = 'menu_5';
$select4 = 'menu_5_7';
?>
<!--  -->
<? include('inc.header.php');?>
<!--  -->
<? include('inc.navbar.php');?>
<!--  -->
<? include('inc.menu.php');?>
<!-- <div class="layout-main"> -->
  <!--  -->
  <div class="layout-content">
    <div class="layout-content-body">
      <div class="title-bar">
        <h4 class="m-t-0">
          <!-- <span class="d-ib text-danger">--</span> <span class="icon icon-angle-double-right"></span> -->
          <!-- <span class="d-ib">ข้อมูลหลัก</span> -->
          <span class="d-ib text-primary">การตั้งค่า</span> <span class="icon icon-angle-double-right"></span> กระบวนการผลิต <span class="icon icon-angle-double-right"></span> ลำดับขั้นตอนการผลิต
                <!-- <span class="d-ib">
                    <a class="title-bar-shortcut" href="#" title="Add to shortcut list" data-container="body" data-toggle-text="Remove from shortcut list" data-trigger="hover" data-placement="right" data-toggle="tooltip">
                        <span class="sr-only">Add to shortcut list</span>
                    </a>
                  </span> -->
                </h4>
            <!-- <p class="title-bar-description">
                <small>Latest update on 01/01/2017 - 10.57 am.</small>
              </p> -->
            </div>

            <div class="row gutter-xs">
              <div class="col-xs-12">
                <!--  -->
                <div class="card">
                    <!-- <div class="card-header">
                        <a href="javascript:window.history.back(-1);" class="btn btn-sm btn-outline-primary"><span class="icon icon-long-arrow-left"></span>&nbsp;&nbsp;Back</a>&nbsp;&nbsp;
                        <strong class="">Latest update on 01/01/2017 - 10.57 am.</strong>
                      </div> -->
                      <div class="card-body">
                        <!--  -->
                        <? include('inc.home_top_filter.php');?>
                        <!--  -->
                        <div class="table-responsive">
                          <table id="demo-datatables-2" class="table table-striped table-bordered table-nowrap dataTable text-center no-footer" cellspacing="0" width="100%">
                            <thead>
                              <tr class="at_bg_table_blue">
                                <th class="text-center">รหัสกระบวนการผลิต</th>
                                <th class="text-center">ลำดับขั้นตอน</th>
                                <th class="text-center">รหัสขั้นตอน</th>
                                <th class="text-center">เริ่มใช้งาน</th>                                        
                                <th class="text-center">ผู้บันทึก</th>                                        
                                <th class="text-center">ปรับแก้ไข</th>
                              </tr>
                            </thead>
                            <tbody>

<?php $column1 = array(

"C01T", 
"C01T", 
"C01T", 
"C01T", 
"C01T", 
"C01T", 
"C01T", 
"C01T", 
"C01T", 
"C01T", 
"C02Y", 
"C02Y", 
"C02Y", 
"C02Y", 
"C02Y", 
"C02Y", 
"C02Y", 
"C02Y", 
"C02Y", 
"C02Y", 
"C02Y", 
"C02Y", 
"C02Y", 
"C02Y", 
"C02Y", 
"C07T", 
"C07T", 
"C07T", 
"C07T", 
"C07T", 
"C07T", 
"C07T", 
"C07T", 
"C07T", 
"C07T", 
"C07T", 
"C07T", 
"C07T", 
"C07T", 
"C09Y", 
"C09Y", 
"C09Y",
"C09Y", 
"C09Y", 
"C09Y", 
"C09Y", 
"C09Y", 
"C09Y", 
"C09Y", 
"C09Y", 
"C09Y", 
"C09Y", 
"C09Y", 
"C09Y", 
"C09Y", 
"c10y", 
"c10y", 
"c10y", 
"c10y", 
"c10y", 
"c10y", 
"c10y", 
"c10y", 
"c10y", 
"c10y", 
"c10y", 
"c10y", 
"c10y", 
"C12T", 
"C12T", 
"C12T", 
"C12T", 
"C12T", 
"C12T", 
"C12T", 
"C12T", 
"C13T", 
"C13T", 
"C13T",
"C13T", 
"C13T", 
"C13T", 
"C13T", 
"C13T", 
"C13T", 
"C13T", 
"C14T", 
"C14T", 
"C14T", 
"C14T", 
"C14T", 
"C14T", 
"C14T", 
"C14T", 
"C14T", 
"C14T", 
"C14T", 
"C14T", 
"C14T", 
"C14T", 
);
 $arrlength = count($column1);

$column2 = array(

1,
2,
3,
4,
5,
6,
7,
8,
9,
10,
1,
2,
3,
4,
5,
6,
7,
8,
9,
10,
11,
12,
13,
14,
15,
1,
2,
3,
4,
5,
6,
7,
8,
9,
10,
11,
12,
13,
14,
1,
2,
3,
4,
5,
6,
7,
8,
9,
10,
11,
12,
13,
14,
15,
16,
1,
2,
3,
4,
5,
6,
7,
8,
9,
10,
11,
12,
13,
1,
2,
3,
4,
5,
6,
7,
8,
1,
2,
3,
4,
5,
6,
7,
8,
9,
10,
1,
2,
3,
4,
5,
6,
7,
8,
9,
10,
11,
12,
13,
14,
);

$column3 = array(

"A00", 
"B00", 
"C02",  
"E03",  
"E06",  
"F00",  
"G00",  
"G04",  
"G05",  
"G06",  
"A00",  
"B00",  
"C00",  
"C02",  
"D00",  
"D01",  
"D02",  
"D04",  
"E00",  
"E01",  
"E07",  
"F00",  
"G00",  
"G05",  
"G06",  
"A01",  
"B00",  
"D00",  
"D01",  
"D02",  
"D04",  
"E02",  
"D05",  
"G07",  
"E06",  
"F00", 
"G00",  
"G05",  
"G06",  
"A00",  
"B00",  
"E05",  
"C00",  
"C02",  
"D00",  
"D01",  
"D02",  
"D04",  
"E00",  
"E01",  
"E07",  
"F00",  
"G00",  
"G05",  
"G06",  
"A01",  
"B00",  
"C00",  
"C02",  
"D02",  
"D04",  
"E00",  
"E01",  
"E07",  
"F00",  
"G00",  
"G05",  
"G06",  
"A00",  
"B00",  
"C02",  
"E03",  
"G04",  
"G05",  
"G00",  
"G06",  
"A00",  
"B00",  
"C02",  
"E03",  
"E06",  
"F00",  
"G04",  
"G05", 
"G00",  
"G06",  
"A01",  
"B00",  
"C02",  
"C00",  
"D00",  
"E00",  
"E01",  
"D01",  
"D02",  
"D04",  
"E02",  
"E06",  
"F00",  
"G00",  
);

$column4 = array(
"DD/MM/YYYY"
);
$column5 = array(
"USER_ID"
);
?>

                              <?php for($i=0;$i<$arrlength;$i++){?>

                                <tr height=19 style='height:14.25pt'>
                                  <td height=19 align=left style='height:14.25pt;'><?php echo $column1[$i];?><span
                                    style='mso-spacerun:yes'>&nbsp;</span></td>
                                    <td align=left><?php echo $column2[$i];?></td>
                                    <td align=left><?php echo $column3[$i];?><span
                                      style='mso-spacerun:yes'>&nbsp;&nbsp;</span></td>
                                      <td align=right><?php echo $column4[0];?></td>
                                      <td align=right><?php echo $column5[0];?></td>
                                      <td class="text-center" style="display: table-cell;">
                                       <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_5_7.php" type="button">
                                        <span class="icon icon-lg icon-edit"></span>
                                      </a>
                                      <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                                        <span class="icon icon-lg icon-close"></span>
                                      </button>
                                    </td>
                                  </tr>
                                  <?}?>
                                </table>
                              </div>
                            </div>
                          </div>
                          <!--  -->
                          <!--  -->
                          <!--  -->
                          <!--  -->
                          <!--  -->
                          <!--  -->
                        </div>
                      </div>










                    </div>
                  </div>
                  <!--  -->
                  <? include('inc.footer.php');?>
                  <!--  -->
                  <? include('inc.footer.script.php');?>
                  <!--  -->
