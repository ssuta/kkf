﻿<?php
    $filterStatus = array();
    $filterOrder = array();
    if(isset($statusText)){
        $filterStatus = $statusText;
        $filterStatus = str_replace('<br>', ' ', $filterStatus);
    }

    if(isset($orders)){
        $filterOrder = $orders;
    }
?>

<div class="at_add_box">
    <div class="card" aria-expanded="true">
        <div class="card-header at_bg_table_blue">
            <div class="card-actions">
                    <button type="button" class="card-action card-toggler" title="Collapse" aria-expanded="true"></button>
                    <!-- <button type="button" class="card-action card-reload" title="Reload"></button>
                    <button type="button" class="card-action card-remove" title="Remove"></button> -->
            </div>
            <strong>ตัวเลือกการค้นหา</strong>
        </div>
        <div class="card-body" style="display: block;">
            <div class="row">
                <div class="col-sm-3">
                    <form class="row form form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-3 control-label text-right" for="form-control-14">สาขา</label>
                            <div class="col-sm-9 has-feedback">
                                <select id="demo-select2-1" class="form-control">
                                    <option value="0">ทั้งหมด</option>
                                    <option value="0">ขอนแก่นแหอวน</option>
                                </select>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-sm-3">
                    <form class="row form form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-5 control-label text-right" for="form-control-14">ประเภทการขาย</label>
                            <div class="col-sm-7 has-feedback">
                                <select id="demo-select2-2" class="form-control">
                                    <option value="0">ทั้งหมด</option>
                                    <option value="0">ขายใน</option>
                                    <option value="0">ขายต่าง</option>
                                </select>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-sm-3">
                    <form class="row form form-horizontal <?=!isset($orders)?"hide":""?>">
                        <div class="form-group">
                            <label class="col-sm-5 control-label text-right" for="form-control-14">ประเภทรายการ</label>
                            <div class="col-sm-7 has-feedback">
                                <select id="demo-select2-2" class="form-control">
                                    <option value="0">ทั้งหมด</option>
                                    <?php for($i=0; $i<count($filterOrder); $i++){ ?>
                                        <option value="0"><?=$filterOrder[$i]?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </form>
                </div>
                
                
                <div class="col-sm-3">
                    <form class="form form-horizontal">
                        <div class="text-right">
                            <a class="btn btn-md btn-primary width-fit" href="javaascript:void(0)" type="submit" onclick="window.location.reload()">ค้นหา</a>
                        </div>
                    </form>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-3">
                    <form class="row form form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-3 control-label text-right" for="form-control-14">สถานะ</label>
                            <div class="col-sm-9 has-feedback">
                                <select id="demo-select2-3" class="form-control">
                                    <option value="0">ทั้งหมด</option>
                                    <?php for($i=0; $i<count($filterStatus); $i++){ ?>
                                        <option value="0"><?=$filterStatus[$i]?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-sm-3">
                    <form class="row form form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-5 control-label text-right" for="form-control-14">ตามช่วงเวลา</label>
                            <div class="col-sm-7 has-feedback">
                                <select id="demo-select2-3-3" class="form-control">
                                    <option value="0">สั่งซื้อ</option>
                                    <option value="0">จัดส่งสินค้า</option>
                                    <option value="0">ผลิต</option>
                                </select>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-sm-3">
                    <div class="input-group input-daterange" data-provide="datepicker" data-date-autoclose="true" data-date-format="dd-M-yyyy">
                        <input class="form-control" type="text" value="เริ่มต้น">
                        <span class="input-group-addon">ถึง</span>
                        <input class="form-control" type="text" value="สิ้นสุด">
                    </div>
                </div>
            </div>
		</div>
	</div>
</div>

<div class="at_add_box extra">
    <div class="row">
        <div class="col-sm-12 text-right">
            <? if ($select3 == 'menu_all'){?>
                <!-- <a href="menu_1_1_all.php" class="btn btn-info"><span class="icon icon-lg icon-table"></span>&nbsp;&nbsp;แสดงทั้งหมด</a> -->
            <? }else{?>
                <!-- <a href="menu_1_1.php" class="btn btn-info"><span class="icon icon-lg icon-table"></span>&nbsp;&nbsp;มุมมองปกติ</a> -->
            <? } ?>
            <!--<a href="menu_1_1_history.php" class="btn btn-info"><span class="icon icon-lg icon-history"></span>&nbsp;&nbsp;ประวัติการแก้ไข</a> -->
            <button onClick="window.print()" class="btn btn-default" type="button"><span class="icon icon-lg icon-print"></span>&nbsp;&nbsp;พิมพ์</button>
            <a href="images/SampleXLSFile_212kb.xls" class="btn btn-warning" type="button"><span class="icon icon-lg icon-file-text-o"></span>&nbsp;&nbsp;ส่งออกข้อมูล</a>
        </div>
    </div>

    <div class="suggestion status in-filter">
        <div class="content">
            <div class="element">
                <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="javascript:void(0)" target="_blank" type="button" title="แสดงแผน">
                    <span class="icon icon-lg icon-file-text-o"></span>
                </a>
                <div class="text">แสดงแผน</div></div>
            <div class="element">
                <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="javascript:void(0)" type="button" title="แก้ไข">
                    <span class="icon icon-lg icon-edit"></span>
                </a>
                <div class="text">แก้ไข</div>
            </div>
            <?php if(!isset($hasDeleteButton) || (isset($hasDeleteButton) && $hasDeleteButton != false)){ ?>
                <div class="element">
                    <button class="btn btn-outline-danger btn-pill btn-xs" type="button" title="ลบ">
                        <span class="icon icon-lg icon-close"></span>
                    </button>
                    <div class="text">ลบ</div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>