<?php
    $select = 'open_menu_1';
    $select2 = 'menu_2';
    $select3 = 'menu_2';
    $select4 = 'menu_2_1';
?>
<!--  -->
<? include('inc.header.php');?>
<!--  -->
<? include('inc.navbar.php');?>
<!--  -->
<? include('inc.menu.php');?>
<!-- <div class="layout-main"> -->
<!--  -->
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <h4 class="m-t-0">
                <!-- <span class="d-ib text-danger">--</span> <span class="icon icon-angle-double-right"></span> -->
                <!-- <span class="d-ib">ข้อมูลหลัก</span> -->
                <span class="d-ib text-primary">ข้อมูลหลัก</span> <span class="icon icon-angle-double-right"></span> สินค้า <span class="icon icon-angle-double-right"></span> สินค้า
                <!-- <span class="d-ib">
                    <a class="title-bar-shortcut" href="#" title="Add to shortcut list" data-container="body" data-toggle-text="Remove from shortcut list" data-trigger="hover" data-placement="right" data-toggle="tooltip">
                        <span class="sr-only">Add to shortcut list</span>
                    </a>
                </span> -->
            </h4>
            <!-- <p class="title-bar-description">
                <small>Latest update on 01/01/2017 - 10.57 am.</small>
            </p> -->
        </div>

        <div class="row gutter-xs">
            <div class="col-xs-12">
                <!--  -->
                <div class="card">
                    <!-- <div class="card-header">
                        <a href="javascript:window.history.back(-1);" class="btn btn-sm btn-outline-primary"><span class="icon icon-long-arrow-left"></span>&nbsp;&nbsp;Back</a>&nbsp;&nbsp;
                        <strong class="">Latest update on 01/01/2017 - 10.57 am.</strong>
                    </div> -->
                    <div class="card-body">
                        <!--  -->
                        <? include('inc.home_top_filter.php');?>
                        <!--  -->
                        <div class="table-responsive">
                            <table id="demo-datatables-2" class="table table-striped table-bordered table-nowrap dataTable text-center no-footer" cellspacing="0" width="100%">
                                <thead>
<tr class="at_bg_table_blue">
    <th rowspan="2" class="text-center">รหัสสินค้า</th>
    <th rowspan="2" class="text-center">ชื่อสินค้า</th>
    <th rowspan="2" class="text-center">ประเภทสินค้า</th>
    <th rowspan="2" class="text-center">ประเภทใย</th>
    <th rowspan="2" class="text-center">เบอร์ใยผลิต</th>
    <th rowspan="2" class="text-center">เบอร์ใยติดป้าย</th>
    <th rowspan="2" class="text-center">เงื่อน</th>
    <th rowspan="2" class="text-center">อบ</th>
    <th rowspan="2" class="text-center">สีหู</th>
    <th rowspan="2" class="text-center">รหัสรุ่น</th>
    <th rowspan="2" class="text-center">ชื่อรุ่น</th>
    <th rowspan="2" class="text-center">ขนาดตาผลิต</th>
    <th rowspan="2" class="text-center">ขนาดตาติดป้าย</th>
    <th rowspan="2" class="text-center">จำนวนตาผลิต</th>
    <th rowspan="2" class="text-center">จำนวนตาติดป้าย</th>
    <th rowspan="2" class="text-center">ความยาวผลิต</th>
    <th rowspan="2" class="text-center">ความยาวติดป้าย</th>
    <th rowspan="2" class="text-center">สีอวน</th>
    <th rowspan="2" class="text-center">สีอวนติดป้าย</th>
    <th rowspan="2" class="text-center">รหัสป้าย</th>
    <th rowspan="2" class="text-center">ชื่อป้าย</th>
    <th rowspan="2" class="text-center">กลุ่มตรวจนับ</th>
    <th rowspan="2" class="text-center">ProductTypeCode</th>
    <th rowspan="2" class="text-center">GCode</th>
    <th rowspan="2" class="text-center">TwineTypeCode</th>
    <th rowspan="2" class="text-center">TwineSizeNo</th>
    <th rowspan="2" class="text-center">KnotTypeCode</th>
    <th rowspan="2" class="text-center">StretchingTypeCode</th>
    <th rowspan="2" class="text-center">EarColorCode</th>
    <th rowspan="2" class="text-center">EyeSizeNo</th>
    <th rowspan="2" class="text-center">EyeFormu</th>
    <th rowspan="2" class="text-center">EyeFormuD</th>
    <th rowspan="2" class="text-center">EyeAmountNo</th>
    <th rowspan="2" class="text-center">LengthFormu</th>
    <th rowspan="2" class="text-center">LengthFormuD</th>
    <th rowspan="2" class="text-center">LengthNO</th>
    <th rowspan="2" class="text-center">SelvageMKCode</th>
    <th rowspan="2" class="text-center">SelvageWVCode</th>
    <th rowspan="2" class="text-center">SelvageSTCode</th>
    <th rowspan="2" class="text-center">MultiLayerCode</th>
    <th rowspan="2" class="text-center">CostUnit</th>
    <th rowspan="2" class="text-center">ProdUnit</th>
    <th rowspan="2" class="text-center">AvgCost</th>
    <th rowspan="2" class="text-center">Weight</th>
    <th rowspan="2" class="text-center">Volume</th>
    <th rowspan="2" class="text-center">Qty_Bale</th>
    <th rowspan="2" class="text-center">AccGrpCode</th>
    <th rowspan="2" class="text-center">ProdBrn</th>
    <th rowspan="2" class="text-center">TaxProd</th>
    <th rowspan="2" class="text-center">Active</th>
    <th rowspan="2" class="text-center">Temp</th>
    <th rowspan="2" class="text-center">Status</th>
    <th rowspan="2" class="text-center">CopyCode</th>
    <th rowspan="2" class="text-center">CDESC</th>
    <th rowspan="2" class="text-center">CKDUP</th>
    <th rowspan="2" class="text-center">INPID</th>
    <th rowspan="2" class="text-center">INPDT</th>
    <th rowspan="2" class="text-center">LINENO</th>
    <th rowspan="2" class="text-center">YAINO</th>
    <th rowspan="2" class="text-center">PROFIT</th>
    <th rowspan="2" class="text-center">Weight_Sale</th>
    <th rowspan="2" class="text-center">Weight_Stock</th>
    <th rowspan="2" class="text-center">Weight_Avg</th>
    <th rowspan="2" class="text-center">PQGradeCD</th>
    <th rowspan="2" class="text-center">SalePrice</th>
    <th rowspan="2" class="text-center">BarCode</th>
    <th rowspan="2" class="text-center">gProdDesc</th>
    <th rowspan="2" class="text-center">GenID</th>
    <th rowspan="2" class="text-center">DenierID</th>
    <th rowspan="2" class="text-center">DenierName</th>
    <th rowspan="2" class="text-center">DenierLineID</th>
    <th rowspan="2" class="text-center">DenierLineNm</th>
    <th rowspan="2" class="text-center">Curse</th>
    <th rowspan="2" class="text-center">PriceOK</th>
    <th rowspan="2" class="text-center">MinWei</th>
    <th rowspan="2" class="text-center">MaxWei</th>
    <th rowspan="2" class="text-center">NProdCode</th>
    <th rowspan="2" class="text-center">dFlag</th>
    <th rowspan="2" class="text-center">OkFlag</th>
    <th rowspan="2" class="text-center">BONE_NO</th>
    <th rowspan="2" class="text-center">ExFlag</th>
    <th rowspan="2" class="text-center">CHUB_ID</th>
    <th rowspan="2" class="text-center">STRICK_C</th>
    <th rowspan="2" class="text-center">BONE</th>
    <th rowspan="2" class="text-center">ProdDescEng</th>
    <th rowspan="2" class="text-center">PriceId</th>
    <th rowspan="2" class="text-center">PriceDate</th>
    <th rowspan="2" class="text-center">PriceOK1</th>
    <th rowspan="2" class="text-center">PriceId1</th>
    <th rowspan="2" class="text-center">PriceDate1</th>
    <th rowspan="2" class="text-center">LPrice</th>
    <th rowspan="2" class="text-center">LDisCode</th>
    <th rowspan="2" class="text-center">AVPrice</th>
    <th rowspan="2" class="text-center">AVDate</th>
    <th rowspan="2" class="text-center">PrdWeight</th>
    <th rowspan="2" class="text-center">BagId</th>
    <th rowspan="2" class="text-center">SticId</th>
    <th rowspan="2" class="text-center">ProcessCD</th>
    <th rowspan="2" class="text-center">SoftQualCD</th>
    <th rowspan="2" class="text-center">Weight_Cost</th>
    <th rowspan="2" class="text-center">StdCost</th>
    <th rowspan="2" class="text-center">StdCostNFC</th>
    <th rowspan="2" class="text-center">StdCostMR</th>
    <th rowspan="2" class="text-center">StdCostMRNFC</th>
    <th rowspan="2" class="text-center">StdPrice</th>
    <th rowspan="2" class="text-center">StdPriceNFC</th>
    <th rowspan="2" class="text-center">StdPriceMR</th>
    <th rowspan="2" class="text-center">StdPriceMRNFC</th>
    <th rowspan="2" class="text-center">StdPrice_wip</th>
    <th rowspan="2" class="text-center">StdPriceNFC_wip</th>
    <th rowspan="2" class="text-center">StdPriceMR_wip</th>
    <th rowspan="2" class="text-center">StdPriceMRNFC_wip</th>
    <th rowspan="2" class="text-center">StdCost_wip</th>
    <th rowspan="2" class="text-center">StdCostNFC_wip</th>
    <th rowspan="2" class="text-center">StdCostMR_wip</th>
    <th rowspan="2" class="text-center">StdCostMRNFC_wip</th>
    <th rowspan="2" class="text-center">PiecePerDay</th>
    <th rowspan="2" class="text-center">StdPriceMR_Exp</th>
    <th rowspan="2" class="text-center">StdCostMR_Exp</th>
    <th rowspan="2" class="text-center">StdOK</th>
    <th rowspan="2" class="text-center">UpWeiCost</th>
    <th rowspan="2" class="text-center">StdId</th>
    <th rowspan="2" class="text-center">Foh_Rum</th>
    <th rowspan="2" class="text-center">Rum_cost</th>
    <th rowspan="2" class="text-center">Accessary_cost</th>
    <th rowspan="2" class="text-center">Accessary_wei</th>
    <th rowspan="2" class="text-center">SemiFgCost</th>
    <th rowspan="2" class="text-center">Body_Wei</th>
    <th rowspan="2" class="text-center">DayQty</th>
    <th rowspan="2" class="text-center">RopeId</th>
    <th rowspan="2" class="text-center">Stock</th>
    <th rowspan="2" class="text-center">StockName</th>
    <th rowspan="2" class="text-center">PriceFlag</th>
    <th rowspan="2" class="text-center">AccGroupNM</th>
    <th rowspan="2" class="text-center">zRumID</th>
    <th rowspan="2" class="text-center">zRumName</th>
    <th rowspan="2" class="text-center">zMultiLayerCodeNew1</th>
    <th rowspan="2" class="text-center">MultiLayerCodeNew1</th>
    <th rowspan="2" class="text-center">zRumName1</th>
    <th rowspan="2" class="text-center">StdPackQty</th>
    <th colspan="2" class="text-center">จำนวนสั่งผลิตขั้นต่ำ    </th>
    <th rowspan="2" class="text-center">เริ่มใช้งาน</th>
    <th rowspan="2" class="text-center">ผู้บันทึก</th>
    <th rowspan="2" class="text-center">วันที่บันทึก</th>
    <th rowspan="2" class="text-center">ปรับแก้ไข</th>
</tr>
<tr class="at_bg_table_blue">
    <th class="text-center">จำนวน</th>
    <th class="text-center">หน่วย</th>
</tr>
                                </thead>
                                <tbody>
<tr>
    <td>1119P/013010400</td>
    <td>ข่ายเอ็น 0.09 SK 1.3*40*180A. สีง SK00 YOKO เรือใบเงิน</td>
    <td>ข่ายเอ็น</td>
    <td>MONO</td>
    <td>0085</td>
    <td>0.09 mm</td>
    <td>SK</td>
    <td>YOKO</td>
    <td>สีหูเหมือนตัวอวน</td>
    <td></td>
    <td>SK YOKO เรือใบเงิน</td>
    <td>1.3</td>
    <td>1.30 cm</td>
    <td>40</td>
    <td>40 md</td>
    <td>175</td>
    <td>180A.</td>
    <td>SK00</td>
    <td>สีง SK00</td>
    <td>74</td>
    <td>เรือใบเงิน</td>
    <td>03</td>
    <td>1</td>
    <td>1M</td>
    <td>M</td>
    <td>19</td>
    <td>SK</td>
    <td>Y</td>
    <td>SAME</td>
    <td>0130</td>
    <td>MD</td>
    <td>B</td>
    <td>040</td>
    <td>M</td>
    <td>B</td>
    <td>P</td>
    <td>1</td>
    <td>G</td>
    <td>A</td>
    <td>/</td>
    <td></td>
    <td>PC</td>
    <td>0</td>
    <td>0</td>
    <td>0</td>
    <td>200</td>
    <td>03</td>
    <td>B&amp;S</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>PANUMAS</td>
    <td>25/03/2013 14:36:23</td>
    <td>0</td>
    <td>0.09</td>
    <td>0</td>
    <td>1000</td>
    <td>150</td>
    <td>0</td>
    <td>P</td>
    <td>631.94</td>
    <td>8854445991025</td>
    <td>ข่ายเอ็น 0.09 SK 1.3*40*180A. สีง SK00 YOKO เรือใบเงิน</td>
    <td>40604</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>0</td>
    <td></td>
    <td>157.5</td>
    <td>165.28</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>MONO 0.09 SK 1.3*40*180A. สีง SK00 YOKO REI-BAI เงิน</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>0</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>C02Y</td>
    <td>A26</td>
    <td>161</td>
    <td>287.13</td>
    <td>243.9</td>
    <td>318.33</td>
    <td>270.17</td>
    <td>1783.42</td>
    <td>1514.91</td>
    <td>1977.21</td>
    <td>1678.07</td>
    <td>1783.73</td>
    <td>1515.22</td>
    <td>1977.52</td>
    <td>1678.39</td>
    <td>287.18</td>
    <td>243.95</td>
    <td>318.38</td>
    <td>270.22</td>
    <td>7.79</td>
    <td></td>
    <td></td>
    <td>Y</td>
    <td>31/08/2017 08:38:48</td>
    <td>5701241</td>
    <td>0</td>
    <td>0</td>
    <td>0</td>
    <td>0</td>
    <td>63.16</td>
    <td>161</td>
    <td>0</td>
    <td>W6</td>
    <td></td>
    <td></td>
    <td></td>
    <td>อวน SK</td>
    <td>1/</td>
    <td></td>
    <td>1</td>
    <td></td>
    <td></td>
    <td>300</td>
    <td></td>
    <td></td>
    <td>21/07/2549 14:46 น.</td>
    <td>USER001</td>
    <td>21/07/2549 14:46 น.</td>
    <td class="text-center" style="display: table-cell;">
        <button class="btn btn-lg btn-outline-primary btn-pill btn-xs" data-toggle="modal" data-target="#menu_1_1_edit" type="button">
            <span class="icon icon-lg icon-edit"></span>
        </button>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
<tr>
    <td>11090/013000402</td>
    <td>ข่ายเอ็น 0.09 SK 1.3*40*180 SK00 YOKO ป้ายเหลืองพิมพ์ 2*3 นิ้ว (ไม่ได้มาตรฐาน)
    </td>
    <td>ข่ายเอ็น</td>
    <td>MONO</td>
    <td>0090</td>
    <td>0.09 mm</td>
    <td>SK</td>
    <td>YOKO</td>
    <td>สีหูเหมือนตัวอวน</td>
    <td></td>
    <td>SK YOKO</td>
    <td>1.3</td>
    <td>1.30 cm</td>
    <td>40</td>
    <td>40 md</td>
    <td>180</td>
    <td>180 m</td>
    <td>SK00</td>
    <td>SK00</td>
    <td>22</td>
    <td>ป้ายเหลืองพิมพ์ 2*3 นิ้ว (ไม่ได้มาตรฐาน)</td>
    <td>10</td>
    <td>1</td>
    <td>1M</td>
    <td>M</td>
    <td>09</td>
    <td>SK</td>
    <td>Y</td>
    <td>SAME</td>
    <td>0130</td>
    <td>MD</td>
    <td>B</td>
    <td>040</td>
    <td>M</td>
    <td>B</td>
    <td>0</td>
    <td>0</td>
    <td>G</td>
    <td>A</td>
    <td>/</td>
    <td></td>
    <td>PC</td>
    <td>0</td>
    <td>0</td>
    <td>0</td>
    <td>350</td>
    <td>03</td>
    <td>B&amp;S</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>WANLAPA</td>
    <td>31/08/2012 12:39:40</td>
    <td>0</td>
    <td>0.09</td>
    <td>0</td>
    <td>0</td>
    <td>0</td>
    <td>0</td>
    <td>N</td>
    <td>787</td>
    <td>8854445469944</td>
    <td>ข่ายเอ็น 0.09 SK 1.3*40*180 SK00 YOKO ป้ายเหลืองพิมพ์ 2*3 นิ้ว (ไม่ได้มาตรฐาน)
    </td>
    <td>39371</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>0</td>
    <td>Y</td>
    <td>160</td>
    <td>170</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>MONO 0.09 SK 1.3*40*180 SK00 YOKO ป้ายเหลืองพิมพ์ 2*3 นิ้ว (ไม่ได้มาตรฐาน)
    </td>
    <td>5103025</td>
    <td>05/02/2017 10:33:33</td>
    <td>Y</td>
    <td>5205677</td>
    <td>02/02/2017 15:53:35</td>
    <td>0</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>M03Y</td>
    <td>A26</td>
    <td>0</td>
    <td>0</td>
    <td></td>
    <td></td>
    <td></td>
    <td>0</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>0</td>
    <td></td>
    <td></td>
    <td>0</td>
    <td>W6</td>
    <td></td>
    <td></td>
    <td></td>
    <td>อวน SK</td>
    <td>1/</td>
    <td></td>
    <td>1</td>
    <td></td>
    <td></td>
    <td>400</td>
    <td></td>
    <td></td>
    <td>1/07/2552 18:01 น.</td>
    <td>USER001</td>
    <td>1/07/2552 18:01 น.</td>
    <td class="text-center" style="display: table-cell;">
        <button class="btn btn-lg btn-outline-primary btn-pill btn-xs" data-toggle="modal" data-target="#menu_1_1_edit" type="button">
            <span class="icon icon-lg icon-edit"></span>
        </button>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
<tr>
    <td>11190/013010400</td>
    <td>ข่ายเอ็น 0.09 SK 1.3*40*180 สีง SK00 YOKO เรือใบเงิน</td>
    <td>ข่ายเอ็น</td>
    <td>MONO</td>
    <td>0085</td>
    <td>0.09 mm</td>
    <td>SK</td>
    <td>YOKO</td>
    <td>สีหูเหมือนตัวอวน</td>
    <td></td>
    <td>SK YOKO เรือใบเงิน</td>
    <td>1.3</td>
    <td>1.30 cm</td>
    <td>40</td>
    <td>40 md</td>
    <td>180</td>
    <td>180 m</td>
    <td>SK00</td>
    <td>สีง SK00</td>
    <td>74</td>
    <td>เรือใบเงิน</td>
    <td>01</td>
    <td>1</td>
    <td>1M</td>
    <td>M</td>
    <td>19</td>
    <td>SK</td>
    <td>Y</td>
    <td>SAME</td>
    <td>0130</td>
    <td>MD</td>
    <td>B</td>
    <td>040</td>
    <td>M</td>
    <td>B</td>
    <td>0</td>
    <td>1</td>
    <td>G</td>
    <td>A</td>
    <td>/</td>
    <td></td>
    <td>PC</td>
    <td>0</td>
    <td>0</td>
    <td>0</td>
    <td>350</td>
    <td>03</td>
    <td>B&amp;S</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>PANUMAS</td>
    <td>25/03/2013 14:39:32</td>
    <td>0</td>
    <td>0.09</td>
    <td>0</td>
    <td>1000</td>
    <td>154</td>
    <td>0</td>
    <td>P</td>
    <td>787</td>
    <td>8854445058049</td>
    <td>ข่ายเอ็น 0.09 SK 1.3*40*180 สีง SK00 YOKO เรือใบเงิน</td>
    <td>40605</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>0</td>
    <td>Y</td>
    <td>160</td>
    <td>170</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>MONO 0.09 SK 1.3*40*180 สีง SK00 YOKO REI-BAI เงิน</td>
    <td>5103025</td>
    <td>09/02/2017 14:50:45</td>
    <td>Y</td>
    <td>5801363</td>
    <td>25/02/2017 14:45:31</td>
    <td>0</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>ZE</td>
    <td>93</td>
    <td>C02Y</td>
    <td>A26</td>
    <td>165</td>
    <td>294.93</td>
    <td>250.45</td>
    <td>326.93</td>
    <td>277.38</td>
    <td>1787.45</td>
    <td>1517.88</td>
    <td>1981.39</td>
    <td>1681.09</td>
    <td>1778.18</td>
    <td>1508.61</td>
    <td>1972</td>
    <td>1671.7</td>
    <td>293.4</td>
    <td>248.92</td>
    <td>325.38</td>
    <td>275.83</td>
    <td>7.57</td>
    <td></td>
    <td></td>
    <td>Y</td>
    <td>06/11/2017 13:00:32</td>
    <td>6001807</td>
    <td>0</td>
    <td>0</td>
    <td>0</td>
    <td>0</td>
    <td>64.77</td>
    <td>165</td>
    <td>0</td>
    <td>W6</td>
    <td></td>
    <td></td>
    <td></td>
    <td>อวน SK</td>
    <td>1/</td>
    <td></td>
    <td>1</td>
    <td></td>
    <td></td>
    <td>350</td>
    <td></td>
    <td></td>
    <td>21/07/2549 14:43 น.</td>
    <td>USER001</td>
    <td>21/07/2549 14:43 น.</td>
    <td class="text-center" style="display: table-cell;">
        <button class="btn btn-lg btn-outline-primary btn-pill btn-xs" data-toggle="modal" data-target="#menu_1_1_edit" type="button">
            <span class="icon icon-lg icon-edit"></span>
        </button>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
<tr>
    <td>19090/013000402</td>
    <td>ข่ายเอ็น 0.09 SK 1.3*40*180 SK00 YOKO ป้ายม้าน้ำเดี่ยว</td>
    <td>ข่ายเอ็น</td>
    <td>MONO</td>
    <td>0090</td>
    <td>0.09 mm</td>
    <td>SK</td>
    <td>YOKO</td>
    <td>สีหูเหมือนตัวอวน</td>
    <td></td>
    <td></td>
    <td>1.3</td>
    <td>1.30 cm</td>
    <td>40</td>
    <td>40 md</td>
    <td>180</td>
    <td>180 m</td>
    <td>SK00</td>
    <td>SK00</td>
    <td>AX</td>
    <td>ป้ายม้าน้ำเดี่ยว</td>
    <td>10</td>
    <td>1</td>
    <td>1M</td>
    <td>M</td>
    <td>09</td>
    <td>SK</td>
    <td>Y</td>
    <td>SAME</td>
    <td>0130</td>
    <td>MD</td>
    <td>B</td>
    <td>040</td>
    <td>M</td>
    <td>B</td>
    <td>0</td>
    <td>0</td>
    <td>G</td>
    <td>A</td>
    <td>/</td>
    <td></td>
    <td>PC</td>
    <td>0</td>
    <td>0</td>
    <td>0</td>
    <td>350</td>
    <td>03</td>
    <td>B&amp;S</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>11090/013000402</td>
    <td></td>
    <td></td>
    <td>PORIN</td>
    <td>11/10/2010 10:16:26</td>
    <td>0</td>
    <td>0.09</td>
    <td>0</td>
    <td>1000</td>
    <td>154</td>
    <td>0</td>
    <td>N</td>
    <td>787</td>
    <td>8854445086479</td>
    <td>ข่ายเอ็น 0.09 SK 1.3*40*180 SK00 YOKO ป้ายม้าน้ำเดี่ยว</td>
    <td>28717</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>0</td>
    <td>Y</td>
    <td>160</td>
    <td>170</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>MONO 0.09 SK 1.3*40*180 SK00 YOKO ป้ายม้าน้ำเดี่ยว</td>
    <td>5103025</td>
    <td>05/02/2017 10:33:35</td>
    <td>Y</td>
    <td>5205677</td>
    <td>02/02/2017 15:54:13</td>
    <td>0</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>M03Y</td>
    <td>A26</td>
    <td>165</td>
    <td>204.73</td>
    <td></td>
    <td></td>
    <td></td>
    <td>0</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>0</td>
    <td></td>
    <td></td>
    <td>0</td>
    <td>W6</td>
    <td></td>
    <td></td>
    <td></td>
    <td>อวน SK</td>
    <td>1/</td>
    <td></td>
    <td>1</td>
    <td></td>
    <td></td>
    <td>300</td>
    <td></td>
    <td></td>
    <td>8/09/2552 20:50 น.</td>
    <td>USER001</td>
    <td>8/09/2552 20:50 น.</td>
    <td class="text-center" style="display: table-cell;">
        <button class="btn btn-lg btn-outline-primary btn-pill btn-xs" data-toggle="modal" data-target="#menu_1_1_edit" type="button">
            <span class="icon icon-lg icon-edit"></span>
        </button>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
<tr>
    <td>19094/013000502</td>
    <td>ข่ายเอ็น 0.09 SK 1.3*50*90 สีหู SK00,SK00 YOKO ปลา SHARK BRAND</td>
    <td>ข่ายเอ็น</td>
    <td>MONO</td>
    <td>0090</td>
    <td>0.09 mm</td>
    <td>SK</td>
    <td>YOKO</td>
    <td>SK00</td>
    <td></td>
    <td>SK YOKO สีหู SK00</td>
    <td>1.3</td>
    <td>1.30 cm</td>
    <td>50</td>
    <td>50 md</td>
    <td>85</td>
    <td>90</td>
    <td>SK00</td>
    <td>SK00</td>
    <td>0N</td>
    <td>ปลา SHARK BRAND</td>
    <td>10</td>
    <td>1</td>
    <td>1M</td>
    <td>M</td>
    <td>09</td>
    <td>SK</td>
    <td>Y</td>
    <td>SK00</td>
    <td>0130</td>
    <td>MD</td>
    <td>B</td>
    <td>050</td>
    <td>M</td>
    <td>B</td>
    <td>4</td>
    <td>0</td>
    <td>G</td>
    <td>A</td>
    <td>/</td>
    <td></td>
    <td>PC</td>
    <td>0</td>
    <td>0</td>
    <td>0</td>
    <td>200</td>
    <td>03</td>
    <td>B&amp;S</td>
    <td></td>
    <td>N</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>ITSARAP</td>
    <td>30/07/2012 15:39:42</td>
    <td>0</td>
    <td>0.09</td>
    <td>0</td>
    <td>0</td>
    <td>0</td>
    <td>0</td>
    <td>S</td>
    <td>405</td>
    <td>8854445303286</td>
    <td>ข่ายเอ็น 0.09 SK 1.3*50*90 สีหู SK00,SK00 YOKO ปลา SHARK BRAND</td>
    <td>38742</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>0</td>
    <td></td>
    <td>86.42</td>
    <td>91.14</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>MONO 0.09 SK 1.3*50*90 สีหู SK00,SK00 YOKO ปลา SHARK BRAND</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>0</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>S02Y</td>
    <td>A26</td>
    <td>88.78</td>
    <td>145.53</td>
    <td>131.51</td>
    <td>161.22</td>
    <td>145.58</td>
    <td>1639.22</td>
    <td>1481.3</td>
    <td>1815.95</td>
    <td>1639.78</td>
    <td>1630.55</td>
    <td>1472.85</td>
    <td>1807.16</td>
    <td>1631.34</td>
    <td>144.76</td>
    <td>130.76</td>
    <td>160.44</td>
    <td>144.83</td>
    <td>13.12</td>
    <td></td>
    <td></td>
    <td>Y</td>
    <td>25/02/2014 14:29:41</td>
    <td>5201164</td>
    <td>0</td>
    <td>0</td>
    <td>0</td>
    <td>0</td>
    <td>31.75</td>
    <td>88.78</td>
    <td>0</td>
    <td>W6</td>
    <td></td>
    <td></td>
    <td></td>
    <td>อวน SK</td>
    <td>1/</td>
    <td></td>
    <td>1</td>
    <td></td>
    <td></td>
    <td>300</td>
    <td></td>
    <td></td>
    <td>6/10/2549 16:15 น.</td>
    <td>USER001</td>
    <td>6/10/2549 16:15 น.</td>
    <td class="text-center" style="display: table-cell;">
        <button class="btn btn-lg btn-outline-primary btn-pill btn-xs" data-toggle="modal" data-target="#menu_1_1_edit" type="button">
            <span class="icon icon-lg icon-edit"></span>
        </button>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
<tr>
    <td>210962013000400</td>
    <td>ข่ายรุม 0.09 SK 1.3*40*75-6 รุม กลมล่าง 2 เส้น สูตร 3 กทม.
        SK00 YOKO เรือใบเงิน</td>
    <td>ข่ายรุม</td>
    <td>MONO</td>
    <td>0085</td>
    <td>0.09 mm</td>
    <td>SK</td>
    <td>YOKO</td>
    <td>สีหูเหมือนตัวอวน</td>
    <td></td>
    <td>SK YOKO เรือใบเงิน</td>
    <td>1.3</td>
    <td>1.30 cm</td>
    <td>40</td>
    <td>40 md</td>
    <td>30</td>
    <td>75-6</td>
    <td>SK00</td>
    <td>SK00</td>
    <td>74</td>
    <td>เรือใบเงิน</td>
    <td>06</td>
    <td>2</td>
    <td>2M</td>
    <td>M</td>
    <td>09</td>
    <td>SK</td>
    <td>Y</td>
    <td>SAME</td>
    <td>0130</td>
    <td>MD</td>
    <td>B</td>
    <td>040</td>
    <td>M</td>
    <td>B</td>
    <td>6</td>
    <td>0</td>
    <td>G</td>
    <td>A</td>
    <td>2</td>
    <td></td>
    <td>PC</td>
    <td>0</td>
    <td>0</td>
    <td>0</td>
    <td>150</td>
    <td>06</td>
    <td>KKF4</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>06</td>
    <td>5500975</td>
    <td>22/06/2013 14:17:45</td>
    <td>0</td>
    <td>0.09</td>
    <td>0</td>
    <td>0</td>
    <td>0</td>
    <td>0</td>
    <td>P</td>
    <td>131.17</td>
    <td>8854445047869</td>
    <td>ข่ายรุม 0.09 SK 1.3*40*75-6 รุม กลมล่าง 2 เส้น สูตร 3 กทม.
        SK00 YOKO เรือใบเงิน</td>
    <td>28405</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>0</td>
    <td>Y</td>
    <td>202</td>
    <td>203.33</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>RUM 0.09 SK 1.3*40*75-6 รุม กลมล่าง 2 เส้น สูตร 3 กทม. SK00 YOKO REI-BAI เงิน</td>
    <td>5103025</td>
    <td>09/02/2017 14:57:07</td>
    <td>Y</td>
    <td>5205677</td>
    <td>08/03/2017 13:03:06</td>
    <td>141</td>
    <td>00143</td>
    <td>146</td>
    <td>22/03/2010</td>
    <td></td>
    <td>Zศ</td>
    <td>9V</td>
    <td>C02Y</td>
    <td>A26</td>
    <td>186.33</td>
    <td>92.11</td>
    <td>82.47</td>
    <td>99.69</td>
    <td>88.96</td>
    <td>494.34</td>
    <td>442.6</td>
    <td>535.02</td>
    <td>477.43</td>
    <td>413.35</td>
    <td>373.53</td>
    <td>445.45</td>
    <td>401.12</td>
    <td>77.02</td>
    <td>69.6</td>
    <td>83</td>
    <td>74.74</td>
    <td>0</td>
    <td>866.67</td>
    <td>79.3</td>
    <td>Y</td>
    <td>09/07/2018 10:02:06</td>
    <td>5701241</td>
    <td>6</td>
    <td>7</td>
    <td>14.48</td>
    <td>158.83</td>
    <td>28.52</td>
    <td>27.5</td>
    <td></td>
    <td>W8</td>
    <td>120B0/013H0401P</td>
    <td>ข่ายเอ็น 0085 1.3*40*180 SKในYหูอวน สี SK00 (P)</td>
    <td>A</td>
    <td>สินค้าแปรรูป</td>
    <td>22</td>
    <td>รุม กลมล่าง 2 เส้น สูตร 3 กทม.</td>
    <td>21</td>
    <td>1</td>
    <td>รุม กลมล่าง 2 เส้น สูตร 2 กทม.</td>
    <td>150</td>
    <td></td>
    <td></td>
    <td>10/10/2560 16:28 น.</td>
    <td>USER001</td>
    <td>10/10/2560 16:28 น.</td>
    <td class="text-center" style="display: table-cell;">
        <button class="btn btn-lg btn-outline-primary btn-pill btn-xs" data-toggle="modal" data-target="#menu_1_1_edit" type="button">
            <span class="icon icon-lg icon-edit"></span>
        </button>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
<tr>
    <td>210922013000400</td>
    <td>ข่ายรุม 0.09 SK 1.3*40*EE รุม กลมล่าง 2 เส้น สูตร 3 กทม.SK00 YOKO เรือใบเงิน</td>
    <td>ข่ายรุม</td>
    <td>MONO</td>
    <td>0085</td>
    <td>0.09 mm</td>
    <td>SK</td>
    <td>YOKO</td>
    <td>สีหูเหมือนตัวอวน</td>
    <td></td>
    <td>SK YOKO เรือใบเงิน</td>
    <td>1.3</td>
    <td>1.30 cm</td>
    <td>40</td>
    <td>40 md</td>
    <td>36</td>
    <td>EE</td>
    <td>SK00</td>
    <td>SK00</td>
    <td>74</td>
    <td>เรือใบเงิน</td>
    <td>06</td>
    <td>2</td>
    <td>2M</td>
    <td>M</td>
    <td>09</td>
    <td>SK</td>
    <td>Y</td>
    <td>SAME</td>
    <td>0130</td>
    <td>MD</td>
    <td>B</td>
    <td>040</td>
    <td>M</td>
    <td>B</td>
    <td>2</td>
    <td>0</td>
    <td>G</td>
    <td>A</td>
    <td>2</td>
    <td>PC</td>
    <td>PC</td>
    <td>0</td>
    <td>0</td>
    <td>0</td>
    <td>150</td>
    <td>06</td>
    <td>KKF4</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>CONVERT RUM</td>
    <td>5203301</td>
    <td>10/04/2012 17:46:40</td>
    <td>0</td>
    <td>0.09</td>
    <td>0</td>
    <td>0</td>
    <td>33</td>
    <td>0</td>
    <td>P</td>
    <td>200</td>
    <td>8854445262347</td>
    <td>ข่ายรุม 0.09 SK 1.3*40*EE รุม กลมล่าง 2 เส้น สูตร 3 กทม.SK00 YOKO เรือใบเงิน</td>
    <td>34737</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>0</td>
    <td>N</td>
    <td>291</td>
    <td>295</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>RUM 0.09 SK 1.3*40*EE รุม กลมล่าง 2 เส้น สูตร 3 กทม. SK00 YOKO REI-BAI เงิน</td>
    <td>5103025</td>
    <td></td>
    <td>N</td>
    <td>PERAPONG</td>
    <td></td>
    <td>200</td>
    <td>00114</td>
    <td>200</td>
    <td></td>
    <td></td>
    <td>Zศ</td>
    <td>9V</td>
    <td>C02Y</td>
    <td>A26</td>
    <td>293</td>
    <td>124.43</td>
    <td>112.37</td>
    <td>135.97</td>
    <td>122.53</td>
    <td>592.52</td>
    <td>535.1</td>
    <td>647.48</td>
    <td>583.48</td>
    <td>458.86</td>
    <td>417.86</td>
    <td>498.57</td>
    <td>452.86</td>
    <td>96.36</td>
    <td>87.75</td>
    <td>104.7</td>
    <td>95.1</td>
    <td>0</td>
    <td>355.22</td>
    <td>73.98</td>
    <td>Y</td>
    <td>27/02/2017 14:42:13</td>
    <td>5701241</td>
    <td>0</td>
    <td>9</td>
    <td>16.92</td>
    <td>210</td>
    <td>26.52</td>
    <td>83</td>
    <td></td>
    <td>W8</td>
    <td>120B0/013H0401P</td>
    <td>ข่ายเอ็น 0085 1.3*40*180 SKในYหูอวน สี SK00 (P)</td>
    <td>L</td>
    <td>สินค้าแปรรูป</td>
    <td>22</td>
    <td>รุม กลมล่าง 2 เส้น สูตร 3 กทม.</td>
    <td>22</td>
    <td>2</td>
    <td>รุม กลมล่าง 2 เส้น สูตร 3 กทม.</td>
    <td>150</td>
    <td></td>
    <td></td>
    <td>9/01/2557 11:12 น.</td>
    <td>USER001</td>
    <td>9/01/2557 11:12 น.</td>
    <td class="text-center" style="display: table-cell;">
        <button class="btn btn-lg btn-outline-primary btn-pill btn-xs" data-toggle="modal" data-target="#menu_1_1_edit" type="button">
            <span class="icon icon-lg icon-edit"></span>
        </button>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
<tr>
    <td>5204D+030006002</td>
    <td>อวนปั๊ม 210/4 30/50*600*-- สีครีม P904 ไม่ชุบ มีกระดูก ป้ายเหลืองพิมพ์ 2*3 นิ้ว (ไม่ได้มาตรฐาน)</td>
    <td>อวนปั๊ม</td>
    <td>NYLON</td>
    <td>210/4</td>
    <td>210/4</td>
    <td>SK</td>
    <td>TATE</td>
    <td></td>
    <td></td>
    <td>210</td>
    <td>30</td>
    <td>30/50</td>
    <td>600</td>
    <td>600 md</td>
    <td>50</td>
    <td>--</td>
    <td>P904</td>
    <td>สีครีม P904</td>
    <td>22</td>
    <td>ป้ายเหลืองพิมพ์ 2*3 นิ้ว (ไม่ได้มาตรฐาน)</td>
    <td>10</td>
    <td>5</td>
    <td>5N</td>
    <td>N</td>
    <td>04</td>
    <td>SK</td>
    <td>T</td>
    <td></td>
    <td>0300</td>
    <td>MD</td>
    <td>B</td>
    <td>600</td>
    <td>M</td>
    <td>B</td>
    <td>D</td>
    <td>0</td>
    <td>G</td>
    <td>E</td>
    <td>+</td>
    <td></td>
    <td>PC</td>
    <td>0</td>
    <td>0</td>
    <td>0</td>
    <td>1</td>
    <td>15</td>
    <td>KKF1</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>PANUMAS</td>
    <td>04/01/2011 09:27:32</td>
    <td>4</td>
    <td>210</td>
    <td>0</td>
    <td>0</td>
    <td>0</td>
    <td>0</td>
    <td>N</td>
    <td>310</td>
    <td>8854445271776</td>
    <td>อวนปั๊ม 210/4 30/50*600*-- สีครีม P904 ไม่ชุบ มีกระดูก ป้ายเหลืองพิมพ์ 2*3 นิ้ว (ไม่ได้มาตรฐาน)</td>
    <td>35008</td>
    <td>2</td>
    <td>210</td>
    <td>04</td>
    <td>4</td>
    <td>0</td>
    <td>Y</td>
    <td>0</td>
    <td>0</td>
    <td></td>
    <td></td>
    <td></td>
    <td>0</td>
    <td></td>
    <td>1</td>
    <td>1</td>
    <td>1</td>
    <td>อวนปั๊ม 210/4 30/50*600*-- สีครีม P904 ไม่ชุบ มีกระดูก ป้ายเหลืองพิมพ์ 2*3 นิ้ว (ไม่ได้มาตรฐาน)</td>
    <td>5103025</td>
    <td>31/05/2017 17:27:19</td>
    <td>Y</td>
    <td>5205677</td>
    <td>31/05/2017 14:17:23</td>
    <td>0</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>A07</td>
    <td>0</td>
    <td>0</td>
    <td></td>
    <td></td>
    <td></td>
    <td>0</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>0</td>
    <td></td>
    <td></td>
    <td>0</td>
    <td>05</td>
    <td></td>
    <td></td>
    <td></td>
    <td>อวนปั๊ม</td>
    <td>5+</td>
    <td></td>
    <td>5</td>
    <td></td>
    <td></td>
    <td>1</td>
    <td></td>
    <td></td>
    <td>21/07/2549 14:43 น.</td>
    <td>USER001</td>
    <td>21/07/2549 14:43 น.</td>
    <td class="text-center" style="display: table-cell;">
        <button class="btn btn-lg btn-outline-primary btn-pill btn-xs" data-toggle="modal" data-target="#menu_1_1_edit" type="button">
            <span class="icon icon-lg icon-edit"></span>
        </button>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
<tr>
    <td>5304D+030006002</td>
    <td>อวนปั๊ม 210/4 30/50*600*-- สีครีม P904 ไม่ชุบ มีกระดูก ป้ายเรือใบทอง
    </td>
    <td>อวนปั๊ม</td>
    <td>NYLON</td>
    <td>210/4</td>
    <td>210/4</td>
    <td>SK</td>
    <td>TATE</td>
    <td></td>
    <td></td>
    <td>210</td>
    <td>30</td>
    <td>30/50</td>
    <td>600</td>
    <td>600 md</td>
    <td>50</td>
    <td>--</td>
    <td>P904</td>
    <td>สีครีม P904</td>
    <td>72</td>
    <td>ป้ายเรือใบทอง</td>
    <td>08</td>
    <td>5</td>
    <td>5N</td>
    <td>N</td>
    <td>04</td>
    <td>SK</td>
    <td>T</td>
    <td></td>
    <td>0300</td>
    <td>MD</td>
    <td>B</td>
    <td>600</td>
    <td>M</td>
    <td>B</td>
    <td>D</td>
    <td>0</td>
    <td>G</td>
    <td>E</td>
    <td>+</td>
    <td></td>
    <td>PC</td>
    <td>0</td>
    <td>0</td>
    <td>0</td>
    <td>1</td>
    <td>15</td>
    <td>KKF1</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>15</td>
    <td>2800002</td>
    <td>08/12/2014 15:46:04</td>
    <td>4</td>
    <td>210</td>
    <td>0</td>
    <td>0</td>
    <td>0</td>
    <td>0</td>
    <td>P</td>
    <td>310</td>
    <td>8854445221719</td>
    <td>อวนปั๊ม 210/4 30/50*600*-- สีครีม P904 ไม่ชุบ มีกระดูก ป้ายเรือใบทอง
    </td>
    <td>27817</td>
    <td>3</td>
    <td>210</td>
    <td>04</td>
    <td>4</td>
    <td>46</td>
    <td>Y</td>
    <td>0</td>
    <td>0</td>
    <td></td>
    <td></td>
    <td></td>
    <td>50</td>
    <td></td>
    <td>1</td>
    <td>1</td>
    <td>1</td>
    <td>อวนปั๊ม 210/4 30/50*600*-- สีครีม P904 ไม่ชุบ มีกระดูก ป้ายREI-BAIทอง
    </td>
    <td>5103025</td>
    <td>31/05/2017 17:27:19</td>
    <td>Y</td>
    <td>5205677</td>
    <td>31/05/2017 14:17:24</td>
    <td>235</td>
    <td>00114</td>
    <td>235</td>
    <td></td>
    <td></td>
    <td>Zฤ</td>
    <td>55</td>
    <td>D08T</td>
    <td>A07</td>
    <td>10320</td>
    <td>2003.13</td>
    <td>1808.8</td>
    <td>2085.16</td>
    <td>1867.13</td>
    <td>194.1</td>
    <td>175.27</td>
    <td>202.05</td>
    <td>180.92</td>
    <td>192.42</td>
    <td>173.59</td>
    <td>200.37</td>
    <td>179.24</td>
    <td>1985.78</td>
    <td>1791.45</td>
    <td>2067.81</td>
    <td>1849.78</td>
    <td>10.56</td>
    <td>0</td>
    <td>0</td>
    <td>Y</td>
    <td>25/02/2014 14:39:48</td>
    <td>5201164</td>
    <td>0</td>
    <td>0</td>
    <td>0</td>
    <td>0</td>
    <td>158.82</td>
    <td>10320</td>
    <td></td>
    <td>05</td>
    <td></td>
    <td></td>
    <td>L</td>
    <td>อวนปั๊ม</td>
    <td>5+</td>
    <td></td>
    <td>5</td>
    <td></td>
    <td></td>
    <td>1</td>
    <td></td>
    <td></td>
    <td>22/09/2552 15:30 น.</td>
    <td>USER001</td>
    <td>22/09/2552 15:30 น.</td>
    <td class="text-center" style="display: table-cell;">
        <button class="btn btn-lg btn-outline-primary btn-pill btn-xs" data-toggle="modal" data-target="#menu_1_1_edit" type="button">
            <span class="icon icon-lg icon-edit"></span>
        </button>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
<tr>
    <td>5304D-030006002</td>
    <td>อวนปั๊ม 210/4 30/50*600*-- สีครีม P904 ชุบปกติ มีกระดูก ป้ายเรือใบทอง
    </td>
    <td>อวนปั๊ม</td>
    <td>NYLON</td>
    <td>210/4</td>
    <td>210/4</td>
    <td>SK</td>
    <td>TATE</td>
    <td></td>
    <td></td>
    <td>210</td>
    <td>30</td>
    <td>30/50</td>
    <td>600</td>
    <td>600 md</td>
    <td>50</td>
    <td>--</td>
    <td>P904</td>
    <td>สีครีม P904</td>
    <td>72</td>
    <td>ป้ายเรือใบทอง</td>
    <td>08</td>
    <td>5</td>
    <td>5N</td>
    <td>N</td>
    <td>04</td>
    <td>SK</td>
    <td>T</td>
    <td></td>
    <td>0300</td>
    <td>MD</td>
    <td>B</td>
    <td>600</td>
    <td>M</td>
    <td>B</td>
    <td>D</td>
    <td>0</td>
    <td>G</td>
    <td>E</td>
    <td>-</td>
    <td></td>
    <td>PC</td>
    <td>0</td>
    <td>0</td>
    <td>0</td>
    <td>1</td>
    <td>15</td>
    <td>KKF1</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>15</td>
    <td>2800002</td>
    <td>08/12/2014 15:46:39</td>
    <td>4</td>
    <td>210</td>
    <td>0</td>
    <td>0</td>
    <td>0</td>
    <td>0</td>
    <td>P</td>
    <td>310</td>
    <td>8854445851121</td>
    <td>อวนปั๊ม 210/4 30/50*600*-- สีครีม P904 ชุบปกติ มีกระดูก ป้ายเรือใบทอง
    </td>
    <td>27899</td>
    <td>3</td>
    <td>210</td>
    <td>04</td>
    <td>4</td>
    <td>46</td>
    <td>Y</td>
    <td>0</td>
    <td>0</td>
    <td></td>
    <td></td>
    <td></td>
    <td>50</td>
    <td></td>
    <td>0</td>
    <td>1</td>
    <td>1</td>
    <td>อวนปั๊ม 210/4 30/50*600*-- สีครีม P904 ชุบปกติ มีกระดูก ป้ายREI-BAIทอง
    </td>
    <td>5103025</td>
    <td>31/05/2017 17:27:19</td>
    <td>Y</td>
    <td>5205677</td>
    <td>31/05/2017 14:17:25</td>
    <td>235</td>
    <td>00114</td>
    <td>200</td>
    <td>28/02/2010</td>
    <td></td>
    <td>Zฤ</td>
    <td>55</td>
    <td>D08T</td>
    <td>A07</td>
    <td>18085.94</td>
    <td>3190.73</td>
    <td>2832.51</td>
    <td>3344.23</td>
    <td>2942.31</td>
    <td>176.42</td>
    <td>156.61</td>
    <td>184.91</td>
    <td>162.68</td>
    <td>176.06</td>
    <td>156.26</td>
    <td>184.55</td>
    <td>162.33</td>
    <td>3184.24</td>
    <td>2826.02</td>
    <td>3337.74</td>
    <td>2935.82</td>
    <td>6.14</td>
    <td>150</td>
    <td>150</td>
    <td>Y</td>
    <td>09/12/2015 16:24:45</td>
    <td>5101328</td>
    <td>0</td>
    <td>0</td>
    <td>0</td>
    <td>0</td>
    <td>298.08</td>
    <td>18085.94</td>
    <td></td>
    <td>05</td>
    <td></td>
    <td></td>
    <td>A</td>
    <td>อวนปั๊ม</td>
    <td>5-</td>
    <td></td>
    <td>5</td>
    <td></td>
    <td></td>
    <td>1</td>
    <td></td>
    <td></td>
    <td>5/04/2560 14:45 น.</td>
    <td>USER001</td>
    <td>5/04/2560 14:45 น.</td>
    <td class="text-center" style="display: table-cell;">
        <button class="btn btn-lg btn-outline-primary btn-pill btn-xs" data-toggle="modal" data-target="#menu_1_1_edit" type="button">
            <span class="icon icon-lg icon-edit"></span>
        </button>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
<tr>
    <td>5004D+030006002</td>
    <td>อวนปั๊ม 210/4 30/50*600*-- สีครีม P904 ไม่ชุบ มีกระดูก ป้ายม้าน้ำเดี่ยว
    </td>
    <td>อวนปั๊ม</td>
    <td>NYLON</td>
    <td>210/4</td>
    <td>210/4</td>
    <td>SK</td>
    <td>TATE</td>
    <td></td>
    <td></td>
    <td>210</td>
    <td>30</td>
    <td>30/50</td>
    <td>600</td>
    <td>600 md</td>
    <td>50</td>
    <td>--</td>
    <td>P904</td>
    <td>สีครีม P904</td>
    <td>AX</td>
    <td>ป้ายม้าน้ำเดี่ยว</td>
    <td>10</td>
    <td>5</td>
    <td>5N</td>
    <td>N</td>
    <td>04</td>
    <td>SK</td>
    <td>T</td>
    <td></td>
    <td>0300</td>
    <td>MD</td>
    <td>B</td>
    <td>600</td>
    <td>M</td>
    <td>B</td>
    <td>D</td>
    <td>0</td>
    <td>G</td>
    <td>E</td>
    <td>+</td>
    <td></td>
    <td>PC</td>
    <td>0</td>
    <td>0</td>
    <td>0</td>
    <td>1</td>
    <td>15</td>
    <td>KKF1</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>PANUMAS</td>
    <td>16/08/2010 13:07:24</td>
    <td>4</td>
    <td>210</td>
    <td>0</td>
    <td>0</td>
    <td>0</td>
    <td>0</td>
    <td>N</td>
    <td>310</td>
    <td>8854445887113</td>
    <td>อวนปั๊ม 210/4 30/50*600*-- สีครีม P904 ไม่ชุบ มีกระดูก ป้ายม้าน้ำเดี่ยว
    </td>
    <td>30121</td>
    <td>0</td>
    <td>210</td>
    <td>04</td>
    <td>4</td>
    <td>46</td>
    <td>Y</td>
    <td>0</td>
    <td>0</td>
    <td></td>
    <td></td>
    <td></td>
    <td>0</td>
    <td></td>
    <td>1</td>
    <td>1</td>
    <td>1</td>
    <td>อวนปั๊ม 210/4 30/50*600*-- สีครีม P904 ไม่ชุบ มีกระดูก ป้ายม้าน้ำเดี่ยว
    </td>
    <td>5103025</td>
    <td>31/05/2017 17:27:20</td>
    <td>Y</td>
    <td>5205677</td>
    <td>31/05/2017 14:17:25</td>
    <td>0</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>A07</td>
    <td>0</td>
    <td>0</td>
    <td></td>
    <td></td>
    <td></td>
    <td>0</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>0</td>
    <td></td>
    <td></td>
    <td>0</td>
    <td>05</td>
    <td></td>
    <td></td>
    <td></td>
    <td>อวนปั๊ม</td>
    <td>5+</td>
    <td></td>
    <td>5</td>
    <td></td>
    <td></td>
    <td>1</td>
    <td></td>
    <td></td>
    <td>21/07/2549 14:45 น.</td>
    <td>USER001</td>
    <td>21/07/2549 14:45 น.</td>
    <td class="text-center" style="display: table-cell;">
        <button class="btn btn-lg btn-outline-primary btn-pill btn-xs" data-toggle="modal" data-target="#menu_1_1_edit" type="button">
            <span class="icon icon-lg icon-edit"></span>
        </button>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
<tr>
    <td>718312AX</td>
    <td>ด้ายโปลี 380/12 รุ่น 20 ไจ/1 กก. สีเขียวขี้ม้า L540 ป้ายม้าน้ำเดี่ยว
    </td>
    <td>ด้ายโปลี</td>
    <td>POLY</td>
    <td>380/12</td>
    <td>380/12</td>
    <td></td>
    <td></td>
    <td></td>
    <td>29</td>
    <td>รุ่น 20 ไจ/1 กก.</td>
    <td>0</td>
    <td></td>
    <td>0</td>
    <td></td>
    <td>0</td>
    <td></td>
    <td>L540</td>
    <td>สีเขียวขี้ม้า L540</td>
    <td>AX</td>
    <td>ป้ายม้าน้ำเดี่ยว</td>
    <td>12</td>
    <td>7</td>
    <td>7L</td>
    <td>L</td>
    <td>312</td>
    <td>SK</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>KG</td>
    <td>0</td>
    <td>0</td>
    <td>0</td>
    <td>0</td>
    <td>02</td>
    <td>FM</td>
    <td></td>
    <td>Y</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>5800226</td>
    <td>27/01/2018 14:15:11</td>
    <td>0</td>
    <td>0</td>
    <td>0</td>
    <td>0</td>
    <td>0</td>
    <td>0</td>
    <td>N</td>
    <td>103</td>
    <td>8854445308212</td>
    <td>ด้ายโปลี 380/12 รุ่น 20 ไจ/1 กก. สีเขียวขี้ม้า L540 ป้ายม้าน้ำเดี่ยว
    </td>
    <td>88839</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>Y</td>
    <td>0</td>
    <td>0</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>DPOLY 380/12 รุ่น 20 ไจ/1 กก. สีเขียวขี้ม้า L540 ป้ายม้าน้ำเดี่ยว
    </td>
    <td>5103025</td>
    <td>24/02/2018 16:36:52</td>
    <td>Y</td>
    <td>6000311</td>
    <td>24/02/2018 16:41:58</td>
    <td>0</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>1000</td>
    <td>77.75</td>
    <td></td>
    <td>79.3</td>
    <td></td>
    <td>77.75</td>
    <td></td>
    <td>79.3</td>
    <td></td>
    <td>77.75</td>
    <td></td>
    <td>79.3</td>
    <td></td>
    <td>77.75</td>
    <td></td>
    <td>79.3</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>Y</td>
    <td>30/06/2018 10:17:43</td>
    <td>4500536</td>
    <td></td>
    <td></td>
    <td></td>
    <td>0</td>
    <td></td>
    <td></td>
    <td>0</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>ด้ายโปลี</td>
    <td></td>
    <td></td>
    <td>7</td>
    <td></td>
    <td></td>
    <td>40</td>
    <td></td>
    <td></td>
    <td>28/02/2555 17:03 น.</td>
    <td>USER001</td>
    <td>28/02/2555 17:03 น.</td>
    <td class="text-center" style="display: table-cell;">
        <button class="btn btn-lg btn-outline-primary btn-pill btn-xs" data-toggle="modal" data-target="#menu_1_1_edit" type="button">
            <span class="icon icon-lg icon-edit"></span>
        </button>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
<tr>
    <td>731312AX</td>
    <td>ด้ายโปลี 380/12 รุ่น 0.95 กก./20 ไจ เขียว L502 ป้ายม้าน้ำเดี่ยว
    </td>
    <td>ด้ายโปลี</td>
    <td>POLY</td>
    <td>380/12</td>
    <td>380/12</td>
    <td></td>
    <td></td>
    <td></td>
    <td>28</td>
    <td>รุ่น 0.95 กก./20 ไจ</td>
    <td>0</td>
    <td></td>
    <td>0</td>
    <td></td>
    <td>0</td>
    <td></td>
    <td>L502</td>
    <td>เขียว L502</td>
    <td>AX</td>
    <td>ป้ายม้าน้ำเดี่ยว</td>
    <td>12</td>
    <td>7</td>
    <td>7L</td>
    <td>L</td>
    <td>312</td>
    <td>SK</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>KG</td>
    <td>0</td>
    <td>0</td>
    <td>0</td>
    <td>0</td>
    <td>02</td>
    <td>FM</td>
    <td></td>
    <td>Y</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>5800226</td>
    <td>20/10/2017 11:42:05</td>
    <td>0</td>
    <td>0</td>
    <td>0</td>
    <td>0</td>
    <td>0</td>
    <td>0</td>
    <td>N</td>
    <td>104</td>
    <td>8854445285988</td>
    <td>ด้ายโปลี 380/12 รุ่น 0.95 กก./20 ไจ เขียว L502 ป้ายม้าน้ำเดี่ยว
    </td>
    <td>87070</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>0</td>
    <td>0</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>DPOLY 380/12 รุ่น 0.95 กก./20 ไจ เขียว L502 ป้ายม้าน้ำเดี่ยว</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>0</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>950</td>
    <td>73.86</td>
    <td></td>
    <td>75.34</td>
    <td></td>
    <td>77.75</td>
    <td></td>
    <td>79.3</td>
    <td></td>
    <td>77.75</td>
    <td></td>
    <td>79.3</td>
    <td></td>
    <td>73.86</td>
    <td></td>
    <td>75.34</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>Y</td>
    <td>30/06/2018 10:17:43</td>
    <td>4500536</td>
    <td></td>
    <td></td>
    <td></td>
    <td>0</td>
    <td></td>
    <td></td>
    <td>0</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>ด้ายโปลี</td>
    <td></td>
    <td></td>
    <td>7</td>
    <td></td>
    <td></td>
    <td>40</td>
    <td></td>
    <td></td>
    <td>21/07/2549 14:43 น.</td>
    <td>USER001</td>
    <td>21/07/2549 14:43 น.</td>
    <td class="text-center" style="display: table-cell;">
        <button class="btn btn-lg btn-outline-primary btn-pill btn-xs" data-toggle="modal" data-target="#menu_1_1_edit" type="button">
            <span class="icon icon-lg icon-edit"></span>
        </button>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
<tr>
    <td>736312AX</td>
    <td>ด้ายโปลี 380/12 รุ่น 0.95 กก./20 ไจ สีดำ L801 ป้ายม้าน้ำเดี่ยว</td>
    <td>ด้ายโปลี</td>
    <td>POLY</td>
    <td>380/12</td>
    <td>380/12</td>
    <td></td>
    <td></td>
    <td></td>
    <td>28</td>
    <td>รุ่น 0.95 กก./20 ไจ</td>
    <td>0</td>
    <td></td>
    <td>0</td>
    <td></td>
    <td>0</td>
    <td></td>
    <td>L801</td>
    <td>สีดำ L801</td>
    <td>AX</td>
    <td>ป้ายม้าน้ำเดี่ยว</td>
    <td>12</td>
    <td>7</td>
    <td>7L</td>
    <td>L</td>
    <td>312</td>
    <td>SK</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>KG</td>
    <td>0</td>
    <td>0</td>
    <td>0</td>
    <td>0</td>
    <td>02</td>
    <td>FM</td>
    <td></td>
    <td>Y</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>5800226</td>
    <td>20/10/2017 11:36:15</td>
    <td>0</td>
    <td>0</td>
    <td>0</td>
    <td>0</td>
    <td>0</td>
    <td>0</td>
    <td>N</td>
    <td>104</td>
    <td>8854445285834</td>
    <td>ด้ายโปลี 380/12 รุ่น 0.95 กก./20 ไจ สีดำ L801 ป้ายม้าน้ำเดี่ยว</td>
    <td>87069</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>0</td>
    <td>0</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>DPOLY 380/12 รุ่น 0.95 กก./20 ไจ สีดำ L801 ป้ายม้าน้ำเดี่ยว</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>0</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>950</td>
    <td>67.34</td>
    <td></td>
    <td>68.69</td>
    <td></td>
    <td>70.88</td>
    <td></td>
    <td>72.3</td>
    <td></td>
    <td>70.88</td>
    <td></td>
    <td>72.3</td>
    <td></td>
    <td>67.34</td>
    <td></td>
    <td>68.69</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>Y</td>
    <td>30/06/2018 10:17:43</td>
    <td>4500536</td>
    <td></td>
    <td></td>
    <td></td>
    <td>0</td>
    <td></td>
    <td></td>
    <td>0</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>ด้ายโปลี</td>
    <td></td>
    <td></td>
    <td>7</td>
    <td></td>
    <td></td>
    <td>40</td>
    <td></td>
    <td></td>
    <td>8/09/2552 20:50 น.</td>
    <td>USER001</td>
    <td>8/09/2552 20:50 น.</td>
    <td class="text-center" style="display: table-cell;">
        <button class="btn btn-lg btn-outline-primary btn-pill btn-xs" data-toggle="modal" data-target="#menu_1_1_edit" type="button">
            <span class="icon icon-lg icon-edit"></span>
        </button>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
<tr>
    <td>7403129C</td>
    <td>ด้ายโปลี 380/12 รุ่น 0.9 กก./หลอด สีแดงสดเข้มL005 ป้ายเรือใบ</td>
    <td>ด้ายโปลี</td>
    <td>POLY</td>
    <td>380/12</td>
    <td>380/12</td>
    <td>SK</td>
    <td></td>
    <td></td>
    <td>25</td>
    <td>รุ่น 0.9 กก./หลอด</td>
    <td>0</td>
    <td></td>
    <td>0</td>
    <td></td>
    <td>0</td>
    <td></td>
    <td>L005</td>
    <td>L005</td>
    <td>9C</td>
    <td>ป้ายเรือใบ</td>
    <td>12</td>
    <td>7</td>
    <td>7L</td>
    <td>L</td>
    <td>312</td>
    <td>SK</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>KG</td>
    <td>KG</td>
    <td>0</td>
    <td>900</td>
    <td>0</td>
    <td>50</td>
    <td>02</td>
    <td>FM</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>02</td>
    <td>SOMJAI</td>
    <td>27/11/2009 15:17:18</td>
    <td>12</td>
    <td>380</td>
    <td>0</td>
    <td>0</td>
    <td>0</td>
    <td>0</td>
    <td>P</td>
    <td>130</td>
    <td>8854445943109</td>
    <td>ด้ายโปลี 380/12 รุ่น 0.9 กก./หลอด สีแดงสดเข้มL005 ป้ายเรือใบ</td>
    <td>16166</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>Y</td>
    <td>855</td>
    <td>945</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>DPOLY 380/12 รุ่น 0.9 กก./หลอด สีแดงสดเข้มL005 ป้าย REI-BAI</td>
    <td>5103025</td>
    <td>08/07/2009 15:59:31</td>
    <td></td>
    <td></td>
    <td></td>
    <td>0</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>Zศ</td>
    <td>9V</td>
    <td></td>
    <td></td>
    <td>900</td>
    <td>73.09</td>
    <td></td>
    <td>74.55</td>
    <td></td>
    <td>81.21</td>
    <td></td>
    <td>82.83</td>
    <td></td>
    <td>81.21</td>
    <td></td>
    <td>82.83</td>
    <td></td>
    <td>73.09</td>
    <td></td>
    <td>74.55</td>
    <td></td>
    <td></td>
    <td>84.15</td>
    <td>75.74</td>
    <td>Y</td>
    <td>30/06/2018 10:17:49</td>
    <td>4500536</td>
    <td></td>
    <td></td>
    <td></td>
    <td>0</td>
    <td></td>
    <td></td>
    <td>5</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>ด้ายโปลี</td>
    <td>7</td>
    <td></td>
    <td>7</td>
    <td></td>
    <td></td>
    <td>25</td>
    <td></td>
    <td></td>
    <td>6/10/2549 16:15 น.</td>
    <td>USER001</td>
    <td>6/10/2549 16:15 น.</td>
    <td class="text-center" style="display: table-cell;">
        <button class="btn btn-lg btn-outline-primary btn-pill btn-xs" data-toggle="modal" data-target="#menu_1_1_edit" type="button">
            <span class="icon icon-lg icon-edit"></span>
        </button>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
            </div>
        </div>










    </div>
</div>
<!--  -->
<? include('inc.footer.php');?>
<!--  -->
<? include('inc.footer.script.php');?>
<!--  -->
