<?php
    $select = 'open_menu_1';
    $select2 = 'menu_1';
    $select3 = 'menu_1';
    $select4 = 'menu_1_1';
    $select5 = 'menu_all';
?>
<!--  -->
<? include('inc.header.php');?>
<!--  -->
<? include('inc.navbar.php');?>
<!--  -->
<? include('inc.menu.php');?>
<!-- <div class="layout-main"> -->
<!--  -->
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <h4 class="m-t-0">
                <!-- <span class="d-ib text-danger">--</span> <span class="icon icon-angle-double-right"></span> -->
                <!-- <span class="d-ib">ข้อมูลหลัก</span> -->
                <span class="d-ib text-primary">การตั้งค่า</span> <span class="icon icon-angle-double-right"></span> สาขา/โรงงาน
                <!-- <span class="d-ib">
                    <a class="title-bar-shortcut" href="#" title="Add to shortcut list" data-container="body" data-toggle-text="Remove from shortcut list" data-trigger="hover" data-placement="right" data-toggle="tooltip">
                        <span class="sr-only">Add to shortcut list</span>
                    </a>
                </span> -->
            </h4>
            <!-- <p class="title-bar-description">
                <small>Latest update on 01/01/2017 - 10.57 am.</small>
            </p> -->
        </div>

        <div class="row gutter-xs">
            <div class="col-xs-12">
                <!--  -->
                <div class="card">
                    <div class="card-body">
                        <!--  -->
                        <div class="at_add_box">
                            <div class="row">
                                <div class="col-sm-12 text-right">
                                    <button class="btn btn-success" data-toggle="modal" data-target="#menu_1_1_add" type="button"><span class="icon icon-lg icon-plus"></span>&nbsp;&nbsp;เพิ่มใหม่</button>
                                    <? if ($select5 == 'menu_all'){?>
                                        <!-- <a href="menu_1_1_all.php" class="btn btn-info"><span class="icon icon-lg icon-table"></span>&nbsp;&nbsp;แสดงทั้งหมด</a> -->
                                    <? }else{?>
                                        <!-- <a href="menu_1_1.php" class="btn btn-info"><span class="icon icon-lg icon-table"></span>&nbsp;&nbsp;มุมมองปกติ</a> -->
                                    <? } ?>
                                    <a href="menu_1_1_history.php" class="btn btn-info"><span class="icon icon-lg icon-history"></span>&nbsp;&nbsp;ประวัติการแก้ไข</a>
                                    <button onClick="window.print()" class="btn btn-default" type="button"><span class="icon icon-lg icon-print"></span>&nbsp;&nbsp;พิมพ์</button>
                                    <a href="images/SampleXLSFile_212kb.xls" class="btn btn-warning" type="button"><span class="icon icon-lg icon-file-text-o"></span>&nbsp;&nbsp;ส่งออกไฟล์</a>
                                    <!-- <a href="javascript:window.history.back(-1);" class="btn btn-info"><span class="icon icon-long-arrow-left"></span>&nbsp;&nbsp;ย้อนกลับ</a> -->
                                </div>
                            </div>
                        </div>
                        <!--  -->
                        <div class="table-responsive">
                            <table id="demo-datatables-2" class="table table-hover table-bordered table-nowrap dataTable text-center no-footer" cellspacing="0" width="100%">
                                <thead>
                                    <tr class="at_bg_table_blue">
                                        <th class="text-center">รหัสสาขา</th>
                                        <th class="text-center">ชื่อสาขา</th>
                                        <th class="text-center">รหัสโรงทอ</th>
                                        <th class="text-center">ชื่อโรงทอ</th>
                                        <th class="text-center">วันที่เริ่มใช้งาน</th>
                                        <th class="text-center">ผู้บันทึก</th>
                                        <th class="text-center">สาขารูดบัตร</th>
                                    	<th class="text-center">ปรับแก้ไข</th>
                                    </tr>
                                </thead>
                                <tbody>

<tr>
    <td colspan="2">B&amp;S</td>
    <td class="text-left">สารคาม</td>
    <td>M <br>S</td>
    <td class="text-left">โรงทอ  M<br>โรงทอ S</td>
    <td>24/08/2561<br>24/08/2561</td>
    <td>USER_ID <br>USER_ID</td>
    <td>M</td>
    <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_1_1.php" type="button">
	 <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
<tr>
    <td>BWC</td>
    <td class="text-left">ศูนย์บวร</td>
    <td>C</td>
    <td class="text-left">โรงทอ  C</td>
    <td>24/08/2561</td>
    <td>USER_ID</td>
    <td>B</td>
    <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_1_1.php" type="button">
	 <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
<tr>
    <td>CY</td>
    <td class="text-left">เชียงยืน</td>
    <td>T<br>V</td>
    <td class="text-left">โรงทอ  T<br>โรงทอ V</td>
    <td>24/08/2561<br>24/08/2561</td>
    <td>USER_ID <br>USER_ID</td>
    <td>C</td>
    <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_1_1.php" type="button">
	 <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

<tr>
    <td>EXT</td>
    <td class="text-left">ภายนอก</td>
    <td>C</td>
    <td class="text-left">โรงทอ  C</td>
    <td>24/08/2561</td>
    <td>USER_ID</td>
    <td>E</td>
    <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_1_1.php" type="button">
	 <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

<tr>
    <td>FM</td>
    <td class="text-left">FM ด้ายโพลี</td>
    <td>F</td>
    <td class="text-left">โรงทอ F</td>
    <td>24/08/2561</td>
    <td>USER_ID</td>
    <td>F</td>
    <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_1_1.php" type="button">
	 <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

<tr>
    <td>FM1</td>
    <td class="text-left">FM อวนโพลี</td>
    <td>L  </td>
    <td class="text-left">โรงทอ L</td>
    <td>24/08/2561</td>
    <td>USER_ID</td>
    <td>F</td>
    <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_1_1.php" type="button">
	 <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
<tr>
    <td>KFI</td>
    <td class="text-left">KFI</td>
    <td>L</td>
    <td class="text-left">โรงทอ L</td>
    <td>24/08/2561</td>
    <td>USER_ID</td>
    <td>N</td>
    <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_1_1.php" type="button">
	 <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

<tr>
    <td>KKF</td>
    <td class="text-left">เค เค เอฟ</td>
    <td>E</td>
    <td class="text-left">โรงทอ E</td>
    <td>24/08/2561</td>
    <td>USER_ID</td>
    <td>K</td>
    <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_1_1.php" type="button">
	 <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

<tr>
    <td>KKF1</td>
    <td class="text-left">อวนปั๊ม</td>
    <td>P</td>
    <td class="text-left">อวนปั๊ม</td>
    <td>24/08/2561</td>
    <td>USER_ID</td>
    <td>N</td>
    <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_1_1.php" type="button">
	 <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

<tr>
    <td>KKF2</td>
    <td class="text-left">สายเอ็น/ด้ายไนล่อน</td>
    <td>N</td>
    <td class="text-left">โรงทอด้าย N</td>
    <td>24/08/2561</td>
    <td>USER_ID</td>
    <td></td>
    <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_1_1.php" type="button">
	 <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

<tr>
    <td>KKF4</td>
    <td class="text-left">อวนรุม/อวนแปรรูป</td>
    <td>N</td>
    <td class="text-left">โรงทอด้าย N</td>
    <td>24/08/2561</td>
    <td>USER_ID</td>
    <td></td>
    <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_1_1.php" type="button">
	 <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

<tr>
    <td>KKF5</td>
    <td class="text-left">ผลิตอุปกรณ์ฉีด-ปั๊ม</td>
    <td>N</td>
    <td class="text-left">โรงทอด้าย N</td>
    <td>24/08/2561</td>
    <td>USER_ID</td>
    <td></td>
    <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_1_1.php" type="button">
	 <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

<tr>
    <td>KR</td>
    <td class="text-left">เคอาร์</td>
    <td>N</td>
    <td class="text-left">โรงทอด้าย N</td>
    <td>24/08/2561</td>
    <td>USER_ID</td>
    <td>R</td>
    <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_1_1.php" type="button">
	 <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

<tr>
    <td>MK</td>
    <td class="text-left">ตลาด</td>
    <td>N</td>
    <td class="text-left">โรงทอด้าย N</td>
    <td>24/08/2561</td>
    <td>USER_ID</td>
    <td>K</td>
    <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_1_1.php" type="button">
	 <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

<tr>
    <td>NR</td>
    <td class="text-left">หนองเรือ</td>
    <td>A</td>
    <td class="text-left">โรงทอด้าย A</td>
    <td>24/08/2561</td>
    <td>USER_ID</td>
    <td>N</td>
    <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_1_1.php" type="button">
	 <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>


<tr>
    <td>XFF</td>
    <td class="text-left">สาขาเมืองจีน</td>
    <td>X</td>
    <td class="text-left">โรงทอ สาขา เมืองจีน</td>
    <td>24/08/2561</td>
    <td>USER_ID</td>
    <td>X</td>
    <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_1_1.php" type="button">
	 <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>








                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
            </div>
        </div>










    </div>
</div>
<!--  -->
<? include('inc.footer.php');?>
<!--  -->
<? include('inc.footer.script.php');?>
<!--  -->
