<?php
    $select = 'open_menu_1';
    $select2 = 'menu_6';
    $select3 = 'menu_6';
    $select4 = 'menu_6_6';
	
?>
<!--  -->
<? include('inc.header.php');?>
<!--  -->
<? include('inc.navbar.php');?>
<!--  -->
<? include('inc.menu.php');?>
<!-- <div class="layout-main"> -->
<!--  -->
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <h4 class="m-t-0">
                <!-- <span class="d-ib text-danger">--</span> <span class="icon icon-angle-double-right"></span> -->
                <!-- <span class="d-ib">ข้อมูลหลัก</span> -->
                <span class="d-ib text-primary">การตั้งค่า</span> <span class="icon icon-angle-double-right"></span> ซ่อมบำรุง <span class="icon icon-angle-double-right"></span> กิจกรรมงานซ่อมบำรุง
                <!-- <span class="d-ib">
                    <a class="title-bar-shortcut" href="#" title="Add to shortcut list" data-container="body" data-toggle-text="Remove from shortcut list" data-trigger="hover" data-placement="right" data-toggle="tooltip">
                        <span class="sr-only">Add to shortcut list</span>
                    </a>
                </span> -->
            </h4>
            <!-- <p class="title-bar-description">
                <small>Latest update on 01/01/2017 - 10.57 am.</small>
            </p> -->
        </div>

        <div class="row gutter-xs">
            <div class="col-xs-12">
                <!--  -->
                <div class="card">
                    <!-- <div class="card-header">
                        <a href="javascript:window.history.back(-1);" class="btn btn-sm btn-outline-primary"><span class="icon icon-long-arrow-left"></span>&nbsp;&nbsp;Back</a>&nbsp;&nbsp;
                        <strong class="">Latest update on 01/01/2017 - 10.57 am.</strong>
                    </div> -->
                    <div class="card-body">
                        <!--  -->
                        <? include('inc.home_top_filter.php');?>
                        <!--  -->
                        <div class="table-responsive">
                            <table id="demo-datatables-2" class="table table-hover table-bordered table-nowrap dataTable text-center no-footer" cellspacing="0" width="100%">
                                <thead>
                                    <tr class="at_bg_table_blue">
                                        <th class="text-center">รหัสกิจกรรมงาน</th>
                                        <th class="text-center">กิจกรรมงาน</th>
                                        <th class="text-center">รหัสกลุ่มเครื่องทอ</th>
                                        <th class="text-center">ระยะเวลา</th>
                                        <th class="text-center">หน่วย</th>
                                        <th class="text-center">กำลังคนมาตรฐาน</th>
                                        <th class="text-center">ต้องการจอดเครื่อง</th>
                                        <th class="text-center">ปรับแก้ไข</th>
                                    </tr>
                                </thead>
                                 <tbody>
<tr height=19 style='height:14.4pt'>
  <td height=19 class=xl65 width=103 style='height:14.4pt;width:77pt'>A01</td>
  <td class="text-left" width=289 style='border-left:none;width:217pt'>&#3585;&#3634;&#3619;&#3648;&#3611;&#3621;&#3637;&#3656;&#3618;&#3609;&#3629;&#3640;&#3611;&#3585;&#3619;&#3603;&#3660;&#3619;&#3629;&#3610;
  1 &#3648;&#3604;&#3639;&#3629;&#3609;</td>
  <td class=xl66 width=114 style='border-left:none;width:86pt'>CN<span
  style='mso-spacerun:yes'>        </span></td>
  <td class=xl67 width=62 style='border-left:none;width:47pt'>0:30</td>
  <td class=xl65 width=46 style='border-left:none;width:35pt'>&#3594;&#3633;&#3623;&#3650;&#3617;&#3591;</td>
  <td class=xl65 width=113 style='border-left:none;width:85pt'>1</td>
  <td class="text-left-extra" width=117 style='border-left:none;width:88pt'><span class="icon icon-lg
  icon-check-square"></span>&nbsp; &#3592;&#3629;&#3604;</td>
  <td colspan=31 width=1984 style='mso-ignore:colspan;width:1488pt'><a
  class="btn btn-lg btn-outline-primary btn-pill btn-xs"
  href="edit_menu_6_6.php" type="button"><span
  class="icon icon-lg icon-edit"></span></a><button
  class="btn btn-outline-danger btn-pill btn-xs"
  data-toggle="modal" data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 class=xl65 style='height:14.4pt;border-top:none'>A02</td>
  <td class="text-left" style='border-top:none;border-left:none'>&#3585;&#3634;&#3619;&#3648;&#3611;&#3621;&#3637;&#3656;&#3618;&#3609;&#3629;&#3640;&#3611;&#3585;&#3619;&#3603;&#3660;&#3619;&#3629;&#3610;
  3 &#3648;&#3604;&#3639;&#3629;&#3609;</td>
  <td class=xl66 style='border-top:none;border-left:none'>CN<span
  style='mso-spacerun:yes'>        </span></td>
  <td class=xl67 style='border-top:none;border-left:none'>1:00</td>
  <td class=xl65 style='border-top:none;border-left:none'>&#3594;&#3633;&#3623;&#3650;&#3617;&#3591;</td>
  <td class=xl65 style='border-top:none;border-left:none'>2</td>
  <td class="text-left-extra" style='border-top:none;border-left:none'><span class="icon icon-lg
  icon-check-square"></span>&nbsp; &#3592;&#3629;&#3604;</td>
  <td colspan=31 style='mso-ignore:colspan'><a class="btn btn-lg
  btn-outline-primary btn-pill btn-xs" href="edit_menu_6_6.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 class=xl65 style='height:14.4pt;border-top:none'>A03</td>
  <td class="text-left" style='border-top:none;border-left:none'>&#3585;&#3634;&#3619;&#3648;&#3611;&#3621;&#3637;&#3656;&#3618;&#3609;&#3629;&#3640;&#3611;&#3585;&#3619;&#3603;&#3660;&#3619;&#3629;&#3610;
  6 &#3648;&#3604;&#3639;&#3629;&#3609;</td>
  <td class=xl66 style='border-top:none;border-left:none'>CN<span
  style='mso-spacerun:yes'>        </span></td>
  <td class=xl67 style='border-top:none;border-left:none'>1:30</td>
  <td class=xl65 style='border-top:none;border-left:none'>&#3594;&#3633;&#3623;&#3650;&#3617;&#3591;</td>
  <td class=xl65 style='border-top:none;border-left:none'>2</td>
  <td class="text-left-extra" style='border-top:none;border-left:none'><span class="icon icon-lg
  icon-check-square"></span>&nbsp; &#3592;&#3629;&#3604;</td>
  <td colspan=31 style='mso-ignore:colspan'><a class="btn btn-lg
  btn-outline-primary btn-pill btn-xs" href="edit_menu_6_6.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 class=xl65 style='height:14.4pt;border-top:none'>A04</td>
  <td class="text-left" style='border-top:none;border-left:none'>&#3585;&#3634;&#3619;&#3648;&#3611;&#3621;&#3637;&#3656;&#3618;&#3609;&#3629;&#3640;&#3611;&#3585;&#3619;&#3603;&#3660;&#3619;&#3629;&#3610;
  12 &#3648;&#3604;&#3639;&#3629;&#3609;</td>
  <td class=xl66 style='border-top:none;border-left:none'>CN<span
  style='mso-spacerun:yes'>        </span></td>
  <td class=xl67 style='border-top:none;border-left:none'>2:00</td>
  <td class=xl65 style='border-top:none;border-left:none'>&#3594;&#3633;&#3623;&#3650;&#3617;&#3591;</td>
  <td class=xl65 style='border-top:none;border-left:none'>3</td>
  <td class="text-left-extra" style='border-top:none;border-left:none'><span class="icon icon-lg
  icon-check-square"></span>&nbsp; &#3592;&#3629;&#3604;</td>
  <td colspan=31 style='mso-ignore:colspan'><a class="btn btn-lg
  btn-outline-primary btn-pill btn-xs" href="edit_menu_6_6.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 class=xl65 style='height:14.4pt;border-top:none'>A05</td>
  <td class="text-left" style='border-top:none;border-left:none'>&#3585;&#3634;&#3619;&#3648;&#3611;&#3621;&#3637;&#3656;&#3618;&#3609;&#3629;&#3640;&#3611;&#3585;&#3619;&#3603;&#3660;&#3619;&#3629;&#3610;
  24 &#3648;&#3604;&#3639;&#3629;&#3609;</td>
  <td class=xl66 style='border-top:none;border-left:none'>CN<span
  style='mso-spacerun:yes'>        </span></td>
  <td class=xl67 style='border-top:none;border-left:none'>2:30</td>
  <td class=xl65 style='border-top:none;border-left:none'>&#3594;&#3633;&#3623;&#3650;&#3617;&#3591;</td>
  <td class=xl65 style='border-top:none;border-left:none'>3</td>
  <td class="text-left-extra" style='border-top:none;border-left:none'><span class="icon icon-lg
  icon-check-square"></span>&nbsp; &#3592;&#3629;&#3604;</td>
  <td colspan=31 style='mso-ignore:colspan'><a class="btn btn-lg
  btn-outline-primary btn-pill btn-xs" href="edit_menu_6_6.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 class=xl65 style='height:14.4pt;border-top:none'>B01</td>
  <td class="text-left" style='border-top:none;border-left:none'>&#3585;&#3634;&#3619;&#3605;&#3619;&#3623;&#3592;&#3626;&#3629;&#3610;&#3626;&#3616;&#3634;&#3614;&#3648;&#3588;&#3619;&#3639;&#3656;&#3629;&#3591;&#3611;&#3619;&#3632;&#3592;&#3635;&#3626;&#3633;&#3611;&#3604;&#3634;&#3627;&#3660;</td>
  <td class=xl66 style='border-top:none;border-left:none'>CN<span
  style='mso-spacerun:yes'>        </span></td>
  <td class=xl67 style='border-top:none;border-left:none'>0:05</td>
  <td class=xl65 style='border-top:none;border-left:none'>&#3594;&#3633;&#3623;&#3650;&#3617;&#3591;</td>
  <td class=xl65 style='border-top:none;border-left:none'>1</td>
  <td class="text-left-extra" style='border-top:none;border-left:none'><span class="icon icon-lg
  icon-times-circle" style="font-size:20px;"></span> &#3652;&#3617;&#3656;&#3592;&#3629;&#3604;</td>
  <td colspan=31 style='mso-ignore:colspan'><a class="btn btn-lg
  btn-outline-primary btn-pill btn-xs" href="edit_menu_6_6.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 class=xl65 style='height:14.4pt;border-top:none'>B02</td>
  <td class="text-left" style='border-top:none;border-left:none'>&#3585;&#3634;&#3619;&#3605;&#3619;&#3623;&#3592;&#3626;&#3629;&#3610;&#3626;&#3616;&#3634;&#3614;&#3648;&#3588;&#3619;&#3639;&#3656;&#3629;&#3591;&#3611;&#3619;&#3632;&#3592;&#3635;
  15 &#3623;&#3633;&#3609;</td>
  <td class=xl66 style='border-top:none;border-left:none'>CN<span
  style='mso-spacerun:yes'>        </span></td>
  <td class=xl67 style='border-top:none;border-left:none'>0:10</td>
  <td class=xl65 style='border-top:none;border-left:none'>&#3594;&#3633;&#3623;&#3650;&#3617;&#3591;</td>
  <td class=xl65 style='border-top:none;border-left:none'>1</td>
  <td class="text-left-extra" style='border-top:none;border-left:none'><span class="icon icon-lg
  icon-times-circle" style="font-size:20px;"></span> &#3652;&#3617;&#3656;&#3592;&#3629;&#3604;</td>
  <td colspan=31 style='mso-ignore:colspan'><a class="btn btn-lg
  btn-outline-primary btn-pill btn-xs" href="edit_menu_6_6.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 class=xl65 style='height:14.4pt;border-top:none'>B03</td>
  <td class="text-left" style='border-top:none;border-left:none'>&#3585;&#3634;&#3619;&#3605;&#3619;&#3623;&#3592;&#3626;&#3629;&#3610;&#3626;&#3616;&#3634;&#3614;&#3648;&#3588;&#3619;&#3639;&#3656;&#3629;&#3591;&#3611;&#3619;&#3632;&#3592;&#3635;
  1 &#3648;&#3604;&#3639;&#3629;&#3609;</td>
  <td class=xl66 style='border-top:none;border-left:none'>CN<span
  style='mso-spacerun:yes'>        </span></td>
  <td class=xl67 style='border-top:none;border-left:none'>0:15</td>
  <td class=xl65 style='border-top:none;border-left:none'>&#3594;&#3633;&#3623;&#3650;&#3617;&#3591;</td>
  <td class=xl65 style='border-top:none;border-left:none'>2</td>
  <td class="text-left-extra" style='border-top:none;border-left:none'><span class="icon icon-lg
  icon-times-circle" style="font-size:20px;"></span> &#3652;&#3617;&#3656;&#3592;&#3629;&#3604;</td>
  <td colspan=31 style='mso-ignore:colspan'><a class="btn btn-lg
  btn-outline-primary btn-pill btn-xs" href="edit_menu_6_6.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 class=xl65 style='height:14.4pt;border-top:none'>B04</td>
  <td class="text-left" style='border-top:none;border-left:none'>&#3585;&#3634;&#3619;&#3605;&#3619;&#3623;&#3592;&#3626;&#3629;&#3610;&#3626;&#3616;&#3634;&#3614;&#3648;&#3588;&#3619;&#3639;&#3656;&#3629;&#3591;&#3611;&#3619;&#3632;&#3592;&#3635;
  3 &#3648;&#3604;&#3639;&#3629;&#3609;</td>
  <td class=xl66 style='border-top:none;border-left:none'>CN<span
  style='mso-spacerun:yes'>        </span></td>
  <td class=xl67 style='border-top:none;border-left:none'>0:20</td>
  <td class=xl65 style='border-top:none;border-left:none'>&#3594;&#3633;&#3623;&#3650;&#3617;&#3591;</td>
  <td class=xl65 style='border-top:none;border-left:none'>3</td>
  <td class="text-left-extra" style='border-top:none;border-left:none'><span class="icon icon-lg
  icon-check-square"></span>&nbsp; &#3592;&#3629;&#3604;</td>
  <td colspan=31 style='mso-ignore:colspan'><a class="btn btn-lg
  btn-outline-primary btn-pill btn-xs" href="edit_menu_6_6.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 class=xl65 style='height:14.4pt;border-top:none'>C01</td>
  <td class="text-left" style='border-top:none;border-left:none'>&#3585;&#3634;&#3619;&#3627;&#3621;&#3656;&#3629;&#3621;&#3639;&#3656;&#3609;&#3648;&#3588;&#3619;&#3639;&#3656;&#3629;&#3591;&#3592;&#3633;&#3585;&#3619;&#3611;&#3619;&#3632;&#3592;&#3635;&#3626;&#3633;&#3611;&#3604;&#3634;&#3627;&#3660;</td>
  <td class=xl66 style='border-top:none;border-left:none'>CN<span
  style='mso-spacerun:yes'>        </span></td>
  <td class=xl67 style='border-top:none;border-left:none'>0:05</td>
  <td class=xl65 style='border-top:none;border-left:none'>&#3594;&#3633;&#3623;&#3650;&#3617;&#3591;</td>
  <td class=xl65 style='border-top:none;border-left:none'>1</td>
  <td class="text-left-extra" style='border-top:none;border-left:none'><span class="icon icon-lg
  icon-times-circle" style="font-size:20px;"></span> &#3652;&#3617;&#3656;&#3592;&#3629;&#3604;</td>
  <td colspan=31 style='mso-ignore:colspan'><a class="btn btn-lg
  btn-outline-primary btn-pill btn-xs" href="edit_menu_6_6.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 class=xl65 style='height:14.4pt;border-top:none'>C02</td>
  <td class="text-left" style='border-top:none;border-left:none'>&#3585;&#3634;&#3619;&#3627;&#3621;&#3656;&#3629;&#3621;&#3639;&#3656;&#3609;&#3648;&#3588;&#3619;&#3639;&#3656;&#3629;&#3591;&#3592;&#3633;&#3585;&#3619;&#3611;&#3619;&#3632;&#3592;&#3635;
  1 &#3648;&#3604;&#3639;&#3629;&#3609;</td>
  <td class=xl66 style='border-top:none;border-left:none'>CN<span
  style='mso-spacerun:yes'>        </span></td>
  <td class=xl67 style='border-top:none;border-left:none'>0:10</td>
  <td class=xl65 style='border-top:none;border-left:none'>&#3594;&#3633;&#3623;&#3650;&#3617;&#3591;</td>
  <td class=xl65 style='border-top:none;border-left:none'>1</td>
  <td class="text-left-extra" style='border-top:none;border-left:none'><span class="icon icon-lg
  icon-times-circle" style="font-size:20px;"></span> &#3652;&#3617;&#3656;&#3592;&#3629;&#3604;</td>
  <td colspan=31 style='mso-ignore:colspan'><a class="btn btn-lg
  btn-outline-primary btn-pill btn-xs" href="edit_menu_6_6.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 class=xl65 style='height:14.4pt;border-top:none'>C03</td>
  <td class="text-left" style='border-top:none;border-left:none'>&#3585;&#3634;&#3619;&#3627;&#3621;&#3656;&#3629;&#3621;&#3639;&#3656;&#3609;&#3648;&#3588;&#3619;&#3639;&#3656;&#3629;&#3591;&#3592;&#3633;&#3585;&#3619;&#3611;&#3619;&#3632;&#3592;&#3635;
  3 &#3648;&#3604;&#3639;&#3629;&#3609;</td>
  <td class=xl66 style='border-top:none;border-left:none'>CN<span
  style='mso-spacerun:yes'>        </span></td>
  <td class=xl67 style='border-top:none;border-left:none'>0:15</td>
  <td class=xl65 style='border-top:none;border-left:none'>&#3594;&#3633;&#3623;&#3650;&#3617;&#3591;</td>
  <td class=xl65 style='border-top:none;border-left:none'>2</td>
  <td class="text-left-extra" style='border-top:none;border-left:none'><span class="icon icon-lg
  icon-check-square"></span>&nbsp; &#3592;&#3629;&#3604;</td>
  <td colspan=31 style='mso-ignore:colspan'><a class="btn btn-lg
  btn-outline-primary btn-pill btn-xs" href="edit_menu_6_6.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 class=xl65 style='height:14.4pt;border-top:none'>C04</td>
  <td class="text-left" style='border-top:none;border-left:none'>&#3585;&#3634;&#3619;&#3607;&#3635;&#3588;&#3623;&#3634;&#3617;&#3626;&#3632;&#3629;&#3634;&#3604;&#3648;&#3588;&#3619;&#3639;&#3656;&#3629;&#3591;&#3592;&#3633;&#3585;&#3619;</td>
  <td class=xl66 style='border-top:none;border-left:none'>CN<span
  style='mso-spacerun:yes'>        </span></td>
  <td class=xl67 style='border-top:none;border-left:none'>0:20</td>
  <td class=xl65 style='border-top:none;border-left:none'>&#3594;&#3633;&#3623;&#3650;&#3617;&#3591;</td>
  <td class=xl65 style='border-top:none;border-left:none'>2</td>
  <td class="text-left-extra" style='border-top:none;border-left:none'><span class="icon icon-lg
  icon-times-circle" style="font-size:20px;"></span> &#3652;&#3617;&#3656;&#3592;&#3629;&#3604;</td>
  <td colspan=31 style='mso-ignore:colspan'><a class="btn btn-lg
  btn-outline-primary btn-pill btn-xs" href="edit_menu_6_6.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 class=xl65 style='height:14.4pt;border-top:none'>C05</td>
  <td class="text-left" style='border-top:none;border-left:none'>&#3585;&#3634;&#3619;&#3607;&#3635;&#3588;&#3623;&#3634;&#3617;&#3626;&#3632;&#3629;&#3634;&#3604;&#3621;&#3657;&#3634;&#3591;&#3619;&#3634;&#3591;&#3609;&#3657;&#3635;&#3618;&#3634;</td>
  <td class=xl66 style='border-top:none;border-left:none'>CN<span
  style='mso-spacerun:yes'>        </span></td>
  <td class=xl67 style='border-top:none;border-left:none'>0:25</td>
  <td class=xl65 style='border-top:none;border-left:none'>&#3594;&#3633;&#3623;&#3650;&#3617;&#3591;</td>
  <td class=xl65 style='border-top:none;border-left:none'>3</td>
  <td class="text-left-extra" style='border-top:none;border-left:none'><span class="icon icon-lg
  icon-check-square"></span>&nbsp; &#3592;&#3629;&#3604;</td>
  <td colspan=31 style='mso-ignore:colspan'><a class="btn btn-lg
  btn-outline-primary btn-pill btn-xs" href="edit_menu_6_6.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 class=xl65 style='height:14.4pt;border-top:none'>D01</td>
  <td class="text-left" style='border-top:none;border-left:none'>&#3585;&#3634;&#3619;&#3626;&#3656;&#3591;&#3605;&#3637;&#3611;&#3621;&#3629;&#3585;&#3649;&#3617;&#3656;&#3605;&#3632;&#3586;&#3629;</td>
  <td class=xl66 style='border-top:none;border-left:none'>CN<span
  style='mso-spacerun:yes'>        </span></td>
  <td class=xl65 style='border-top:none;border-left:none'>1</td>
  <td class=xl65 style='border-top:none;border-left:none'>&#3623;&#3633;&#3609;</td>
  <td class=xl65 style='border-top:none;border-left:none'>1</td>
  <td class="text-left-extra" style='border-top:none;border-left:none'><span class="icon icon-lg
  icon-times-circle" style="font-size:20px;"></span> &#3652;&#3617;&#3656;&#3592;&#3629;&#3604;</td>
  <td colspan=31 style='mso-ignore:colspan'><a class="btn btn-lg
  btn-outline-primary btn-pill btn-xs" href="edit_menu_6_6.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 class=xl65 style='height:14.4pt;border-top:none'>D02</td>
  <td class="text-left" style='border-top:none;border-left:none'>&#3585;&#3634;&#3619;&#3626;&#3656;&#3591;&#3595;&#3656;&#3629;&#3617;&#3621;&#3641;&#3585;&#3648;&#3610;&#3637;&#3657;&#3618;&#3623;</td>
  <td class=xl66 style='border-top:none;border-left:none'>CN<span
  style='mso-spacerun:yes'>        </span></td>
  <td class=xl65 style='border-top:none;border-left:none'>14</td>
  <td class=xl65 style='border-top:none;border-left:none'>&#3623;&#3633;&#3609;</td>
  <td class=xl65 style='border-top:none;border-left:none'>2</td>
  <td class="text-left-extra" style='border-top:none;border-left:none'><span class="icon icon-lg
  icon-check-square"></span>&nbsp; &#3592;&#3629;&#3604;</td>
  <td colspan=31 style='mso-ignore:colspan'><a class="btn btn-lg
  btn-outline-primary btn-pill btn-xs" href="edit_menu_6_6.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 class=xl65 style='height:14.4pt;border-top:none'>D03</td>
  <td class="text-left" style='border-top:none;border-left:none'>&#3585;&#3634;&#3619;&#3592;&#3629;&#3604;&#3605;&#3636;&#3604;&#3605;&#3633;&#3657;&#3591;&#3629;&#3640;&#3611;&#3585;&#3619;&#3603;&#3660;&#3605;&#3656;&#3634;&#3591;&#3654;
  &#3651;&#3609;&#3626;&#3656;&#3623;&#3609;&#3591;&#3634;&#3609;&#3650;&#3588;&#3619;&#3591;&#3585;&#3634;&#3619;</td>
  <td class=xl66 style='border-top:none;border-left:none'>CN<span
  style='mso-spacerun:yes'>        </span></td>
  <td class=xl65 style='border-top:none;border-left:none'>14</td>
  <td class=xl65 style='border-top:none;border-left:none'>&#3623;&#3633;&#3609;</td>
  <td class=xl65 style='border-top:none;border-left:none'>3</td>
  <td class="text-left-extra" style='border-top:none;border-left:none'><span class="icon icon-lg
  icon-check-square"></span>&nbsp; &#3592;&#3629;&#3604;</td>
  <td colspan=31 style='mso-ignore:colspan'><a class="btn btn-lg
  btn-outline-primary btn-pill btn-xs" href="edit_menu_6_6.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 class=xl65 style='height:14.4pt;border-top:none'>D04</td>
  <td class="text-left" style='border-top:none;border-left:none'>&#3585;&#3634;&#3619;&#3648;&#3611;&#3621;&#3637;&#3656;&#3618;&#3609;&#3629;&#3640;&#3611;&#3585;&#3619;&#3603;&#3660;&#3619;&#3632;&#3618;&#3632;&#3614;&#3636;&#3607;&#3595;&#3660;</td>
  <td class=xl66 style='border-top:none;border-left:none'>CN<span
  style='mso-spacerun:yes'>        </span></td>
  <td class=xl65 style='border-top:none;border-left:none'>20</td>
  <td class=xl65 style='border-top:none;border-left:none'>&#3623;&#3633;&#3609;</td>
  <td class=xl65 style='border-top:none;border-left:none'>3</td>
  <td class="text-left-extra" style='border-top:none;border-left:none'><span class="icon icon-lg
  icon-check-square"></span>&nbsp; &#3592;&#3629;&#3604;</td>
  <td colspan=31 style='mso-ignore:colspan'><a class="btn btn-lg
  btn-outline-primary btn-pill btn-xs" href="edit_menu_6_6.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
            </div>
        </div>










    </div>
</div>
<!--  -->
<? include('inc.footer.php');?>
<!--  -->
<? include('inc.footer.script.php');?>
<!--  -->
