<?php
    $select = 'open_menu_2';
    $select2 = 'menu_mk_total_production_plan';
    $select3 = 'menu_mk_4';
    $select4 = 'menu_mk_4';

     $numberOrder = 0;
     $numberEnquiry = 0;

     $orders = array('ออเดอร์', 'สอบถาม');
     $typeSale = array('ขายต่าง', 'ขายใน');

     $statusText = array('วางแผนแล้ว', 'กำลังผลิต', 'รอแก้ไข');
     $statusClass = array('info tone3', 'info tone2', 'warning');
?>

<!--  -->
<? include('inc.header.php');?>
<!--  -->
<? include('inc.navbar.php');?>
<!--  -->
<? include('inc.menu.php');?>
<!-- <div class="layout-main"> -->
<!--  -->
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <h4 class="m-t-0">
                <!-- <span class="d-ib text-danger">--</span> <span class="icon icon-angle-double-right"></span> -->
                <!-- <span class="d-ib">ข้อมูลหลัก</span> -->
                <span class="d-ib text-primary">การตลาด</span> <span class="icon icon-angle-double-right"></span> แผนการผลิตโดยรวม
                <!-- <span class="d-ib">
                    <a class="title-bar-shortcut" href="#" title="Add to shortcut list" data-container="body" data-toggle-text="Remove from shortcut list" data-trigger="hover" data-placement="right" data-toggle="tooltip">
                        <span class="sr-only">Add to shortcut list</span>
                    </a>
                </span> -->
            </h4>
            <!-- <p class="title-bar-description">
                <small>Latest update on 01/01/2017 - 10.57 am.</small>
            </p> -->
        </div>

        <div class="row gutter-xs">
            <div class="col-xs-12">
                <!--  -->
                <div class="card">
                    <!-- <div class="card-header">
                        <a href="javascript:window.history.back(-1);" class="btn btn-sm btn-outline-primary"><span class="icon icon-long-arrow-left"></span>&nbsp;&nbsp;Back</a>&nbsp;&nbsp;
                        <strong class="">Latest update on 01/01/2017 - 10.57 am.</strong>
                    </div> -->
                    <div class="card-body">
                        <!--  -->

                        <? include('inc.home_top_filter_extra_order2.php');?>
                        
                        <div class="date-range-body">
                            <img src="images/date-range.png" style="max-width:100%;">
                        </div>
                        <!--  -->
                        <div class="graph-body">
                            <section id="graph-1" style="width:100%;">

                            </section>
						</div>
                    </div>
                </div>
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
            </div>
        </div>
    </div>
</div>


<!--  -->
<? include('inc.footer.php');?>
<!--  -->
<? include('inc.footer.script.php');?>
<!--  -->

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/xrange.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

<script src="js/graph.js"></script>


<script>
    $(function(){

    showGraphList();


    });
</script>