<?php
    $select = 'open_menu_5';
    $select2 = 'menu_pp_2';

    $index = array();
    // $type = $_GET['type'];
    // $amount = $_GET['amount'];
    $type = 'all';
    $amount = 3;
    $hexColors = array(0x922B21, 0x6C3483, 0x1F618D, 0x0E6655, 0x0E6655, 0x9C640C, 0x797D7F, 0x212F3C);
    $bgColors = array('FADBD8', 'E8DAEF', 'D6EAF8', 'D1F2EB', 'FCF3CF', 'EAEDED', 'ffdfff');
    $fontColors = array();
    $bgTableFirst = array();
    $bgTableSecond = array();
    
    $numberOrder = 0;
    $numberInquiry = 0;

    // echo 'value: ' . dechex($hexColors[0] + 1);
    
    foreach($hexColors as $item){
        $bgTableFirst[] = dechex($hexColors[0] + 1);
    }

    $typeSale = array('ขายใน', 'ขายต่าง');
    $statusText = array('รอร่างแผน', 'มีการเปลี่ยนแปลง<br>รอร่างแผนใหม่');
    $statusClass = array('warning', 'warning tone2');
    
	$productsName = array('1119P/01301040', '1ฑ090/01300040', '11190/01301040', '19090/01300040', '1ฌ094/01300050');
?>

<!--  -->
<? include('inc.header.php');?>
<!--  -->
<? include('inc.navbar.php');?>
<!--  -->
<? include('inc.menu.php');?>
<!-- <div class="layout-main"> -->
<style>
.ui-draggable-dragging, .cell-highlight {
    z-index: 99;
    background-color: yellow;
}
</style>
<!--  -->
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <h4 class="m-t-0">
                <!-- <span class="d-ib text-danger">--</span> <span class="icon icon-angle-double-right"></span> -->
                <!-- <span class="d-ib">ข้อมูลหลัก</span> -->
                <span class="d-ib text-primary">การวางแผนการผลิต</span> <span class="icon icon-angle-double-right"></span> วางแผนการผลิต
                <!-- <span class="d-ib">
                    <a class="title-bar-shortcut" href="#" title="Add to shortcut list" data-container="body" data-toggle-text="Remove from shortcut list" data-trigger="hover" data-placement="right" data-toggle="tooltip">
                        <span class="sr-only">Add to shortcut list</span>
                    </a>
                </span> -->
            </h4>
            <!-- <p class="title-bar-description">
                <small>Latest update on 01/01/2017 - 10.57 am.</small>
            </p> -->
        </div>
        <div class="row gutter-xs">
            <div class="col-xs-12">
                <div class="card">
                    <div class="card-body">
                        <div class="at_add_box">
                            <div class="card filter" aria-expanded="true">
                                <div class="card-header at_bg_table_blue">
                                    <div class="card-actions">
                                            <button type="button" class="card-action card-toggler" title="Collapse" aria-expanded="true"></button>
                                    </div>
                                    <strong>เลือกรายการ</strong>
                                </div>
                                <div class="card-body" style="display: block;">
                                    <form id="form-filter" method="get" class="form form-horizontal">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label text-right" for="form-control-14">เลือกจาก : </label>
                                                    <div class="col-sm-9 has-feedback">
                                                        <select id="demo-select2-1" class="form-control" name="branch">
                                                            <option value="all">ทั้งหมด</option>
                                                            <option value="1">รายการออเดอร์</option>
                                                            <option value="2">รายการใบสอบถาม</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-8 text-right">
                                                <div class="form-group">
                                                    <div class="col-sm-12">
                                                        <button id="save-filter" class="btn btn-outline-primary btn-sm buttons-collection buttons-colvis width-fit hide" type="button">
                                                            <span class="icon icon-save"></span>
                                                            &nbsp;&nbsp;บันทึก
                                                        </button>

                                                        <button class="btn btn-sm btn-primary btn-thick width-fit" type="submit">
                                                            <span class="icon icon-search"></span>
                                                            &nbsp;&nbsp;ค้นหา
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label text-right" for="form-control-14">สาขา : </label>
                                                    <div class="col-sm-9 has-feedback">
                                                        <select id="demo-select2-1" class="form-control" name="branch">
                                                            <option value="all">ทั้งหมด</option>
                                                            <option value="1">ขอนแก่นแหอวน</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label class="col-sm-5 control-label text-right" for="form-control-14">ประเภทการขาย : </label>
                                                    <div class="col-sm-7 has-feedback">
                                                        <select id="demo-select2-2" class="form-control" name="type_sale">
                                                            <option value="all">ทั้งหมด</option>
                                                            <option value="1">ขายใน</option>
                                                            <option value="2">ขายต่าง</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label text-right" for="form-control-14">สถานะ</label>
                                                    <div class="col-sm-9 has-feedback">
                                                        <select id="demo-select2-3" class="form-control">
                                                            <option value="0">ทั้งหมด</option>
                                                            <option value="0">รอร่างแผน</option>
                                                            <option value="0">มีการเปลี่ยนแปลง รอร่างแผนใหม่</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 control-label text-right" for="form-control-14">ตามช่วงเวลา</label>
                                                        <div class="col-sm-7 has-feedback">
                                                            <select id="demo-select2-3-2" class="form-control">
                                                                <option value="1">สั่งซื้อ</option>
                                                                <option value="0">จัดส่งสินค้า</option>
                                                                <option value="0">ผลิต</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="input-group input-daterange" data-provide="datepicker" data-date-autoclose="true" data-date-format="dd-M-yyyy">
                                                    <input class="form-control" type="text" value="เริ่มต้น">
                                                    <span class="input-group-addon">ถึง</span>
                                                    <input class="form-control" type="text" value="สิ้นสุด">
                                                </div>
                                            </div>
                                        </div>
                                    </form>

                                    <section id="section-order" data-type="order">
                                        <form id="form-order" method="get" class="form form-horizontal">
                                            <!-- <div class="row top-header">
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" placeholder="ค้นหา...">
                                                </div>
                                                <div class="col-sm-4 text-right text-fade">
                                                    <span class="icon icon-lg icon-hourglass-half" title="รอร่างแผน"></span> = รอร่างแผน,
                                                    <span class="icon icon-lg icon-edit" style="margin-left:15px;"></span> = มีการเปลี่ยนแปลง รอร่างแผนใหม่
                                                </div>
                                                <div class="col-sm-12 text-right">
                                                    <button id="start-plan-order" class="btn btn-sm btn-primary btn-thick" type="submit" onclick="openToPlaning('order')">
                                                        <span class="icon icon-download"></span>
                                                        &nbsp;&nbsp;เริ่มวางแผน
                                                    </button>
                                                </div>
                                            </div> -->

                                            <button id="start-plan-order" class="btn btn-sm btn-primary btn-thick start-plan width-fit" type="submit" onclick="openToPlaning('order')" style="right:16px;">
                                                <span class="icon icon-calendar"></span>
                                                &nbsp;&nbsp;เริ่มวางแผน
                                            </button>
                                            
                                            <div class="row">
                                                <div class="col-sm-12">

                                                    <!-- <div class="suggestion status">
                                                        <div class="content">
                                                            <div class="element"><span class="icon icon-lg icon-hourglass-half"></span><div class="text">รอร่างแผน</div></div>
                                                            <div class="element"><span class="icon icon-lg icon-edit"></span><div class="text">มีการเปลี่ยนแปลง รอร่างแผนใหม่</div></div>
                                                        </div>
                                                    </div> -->

                                                    <div class="table-responsive" style="margin-top: 40px;">
                                                        <table class="table table-hover table-bordered text-center table-items" cellspacing="0" width="100%" aria-describedby="demo-datatables-2_info" role="grid" style="width: 100%;">
                                                            <thead>
                                                                <tr class="at_bg_table_blue">
                                                                    <th class="text-center middle no-sort" rowspan="3">เลือก</th>
                                                                    <th class="text-center middle" rowspan="3">เลขที่<br>รายการ</th>
                                                                    <th class="text-center middle" rowspan="3">ประเภท<br>ขาย</th>
                                                                    <th class="text-center middle" rowspan="3">วันที่<br>สั่งซื้อ</th>
                                                                    <th class="text-center middle" colspan="4">วันที่ผลิต</th>
                                                                    <th class="text-center middle" colspan="2" rowspan="2">วันที่ส่งคลังสินค้า</th>
                                                                    <th class="text-center middle" rowspan="3">วันที่<br>จัดส่งสินค้า</th>
                                                                    <th class="text-center middle no-sort" rowspan="3" width="50px;">สถานะ</th>
                                                                </tr>
                                                                <tr class="at_bg_table_blue">
                                                                    <th class="text-center" colspan="2">ทอ</th>
                                                                    <th class="text-center" colspan="2">วงจรส่วนหลัง</th>
                                                                </tr>
                                                                <tr class="at_bg_table_blue">
                                                                    <th class="text-center">เริ่ม</th>
                                                                    <th class="text-center">เสร็จ</th>
                                                                    <th class="text-center">เริ่ม</th>
                                                                    <th class="text-center">เสร็จ</th>
                                                                    <th class="text-center">เริ่ม</th>
                                                                    <th class="text-center">เสร็จ</th>
                                                                </tr>
                                                            </thead>
                                                            <!-- <tfoot>
                                                                <tr>
                                                                    <th class="text-center no-sort" >ค้นหา</th>
                                                                    <th class="text-center" >ค้นหา</th>
                                                                    <th class="text-center" >ค้นหา</th>
                                                                    <th class="text-center" >ค้นหา</th>
                                                                    <th class="text-center" >ค้นหา</th>
                                                                    <th class="text-center" >ค้นหา</th>
                                                                    <th class="text-center" >ค้นหา</th>
                                                                    <th class="text-center" >ค้นหา</th>
                                                                    <th class="text-center" >ค้นหา</th>
                                                                    <th class="text-center" >ค้นหา</th>
                                                                    <th class="text-center" >ค้นหา</th>
                                                                    <th class="text-center" >ค้นหา</th>
                                                                    <th class="text-center no-sort" >ค้นหา</th>
                                                                </tr>
                                                            </tfoot> -->
                                                            <tbody class="text-center">
                                                                <?php for($i=0; $i<10; $i++){ ?>
                                                                    <?php 
                                                                        $statusRand = rand(0,1);
                                                                        $typeRand = rand(0,1);
                                                                        if($typeRand == 0){
                                                                            $numberOrder++;
                                                                        }else{
                                                                            $numberInquiry++;
                                                                        }
                                                                    ?>
                                                                    <tr>
                                                                        <td>
                                                                            <label class="custom-control custom-control-primary custom-checkbox">
                                                                                <input class="custom-control-input" type="checkbox" name="select_checkbox[]" onclick="onSelectItem(this)" value="1">
                                                                                <span class="custom-control-indicator"></span>
                                                                            </label>
                                                                        </td>
                                                                        <td data-toggle="collapse" data-target="#item-<?=$i+1?>"><?=$typeRand==0?"ORDER00".$numberOrder:"EQR00".$numberInquiry?></td>
                                                                        <td data-toggle="collapse" data-target="#item-<?=$i+1?>"><?=$typeSale[rand(0,1)]?></td>
                                                                        <td data-toggle="collapse" data-target="#item-<?=$i+1?>">02/08/2556</td>
                                                                        <td data-toggle="collapse" data-target="#item-<?=$i+1?>"><?=$statusRand==1?"05/08/2556":"-"?></td>
                                                                        <td data-toggle="collapse" data-target="#item-<?=$i+1?>"><?=$statusRand==1?"08/08/2556":"-"?></td>
                                                                        <td data-toggle="collapse" data-target="#item-<?=$i+1?>">-</td>
                                                                        <td data-toggle="collapse" data-target="#item-<?=$i+1?>">-</td>
                                                                        <td data-toggle="collapse" data-target="#item-<?=$i+1?>">-</td>
                                                                        <td data-toggle="collapse" data-target="#item-<?=$i+1?>">-</td>
                                                                        <td data-toggle="collapse" data-target="#item-<?=$i+1?>">29/08/2556</td>
                                                                        <?php
                                                                            $elementDiv = '';
                                                                            if($statusRand==3 || $statusRand==5){
                                                                                $elementDiv = "data-toggle=\"modal\" data-target=\"#approve-plan\"";
                                                                            }else if($statusRand==6){
                                                                                $elementDiv = "data-toggle=\"modal\" data-target=\"#modalConfirmInquiry\"";
                                                                            }
                                                                        ?>
                                                                        <td class="left"><button class="btn btn-<?=$statusClass[$statusRand]?> status " type="button" <?=$elementDiv?>><?=$statusText[$statusRand]?></button></td>
                                                                    </tr>

                                                                    <tr class="cl">
                                                                        <td colspan="13" class="bg_item_in n-p-i">
                                                                            <div class="accordian-body collapse" id="item-<?=$i+1?>"> 
                                                                                <table class="table table-bordered text-center table-hover" cellspacing="0" width="100%">
                                                                                    <thead>
                                                                                    <tr class="bg_table_in">
                                                                                        <th class="text-center middle" rowspan="3">หมายเลขร่างแผน</th>
                                                                                        <th class="text-center middle" rowspan="3">รหัสสินค้า</th>
                                                                                        <th class="text-center middle" rowspan="3">ประเภท<br>สินค้า</th>
                                                                                        <th class="text-center middle" colspan="2" rowspan="2">สั่งซื้อ</th>
                                                                                        <th class="text-center middle" colspan="4">วันที่ผลิต</th>
                                                                                        <th class="text-center middle" rowspan="3">จำนวน<br>ชุดทอ</th>
                                                                                        <th class="text-center middle" rowspan="3">จำนวน<br>ผืนทอ</th>
                                                                                        <th class="text-center middle" colspan="2" rowspan="2">วันที่ส่งคลังสินค้า</th>
                                                                                        <th class="text-center middle" rowspan="3">วันที่<br>จัดส่งสินค้า</th>
                                                                                        <th class="text-center middle" rowspan="3">สถานะ<br>ในคลัง<br>สินค้า<br>ปัจจุบัน</th>
                                                                                    </tr>
                                                                                    <tr class="bg_table_in">
                                                                                        <th class="text-center" colspan="2">ทอ</th>
                                                                                        <th class="text-center" colspan="2">วงจรส่วนหลัง</th>
                                                                                    </tr>
                                                                                    <tr class="bg_table_in">
                                                                                        <th class="text-center">จำนวน</th>
                                                                                        <th class="text-center">หน่วย</th>
                                                                                        <th class="text-center">เริ่ม</th>
                                                                                        <th class="text-center">เสร็จ</th>
                                                                                        <th class="text-center">เริ่ม</th>
                                                                                        <th class="text-center">เสร็จ</th>
                                                                                        <th class="text-center">เริ่ม</th>
                                                                                        <th class="text-center">เสร็จ</th>
                                                                                    </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                        <?php for($j=0; $j<count($productsName); $j++){ ?>
                                                                                            <?php 
                                                                                                $amountItem = rand(100, 300);
                                                                                                $amountWovenDress = rand(10,20);
                                                                                                $amountWeave = rand(300, 500);
                                                                                            ?>
                                                                                            <tr>
                                                                                                <td class="<?=$j!=0?'f-white':''?>">DRAFT.ENQUIRE001R1</td>
                                                                                                <td><?=$productsName[$j]?></td>
                                                                                                <td>ข่ายเอ็น</td>
                                                                                                <td><?=$amountItem?></td>
                                                                                                <td>PC</td>
                                                                                                <td>05/08/2556</td>
                                                                                                <td>09/08/2556</td>
                                                                                                <td>10/08/2556</td>
                                                                                                <td>13/08/2556</td>
                                                                                                <td><?=$amountWovenDress?></td>
                                                                                                <td><?=$amountWeave?></td>
                                                                                                <td>17/08/2556</td>
                                                                                                <td>20/08/2556</td>
                                                                                                <td>24/08/2556</td>
                                                                                                <td><?=$amountWeave?></td>
                                                                                            </tr>
                                                                                        <?php } ?>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                <?php } ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </section>
                                </div>
                            </div>

                            <div class="card planning" aria-expanded="true" style="display:none;">
                                <div class="card-header at_bg_table_blue">
                                    <div class="card-actions">
                                        <button type="button" class="card-action card-toggler" title="Collapse" aria-expanded="true"></button>
                                    </div>
                                    <strong>วางแผนการผลิต</strong>
                                </div>  
                                <div class="card-body" style="display: block;">
                                    <form id="form-planning" method="get" class="form form-horizontal">
                                        <?php for($i=0; $i<$amount; $i++){ ?>
                                            <div class="card planning-item" aria-expanded="true" data-color="<?=$bgColors[$i]?>">
                                                <div class="card-header" style="background:#<?=$bgColors[$i]?>;color:#333;">
                                                    <div class="card-actions">
                                                            <button type="button" class="card-action card-toggler" title="Collapse" aria-expanded="true"></button>
                                                    </div>
                                                    <strong>รายการ <?=$i==2?"EQR":"ORDER"?>00<?=$i+1?></strong>
                                                </div>
                                                <div class="card-body item" style="display: block;padding-top: 10px;">
                                                    <div class="row relative header-detail">
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                <div class="form-group">
                                                                    <label class="col-sm-6 control-label">วันที่สอบถาม/สั่งซื้อ</label>
                                                                    <div class="col-sm-6 n-p-l">
                                                                        <p class="form-control-static">12/08/2556</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <div class="form-group">
                                                                    <label class="col-sm-6 control-label">ชื่อลูกค้า</label>
                                                                    <div class="col-sm-6 n-p-l">
                                                                        <p class="form-control-static">CUSTOMER001</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                <div class="form-group">
                                                                    <label class="col-sm-6 control-label">ประเภทรายการ</label>
                                                                    <div class="col-sm-6 n-p-l">
                                                                        <p class="form-control-static"><?=$i==2?"สอบถาม":"ออร์เดอร์"?></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <div class="form-group">
                                                                    <label class="col-sm-6 control-label">ประเภทขาย</label>
                                                                    <div class="col-sm-6 n-p-l">
                                                                        <p class="form-control-static">ขายต่าง</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                <div class="form-group">
                                                                    <label class="col-sm-6 control-label">สถานะ</label>
                                                                    <div class="col-sm-6 n-p-l">
                                                                        <p class="form-control-static">รอวางแผน</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- <div class="col-sm-6">
                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <div class="col-sm-12">
                                                                        <strong>รหัสรายการ : </strong><span id="order-id">ORDER00<?=$i+1?></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <div class="col-sm-12">
                                                                        <strong>ชื่อลูกค้า : </strong><span id="customer-name">CUSTOMER00<?=$i+1?></span>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <div class="col-sm-12">
                                                                        <strong>ประเภทขาย : </strong><span id="type-sale">ขายใน</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <div class="col-sm-12">
                                                                        <strong>วันที่สอบถาม/วันที่สั่งซื้อ : </strong><span id="date-buy">03/08/2556</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div> -->

                                                        <button class="btn btn-sm btn-primary btn-thick pull-right-bottom plan-all" type="button" onclick="onPlanAll(this)">
                                                            <span class="icon icon-calendar"></span>
                                                            <!-- <span class="spinner spinner-default spinner-sm input-icon"></span> -->
                                                            &nbsp;&nbsp;วางแผนอัตโนมัติ
                                                        </button>
                                                    </div>

                                                    <div class="table-responsive">
                                                        <table id="planing-table" class="table table-hover table-bordered table-nowrap dataTable text-center no-footer choose" cellspacing="0" width="100%" aria-describedby="demo-datatables-2_info" role="grid" style="width: 100%;">
                                                            <thead>
                                                                <tr class="bg_table_in">
                                                                <th class="text-center middle" rowspan="3">รหัสสินค้า</th>
                                                                <th class="text-center middle" rowspan="3">ประเภท<br>สินค้า</th>
                                                                <th class="text-center middle" colspan="2" rowspan="2">สั่งซื้อ</th>
                                                                <th class="text-center middle" colspan="4">วันที่ผลิต</th>
                                                                <th class="text-center middle" rowspan="3">จำนวน<br>ชุดทอ</th>
                                                                <th class="text-center middle" rowspan="3">จำนวน<br>ผืนทอ</th>
                                                                <th class="text-center middle" colspan="2" rowspan="2">วันที่ส่งคลังสินค้า</th>
                                                                <th class="text-center middle" rowspan="3">วันที่<br>จัดส่งสินค้า</th>
                                                                <th class="text-center middle" rowspan="3">สถานะ<br>ในคลัง<br>สินค้า<br>ปัจจุบัน</th>
                                                                <th class="text-center middle" rowspan="3">สถานะ</th>
                                                                <th class="text-center middle" rowspan="3">ควบคุม</th>
                                                            </tr>
                                                            <tr class="bg_table_in">
                                                                <th class="text-center" colspan="2">ทอ</th>
                                                                <th class="text-center" colspan="2">วงจรส่วนหลัง</th>
                                                            </tr>
                                                            <tr class="bg_table_in">
                                                                <th class="text-center">จำนวน</th>
                                                                <th class="text-center">หน่วย</th>
                                                                <th class="text-center">เริ่ม</th>
                                                                <th class="text-center">เสร็จ</th>
                                                                <th class="text-center">เริ่ม</th>
                                                                <th class="text-center">เสร็จ</th>
                                                                <th class="text-center">เริ่ม</th>
                                                                <th class="text-center">เสร็จ</th>
                                                            </tr>
                                                                <!-- <tr class="at_bg_table_light_gray">
                                                                    <th class="text-center"></th>
                                                                    <th class="text-center"></th>
                                                                    <th class="text-center" colspan="2">วันที่ผลิต</th>
                                                                    <th class="text-center"></th>
                                                                    <th class="text-center"></th>
                                                                </tr>
                                                                <tr class="at_bg_table_light_gray">
                                                                    <th class="text-center">รหัสสินค้า</th>
                                                                    <th class="text-center">ประเภทสินค้า</th>
                                                                    <th class="text-center" style="width:100px;">เริ่ม</th>
                                                                    <th class="text-center" style="width:100px;">เสร็จ</th>
                                                                    <th class="text-center" style="width: 120px;">สถานะ</th>
                                                                    <th class="text-center" style="width: 120px;">ควบคุม</th>
                                                                </tr> -->
                                                            </thead>
                                                            <tbody class="text-center">
                                                                <?php 
                                                                    for($j=0; $j<count($productsName); $j++){
                                                                        $amountItem = rand(100, 300);
                                                                        $amountWovenDress = rand(10,20);
                                                                        $amountWeave = rand(300, 500);
                                                                        $producId = $productsName[$j].($i+1);
                                                                ?>
                                                                <tr data-name="<?=$producId?>" data-duration="3" style="background:#<?=$bgColors[$i]?>;color:#333;">
                                                                    <td><?=$producId?></td>
                                                                    <td>ข่ายเอ็น</td>
                                                                    <td><?=$amountItem?></td>
                                                                    <td>PC</td>
                                                                    <td class="column-start">-</td>
                                                                    <td class="column-end">-</td>
                                                                    <td>-</td>
                                                                    <td>-</td>
                                                                    <td><?=$amountWovenDress?></td>
                                                                    <td><?=$amountWeave?></td>
                                                                    <td>17/08/2556</td>
                                                                    <td>20/08/2556</td>
                                                                    <td>24/08/2556</td>
                                                                    <td><?=$amountWeave?></td>
                                                                    <td class="column-status at_bg_table_orange">(0%) วางแผน</td>
                                                                    <td class="control">
                                                                        <button id="btn-auto-<?=$producId?>" class="btn btn-sm  btn-outline-primary outline btn-thick width-fit" type="button" name="plan-custom" data-toggle="modal" data-target="#modalPlanningCustom" onclick="onPlanItem(this, 'custom')">
                                                                            วางแผน
                                                                        </button>
                                                                        <button id="btn-planning-<?=$producId?>" class="btn btn-sm btn-primary btn-thick width-fit" type="button" name="plan-auto" onclick="onPlanItem(this, 'auto')">
                                                                            อัตโนมัติ
                                                                        </button>
                                                                    </td>
                                                                </tr>
                                                                <?php } ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>

                                        <div class="at_add_box">
                                            <div class="card" aria-expanded="true">
                                                <!-- <div class="card-header at_bg_table_light_gray">
                                                    <div class="card-actions">
                                                            <button type="button" class="card-action card-toggler" title="Collapse" aria-expanded="true"></button>
                                                    </div>
                                                    <strong>เลือกรายการ</strong>
                                                </div> -->
                                                <div class="card-body" style="display: block;">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="at_add_box relative">
                                                                <button class="btn btn-sm btn-primary btn-thick start-plan width-fit save-plan" type="button"  data-toggle="modal" data-target="#modalSavePlanning">
                                                                    <span class="icon icon-save"></span>
                                                                    &nbsp;&nbsp;บันทึกแผน
                                                                </button>
                                                                <div id="scheduler" class="dhx_cal_container" style='width:100%; height:1000px;'>
                                                                    <div class="dhx_cal_navline" style="width:1085px;">
                                                                        <div class="dhx_cal_prev_button">&nbsp;</div>
                                                                        <div class="dhx_cal_next_button">&nbsp;</div>
                                                                        <div class="dhx_cal_today_button" name="today_tab"></div>
                                                                        <div class="dhx_cal_date"></div>
                                                                        <div class="dhx_cal_tab" name="timeline_7days_tab" style="left: 0px;"></div>
                                                                        <div class="dhx_cal_tab" name="timeline_15days_tab" style="left: 70px;"></div>
                                                                        <div class="dhx_cal_tab" name="timeline_30days_tab" style="left: 140px;"></div>
                                                                    </div>
                                                                    <div class="dhx_cal_header">
                                                                    </div>
                                                                    <div class="dhx_cal_data">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
    
?>

<? include('inc.footer.php');?>
<!--  -->
<? include('inc.footer.script.php');?>

<script src="js/planning.js"></script>

<script src="js/jquery/ui/1.12.1/jquery-ui.min.js"></script>
<script src="scheduler/codebase/dhtmlxscheduler.js"></script>
<script src="scheduler/codebase/ext/dhtmlxscheduler_timeline.js"></script>
<script src='scheduler/codebase/dhtmlxscheduler-v5.js' type="text/javascript" charset="utf-8"></script>
<script src='scheduler/codebase/ext/dhtmlxscheduler_timeline.js' type="text/javascript" charset="utf-8"></script>
<script src='scheduler/codebase/ext/dhtmlxscheduler_daytimeline.js' type="text/javascript" charset="utf-8"></script>
<script src='scheduler/codebase/ext/dhtmlxscheduler_treetimeline.js' type="text/javascript" charset="utf-8"></script>



<!-- <script src="js/jquery/ui/1.12.1/jquery-ui.min.js"></script>
<script src="scheduler/codebase/dhtmlxscheduler.js"></script>
<script src="scheduler/codebase/ext/dhtmlxscheduler_timeline.js"></script>
<script src='scheduler/codebase/dhtmlxscheduler-v5.js' type="text/javascript" charset="utf-8"></script>
<script src='scheduler/codebase/ext/dhtmlxscheduler_timeline.js' type="text/javascript" charset="utf-8"></script>
<script src='scheduler/codebase/ext/dhtmlxscheduler_daytimeline.js' type="text/javascript" charset="utf-8"></script>
<script src='scheduler/codebase/ext/dhtmlxscheduler_treetimeline.js' type="text/javascript" charset="utf-8"></script>
 -->

<script src="js/timeline-plan.js"></script>

<script>

</script>
