<?php
    $select = 'open_menu_1';
    $select2 = 'menu_3';
    $select3 = 'menu_3';
    $select4 = 'menu_3_2';
?>
<!--  -->
<? include('inc.header.php');?>
<!--  -->
<? include('inc.navbar.php');?>
<!--  -->
<? include('inc.menu.php');?>
<!-- <div class="layout-main"> -->
<!--  -->
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <h4 class="m-t-0">
                <!-- <span class="d-ib text-danger">--</span> <span class="icon icon-angle-double-right"></span> -->
                <!-- <span class="d-ib">ข้อมูลหลัก</span> -->
                <span class="d-ib text-primary">ข้อมูลหลัก</span> <span class="icon icon-angle-double-right"></span> เครื่องจักร <span class="icon icon-angle-double-right"></span> กลุ่มเครื่องทอ
                <!-- <span class="d-ib">
                    <a class="title-bar-shortcut" href="#" title="Add to shortcut list" data-container="body" data-toggle-text="Remove from shortcut list" data-trigger="hover" data-placement="right" data-toggle="tooltip">
                        <span class="sr-only">Add to shortcut list</span>
                    </a>
                </span> -->
            </h4>
            <!-- <p class="title-bar-description">
                <small>Latest update on 01/01/2017 - 10.57 am.</small>
            </p> -->
        </div>

        <div class="row gutter-xs">
            <div class="col-xs-12">
                <!--  -->
                <div class="card">
                    <!-- <div class="card-header">
                        <a href="javascript:window.history.back(-1);" class="btn btn-sm btn-outline-primary"><span class="icon icon-long-arrow-left"></span>&nbsp;&nbsp;Back</a>&nbsp;&nbsp;
                        <strong class="">Latest update on 01/01/2017 - 10.57 am.</strong>
                    </div> -->
                    <div class="card-body">
                        <!--  -->
                        <? include('inc.home_top_filter.php');?>
                        <!--  -->
                        <div class="table-responsive">
                            <table id="demo-datatables-2" class="table table-striped table-bordered table-nowrap dataTable text-center no-footer" cellspacing="0" width="100%">
                                <thead>
                                    <tr class="at_bg_table_blue">
                                        <th class="text-center">รหัสกลุ่มเครื่องทอ</th>
                                        <th class="text-center">ชื่อกลุ่มเครื่องทอ</th>
                                        <th class="text-center">รหัสสาขา</th>
                                        <th class="text-center">จน. กระสวย</th>
                                        <th class="text-center">มตฐ.กำไร/ค/ว</th>
                                        <th class="text-center">% ปสภ.มตฐ.</th>
                                        <th class="text-center">% ปสภ.คิดมิเตอร์</th>
                                        <th class="text-center">% ปสภ.<br>ในการวางแผน</th>
                                        <th class="text-center">ระยะพิท</th>
                                        <th class="text-center">Flag อวนปั๊ม</th>
                                        <th class="text-center">เริ่มต้นใช้งาน</th>
                                        <th class="text-center">ปรับแก้ไข</th>
                                    </tr>
                                </thead>
                                <tbody>
                                
<tr>
    <td>DH 1</td>
    <td>010 - 012 DK std</td>
    <td>B&S</td>
    <td>561</td>
    <td>3500</td>
    <td>100</td>
    <td>95</td>
    <td>95</td>
    <td>6</td>
    <td>N</td>
    <td>11/13/2009  5:09:08น.</td>
    <td class="text-center" style="display: table-cell;">
          <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_4_1.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

<tr>
    <td>DH 2</td>
    <td>010 - 012 DK std</td>
    <td>B&S</td>
    <td>460</td>
    <td>1800</td>
    <td>0</td>
    <td>0</td>
    <td>0</td>
    <td>8</td>
    <td>N</td>
    <td>28/11/2011 11:47น.</td>
    <td class="text-center" style="display: table-cell;">
          <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_4_1.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

<tr>
    <td>DH10</td>
    <td>021-023</td>
    <td>B&S</td>
    <td>810</td>
    <td>3500</td>
    <td>95</td>
    <td>115</td>
    <td>95</td>
    <td>7.5</td>
    <td>N</td>
    <td>7/12/2016 12:24น.</td>
    <td class="text-center" style="display: table-cell;">
          <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_4_1.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

<tr>
    <td>C1</td>
    <td>9.0DK 033-040  012-015*8,10,12  020*3,4  210/9</td>
    <td>BWC</td>
    <td>430</td>
    <td>4000</td>
    <td>100</td>
    <td>110</td>
    <td>95</td>
    <td>9</td>
    <td>N</td>
    <td>18/09/2017 11:36 น.</td>
    <td class="text-center" style="display: table-cell;">
          <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_4_1.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

<tr>
    <td>C5</td>
    <td>DK12.7 045-055  020*5-8  210/15-18</td>
    <td>BWC</td>
    <td>431</td>
    <td>4500</td>
    <td>100</td>
    <td>110</td>
    <td>95</td>
    <td>0</td>
    <td>N</td>
    <td>26/03/2014 9:29 น.</td>
    <td class="text-center" style="display: table-cell;">
          <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_4_1.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

<tr>
    <td>C6</td>
    <td>DK 19.0  070-117 210/36-48</td>
    <td>BWC</td>
    <td>210</td>
    <td>5000</td>
    <td>100</td>
    <td>110</td>
    <td>95</td>
    <td>22</td>
    <td>N</td>
    <td>8/03/2017 10:13 น.</td>
    <td class="text-center" style="display: table-cell;">
          <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_4_1.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

<tr>
    <td>SK10</td>
    <td>กลุ่ม 013 - 016 >30 ตา</td>
    <td>CY</td>
    <td>461</td>
    <td>1000</td>
    <td>100</td>
    <td>105</td>
    <td>95</td>
    <td>0</td>
    <td>N</td>
    <td>12/10/2010 8:05 น.</td>
    <td class="text-center" style="display: table-cell;">
          <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_4_1.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

<tr>
    <td>SK3</td>
    <td>กลุ่ม 010 <= 30 ตา</td>
    <td>CY</td>
    <td>461</td>
    <td>1000</td>
    <td>100</td>
    <td>105</td>
    <td>95</td>
    <td>0</td>
    <td>N</td>
    <td>24/10/2014 12:09 น.</td>
    <td class="text-center" style="display: table-cell;">
          <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_4_1.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

<tr>
    <td>SK4</td>
    <td>กลุ่ม 010 > 30 ตา</td>
    <td>CY</td>
    <td>461</td>
    <td>1000</td>
    <td>100</td>
    <td>105</td>
    <td>95</td>
    <td>0</td>
    <td>N</td>
    <td>9/01/2015 11:24 น.</td>
    <td class="text-center" style="display: table-cell;">
          <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_4_1.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

<tr>
    <td>CTLS 12.7</td>
    <td>380/12-380/15 ตาถี่ (1.9-2.3 cm)</td>
    <td>FM1</td>
    <td>480</td>
    <td>100</td>
    <td>90</td>
    <td>105</td>
    <td>105</td>
    <td>12.7</td>
    <td>N</td>
    <td>25/10/2013 14:34 น.</td>
    <td class="text-center" style="display: table-cell;">
          <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_4_1.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

<tr>
    <td>ZRS 11.0A</td>
    <td>380/6 ตาห่าง (2.31 cm Up)</td>
    <td>FM1</td>
    <td>630</td>
    <td>100</td>
    <td>90</td>
    <td>105</td>
    <td>105</td>
    <td>11</td>
    <td>N</td>
    <td>9/11/2013 10:06 น.</td>
    <td class="text-center" style="display: table-cell;">
          <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_4_1.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

<tr>
    <td>ZRS 12.6A</td>
    <td>380/9 ตาห่าง (2.31 cm Up)</td>
    <td>FM1</td>
    <td>550</td>
    <td>100</td>
    <td>90</td>
    <td>110</td>
    <td>110</td>
    <td>12.6</td>
    <td>N</td>
    <td>25/10/2013 14:26 น.</td>
    <td class="text-center" style="display: table-cell;">
          <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_4_1.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

<tr>
    <td>EA</td>
    <td>SK 5.334*460</td>
    <td>KKF</td>
    <td>460</td>
    <td>1200</td>
    <td>100</td>
    <td>110</td>
    <td>95</td>
    <td>5.34</td>
    <td>N</td>
    <td>2/08/2011 14:58 น.</td>
    <td class="text-center" style="display: table-cell;">
          <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_4_1.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

<tr>
    <td>EC</td>
    <td>SH7.0,SK (020sk,210/2-3 6-30cm,210/4-6 1.27-4m)</td>
    <td>KKF</td>
    <td>525</td>
    <td>1600</td>
    <td>100</td>
    <td>120</td>
    <td>95</td>
    <td>525</td>
    <td>N</td>
    <td>22/10/2016 11:57 น.</td>
    <td class="text-center" style="display: table-cell;">
          <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_4_1.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

<tr>
    <td>EP1</td>
    <td>เครื่องทออวนปั้ม 110/4,210/2</td>
    <td>KKF1</td>
    <td>1200</td>
    <td>0</td>
    <td>0</td>
    <td>0</td>
    <td>0</td>
    <td>0</td>
    <td>Y</td>
    <td>11/11/2014 11:05 น.</td>
    <td class="text-center" style="display: table-cell;">
          <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_4_1.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

<tr>
    <td>EP3</td>
    <td>เครื่องทอเครื่องทออวนปั้ม 210/5-210/10</td>
    <td>KKF1</td>
    <td>1200</td>
    <td>0</td>
    <td>0</td>
    <td>0</td>
    <td>0</td>
    <td>0</td>
    <td>Y</td>
    <td>8/06/2015 14:39 น.</td>
    <td class="text-center" style="display: table-cell;">
          <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_4_1.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

<tr>
    <td>CAD</td>
    <td>021SK</td>
    <td>NR</td>
    <td>650</td>
    <td>2000</td>
    <td>100</td>
    <td>100</td>
    <td>100</td>
    <td>8</td>
    <td>N</td>
    <td>11/10/2017 15:24 น.</td>
    <td class="text-center" style="display: table-cell;">
          <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_4_1.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

<tr>
    <td>CN</td>
    <td>021- 023 ,024-029(543-800ตา),030(400,650-800ตา)</td>
    <td>NR</td>
    <td>810</td>
    <td>3500</td>
    <td>95</td>
    <td>115</td>
    <td>95</td>
    <td>7.5</td>
    <td>N</td>
    <td>18/09/2017 14:38 น.</td>
    <td class="text-center" style="display: table-cell;">
          <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_4_1.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

                                

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
            </div>
        </div>










    </div>
</div>
<!--  -->
<? include('inc.footer.php');?>
<!--  -->
<? include('inc.footer.script.php');?>
<!--  -->
