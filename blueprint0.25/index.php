<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>ITC - KKFblueprint 50%</title>
    <!--  -->
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">
    <meta name="description" content="ITC - KKFblueprint 50%">
    <meta property="og:url" content="http://">
    <meta property="og:type" content="website">
    <meta property="og:title" content="ITC - KKFblueprint 50%">
    <meta property="og:description" content="ITC - KKFblueprint 50%">
    <meta property="og:image" content="http://">
    <meta name="twitter:card" content="">
    <meta name="twitter:site" content="">
    <meta name="twitter:creator" content="">
    <meta name="twitter:title" content="">
    <meta name="twitter:description" content="">
    <meta name="twitter:image" content="http://">
    <!--  -->
    <link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon.png">
    <link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="manifest.json">
    <link rel="mask-icon" href="safari-pinned-tab.svg" color="#0288d1">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,400italic,500,700">
    <link rel="stylesheet" href="css/vendor.min.css">
    <link rel="stylesheet" href="css/elephant.css">
    <link rel="stylesheet" href="css/application.min.css">
    <link rel="stylesheet" href="css/demo.min.css">
    <link rel="stylesheet" href="css/login-3.min.css">
    <link rel="stylesheet" href="css/at.css">
</head>
<body class="" style=" padding-top: 0; background-image: url(img/bg.jpg); background-size:cover;">
<!--  -->
<div class="login">
    <!-- <div class="">
        <img class="img-responsive" src="img/7943544458.jpg" alt="Elephant">
    </div> -->
    <div class="login-body">
        <!-- <div class="at_login_logo">Smart Factory - Demo</div> -->
        <!-- <a class="login-brand" href="index.html">
            <img class="img-responsive" src="img/logo.png" alt="Elephant">
        </a> -->
        <div class="at_login_logo">โปรแกรมวางแผนการผลิต</div>
        <!-- <h3 class="login-heading">Sign in</h3> -->
        <div class="login-form">
            <form data-toggle="md-validator">
                <div class="md-form-group md-label-floating">
                    <input class="md-form-control" type="text" name="email" spellcheck="false" autocomplete="new-password" data-msg-required="กรุณากรอกรหัสพนักงาน" required>
                    <label class="md-control-label">รหัสพนักงาน</label>
                </div>
                <div class="md-form-group md-label-floating">
                    <input class="md-form-control" type="password" name="password" minlength="6" data-msg-minlength="รหัสผ่านต้องมีความยาวอย่างน้อย 6 ตัวอักษร" data-msg-required="กรุณากรอกรหัสผ่าน" autocomplete="new-password" required>
                    <label class="md-control-label">รหัสผ่าน</label>
                </div>
                
                <button class="btn btn-lg btn-primary btn-block" type="submit" onclick="window.location.href='menu_dashboard.php'">เข้าสู่ระบบ</button>

				<div class="md-form-group md-custom-controls">
                    <a href="forgotpassword.php" style="width: 50%;">ลืมรหัสผ่าน?</a>
					<select class="form-control" style="width: 50%;float: right;">
                        <option value="th">ภาษาไทย</option>
                        <option value="en">English</option>
                        <option value="java">中國</option>
                      </select>
                </div>
            </form>
        </div>
    </div>
    <!-- <div class="login-footer">
        Don't have an account? <a href="signup-3.html">Sign Up</a>
    </div> -->
</div>
<!--  -->
<? include('inc.footer.script.php');?>
<!--  -->
