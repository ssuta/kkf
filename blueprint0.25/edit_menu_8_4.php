<?php
    $select = 'open_menu_1';
    $select2 = 'menu_9';
    $select3 = 'menu_9';
    $select4 = 'menu_8_4';
	
?>
<!--  -->
<? include('inc.header.php');?>
<!--  -->
<? include('inc.navbar.php');?>
<!--  -->
<? include('inc.menu.php');?>
<!-- <div class="layout-main"> -->
<!--  -->
<div class="layout-content">
    <div class="layout-content-body">
		<div class="title-bar">
            <h1 class="title-bar-title">
              <span class="d-ib">มาตรฐานการตอบเป้าหมาย</span>
            </h1>
            <div class="title-bar-description">
				  <span class="d-ib text-primary">การตั้งค่า</span> <span class="icon icon-angle-double-right"></span> ข้อมูลอ้างอิง <span class="icon icon-angle-double-right"></span> มาตรฐานการตอบเป้าหมาย <span class="icon icon-angle-double-right"></span> แก้ไข
			</div>
		</div>

		<div class="row">
		
            <div class="col-md-12">
				 
                <!--  -->
                <div class="demo-form-wrapper">
                        <!--  -->
					<form class="form form-horizontal">
						<div class="panel m-b-lg">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#information" data-toggle="tab" aria-expanded="true">ข้อมูลทั่วไป</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane fade active in" id="information">
								<div class="card" aria-expanded="true">
                            <div class="card-header at_bg_table_light_gray">
                               <div class="card-actions">
                                    <button type="button" class="card-action card-toggler" title="Collapse" aria-expanded="true"></button>
                                    <!-- <button type="button" class="card-action card-reload" title="Reload"></button>
                                    <button type="button" class="card-action card-remove" title="Remove"></button> -->
                                </div>
                                <strong>มาตรฐานการตอบเป้าหมาย</strong>
                            </div>
                            <div class="card-body" style="display: block;">
								<form class="form horizontal">
									<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">สาขา</label>
												<div class="col-sm-4">
													<select id="form-control-6" class="form-control">
														<option value="1">B&S : สารคาม</option>
														<option value="2">BWC : ศูนย์บวร</option>
														<option value="3">CY : เชียงยืน</option>
														<option value="3">EXT : ภายนอก</option>
														<option value="3">XFF : สาขาเมืองจีน</option>
													</select>
												</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="form-control-1">รหัสประเภทการขาย</label>
										<div class="col-sm-4">
											<input id="form-control-1" class="form-control" type="text" value="0">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="form-control-1">จน. วันที่ฝ่ายวางแผนตอบเป้าหมาย</label>
										<div class="col-sm-4">
											<input id="form-control-1" class="form-control" type="text" value="3">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="form-control-1">จน. วันที่ฝ่ายขายอนุมัติแผน</label>
										<div class="col-sm-4">
											<input id="form-control-1" class="form-control" type="text" value="2">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="form-control-1">เริ่มใช้งาน</label>
										<div class="col-sm-4">
											 <div class=" input-with-icon">
											  <input class="form-control" type="text" data-provide="datepicker" value="11/13/2009  5:09:08 PM">
												<span class="icon icon-calendar input-icon"></span>
											 </div>
										</div>
									</div>
								</form>
                            </div>
							</div>
							</div>
							</div>
						</div>
						<div class="m-t text-center">
						<a class="btn btn-lg btn-primary" href="menu_8_4.php" type="submit">บันทึก</a>
                        <a class="btn btn-lg btn-default" href="menu_8_4.php" type="button>">ยกเลิก</a>
						</div>
					</form>
                    
                        <!--  -->
                    </div>
                </div>
                <!--  -->
            </div>
        </div>

    </div>
</div>
<!--  -->
<? include('inc.footer.php');?>
<!--  -->
<? include('inc.footer.script.php');?>
<!--  -->