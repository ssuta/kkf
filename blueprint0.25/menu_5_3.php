<?php
 $select = 'open_menu_1';
    $select2 = 'menu_5';
    $select3 = 'menu_5';
    $select4 = 'menu_5_3';
?>
<!--  -->
<? include('inc.header.php');?>
<!--  -->
<? include('inc.navbar.php');?>
<!--  -->
<? include('inc.menu.php');?>
<!-- <div class="layout-main"> -->
<!--  -->
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <h4 class="m-t-0">
                <!-- <span class="d-ib text-danger">--</span> <span class="icon icon-angle-double-right"></span> -->
                <!-- <span class="d-ib">ข้อมูลหลัก</span> -->
                <span class="d-ib text-primary">ข้อมูลหลัก</span> <span class="icon icon-angle-double-right"></span> กระบวนการผลิต <span class="icon icon-angle-double-right"></span> วงจรการผลิต
                <!-- <span class="d-ib">
                    <a class="title-bar-shortcut" href="#" title="Add to shortcut list" data-container="body" data-toggle-text="Remove from shortcut list" data-trigger="hover" data-placement="right" data-toggle="tooltip">
                        <span class="sr-only">Add to shortcut list</span>
                    </a>
                </span> -->
            </h4>
            <!-- <p class="title-bar-description">
                <small>Latest update on 01/01/2017 - 10.57 am.</small>
            </p> -->
        </div>

        <div class="row gutter-xs">
            <div class="col-xs-12">
                <!--  -->
                <div class="card">
                    <!-- <div class="card-header">
                        <a href="javascript:window.history.back(-1);" class="btn btn-sm btn-outline-primary"><span class="icon icon-long-arrow-left"></span>&nbsp;&nbsp;Back</a>&nbsp;&nbsp;
                        <strong class="">Latest update on 01/01/2017 - 10.57 am.</strong>
                    </div> -->
                    <div class="card-body">
                        <!--  -->
                        <? include('inc.home_top_filter.php');?>
                        <!--  -->
                        <div class="table-responsive">
                          <table id="demo-datatables-2" class="table table-striped table-bordered table-nowrap dataTable text-center no-footer" cellspacing="0" width="100%">
                                                <thead>
                                                    <tr class="at_bg_table_blue">                                        
                                                          <td class="text-center">รหัสสาขา</td>
                                                          <td class="text-center">รหัสประเภทการขาย</td>
                                                          <td class="text-center">รหัสระดับคุณภาพ</td>
                                                          <td class="text-center">ปภ.กลุ่มวัตถุดิบ</td>
                                                          <td class="text-center">รหัสเงื่อน</td>
                                                          <td class="text-center">รหัสประเภทการอบ</td>
                                                          <td class="text-center">รหัสประเภทหูทอ</td>
                                                          <td class="text-center">รหัสประเภทการฉีด</td>
                                                          <td class="text-center">เบอร์ใยต่ำสุด</td>
                                                          <td class="text-center">ขนาดตาสูงสุด</td>
                                                          <td class="text-center">จน. ตา สูงสุด</td>
                                                          <td class="text-center">ความยาวสูงสุด</td>
                                                          <td class="text-center">เริ่มใช้งาน</td>
                                                        <th class="text-center">ปรับแก้ไข</th>
                                                    </tr>
                                                </thead>
                                                <tbody>


                        <tr>
                     <td height=19 align=left style='height:14.25pt'>B&S</td>
                  <td>0</td>
                  <td>G</td>
                  <td>M</td>
                  <td>DK</td>
                  <td>Y</td>
                  <td>G</td>
                  <td>1</td>
                  <td>011 Q</td>
                  <td>99</td>
                  <td>999</td>
                  <td>9999</td>
                  <td>26/01/2018 0:00 น. </td>
                    <td class="text-center" style="display: table-cell;">
                        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_5_3.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                            <span class="icon icon-lg icon-close"></span>
                        </button>
                    </td>
                </tr>

                <!-- 2 -->
                 <tr>
                     <td height=19 align=left style='height:14.25pt'>B&S</td>
                  <td>0</td>
                  <td>A</td>
                  <td>M</td>
                  <td>DK</td>
                  <td>Y</td>
                  <td>G</td>
                  <td>1</td>
                  <td>010 </td>
                  <td>20</td>
                  <td>580</td>
                  <td>999</td>
                  <td>22/02/2018 0:00 น.</td>
                    <td class="text-center" style="display: table-cell;">
                        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_5_3.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                            <span class="icon icon-lg icon-close"></span>
                        </button>
                    </td>
                </tr>
                <!-- 3 -->
                 <tr>
                     <td height=19 align=left style='height:14.25pt'>B&S</td>
                  <td>0</td>
                  <td>A</td>
                  <td>M</td>
                  <td>DK</td>
                  <td>Y</td>
                  <td>G</td>
                  <td>2</td>
                  <td>010 </td>
                  <td>20</td>
                  <td>580</td>
                  <td>999</td>
                  <td>22/02/2018 0:00 น.</td>
                    <td class="text-center" style="display: table-cell;">
                        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_5_3.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                            <span class="icon icon-lg icon-close"></span>
                        </button>
                    </td>
                </tr>
                <!-- 4 -->
                 <tr>
                     <td height=19 align=left style='height:14.25pt'>B&S</td>
                  <td>0</td>
                  <td>C</td>
                  <td>M</td>
                  <td>DK</td>
                  <td>Y</td>
                  <td>G</td>
                  <td>1</td>
                  <td>011 </td>
                  <td>20</td>
                  <td>580</td>
                  <td>999</td>
                  <td>22/02/2018 0:00 น.</td>
                    <td class="text-center" style="display: table-cell;">
                        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_5_3.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                            <span class="icon icon-lg icon-close"></span>
                        </button>
                    </td>
                </tr>
                <!-- 5 -->
                 <tr>
                     <td height=19 align=left style='height:14.25pt'>B&S</td>
                  <td>0</td>
                  <td>C</td>
                  <td>M</td>
                  <td>DK</td>
                  <td>Y</td>
                  <td>G</td>
                  <td>2</td>
                  <td>011 </td>
                  <td>20</td>
                  <td>580</td>
                  <td>999</td>
                  <td>22/02/2018 0:00 น.</td>
                    <td class="text-center" style="display: table-cell;">
                        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_5_3.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                            <span class="icon icon-lg icon-close"></span>
                        </button>
                    </td>
                </tr>
                <!-- 6 -->
                 <tr>
                     <td height=19 align=left style='height:14.25pt'>B&S</td>
                 <td>0</td>
                  <td>G</td>
                  <td>M</td>
                  <td>SK</td>
                  <td>Y</td>
                  <td>G</td>
                  <td>1</td>
                  <td>010 Q </td>
                  <td>20</td>
                  <td>580</td>
                  <td>999</td>
                  <td>22/02/2018 0:00 น.</td>
                    <td class="text-center" style="display: table-cell;">
                        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_5_3.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                            <span class="icon icon-lg icon-close"></span>
                        </button>
                    </td>
                </tr>
                <!-- 7 -->
                 <tr>
                     <td height=19 align=left style='height:14.25pt'>B&S</td>
                 <td>1</td>
                  <td>P</td>
                  <td>M</td>
                  <td>SK</td>
                  <td>T</td>
                  <td>G</td>
                  <td>1</td>
                  <td>016</td>
                  <td>99</td>
                  <td>99</td>
                  <td>9999</td>
                  <td>26/02/2018 0:00 น. </td>
                    <td class="text-center" style="display: table-cell;">
                        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_5_3.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                            <span class="icon icon-lg icon-close"></span>
                        </button>
                    </td>
                </tr>
                <!-- 8 -->
                 <tr>
                     <td height=19 align=left style='height:14.25pt'>B&S</td>
                    <td>1</td>
                  <td>S</td>
                  <td>M</td>
                  <td>SK</td>
                  <td>T</td>
                  <td>G</td>
                  <td>1</td>
                  <td>016</td>
                  <td>99</td>
                  <td>999</td>
                  <td>9999</td>
                  <td>26/02/2018 0:00 น. </td>
                    <td class="text-center" style="display: table-cell;">
                        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_5_3.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                            <span class="icon icon-lg icon-close"></span>
                        </button>
                    </td>
                </tr>
               <!-- 9 -->
                 <tr>
                     <td height=19 align=left style='height:14.25pt'>B&S</td>
                  <td>0</td>
                  <td>G</td>
                  <td>M</td>
                  <td>DK</td>
                  <td>Y</td>
                  <td>G</td>
                  <td>1</td>
                  <td>010 Q</td>
                  <td>99</td>
                  <td>999</td>
                  <td>9999</td>
                  <td>1/03/2018 0:00 น.</td>
                    <td class="text-center" style="display: table-cell;">
                        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_5_3.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                            <span class="icon icon-lg icon-close"></span>
                        </button>
                    </td>
                </tr>
                <!-- 10 -->
                 <tr>
                     <td height=19 align=left style='height:14.25pt'>B&S</td>
                  <td>1</td>
                  <td>P</td>
                  <td>M</td>
                  <td>SK</td>
                  <td>T</td>
                  <td>G</td>
                  <td>1</td>
                  <td>016</td>
                  <td>99</td>
                  <td>999</td>
                  <td>9999</td>
                  <td>3/03/2018 0:00 น.</td>
                    <td class="text-center" style="display: table-cell;">
                        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_5_3.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                            <span class="icon icon-lg icon-close"></span>
                        </button>
                    </td>
                </tr>
                11
                 <tr>
                     <td height=19 align=left style='height:14.25pt'>B&S</td>
                  <td>1</td>
                  <td>G</td>
                  <td>M</td>
                  <td>DK</td>
                  <td>Y</td>
                  <td>G</td>
                  <td>1</td>
                  <td>020 Q</td>
                  <td>99</td>
                  <td>999</td>
                  <td>9999</td>
                  <td>9/03/2018 0:00 น.</td>
                    <td class="text-center" style="display: table-cell;">
                        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_5_3.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                            <span class="icon icon-lg icon-close"></span>
                        </button>
                    </td>
                </tr>
                12
                 <tr>
                     <td height=19 align=left style='height:14.25pt'>B&S</td>
                  <td>1</td>
                  <td>G</td>
                  <td>M</td>
                  <td>DK</td>
                  <td>Y</td>
                  <td>G</td>
                  <td>1</td>
                  <td>019</td>
                  <td>99</td>
                  <td>999</td>
                  <td>9999</td>
                  <td>12/03/2018 0:00 น.</td>
                    <td class="text-center" style="display: table-cell;">
                        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_5_3.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                            <span class="icon icon-lg icon-close"></span>
                        </button>
                    </td>
                </tr>
                <!-- 13 -->
                 <tr>
                     <td height=19 align=left style='height:14.25pt'>B&S</td>
                  <td>1</td>
                  <td>P</td>
                  <td>M</td>
                  <td>DK</td>
                  <td>Y</td>
                  <td>G</td>
                  <td>1</td>
                  <td>019</td>
                  <td>99</td>
                  <td>999</td>
                  <td>9999</td>
                  <td>12/03/2018 0:00 น.</td>
                    <td class="text-center" style="display: table-cell;">
                        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_5_3.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                            <span class="icon icon-lg icon-close"></span>
                        </button>
                    </td>
                </tr>
                <!-- 14 -->
                 <tr>
                     <td height=19 align=left style='height:14.25pt'>B&S</td>
                  <td>0</td>
                  <td>G</td>
                  <td>M</td>
                  <td>SK</td>
                  <td>Y</td>
                  <td>S</td>
                  <td>1</td>
                  <td>011 Q</td>
                  <td>99</td>
                  <td>999</td>
                  <td>9999</td>
                  <td>24/03/2018 0:00 น.</td>
                    <td class="text-center" style="display: table-cell;">
                        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_5_3.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                            <span class="icon icon-lg icon-close"></span>
                        </button>
                    </td>
                </tr>
                <!-- 15 -->
                 <tr>
                     <td height=19 align=left style='height:14.25pt'>B&S</td>
                   <td>0</td>
                  <td>G</td>
                  <td>M</td>
                  <td>SK</td>
                  <td>Y</td>
                  <td>G</td>
                  <td>1</td>
                  <td>011 Q</td>
                  <td>99</td>
                  <td>999</td>
                  <td>9999</td>
                  <td>28/03/2018 0:00 น.</td>
                    <td class="text-center" style="display: table-cell;">
                        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_5_3.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                            <span class="icon icon-lg icon-close"></span>
                        </button>
                    </td>
                </tr>
                <!-- 16 -->
                 <tr>
                     <td height=19 align=left style='height:14.25pt'>B&S</td>
                  <td>0</td>
                  <td>G</td>
                  <td>M</td>
                  <td>DK</td>
                  <td>Y</td>
                  <td>G</td>
                  <td>2</td>
                  <td>015 Q</td>
                  <td>99</td>
                  <td>999</td>
                  <td>9999</td>
                  <td>3/04/2018 0:00 น.</td>
                    <td class="text-center" style="display: table-cell;">
                        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_5_3.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                            <span class="icon icon-lg icon-close"></span>
                        </button>
                    </td>
                </tr>
                <!-- 17 -->
                 <tr>
                     <td height=19 align=left style='height:14.25pt'>B&S</td>
                  <td>0</td>
                  <td>P</td>
                  <td>M</td>
                  <td>DK</td>
                  <td>Y</td>
                  <td>G</td>
                  <td>2</td>
                  <td>015 Q</td>
                  <td>99</td>
                  <td>999</td>
                  <td>9999</td>
                  <td>3/04/2018 0:00 น.</td>
                    <td class="text-center" style="display: table-cell;">
                        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_5_3.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                            <span class="icon icon-lg icon-close"></span>
                        </button>
                    </td>
                </tr>
                                          </tbody>
                                            </table>
                        </div>
                    </div>
                </div>
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
            </div>
        </div>
    </div>
</div>
<!--  -->
<? include('inc.footer.php');?>
<!--  -->
<? include('inc.footer.script.php');?>
<!--  -->
<div id="menu_5_3_edit" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">แก้ไข</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-6 m-b">
                            <label>รหัสสาขา</label>
                            <input class="form-control" type="text" placeholder="นางสาวตะข่าย">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>รหัสประเภทการขาย</label>
                            <input class="form-control" type="text" placeholder="กว้างขวาง">
                        </div>
						 <div class="col-xs-6 m-b">
                            <label>รหัสระดับคุณภาพ</label>
                            <input class="form-control" type="text" placeholder="การตลาด">
                        </div>
						 <div class="col-xs-6 m-b">
                            <label>ป</label>
                            <input class="form-control" type="text" placeholder="ผู้จัดการ">
                        </div>

						 <div class="col-xs-6 m-b">
                            <label>สิทธิการใช้งาน</label>
                           <select class="custom-select">
                        <option value="" selected="">Administrator</option>
                        <option value="c-plus-plus">Manager</option>
                        <option value="css">Customer</option>
                      </select>
                        </div>

						 <div class="col-xs-6 m-b">
                            <label>สถานะการใช้งาน</label>
							<div class="custom-controls-stacked m-t">
                      <label class="switch switch-primary">
                        <input class="switch-input" type="checkbox" checked="checked">
                        <span class="switch-track"></span>
                        <span class="switch-thumb"></span>
                      </label>
                      </div>
                        </div>

                    </div>
                    <div class="m-t text-center">
						<button onclick="myFunction()" class="btn btn-lg btn-primary" data-dismiss="modal" type="button">บันทึก</button>
                        <button class="btn btn-lg btn-default" data-dismiss="modal" type="button">ยกเลิก</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--  -->
