<?php
    $select = 'open_menu_3';
    $select2 = 'menu_maintenance_3';
    
?>
<!--  -->
<? include('inc.header.php');?>
<!--  -->
<? include('inc.navbar.php');?>
<!--  -->
<? include('inc.menu.php');?>
<!-- <div class="layout-main"> -->
<!--  -->
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <h4 class="m-t-0">
                <!-- <span class="d-ib text-danger">--</span> <span class="icon icon-angle-double-right"></span> -->
                <!-- <span class="d-ib">ข้อมูลหลัก</span> -->
                <span class="d-ib text-primary">การซ่อมบำรุง</span> <span class="icon icon-angle-double-right"></span> วางแผนซ่อมบำรุง
                <!-- <span class="d-ib">
                    <a class="title-bar-shortcut" href="#" title="Add to shortcut list" data-container="body" data-toggle-text="Remove from shortcut list" data-trigger="hover" data-placement="right" data-toggle="tooltip">
                        <span class="sr-only">Add to shortcut list</span>
                    </a>
                </span> -->
            </h4>
            <!-- <p class="title-bar-description">
                <small>Latest update on 01/01/2017 - 10.57 am.</small>
            </p> -->
        </div>

        <div class="row gutter-xs">
            <div class="col-xs-12">
                <!--  -->
                <div class="card">
                    <!-- <div class="card-header">
                        <a href="javascript:window.history.back(-1);" class="btn btn-sm btn-outline-primary"><span class="icon icon-long-arrow-left"></span>&nbsp;&nbsp;Back</a>&nbsp;&nbsp;
                        <strong class="">Latest update on 01/01/2017 - 10.57 am.</strong>
                    </div> -->
                    <div class="card-body">
                        <!--  -->
                        <? include('inc.home_top_filter.php');?>
                        <!--  -->
                        <div class="table-responsive">
                            <table id="demo-datatables-2" class="table table-striped table-bordered table-nowrap dataTable text-center no-footer" cellspacing="0" width="100%">
                                <thead>
                                    <tr class="at_bg_table_blue">
                                        <th rowspan="2" class="text-center">รหัสเครื่อง</th>
                                        <th rowspan="2" class="text-center">ประเภทงาน</th>
                                        <th rowspan="2" class="text-center">รายละเอียด</th>
                                        <th rowspan="2" class="text-center">จอดเครื่อง</th>
                                        <th colspan="4" class="text-center">แผนซ่อมบำรุง</th>
                                        <th rowspan="2" class="text-center">สถานะวางแผน</th>
                                        <th rowspan="2" class="text-center">สถานะ</th>
                                        <th rowspan="2" class="text-center">ปรับแก้ไข</th>
                                    </tr>
									 <tr class="at_bg_table_blue">
                                        <th class="text-center">วันที่</th>
                                        <th class="text-center">เวลาเริ่ม</th>
                                        <th class="text-center">เวลาเสร็จ</th>
                                        <th class="text-center">กำลังคน</th>
                                    </tr>
                                </thead>
                                <tbody>
								<tr>
    <td>A001</td>
    <td>เปลี่ยนอะไหล่อุปกรณ์ตามรอบ</td>
    <td>เปลี่ยนตะขอล่าง</td>
    <td>จอด</td>
    <td>25/08/2561</td>
    <td>08:00</td>
    <td>15:00</td>
    <td>1</td> 
    <td>ยังไม่ได้ปรับแผน</td>
    <td>ยังไม่แล้วเสร็จ</td>
    <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_6_3.php" type="button">
<span class="icon icon-lg icon-edit"></span>
</a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>


<tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td> 
    <td>ยังไม่ได้ปรับแผน</td>
    <td>ยังไม่แล้วเสร็จ</td>
    <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_6_3.php" type="button">
<span class="icon icon-lg icon-edit"></span>
</a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

<tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td> 
    <td>ยังไม่ได้ปรับแผน</td>
    <td>ยังไม่แล้วเสร็จ</td>
    <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_6_3.php" type="button">
<span class="icon icon-lg icon-edit"></span>
</a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>


<tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td> 
    <td>ยังไม่ได้ปรับแผน</td>
    <td>ยังไม่แล้วเสร็จ</td>
    <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_6_3.php" type="button">
<span class="icon icon-lg icon-edit"></span>
</a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

<tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td> 
    <td>ยังไม่ได้ปรับแผน</td>
    <td>ยังไม่แล้วเสร็จ</td>
    <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_6_3.php" type="button">
<span class="icon icon-lg icon-edit"></span>
</a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

<tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td> 
    <td>ยังไม่ได้ปรับแผน</td>
    <td>ยังไม่แล้วเสร็จ</td>
    <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_6_3.php" type="button">
<span class="icon icon-lg icon-edit"></span>
</a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

<tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td> 
    <td>ยังไม่ได้ปรับแผน</td>
    <td>ยังไม่แล้วเสร็จ</td>
    <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_6_3.php" type="button">
<span class="icon icon-lg icon-edit"></span>
</a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

<tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td> 
    <td>ยังไม่ได้ปรับแผน</td>
    <td>ยังไม่แล้วเสร็จ</td>
    <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_6_3.php" type="button">
<span class="icon icon-lg icon-edit"></span>
</a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

<tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td> 
    <td>ปรับแผนแล้ว</td>
    <td>ยังไม่แล้วเสร็จ</td>
    <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_6_3.php" type="button">
<span class="icon icon-lg icon-edit"></span>
</a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

<tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td> 
    <td>ปรับแผนแล้ว</td>
    <td>ยังไม่แล้วเสร็จ</td>
    <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_6_3.php" type="button">
<span class="icon icon-lg icon-edit"></span>
</a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

<tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td> 
    <td>ปรับแผนแล้ว</td>
    <td>ยังไม่แล้วเสร็จ</td>
    <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_6_3.php" type="button">
<span class="icon icon-lg icon-edit"></span>
</a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

<tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td> 
    <td>ปรับแผนแล้ว</td>
    <td>ยังไม่แล้วเสร็จ</td>
    <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_6_3.php" type="button">
<span class="icon icon-lg icon-edit"></span>
</a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

<tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td> 
    <td>ปรับแผนแล้ว</td>
    <td>ยังไม่แล้วเสร็จ</td>
    <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_6_3.php" type="button">
<span class="icon icon-lg icon-edit"></span>
</a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

<tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td> 
    <td>ปรับแผนแล้ว</td>
    <td>สำเร็จ</td>
    <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_6_3.php" type="button">
<span class="icon icon-lg icon-edit"></span>
</a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

<tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td> 
    <td>ปรับแผนแล้ว</td>
    <td>สำเร็จ</td>
    <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_6_3.php" type="button">
<span class="icon icon-lg icon-edit"></span>
</a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

<tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td> 
    <td>ปรับแผนแล้ว</td>
    <td>สำเร็จ</td>
    <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_6_3.php" type="button">
<span class="icon icon-lg icon-edit"></span>
</a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

<tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td> 
    <td>ปรับแผนแล้ว</td>
    <td>สำเร็จ</td>
    <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_6_3.php" type="button">
<span class="icon icon-lg icon-edit"></span>
</a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

<tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td> 
    <td>ปรับแผนแล้ว</td>
    <td>สำเร็จ</td>
    <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_6_3.php" type="button">
<span class="icon icon-lg icon-edit"></span>
</a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

<tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td> 
    <td>ปรับแผนแล้ว</td>
    <td>สำเร็จ</td>
    <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_6_3.php" type="button">
<span class="icon icon-lg icon-edit"></span>
</a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
            </div>
        </div>










    </div>
</div>
<!--  -->
<? include('inc.footer.php');?>
<!--  -->
<? include('inc.footer.script.php');?>
<!--  -->
