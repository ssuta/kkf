<?php
    $select = 'open_menu_1';
    $select2 = 'menu_9';
    $select3 = 'menu_9';
    $select4 = 'menu_8_4';
	
?>
<!--  -->
<? include('inc.header.php');?>
<!--  -->
<? include('inc.navbar.php');?>
<!--  -->
<? include('inc.menu.php');?>
<!-- <div class="layout-main"> -->
<!--  -->
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <h4 class="m-t-0">
                <!-- <span class="d-ib text-danger">--</span> <span class="icon icon-angle-double-right"></span> -->
                <!-- <span class="d-ib">ข้อมูลหลัก</span> -->
                <span class="d-ib text-primary">การตั้งค่า</span> <span class="icon icon-angle-double-right"></span> ข้อมูลอ้างอิง <span class="icon icon-angle-double-right"></span> มาตรฐานการตอบเป้าหมาย
                <!-- <span class="d-ib">
                    <a class="title-bar-shortcut" href="#" title="Add to shortcut list" data-container="body" data-toggle-text="Remove from shortcut list" data-trigger="hover" data-placement="right" data-toggle="tooltip">
                        <span class="sr-only">Add to shortcut list</span>
                    </a>
                </span> -->
            </h4>
            <!-- <p class="title-bar-description">
                <small>Latest update on 01/01/2017 - 10.57 am.</small>
            </p> -->
        </div>

        <div class="row gutter-xs">
            <div class="col-xs-12">
                <!--  -->
                <div class="card">
                    <!-- <div class="card-header">
                        <a href="javascript:window.history.back(-1);" class="btn btn-sm btn-outline-primary"><span class="icon icon-long-arrow-left"></span>&nbsp;&nbsp;Back</a>&nbsp;&nbsp;
                        <strong class="">Latest update on 01/01/2017 - 10.57 am.</strong>
                    </div> -->
                    <div class="card-body">
                        <!--  -->
                        <? include('inc.home_top_filter.php');?>
                        <!--  -->
                        <div class="table-responsive">
                           <table id="demo-datatables-2" class="table table-striped table-bordered table-nowrap dataTable text-center no-footer" cellspacing="0" width="100%">
                                <thead>
                                    <tr class="at_bg_table_blue">
                                        <th class="text-center">รหัสสาขา</th>
                                        <th class="text-center">ชื่อสาขา</th>
                                        <th class="text-center">รหัสประเภทการขาย</th>
                                        <th class="text-center">จน. วันที่ฝ่ายวางแผนตอบเป้าหมาย</th>
                                        <th class="text-center">จน. วันที่ฝ่ายขายอนุมัติแผน</th>
                                        <th class="text-center">เริ่มใช้งาน</th>
                                        <th class="text-center">ปรับแก้ไข</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                    <tr>
                                        <td>B&S</td>
                                        <td>สารคาม</td>
                                        <td>0</td>
                                        <td>3</td>
                                        <td>2</td>
                                        <td>23/02/2015 0:00 น.</td>
                                        <td class="text-center" style="display: table-cell;">
                                            <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_8_4.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                                            <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                                                <span class="icon icon-lg icon-close"></span>
                                            </button>
                                        </td>
                                    </tr>
                                    <!-- 2 -->
                                    <tr>
                                        <td>B&S</td>
                                        <td>สารคาม</td>
                                        <td>1</td>
                                        <td>5</td>
                                        <td>2</td>
                                       <td> 23/02/2015 0:00 น</td>

                                        <td class="text-center" style="display: table-cell;">
                                            <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_8_4.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                                            <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                                                <span class="icon icon-lg icon-close"></span>
                                            </button>
                                        </td>
                                    </tr>
                                    <!-- 3 -->
                                    <tr>
                                        <td>BWC</td>
                                        <td>ศูนย์บวร</td>
                                        <td>0</td>
                                        <td>3</td>
                                        <td>2</td>
                                        <td>27/02/2015 0:00 น.</td>
                                        <td class="text-center" style="display: table-cell;">
                                            <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_8_4.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                                            <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                                                <span class="icon icon-lg icon-close"></span>
                                            </button>
                                        </td>
                                    </tr>
                                    <!-- 4 -->
                                    <tr>
                                      <td>BWC</td>
                                        <td>ศูนย์บวร</td>
                                        <td>1</td>
                                        <td>5</td>
                                        <td>2</td>
                                        <td>27/02/2015 0:00 น.</td>
                                        <td class="text-center" style="display: table-cell;">
                                            <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_8_4.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                                            <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                                                <span class="icon icon-lg icon-close"></span>
                                            </button>
                                        </td>
                                    </tr>
                                    <!-- 5 -->
                                    <tr>
                                        <td>CY</td>
                                        <td>เชียงยืน</td>
                                        <td>0</td>
                                        <td>3</td>
                                        <td>2</td>
                                        <td>27/02/2015 0:00 น.</td>
                                        <td class="text-center" style="display: table-cell;">
                                            <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_8_4.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                                            <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                                                <span class="icon icon-lg icon-close"></span>
                                            </button>
                                        </td>
                                    </tr>
                                    <!-- 6 -->
                                    <tr>
                                       <td>CY</td>
                                        <td>เชียงยืน</td>
                                        <td>1</td>
                                        <td>5</td>
                                        <td>2</td>
                                        <td>27/02/2015 0:00 น.</td>
                                        <td class="text-center" style="display: table-cell;">
                                            <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_8_4.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                                            <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                                                <span class="icon icon-lg icon-close"></span>
                                            </button>
                                        </td>
                                    </tr>
                                    <!-- 7 -->
                                    <tr>
                                        <td>FM</td>
                                        <td>FM ด้ายโพลี</td>
                                        <td>0</td>
                                        <td>3</td>
                                        <td>2</td>
                                         <td>23/02/2015 0:00 น.</td>
                                        <td class="text-center" style="display: table-cell;">
                                            <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_8_4.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                                            <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                                                <span class="icon icon-lg icon-close"></span>
                                            </button>
                                        </td>
                                    </tr>
                                    <!-- 8 -->
                                    <tr>
                                        <td>FM</td>
                                        <td>FM ด้ายโพลี</td>
                                        <td>1</td>
                                        <td>5</td>
                                        <td>2</td>
                                         <td>23/02/2015 0:00 น.</td>
                                        <td class="text-center" style="display: table-cell;">
                                            <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_8_4.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                                            <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                                                <span class="icon icon-lg icon-close"></span>
                                            </button>
                                        </td>
                                    </tr>
                                    <!-- 9 -->
                                    <tr>
                                       <td>FM 1</td>
                                        <td>FM ด้ายโพลี</td>
                                        <td>0</td>
                                        <td>3</td>
                                        <td>2</td>
                                         <td>23/02/2015 0:00 น.</td>
                                        <td class="text-center" style="display: table-cell;">
                                            <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_8_4.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                                            <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                                                <span class="icon icon-lg icon-close"></span>
                                            </button>
                                        </td>
                                    </tr>
                                    <!-- 10 -->
                                    <tr>
                                        <td>FM 1</td>
                                        <td>FM ด้ายโพลี</td>
                                        <td>1</td>
                                        <td>5</td>
                                        <td>2</td>
                                         <td>23/02/2015 0:00 น.</td>
                                        <td class="text-center" style="display: table-cell;">
                                            <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_8_4.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                                            <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                                                <span class="icon icon-lg icon-close"></span>
                                            </button>
                                        </td>
                                    </tr>
                                    <!-- 11 -->
                                    <tr>
                                        <td>KKF</td>
                                        <td>เค เค เอฟ</td>
                                        <td>0</td>
                                        <td>5</td>
                                        <td>2</td>
                                         <td>27/02/2015 0:00 น.</td>
                                        <td class="text-center" style="display: table-cell;">
                                            <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_8_4.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                                            <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                                                <span class="icon icon-lg icon-close"></span>
                                            </button>
                                        </td>
                                    </tr>
                                    <!-- 12 -->
                                    <tr>
                                         <td>KKF</td>
                                        <td>เค เค เอฟ</td>
                                        <td>1</td>
                                        <td>5</td>
                                        <td>2</td>
                                         <td>27/02/2015 0:00 น.</td>
                                        <td class="text-center" style="display: table-cell;">
                                            <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_8_4.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                                            <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                                                <span class="icon icon-lg icon-close"></span>
                                            </button>
                                        </td>
                                    </tr>
                                    <!-- 13 -->
                                    <tr>
                                          <td>KKF1</td>
                                        <td>อวนปั๊ม</td>
                                        <td>0</td>
                                        <td>5</td>
                                        <td>2</td>
                                         <td>23/02/2015 0:00 น.</td>
                                        <td class="text-center" style="display: table-cell;">
                                            <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_8_4.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                                            <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                                                <span class="icon icon-lg icon-close"></span>
                                            </button>
                                        </td>
                                    </tr>
                                    <!-- 14 -->
                                    <tr>
                                         <td>KKF1</td>
                                        <td>อวนปั๊ม</td>
                                        <td>1</td>
                                        <td>5</td>
                                        <td>2</td>
                                         <td>23/02/2015 0:00 น.</td>
                                        <td class="text-center" style="display: table-cell;">
                                            <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_8_4.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                                            <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                                                <span class="icon icon-lg icon-close"></span>
                                            </button>
                                        </td>
                                    </tr>
                                    <!-- 15 -->
                                    <tr>
                                        <td>KKF2</td>
                                        <td>สายเอ็น/ด้ายในล่อน</td>
                                        <td>0</td>
                                        <td>5</td>
                                        <td>2</td>
                                         <td>23/02/2015 0:00 น.</td>
                                        <td class="text-center" style="display: table-cell;">
                                            <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_8_4.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                                            <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                                                <span class="icon icon-lg icon-close"></span>
                                            </button>
                                        </td>
                                    </tr>
                                    <!-- 16 -->
                                    <tr>
                                         <td>KKF2</td>
                                        <td>สายเอ็น/ด้ายในล่อน</td>
                                        <td>1</td>
                                        <td>5</td>
                                        <td>2</td>
                                         <td>23/02/2015 0:00 น.</td>
                                        <td class="text-center" style="display: table-cell;">
                                            <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_8_4.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                                            <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                                                <span class="icon icon-lg icon-close"></span>
                                            </button>
                                        </td>
                                    </tr>
                                    <!-- 17 -->
                                    <tr>
                                         <td>KKF4</td>
                                        <td>อวนรุม/อวนแปรรูป</td>
                                        <td>0</td>
                                        <td>7</td>
                                        <td>2</td>
                                         <td>23/02/2015 0:00 น.</td>
                                        <td class="text-center" style="display: table-cell;">
                                            <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_8_4.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                                            <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                                                <span class="icon icon-lg icon-close"></span>
                                            </button>
                                        </td>
                                    </tr>
                                    <!-- 18 -->
                                    <tr>
                                        <td>KKF4</td>
                                        <td>อวนรุม/อวนแปรรูป</td>
                                        <td>1</td>
                                        <td>7</td>
                                        <td>2</td>
                                         <td>23/02/2015 0:00 น.</td>
                                        <td class="text-center" style="display: table-cell;">
                                            <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_8_4.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                                            <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                                                <span class="icon icon-lg icon-close"></span>
                                            </button>
                                        </td>
                                    </tr>
                                    <!-- 19 -->
                                    <tr>
                                        <td>NR</td>
                                        <td>หนองเรือ</td>
                                        <td>0</td>
                                        <td>3</td>
                                        <td>2</td>
                                         <td>27/02/2015 0:00 น.</td>
                                        <td class="text-center" style="display: table-cell;">
                                            <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_8_4.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                                            <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                                                <span class="icon icon-lg icon-close"></span>
                                            </button>
                                        </td>
                                    </tr>
                                    <!-- 20 -->
                                    <tr>
                                        <td>NR</td>
                                        <td>หนองเรือ</td>
                                        <td>0</td>
                                        <td>3</td>
                                        <td>2</td>
                                         <td>27/02/2015 0:00 น.</td>
                                        <td class="text-center" style="display: table-cell;">
                                            <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_8_4.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                                            <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                                                <span class="icon icon-lg icon-close"></span>
                                            </button>
                                        </td>
                                    </tr>


                            
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
            </div>
        </div>










    </div>
</div>
<!--  -->
<? include('inc.footer.php');?>
<!--  -->
<? include('inc.footer.script.php');?>
<!--  -->
