<?php
    $select = 'open_menu_1';
    $select2 = 'menu_1';
    $select3 = 'menu_1';
    $select4 = 'menu_1_1';
    $select5 = 'menu_all';
?>
<!--  -->
<? include('inc.header.php');?>
<!--  -->
<? include('inc.navbar.php');?>
<!--  -->
<? include('inc.menu.php');?>
<!-- <div class="layout-main"> -->
<!--  -->
<div class="layout-content">
    <div class="layout-content-body">
		<div class="title-bar">
            <h1 class="title-bar-title">
              <span class="d-ib">สาขา/โรงทอ</span>
            </h1>
            <div class="title-bar-description">
				<span class="d-ib text-primary">การตั้งค่า</span> <span class="icon icon-angle-double-right"></span> สาขา/โรงทอ <span class="icon icon-angle-double-right"></span> แก้ไข
			</div>
		</div>
        <div class="row">
		
            <div class="col-md-12">
				 
                <!--  -->
                <div class="demo-form-wrapper">
				 <form class="form form-horizontal">
				<div class="panel m-b-lg">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#home-11" data-toggle="tab" aria-expanded="true">ข้อมูลหลัก</a></li>
                                <li class=""><a href="#profile-11" data-toggle="tab" aria-expanded="false">ข้อมูลอ้างอิง</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane fade active in" id="home-11">
								<div class="card" aria-expanded="true">
                            <div class="card-header at_bg_table_light_gray">
                               <div class="card-actions">
                                    <button type="button" class="card-action card-toggler" title="Collapse" aria-expanded="true"></button>
                                    <!-- <button type="button" class="card-action card-reload" title="Reload"></button>
                                    <button type="button" class="card-action card-remove" title="Remove"></button> -->
                                </div>
                                <strong>สาขา</strong>
                            </div>
                            <div class="card-body" style="display: block;">
								<form class="form horizontal">
									<div class="form-group">
										<label class="col-sm-2 control-label" for="form-control-1">รหัสสาขา</label>
										<div class="col-sm-4">
											<input id="form-control-1" class="form-control" type="text" value="B&S">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="form-control-1">ชื่อสาขา</label>
										<div class="col-sm-4">
											<input id="form-control-1" class="form-control" type="text" value="สารคาม">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="form-control-1">รหัสสาขา PROFIT</label>
										<div class="col-sm-4">
											<input id="form-control-1" class="form-control" type="text" value="CY">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="form-control-1">ชื่อ สาขา PROFIT</label>
										<div class="col-sm-4">
											<input id="form-control-1" class="form-control" type="text" value="เชียงยืน">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="form-control-1">Flag สาขาภายนอก</label>
										<div class="col-sm-4">
											<input id="form-control-1" class="form-control" type="text" value="N">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="form-control-1">สาขารูดบัตร</label>
										<div class="col-sm-4">
											<input id="form-control-1" class="form-control" type="text" value="M">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="form-control-1">เริ่มใช้งาน</label>
										<div class="col-sm-4">
											 <div class=" input-with-icon">
											  <input class="form-control" type="text" data-provide="datepicker" value="11/13/2009  5:09:08 PM">
												<span class="icon icon-calendar input-icon"></span>
											 </div>
										</div>
									</div>
								</form>
                            </div>
							</div>
								

								<!-- -->
							<!-- -->
                        </div>
                                   
                                <div class="tab-pane fade" id="profile-11">
								<div class="card" aria-expanded="true">
										<div class="card-header at_bg_table_light_gray">
											<div class="card-actions">
												 <button type="button" class="card-action card-toggler" title="Collapse" aria-expanded="true"></button>
												 <!-- <button type="button" class="card-action card-reload" title="Reload"></button>
												 <button type="button" class="card-action card-remove" title="Remove"></button> -->
											</div>
											<strong>ข้อมูลอ้างอิง</strong>
										</div>
										<div class="card-body" style="display: block;">
										<form class="form horizontal">
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">(prd_bill)</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="0">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">(BRN_PRD)</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="L">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">(BRN_PHYSICAL)</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="K">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">(BRN_PERCENADJ)</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="K">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">(prd_billStock)</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="HA">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">(LOCATDOC)</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="S">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">Database</label>
												<div class="col-sm-4">
													<input id="form-control-1" class="form-control" type="text" value="BS_DB">
												</div>
											</div>
										</form>
										</div>
									</div>
                                </div>
                            </div>
                        </div>
						 <div class="m-t text-center">
						<a class="btn btn-lg btn-primary" href="menu_1_1.php" type="submit">บันทึก</a>
                        <a class="btn btn-lg btn-default" href="menu_1_1.php" type="button>">ยกเลิก</a>
                    </div>
					
                </div>
				</form>
            </div>
        </div>










    </div>
</div>
<!--  -->
<? include('inc.footer.php');?>
<!--  -->
<? include('inc.footer.script.php');?>
<!--  -->