<?php
$select = 'open_menu_8';
$select2 = 'menu_us_8_1';
$select3 = 'menu_us_8_1';
$select4 = 'menu_us_8_1';
?>
    <!--  -->
    <?include 'inc.header.php';?>
        <!--  -->
        <?include 'inc.navbar.php';?>
            <!--  -->
            <?include 'inc.menu.php';?>
                <!-- <div class="layout-main"> -->
                <!--  -->
                <div class="layout-content">
                    <div class="layout-content-body">
                        <div class="title-bar">
                            <h1 class="title-bar-title">
              <span class="d-ib">แก้ไขข้อมูลส่วนตัว</span>
            </h1>
                            <div class="title-bar-description">
                                <span class="d-ib text-primary">ผู้ใช้งาน</span> <span class="icon icon-angle-double-right"></span> แก้ไขข้อมูลส่วนตัว
                            </div>
                        </div>
						<div class="row">
		
            <div class="col-md-12 add-padding-top">
				 
                <!--  -->
                <div class="demo-form-wrapper">
                        <!--  -->
					<form class="form form-horizontal">
						<div class="panel m-b-lg a-p-t a-p-b">
                            <div class="card-body" style="display: block;">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-5 control-label" for="form-control-1">รหัสพนักงาน</label>
                                        <div class="col-sm-7">
                                            <input id="form-control-1" class="form-control" type="text" value="MK001">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-5 control-label" for="form-control-1">อีเมล์</label>
                                        <div class="col-sm-7">
                                            <input id="form-control-1" class="form-control" type="text" value="test@kkf.com">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-5 control-label" for="form-control-1">รหัสผ่าน</label>
                                        <div class="col-sm-7">
                                            <input id="form-control-1" class="form-control" type="password" value="1234">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-5 control-label" for="form-control-1">ชื่อ</label>
                                        <div class="col-sm-7">
                                            <input id="form-control-1" class="form-control" type="text" value="ชื่อพนักงาน">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-5 control-label" for="form-control-1">นามสกุล</label>
                                        <div class="col-sm-7">
                                            <input id="form-control-1" class="form-control" type="text" value="ทดสอบระบบ">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-5 control-label" for="form-control-1">วันเกิด</label>
                                        <div class="col-sm-3 n-p-r">
                                            <div class=" input-with-icon">
                                                <input class="form-control" type="text" data-provide="datepicker" value="29/12/2018">
                                                <span class="icon icon-calendar input-icon"></span>
                                            </div>
                                        </div>
                                        <label class="col-sm-2 control-label" for="form-control-1">อายุ</label>
                                        <div class="col-sm-2">
                                            <div class=" input-with-icon">
                                                <input class="form-control" type="text" value="30">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-5 control-label" for="form-control-1">แผนก</label>
                                        <div class="col-sm-7">
                                            <select class="custom-select">
                                                <option value="1">การตลาด</option>
                                                <option value="2">วางแผนการผลิต</option>
                                                <option value="3">การผลิต</option>
                                                <option value="4">การซ่อมบำรุง</option>
                                                <option value="5">ไอที</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-5 control-label" for="form-control-1">ตำแหน่ง</label>
                                        <div class="col-sm-7">
                                            <select class="custom-select">
                                                <option value="1">พนักงานทั่วไป</option>
                                                <option value="2">ผู้จัดการ</option>
                                                <option value="3">ผู้บริหาร</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-5 control-label" for="form-control-1">สาขา</label>
                                        <div class="col-sm-7">
                                            <select class="custom-select">
                                                <option value="1">B&S : สารคาม</option>
                                                <option value="2">BWC : ศูนย์บวร</option>
                                                <option value="3">CY : เชียงยืน</option>
                                                <option value="4">EXT : ภายนอก</option>
                                                <option value="5">XFF : สาขาเมืองจีน</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <div class="image-profile">
                                                <img class="image" src="images/default-profile.jpeg">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="form-control-1">อัปโหลดรูป</label>
                                        <div class="col-sm-7">
                                            <input class="form-control" type="file">
                                        </div>
                                    </div>
                                </div>
                            </div>
						</div>
						<div class="m-t text-center">
						<a class="btn btn-lg btn-primary" href="edit_menu_user_profile.php" type="submit">บันทึก</a>
                        <a class="btn btn-lg btn-default" href="edit_menu_user_profile.php" type="button>">ยกเลิก</a>
						</div>
					</form>
                    
                        <!--  -->
                    </div>
                </div>
                <!--  -->
            </div>
        </div>
                                        </form>
                                    </div>
                                </div>
                                <!--  -->
                            </div>
                        </div>

                    </div>
                </div>
                <!--  -->
                <?include 'inc.footer.php';?>
                    <!--  -->

                    <?include 'inc.footer.script.php';?>
                        <!--  -->