<?php
	$select = 'open_menu_1';
    $select2 = 'menu_5';
    $select3 = 'menu_5';
    $select4 = 'menu_5_5';
?>
<!--  -->
<? include('inc.header.php');?>
<!--  -->
<? include('inc.navbar.php');?>
<!--  -->
<? include('inc.menu.php');?>
<!-- <div class="layout-main"> -->
<!--  -->
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <h4 class="m-t-0">
                <!-- <span class="d-ib text-danger">--</span> <span class="icon icon-angle-double-right"></span> -->
                <!-- <span class="d-ib">ข้อมูลหลัก</span> -->
                <span class="d-ib text-primary">การตั้งค่า</span> <span class="icon icon-angle-double-right"></span> กระบวนการผลิต <span class="icon icon-angle-double-right"></span> ขั้นตอนการผลิต
                <!-- <span class="d-ib">
                    <a class="title-bar-shortcut" href="#" title="Add to shortcut list" data-container="body" data-toggle-text="Remove from shortcut list" data-trigger="hover" data-placement="right" data-toggle="tooltip">
                        <span class="sr-only">Add to shortcut list</span>
                    </a>
                </span> -->
            </h4>
            <!-- <p class="title-bar-description">
                <small>Latest update on 01/01/2017 - 10.57 am.</small>
            </p> -->
        </div>

        <div class="row gutter-xs">
            <div class="col-xs-12">
                <!--  -->
                <div class="card">
                    <!-- <div class="card-header">
                        <a href="javascript:window.history.back(-1);" class="btn btn-sm btn-outline-primary"><span class="icon icon-long-arrow-left"></span>&nbsp;&nbsp;Back</a>&nbsp;&nbsp;
                        <strong class="">Latest update on 01/01/2017 - 10.57 am.</strong>
                    </div> -->
                    <div class="card-body">
                        <!--  -->
                        <? include('inc.home_top_filter.php');?>
                        <!--  -->
                        <div class="table-responsive">
                              <table id="demo-datatables-2" class="table table-striped table-bordered table-nowrap dataTable text-center no-footer" cellspacing="0" width="100%">
                                <thead>
                                    <tr class="at_bg_table_blue">
                                        <th rowspan="2" class="text-center">รหัสขั้นตอน</th>
                                        <th rowspan="2" class="text-center">ชื่อขั้นตอน</th>
                                        <th rowspan="2" class="text-center">ประเภทส่วนงาน</th>
                                        <th colspan="3" class="text-center">ค่ามาตรฐานความสามารถในการผลิตสูงสุด (กก./วัน)</th>
                                        <th rowspan="2" class="text-center">เริ่มใช้งาน</th>
                                        <th rowspan="2" class="text-center">ผู้ใช้งาน</th>
                                        <th rowspan="2" class="text-center">ปรับแก้ไข</th>
                                    </tr>
                                    <tr class="at_bg_table_blue">
                                        <th class="text-center">รหัสสาขา</th>
                                        <th class="text-center">ฤดูปกติ</th>
                                        <th class="text-center">ฤดูทำนา</th>
                                    </tr>
                                </thead>
                                <tbody>
<?php
$column1 = array(
"A00",  
"A01",  
"A02",  
"A10",  
"B00",  
"C00",  
"C02",  
"C04",  
"C10",
"C11",  
"C12",
"D00",
"D01",
"D02",
"D03",
"D04",
"D05",
"E00",
"E01",
"E02",
"E03",
"E04",
"E05",
"E06",
"E07",
"E10",
"E11",
"F00",
"F000",
"F100",
"F101",
"F102",
"F103",
"F11",
"F200",
"F201",
"F202",
"F300",
"F301",
"F400",
"F401",
"F500",
"F501",
"F600",
"F601",
"F602",
"G00",
"G04",
"G05",
"G06",
"G07",
"G08",
"G09",
"G10",
"G11",
"G12",
"G13",
"G14",
"H00",
"N01",
"N02",
"N03",
"N04",
"N05",
"N06",
"N07",
"N08",
"N09",
"N10",
"N11",
"N12",
"N13",
"P00",
"P10",
"P20",
"Q00",
"Q10",
"R00",
"R10",
"R11",
"T01",
"X01",
"X02",
"X03",
"X04",
"X05",
"X06",
"X07",
"X08",
"X09",
"X10"

);
$column2 = array(

"ชักใย ( วัตถุดิบ )",
"ตีด้าย/ตีเกลียว",
"อบใย",
"กรอหลอด-หุ้มฟิล์ม",
"ทอ",
"ร้อยหู",
"ปะอวนดำ",
"ถ่ายหูหลังเครื่อง",
"ดึงหู",
"ต่ออวนดำ",
"เช็คแผล",
"stockอวนดำ",
"เตรียมอวน",
"ต้มล้าง",
"ฟอกย้อม",
"ชุบนุ่ม",
"ชุบน้ำมันดำ",
"ร้อยเหล็ก",
"อบโยโก",
"อบตาเต-ไฟฟ้าปกติ",
"อบตาเตไอน้ำ - ไฟฟ้า",
"อบตาเต - ไฟฟ้า ตีเกลียว",
"อบตาเตไอน้ำ",
"เตรียมจ่ายปะตาเต",
"เตรียมจ่ายปะโยโก",
"อบตาเตเครื่องโยโก",
"อบแนวโยโกด้วยเครื่องตาเตไอน้ำไฟฟ้า",
"ปะอวนขาว",
"ชักใยโพลี",
"ตีด้ายเกลียวใน 1",
"ถ่ายใย",
"ตีด้ายเกลียวใน 2",
"ตีด้ายเกลียวนอก",
"ต่ออวนขาว",
"ปั่นใจ",
"กรอหลอด",
"กรอหลอดแบบไม่ใส่หลอด",
"มัดด้าย",
"มัดยาง",
"อัดก้อน",
"อัดโค่น",
"อบฟิล์มชิ้น",
"อบฟิล์มถาด",
"แพ็คจับคู่รัดสาย",
"แพ็คจับคู่รัดสาย+ห่อผ้าใบ",
"แพ็คห่อผ้าใบ",
"ชั่งน้ำหนัก",
"ปั่น",
"มัด",
"บรรจุ",
"นับเมตร",
"มัดด้าย-ใส่ถุง",
"มัดยาง",
"บรรจุอวนโพลี",
"บรรจุโค่นโพลี",
"บรรจุกระสอบ",
"อัดมัดเล็ก",
"อัดห่ออวนโพลี",
"ชักใยโพลีสำหรับทออวนปั๊ม",
"ตีด้ายเกลียวใน",
"อบใย",
"ปั่นไจไนล่อน",
"ย้อมสีด้ายไนล่อน",
"ชุบน้ำมันดำ",
"ชุบเคมี",
"ถ่ายหลอดสมอ",
"กรอหลอดไนล่อน",
"มัด",
"อบฟิล์ม",
"บรรจุ",
"ตีด้ายเกลียวนอก",
"ตีด้ายเกลียวใน2",
"อบใย/ด้าย",
"กรอ1",
"กรอ2",
"ย้อมด้าย/ใย",
"ชุบนุ่มใยโมโน",
"ปั่นไจ",
"มัดบรรจุใย/ด้าย",
"กรอหลอด",
"ตีด้ายใย MONO",
"ชักใย",
"ถ่ายบีม",
"ปั่นไจสายเอ็น รอบ1",
"ปั่นไจสายเอ็น รอบ2",
"ย้อมสีสายเอ็น",
"ชุบนุ่ม",
"กรอหลอด",
"มัด",
"อบฟิล์ม",
"บรรจุ",
);
$column3 = array(
"ส่วนหน้า",
"ส่วนหน้า",
"ส่วนหน้า",
"ส่วนหน้า",
"ส่วนหน้า",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
"ส่วนหลัง",
);
$arrlength = count($column1);

$column4 = array(
"B&S"
);
$column7 = array(
"24/08/2561"
);
$column8 = array(
"USER_ID"
);

?>


<?php for($i=0 ;$i < $arrlength ;$i++){?>
<tr>
    <td><?php echo $column1[$i];?></td>
    <td><?php echo $column2[$i];?></td>
    <td><?php echo $column3[$i];?></td>
    <td><?php echo $column4[0];?></td>
    <td></td>
    <td></td>
    <td><?php echo $column7[0];?></td>
    <td><?php echo $column8[0];?></td>
   
    <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_2_1.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
<?}?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
            </div>
        </div>










    </div>
</div>
<!--  -->
<? include('inc.footer.php');?>
<!--  -->
<? include('inc.footer.script.php');?>
<!--  -->
