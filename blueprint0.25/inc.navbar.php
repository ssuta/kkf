<div class="layout-header">
    <div class="navbar navbar-default">
        <div class="navbar-header">
            <a class="navbar-brand navbar-brand-center" href="index.php">
                โปรแกรมวางแผนการผลิต
                <!-- <img class="navbar-brand-logo" src="img/logo-inverse.svg" alt=""> -->
            </a>
            <button class="navbar-toggler visible-xs-block collapsed" type="button" data-toggle="collapse" data-target="#sidenav">
                <span class="sr-only">Navigation</span>
                <span class="bars">
                    <span class="bar-line bar-line-1 out"></span>
                <span class="bar-line bar-line-2 out"></span>
                <span class="bar-line bar-line-3 out"></span>
                </span>
                <span class="bars bars-x">
                    <span class="bar-line bar-line-4"></span>
                <span class="bar-line bar-line-5"></span>
                </span>
            </button>
            <button class="navbar-toggler visible-xs-block collapsed" type="button" data-toggle="collapse" data-target="#navbar">
                <span class="sr-only">Navigation</span>
                <span class="arrow-up"></span>
                <span class="ellipsis ellipsis-vertical">
                    <img class="ellipsis-object" width="32" height="32" src="img/0180441436.jpg" alt="">
                </span>
            </button>
        </div>
        <div class="navbar-toggleable">
            <nav id="navbar" class="navbar-collapse collapse">
                <button class="sidenav-toggler collapsed hidden-xs" title="Collapse sidenav ( [ )" aria-expanded="true" type="button">
                    <span class="sr-only">Navigation</span>
                    <span class="bars">
                      <span class="bar-line bar-line-1 out"></span>
                    <span class="bar-line bar-line-2 out"></span>
                    <span class="bar-line bar-line-3 out"></span>
                    <span class="bar-line bar-line-4 in"></span>
                    <span class="bar-line bar-line-5 in"></span>
                    <span class="bar-line bar-line-6 in"></span>
                    </span>
                </button>
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden-xs hidden-sm">
                        <h4 class="navbar-text text-center" style="font-weight: 400;">25 สิงหาคม 2561 &nbsp;-&nbsp; 18:30 น.</h4>
                    </li>
                    <li class="dropdown hidden-xs">
                        <button class="navbar-account-btn language_botton" data-toggle="dropdown" aria-haspopup="true">
                            <span class="icon icon-lg icon-language"></span>&nbsp;&nbsp;ภาษาไทย
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <!-- <li>
                                <a href="#">
                                    <h5 class="navbar-upgrade-heading">
                                      Last Login 01/01/2017
                                      <small class="navbar-upgrade-notification">
                                        Last Login 01/01/2017-12.30<br>
                                        Last Logout 02/01/2017-2017.00
                                      </small>
                                    </h5>
                                </a>
                            </li> -->
                            <!-- <li class="divider"></li> -->
                            <!-- <li class="navbar-upgrade-version">Version: 1.0.0</li>
                            <li class="divider"></li> -->
                            <li><a href="#">English</a></li>
                            <li><a href="#">中國</a></li>
                        </ul>
                    </li>
                    <li class="visible-xs-block">
                        <h4 class="navbar-text text-center">ชื่อผู้ใช้ นามสกุล</h4>
                    </li>
                    <li class="dropdown hidden-xs">
                        <button class="navbar-account-btn" data-toggle="dropdown" aria-haspopup="true">
                            <img class="rounded" width="36" height="36" src="img/2832982242.jpg" alt="Teddy Wilson"> &nbsp;&nbsp;ชื่อผู้ใช้ นามสกุล
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <!-- <li>
                                <a href="#">
                                    <h5 class="navbar-upgrade-heading">
                                      Last Login 01/01/2017
                                      <small class="navbar-upgrade-notification">
                                        Last Login 01/01/2017-12.30<br>
                                        Last Logout 02/01/2017-2017.00
                                      </small>
                                    </h5>
                                </a>
                            </li> -->
                            <!-- <li class="divider"></li> -->
                            <!-- <li class="navbar-upgrade-version">Version: 1.0.0</li>
                            <li class="divider"></li> -->
                            <!-- <li><a href="#">Profile</a></li> -->
                            <li><a href="index.php">ลงชื่อออก</a></li>
                        </ul>
                    </li>
                    <!-- <li class="visible-xs-block">
                        <a href="#">
                            <span class="icon icon-user icon-lg icon-fw"></span> Profile
                        </a>
                    </li> -->
                    <li class="visible-xs-block">
                        <a href="index.php">
                            <span class="icon icon-power-off icon-lg icon-fw"></span> ลงชื่อออก
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</div>