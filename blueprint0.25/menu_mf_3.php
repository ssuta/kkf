<?php
    $select = 'open_menu_4';
    $select2 = 'menu_production_3';
?>
<!--  -->
<? include('inc.header.php');?>
<!--  -->
<? include('inc.navbar.php');?>
<!--  -->
<? include('inc.menu.php');?>
<!-- <div class="layout-main"> -->
<!--  -->
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <h4 class="m-t-0">
                <!-- <span class="d-ib text-danger">--</span> <span class="icon icon-angle-double-right"></span> -->
                <!-- <span class="d-ib">ข้อมูลหลัก</span> -->
                <span class="d-ib text-primary">การผลิต</span> <span class="icon icon-angle-double-right"></span> แผนการทอ
                <!-- <span class="d-ib">
                    <a class="title-bar-shortcut" href="#" title="Add to shortcut list" data-container="body" data-toggle-text="Remove from shortcut list" data-trigger="hover" data-placement="right" data-toggle="tooltip">
                        <span class="sr-only">Add to shortcut list</span>
                    </a>
                </span> -->
            </h4>
            <!-- <p class="title-bar-description">
                <small>Latest update on 01/01/2017 - 10.57 am.</small>
            </p> -->
        </div>

        <div class="row gutter-xs">
            <div class="col-xs-12">
                <!--  -->
                <div class="card">
                    <!-- <div class="card-header">
                        <a href="javascript:window.history.back(-1);" class="btn btn-sm btn-outline-primary"><span class="icon icon-long-arrow-left"></span>&nbsp;&nbsp;Back</a>&nbsp;&nbsp;
                        <strong class="">Latest update on 01/01/2017 - 10.57 am.</strong>
                    </div> -->
                    <div class="card-body">
                        <!--  -->
                        <div class="at_add_box">
                            <div class="row">
                                <!-- <div class="col-sm-3 m-b-sm">
                                    <h4 class="text-primary">สาขา</h4>
                                </div> -->
                                <!-- <div class="col-sm-6 text-right">
                                    555555
                                </div> -->
                                <div class="col-sm-3">
                                    <form class="row form form-horizontal">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label text-right" for="form-control-14">สาขา</label>
                                            <div class="col-sm-9 has-feedback">
                                                <select id="demo-select2-1" class="form-control">
                                                    <option value="0">ทั้งหมด</option>
                                                    <option value="0" selected>B&S</option>
                                                    <option value="0">ตัวเลือก 2</option>
                                                    <option value="0">ตัวเลือก 3</option>
                                                    <option value="0">ตัวเลือก 4</option>
                                                    <option value="0">ตัวเลือก 5</option>
                                                </select>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-sm-3">
                                    <form class="row form form-horizontal">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label text-right" for="form-control-14">การแสดงผล</label>
                                            <div class="col-sm-9 has-feedback">
                                                <select id="demo-select2-1" class="form-control" onchange="selectShowing(this)">
                                                    <option value="1">แสดงแผนการใช้ใย</option>
                                                    <option value="2">แสดงจำนวนใยคงเหลือปัจจุบันและแผนการใช้ใย</option>
                                                </select>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-sm-3">
                                    <form class="row form form-horizontal">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label text-right" for="form-control-14">ตัวเลือก</label>
                                            <div class="col-sm-9 has-feedback">
                                                <select id="demo-select2-1" class="form-control">
                                                    <option value="1">แสดงทั้งหมด</option>
                                                    <option value="2">แสดงเฉพาะที่มีแผนการใช้ใย</option>
                                                </select>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-sm-3 text-right">
                                    <a href="#" class="btn btn-info"><span class="icon icon-lg icon-table"></span>&nbsp;&nbsp;แสดงทั้งหมด</a>
                                    <a href="#" class="btn btn-default"><span class="icon icon-lg icon-print"></span>&nbsp;&nbsp;พิมพ์</a>
                                    <a href="#" class="btn btn-warning"><span class="icon icon-lg icon-file-text-o"></span>&nbsp;&nbsp;ส่งออกไฟล์</a>
                                </div>
                                <!-- <div class="col-sm-2">
                                    <form class="form form-horizontal">
                                    <div class="form-group">
                                    <label class="col-sm-3 control-label" for="form-control-14">Text</label>
                                    <div class="col-sm-12 has-feedback">
                                        <div class="input-with-icon">
                                            <input class="form-control" type="text" placeholder="Search…">
                                            <span class="icon icon-search input-icon"></span>
                                        </div>
                                    </div>
                                    </div>
                                    </form>
                                </div> -->
                                <!-- <div class="col-sm-7 text-right">
                                    <button class="btn btn-success" data-toggle="modal" data-target="#menu_1_1_add" type="button"><span class="icon icon-lg icon-plus"></span>&nbsp;&nbsp;เพิ่มใหม่</button>
                                    <? if ($select3 == 'menu_all'){?>
                                        <a href="menu_1_1_all.php" class="btn btn-info"><span class="icon icon-lg icon-table"></span>&nbsp;&nbsp;แสดงทั้งหมด</a>
                                    <? }else{?>
                                        <a href="menu_1_1.php" class="btn btn-info"><span class="icon icon-lg icon-table"></span>&nbsp;&nbsp;มุมมองปกติ</a>
                                    <? } ?>
                                    <a href="menu_1_1_history.php" class="btn btn-info"><span class="icon icon-lg icon-history"></span>&nbsp;&nbsp;ประวัติการแก้ไข</a>
                                    <button onClick="window.print()" class="btn btn-default" type="button"><span class="icon icon-lg icon-print"></span>&nbsp;&nbsp;พิมพ์</button>
                                    <a href="images/SampleXLSFile_212kb.xls" class="btn btn-warning" type="button"><span class="icon icon-lg icon-file-text-o"></span>&nbsp;&nbsp;ส่งออกไฟล์</a>
                                    <a href="javascript:window.history.back(-1);" class="btn btn-info"><span class="icon icon-long-arrow-left"></span>&nbsp;&nbsp;ย้อนกลับ</a>
                                </div> -->
                                <!-- <div class="col-sm-12 text-right">
                                    555
                                </div> -->
                                <!-- <div class="col-sm-12 text-right">
                                    <button class="btn btn-info" data-toggle="modal" data-target="#Button_Addnew_Planning_Planning_list" type="button"><span class="icon icon-lg icon-plus"></span>&nbsp;&nbsp;Add order row</button>
                                    <button class="btn btn-info" type="button"><span class="icon icon-lg icon-refresh"></span>&nbsp;&nbsp;Update Data</button>
                                    <button class="btn btn-primary" type="button"><span class="icon icon-lg icon-file-excel-o"></span>&nbsp;&nbsp;Addition order file</button>
                                    <label class="btn btn-success file-upload-btn">
                                        <span class="icon icon-lg icon-file-excel-o"></span>&nbsp;&nbsp;Import
                                        <input class="file-upload-input" type="file" name="files[]" multiple="multiple">
                                    </label>
                                    <button class="btn btn-warning" type="button"><span class="icon icon-lg icon-file-text-o"></span>&nbsp;&nbsp;Export</button>
                                    <button class="btn btn-default" type="button"><span class="icon icon-lg icon-print"></span>&nbsp;&nbsp;Print</button>
                                </div> -->
                            </div>
                        </div>
                        <!--  -->
                        <div class="at_add_box">
                            <div class="row">
                                <!-- <div class="col-sm-3 m-b-sm">
                                    <h4 class="text-primary">สาขา</h4>
                                </div> -->
                                <div class="col-sm-3">
                                    <div class="input-group input-daterange" data-provide="datepicker" data-date-autoclose="true" data-date-format="dd-M-yyyy">
                                        <input class="form-control" type="text" value="เริ่มต้น">
                                        <span class="input-group-addon">ถึง</span>
                                        <input class="form-control" type="text" value="สิ้นสุด">												
                                    </div>
                                </div>
								<div class="col-sm-3" style="display: inline-flex;">
                                    <div class="dhx_cal_prev_button" role="button" aria-label="Previous">&nbsp;</div>
									<div class="dhx_cal_next_button" role="button" aria-label="Next">&nbsp;</div>
                                </div>
                                <div class="col-sm-6 text-right">
                                    <a href="#" class="btn btn-info">7 วัน</a>
                                    <a href="#" class="btn btn-info">15 วัน</a>
                                    <a href="#" class="btn btn-info">30 วัน</a>
                                </div>
                                <!-- <div class="col-sm-12 text-right">
                                    555
                                </div> -->
                                <!-- <div class="col-sm-12 text-right">
                                    <button class="btn btn-info" data-toggle="modal" data-target="#Button_Addnew_Planning_Planning_list" type="button"><span class="icon icon-lg icon-plus"></span>&nbsp;&nbsp;Add order row</button>
                                    <button class="btn btn-info" type="button"><span class="icon icon-lg icon-refresh"></span>&nbsp;&nbsp;Update Data</button>
                                    <button class="btn btn-primary" type="button"><span class="icon icon-lg icon-file-excel-o"></span>&nbsp;&nbsp;Addition order file</button>
                                    <label class="btn btn-success file-upload-btn">
                                        <span class="icon icon-lg icon-file-excel-o"></span>&nbsp;&nbsp;Import
                                        <input class="file-upload-input" type="file" name="files[]" multiple="multiple">
                                    </label>
                                    <button class="btn btn-warning" type="button"><span class="icon icon-lg icon-file-text-o"></span>&nbsp;&nbsp;Export</button>
                                    <button class="btn btn-default" type="button"><span class="icon icon-lg icon-print"></span>&nbsp;&nbsp;Print</button>
                                </div> -->
                            </div>
                        </div>
                        <!--  -->

                        <div id="section1">
                            <div class="table-responsive">
                                <table id="demo-datatables-2" class="table table-hover table-bordered text-center dataTable no-footer" cellspacing="0" width="100%">
                                    <thead>
                                        <tr class="at_bg_table_blue">
                                            <th rowspan="2" class="middle text-center add-padding-left">รหัสสาขา</th>
                                            <th rowspan="2" class="middle text-center add-padding-left">รหัสเบอร์ใย</th>
                                            <th rowspan="2" class="middle text-center add-padding-left">รายละเอียด<br>เบอร์ใย</th>
                                            <th rowspan="2" class="middle text-center add-padding-left">รหัส<br>ประเภทการฉีด</th>
                                            <th rowspan="2" class="middle text-center add-padding-left">เบอร์ใย<br>จัดเรียง</th>
                                            <th rowspan="2" class="middle text-center add-padding-left">จำนวน<br>เส้นจัดเรียง</th>
                                            <th rowspan="2" class="middle text-center add-padding-left">ประเภท<br>วัสดุพิเศษ</th>
                                            <th rowspan="2" class="middle text-center add-padding-left">เม็ด</th>
                                            <th rowspan="2" class="middle text-center add-padding-left">สี</th>
                                            <th rowspan="2" class="middle text-center add-padding-left">หมายเหตุ</th>
                                            <th rowspan="2" class="middle text-center add-padding-left">วันที่ต้อง<br>สั่งซื้อวัตถุดิบ</th>
                                            <th rowspan="2" class="middle text-center add-padding-left">วันที่ต้องเริ่มฉีด<br>ใยล่วงหน้า</th>
                                            <th colspan="2" class="middle text-center add-padding-left">ปัจจุบันใยคงเหลือ</th>
                                            <th colspan="7" class="middle text-center add-padding-left">ปริมาณใยที่ต้องฉีดอย่างน้อย (กิโลกรัม) ต่อวัน</th>
                                            <th rowspan="2" class="middle text-center add-padding-left">ปรับแก้ไข</th>
                                        </tr>
                                        <tr class="at_bg_table_blue">
                                            <th>รวม (กล่อง)</th>
                                            <th>รวม (kg.)</th>
                                            <th>25/08/2018</th>
                                            <th>26/08/2018</th>
                                            <th>27/08/2018</th>
                                            <th>28/08/2018</th>
                                            <th>29/08/2018</th>
                                            <th>30/08/2018</th>
                                            <th>31/08/2018</th>
                                        </tr>
                                    </thead>
                                    <tbody>
    <tr height=19 style='height:14.4pt'>
    <td height=19 class=xl66 width=64 style='height:14.4pt;width:48pt'>B&amp;S</td>
    <td class=xl66 width=75 style='width:56pt'>006*21</td>
    <td class=xl66 width=130 style='width:98pt'>&#3650;&#3617;&#3650;&#3609;</td>
    <td class=xl66 width=118 style='width:89pt'>2</td>
    <td class=xl66 width=94 style='width:71pt'>0.06</td>
    <td class=xl66 width=114 style='width:85pt'>21</td>
    <td class=xl66 width=113 style='width:85pt'></td>
    <td class=xl66 width=48 style='width:36pt'>CL</td>
    <td class=xl66 width=111 style='width:83pt'>SK00</td>
    <td class=xl66 width=64 style='width:48pt'></td>
    <td class=xl66 width=137 style='width:103pt'>-</td>
    <td class=xl66 width=162 style='width:121pt'>43337</td>
    <td class=xl66 width=71 style='width:53pt'></td>
    <td class=xl66 width=54 style='width:41pt'></td>
    <td class=xl66 width=76 style='width:57pt'>10</td>
    <td align="center" width=76 style='width:57pt'>10</td>
    <td align="center" width=76 style='width:57pt'>10</td>
    <td align="center" width=76 style='width:57pt'>15</td>
    <td align="center" width=76 style='width:57pt'>15</td>
    <td width=76 style='width:57pt'></td>
    <td width=76 style='width:57pt'></td>
    <td colspan=33 align=left width=2118 style='mso-ignore:colspan;width:1588pt'><div
    class="col-sm-10 col-sm-offset-1"><div
    class="row"><div class="col-sm-12"><div
    class="row extra-button"><button class="btn btn-lg
    btn-outline-primary btn-pill btn-xs full" data-toggle="modal"
    data-target="#modal-edit-manage-product" type="button"
    title="&#3649;&#3585;&#3657;&#3652;&#3586;"><span
    class="icon icon-lg
    icon-edit"></span></button></div></div></div></div></td>
    </tr>
    <tr height=19 style='height:14.4pt'>
    <td height=19 class=xl66 style='height:14.4pt'>B&amp;S</td>
    <td class=xl66>006*28</td>
    <td class=xl66>&#3604;&#3657;&#3634;&#3618;&#3650;&#3617;&#3650;&#3609;&#3605;&#3637;&#3648;&#3585;&#3621;&#3637;&#3618;&#3623;</td>
    <td class=xl66>2</td>
    <td class=xl66>0.06</td>
    <td class=xl66>28</td>
    <td class=xl66></td>
    <td class=xl66>CL</td>
    <td class=xl66>SK00</td>
    <td class=xl66></td>
    <td class=xl66>-</td>
    <td class=xl66>43332</td>
    <td class=xl66></td>
    <td class=xl66></td>
    <td class=xl66>15</td>
    <td align="center">15</td>
    <td align="center">15</td>
    <td align="center">15</td>
    <td align="center">15</td>
    <td align="center">15</td>
    <td align="center">15</td>
    <td colspan=33 align=left style='mso-ignore:colspan'><div
    class="col-sm-10 col-sm-offset-1"><div
    class="row"><div class="col-sm-12"><div
    class="row extra-button"><button class="btn btn-lg
    btn-outline-primary btn-pill btn-xs full" data-toggle="modal"
    data-target="#modal-edit-manage-product" type="button"
    title="&#3649;&#3585;&#3657;&#3652;&#3586;"><span
    class="icon icon-lg
    icon-edit"></span></button></div></div></div></div></td>
    </tr>
    <tr height=19 style='height:14.4pt'>
    <td height=19 class=xl66 style='height:14.4pt'>B&amp;S</td>
    <td class=xl66>006*42</td>
    <td class=xl66>&#3651;&#3618;&#3650;&#3617;&#3650;&#3609;&#3619;&#3629;&#3605;&#3637;&#3648;&#3585;&#3621;&#3637;&#3618;&#3623;</td>
    <td class=xl66>2</td>
    <td class=xl66>0.06</td>
    <td class=xl66>42</td>
    <td class=xl66>&#3623;&#3633;&#3626;&#3604;&#3640;&#3614;&#3636;&#3648;&#3624;&#3625;</td>
    <td class=xl66>CL</td>
    <td class=xl66>SK06</td>
    <td class=xl66></td>
    <td class=xl66>43323</td>
    <td class=xl66>43338</td>
    <td class=xl66></td>
    <td class=xl66></td>
    <td class=xl66></td>
    <td align="center">5</td>
    <td align="center">5</td>
    <td align="center">5</td>
    <td align="center">5</td>
    <td align="center">5</td>
    <td align="center">5</td>
    <td colspan=33 align=left style='mso-ignore:colspan'><div
    class="col-sm-10 col-sm-offset-1"><div
    class="row"><div class="col-sm-12"><div
    class="row extra-button"><button class="btn btn-lg
    btn-outline-primary btn-pill btn-xs full" data-toggle="modal"
    data-target="#modal-edit-manage-product" type="button"
    title="&#3649;&#3585;&#3657;&#3652;&#3586;"><span
    class="icon icon-lg
    icon-edit"></span></button></div></div></div></div></td>
    </tr>
    <tr height=19 style='height:14.4pt'>
    <td height=19 class=xl66 style='height:14.4pt'>B&amp;S</td>
    <td class=xl66>006*63</td>
    <td class=xl66>&#3650;&#3614;&#3621;&#3637;&#3648;&#3629;&#3626;&#3648;&#3605;&#3629;&#3619;&#3660;</td>
    <td class=xl66>2</td>
    <td class=xl66>0.06</td>
    <td class=xl66>63</td>
    <td class=xl66></td>
    <td class=xl66>CL</td>
    <td class=xl66>SK06</td>
    <td class=xl66></td>
    <td class=xl66>-</td>
    <td class=xl66>43338</td>
    <td class=xl66></td>
    <td class=xl66></td>
    <td class=xl66></td>
    <td align="center">30</td>
    <td align="center">30</td>
    <td align="center">30</td>
    <td align="center">30</td>
    <td align="center">30</td>
    <td></td>
    <td colspan=33 align=left style='mso-ignore:colspan'><div
    class="col-sm-10 col-sm-offset-1"><div
    class="row"><div class="col-sm-12"><div
    class="row extra-button"><button class="btn btn-lg
    btn-outline-primary btn-pill btn-xs full" data-toggle="modal"
    data-target="#modal-edit-manage-product" type="button"
    title="&#3649;&#3585;&#3657;&#3652;&#3586;"><span
    class="icon icon-lg
    icon-edit"></span></button></div></div></div></div></td>
    </tr>
    <tr height=19 style='height:14.4pt'>
    <td height=19 class=xl66 style='height:14.4pt'>B&amp;S</td>
    <td class=xl66>006*84</td>
    <td class=xl66>&#3629;&#3623;&#3609;&#3619;&#3640;&#3617;/&#3629;&#3623;&#3609;&#3627;&#3621;&#3634;&#3618;&#3594;&#3633;&#3657;&#3609;</td>
    <td class=xl66>2</td>
    <td class=xl66>0.06</td>
    <td class=xl66>84</td>
    <td class=xl66></td>
    <td class=xl66>CL</td>
    <td class=xl66>M206 &#3651;&#3618;&#3651;&#3627;&#3617;&#3656; SLX</td>
    <td class=xl66></td>
    <td class=xl66>-</td>
    <td class=xl66>43327</td>
    <td class=xl66></td>
    <td class=xl66></td>
    <td class=xl66>15</td>
    <td align="center">15</td>
    <td align="center">15</td>
    <td align="center">15</td>
    <td align="center">15</td>
    <td align="center">15</td>
    <td align="center">15</td>
    <td colspan=33 align=left style='mso-ignore:colspan'><div
    class="col-sm-10 col-sm-offset-1"><div
    class="row"><div class="col-sm-12"><div
    class="row extra-button"><button class="btn btn-lg
    btn-outline-primary btn-pill btn-xs full" data-toggle="modal"
    data-target="#modal-edit-manage-product" type="button"
    title="&#3649;&#3585;&#3657;&#3652;&#3586;"><span
    class="icon icon-lg
    icon-edit"></span></button></div></div></div></div></td>
    </tr>
    <tr height=19 style='height:14.4pt'>
    <td height=19 class=xl66 style='height:14.4pt'>B&amp;S</td>
    <td class=xl66>80</td>
    <td class=xl66>&#3652;&#3609;&#3621;&#3656;&#3629;&#3609;</td>
    <td class=xl66>1</td>
    <td class=xl66>0.08</td>
    <td class=xl66>0</td>
    <td class=xl66></td>
    <td class=xl66>CL</td>
    <td class=xl66>SK06</td>
    <td class=xl66></td>
    <td class=xl66>-</td>
    <td class=xl66>43327</td>
    <td class=xl66></td>
    <td class=xl66></td>
    <td class=xl66>50</td>
    <td align="center">50</td>
    <td align="center">50</td>
    <td align="center">50</td>
    <td align="center">50</td>
    <td align="center">50</td>
    <td align="center">50</td>
    <td colspan=33 align=left style='mso-ignore:colspan'><div
    class="col-sm-10 col-sm-offset-1"><div
    class="row"><div class="col-sm-12"><div
    class="row extra-button"><button class="btn btn-lg
    btn-outline-primary btn-pill btn-xs full" data-toggle="modal"
    data-target="#modal-edit-manage-product" type="button"
    title="&#3649;&#3585;&#3657;&#3652;&#3586;"><span
    class="icon icon-lg
    icon-edit"></span></button></div></div></div></div></td>
    </tr>
    <tr height=19 style='height:14.4pt'>
    <td height=19 class=xl66 style='height:14.4pt'>B&amp;S</td>
    <td class=xl66>84</td>
    <td class=xl66>&#3652;&#3609;&#3621;&#3656;&#3629;&#3609;&#3648;&#3614;&#3639;&#3656;&#3629;&#3629;&#3623;&#3609;&#3611;&#3633;&#3658;&#3617;</td>
    <td class=xl66>1</td>
    <td class=xl66>0.084</td>
    <td class=xl66>0</td>
    <td class=xl66></td>
    <td class=xl66>CL</td>
    <td class=xl66>SK06</td>
    <td class=xl66>&#3626;&#3637;&#3648;&#3586;&#3657;&#3617;</td>
    <td class=xl66>-</td>
    <td class=xl66>43340</td>
    <td class=xl66></td>
    <td class=xl66></td>
    <td class=xl66></td>
    <td></td>
    <td></td>
    <td align="center">30</td>
    <td align="center">30</td>
    <td align="center">30</td>
    <td align="center">30</td>
    <td colspan=33 align=left style='mso-ignore:colspan'><div
    class="col-sm-10 col-sm-offset-1"><div
    class="row"><div class="col-sm-12"><div
    class="row extra-button"><button class="btn btn-lg
    btn-outline-primary btn-pill btn-xs full" data-toggle="modal"
    data-target="#modal-edit-manage-product" type="button"
    title="&#3649;&#3585;&#3657;&#3652;&#3586;"><span
    class="icon icon-lg
    icon-edit"></span></button></div></div></div></div></td>
    </tr>
    <tr height=19 style='height:14.4pt'>
    <td height=19 class=xl66 style='height:14.4pt'>B&amp;S</td>
    <td class=xl66>85</td>
    <td class=xl66>&#3650;&#3614;&#3621;&#3637;</td>
    <td class=xl66>1</td>
    <td class=xl66>0.085</td>
    <td class=xl66>0</td>
    <td class=xl66></td>
    <td class=xl66>CL</td>
    <td class=xl66>M206 &#3651;&#3618;&#3651;&#3627;&#3617;&#3656; SLX</td>
    <td class=xl66></td>
    <td class=xl66>-</td>
    <td class=xl66>43340</td>
    <td class=xl66></td>
    <td class=xl66></td>
    <td class=xl66></td>
    <td></td>
    <td></td>
    <td align="center">30</td>
    <td align="center">15</td>
    <td align="center">15</td>
    <td align="center">50</td>
    <td colspan=33 align=left style='mso-ignore:colspan'><div
    class="col-sm-10 col-sm-offset-1"><div
    class="row"><div class="col-sm-12"><div
    class="row extra-button"><button class="btn btn-lg
    btn-outline-primary btn-pill btn-xs full" data-toggle="modal"
    data-target="#modal-edit-manage-product" type="button"
    title="&#3649;&#3585;&#3657;&#3652;&#3586;"><span
    class="icon icon-lg
    icon-edit"></span></button></div></div></div></div></td>
    </tr>
    <tr height=19 style='height:14.4pt'>
    <td height=19 class=xl66 style='height:14.4pt'>B&amp;S</td>
    <td class=xl66>90</td>
    <td class=xl66>&#3650;&#3614;&#3621;&#3637;&#3648;&#3614;&#3639;&#3656;&#3629;&#3629;&#3623;&#3609;&#3611;&#3633;&#3658;&#3617;</td>
    <td class=xl66>1</td>
    <td class=xl66>0.09</td>
    <td class=xl66>0</td>
    <td class=xl66></td>
    <td class=xl66>CL</td>
    <td class=xl66>SK06</td>
    <td class=xl66></td>
    <td class=xl66>-</td>
    <td class=xl66>43340</td>
    <td class=xl66></td>
    <td class=xl66></td>
    <td class=xl66></td>
    <td></td>
    <td></td>
    <td align="center">10</td>
    <td align="center">10</td>
    <td align="center">10</td>
    <td align="center">10</td>
    <td colspan=33 align=left style='mso-ignore:colspan'><div
    class="col-sm-10 col-sm-offset-1"><div
    class="row"><div class="col-sm-12"><div
    class="row extra-button"><button class="btn btn-lg
    btn-outline-primary btn-pill btn-xs full" data-toggle="modal"
    data-target="#modal-edit-manage-product" type="button"
    title="&#3649;&#3585;&#3657;&#3652;&#3586;"><span
    class="icon icon-lg
    icon-edit"></span></button></div></div></div></div></td>
    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    
                        <div id="section2" class="hide">
                        <div class="table-responsive">
                                <table id="datatables-base2" class="table table-hover table-bordered text-center dataTable no-footer" cellspacing="0" width="100%">
                                    <thead>
                                        <tr class="at_bg_table_blue">
                                            <th rowspan="2" class="middle text-center add-padding-left">รหัสสาขา</th>
                                            <th rowspan="2" class="middle text-center add-padding-left">รหัสเบอร์ใย</th>
                                            <th rowspan="2" class="middle text-center add-padding-left">รายละเอียด<br>เบอร์ใย</th>
                                            <th rowspan="2" class="middle text-center add-padding-left">รหัส<br>ประเภทการฉีด</th>
                                            <th rowspan="2" class="middle text-center add-padding-left">เบอร์ใย<br>จัดเรียง</th>
                                            <th rowspan="2" class="middle text-center add-padding-left">จำนวน<br>เส้นจัดเรียง</th>
                                            <th rowspan="2" class="middle text-center add-padding-left">ประเภท<br>วัสดุพิเศษ</th>
                                            <th rowspan="2" class="middle text-center add-padding-left">เม็ด</th>
                                            <th rowspan="2" class="middle text-center add-padding-left">สี</th>
                                            <th rowspan="2" class="middle text-center add-padding-left">หมายเหตุ</th>
                                            <th rowspan="2" class="middle text-center add-padding-left">วันที่ต้อง<br>สั่งซื้อวัตถุดิบ</th>
                                            <th rowspan="2" class="middle text-center add-padding-left">วันที่ต้องเริ่มฉีด<br>ใยล่วงหน้า</th>
                                            <th colspan="5" class="middle text-center add-padding-left">ปัจจุบันใยคงเหลือ</th>
                                            <th rowspan="2" class="middle text-center add-padding-left">ใยจะหมดวันที่</th>
                                            <th rowspan="2" class="middle text-center add-padding-left">มตฐ. วัน Stok ใย</th>
                                            <th colspan="2" class="middle text-center add-padding-left">ปริมาณการใช้ใย</th>
                                            <th colspan="2" class="middle text-center add-padding-left">ใช้ใย กก./ค./ว.</th>
                                            <th colspan="2" class="middle text-center add-padding-left">ศูนย์ที่ทอ</th>
                                            <th rowspan="2" class="middle text-center add-padding-left">ปริมาณการใช้ใยรวม</th>
                                            <th colspan="7" class="middle text-center add-padding-left">ปริมาณใยที่ต้องฉีดอย่างน้อย (กิโลกรัม) ต่อวัน</th>
                                            <th rowspan="2" class="middle text-center add-padding-left">ปรับแก้ไข</th>
                                        </tr>
                                        <tr class="at_bg_table_blue">
                                            <th class="text-center">คลัง</th>
                                            <th class="text-center">ทอ V</th>
                                            <th class="text-center">ทอ M</th>
                                            <th class="text-center">รวม (กล่อง)</th>
                                            <th class="text-center">รวม (kg.)</th>
                                            <th class="text-center">CY</th>
                                            <th class="text-center">BS</th>
                                            <th class="text-center">CY</th>
                                            <th class="text-center">BS</th>
                                            <th class="text-center">CY</th>
                                            <th class="text-center">BS</th>
                                            <th class="text-center">25/08/2018</th>
                                            <th class="text-center">26/08/2018</th>
                                            <th class="text-center">27/08/2018</th>
                                            <th class="text-center">28/08/2018</th>
                                            <th class="text-center">29/08/2018</th>
                                            <th class="text-center">30/08/2018</th>
                                            <th class="text-center">31/08/2018</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <tr height=19 style='height:14.4pt'>
  <td height=19 class=xl66 width=64 style='height:14.4pt;width:48pt'>B&amp;S</td>
  <td class=xl66 width=75 style='width:56pt'>006*21</td>
  <td class=xl66 width=130 style='width:98pt'>&#3650;&#3617;&#3650;&#3609;</td>
  <td class=xl66 width=118 style='width:89pt'>2</td>
  <td class=xl66 width=94 style='width:71pt'>0.06</td>
  <td class=xl66 width=114 style='width:85pt'>21</td>
  <td class=xl66 width=113 style='width:85pt'></td>
  <td class=xl66 width=48 style='width:36pt'>CL</td>
  <td class=xl66 width=111 style='width:83pt'>SK00</td>
  <td class=xl66 width=64 style='width:48pt'></td>
  <td class=xl66 width=137 style='width:103pt'>-</td>
  <td class=xl66 width=162 style='width:121pt'>43337</td>
  <td class=xl66 width=62 style='width:47pt'></td>
  <td class=xl66 width=62 style='width:47pt'></td>
  <td class=xl66 width=62 style='width:47pt'></td>
  <td class=xl66 width=71 style='width:53pt'></td>
  <td class=xl66 width=54 style='width:41pt'></td>
  <td class=xl66 width=90 style='width:67pt'></td>
  <td class=xl66 width=104 style='width:78pt'></td>
  <td class=xl66 width=62 style='width:47pt'>0</td>
  <td class=xl66 width=62 style='width:47pt'>56</td>
  <td class=xl66 width=62 style='width:47pt'></td>
  <td class=xl66 width=62 style='width:47pt'>14</td>
  <td class=xl66 width=62 style='width:47pt'></td>
  <td class=xl66 width=62 style='width:47pt'>4</td>
  <td class=xl66 width=126 style='width:95pt'>56</td>
  <td class=xl66 width=76 style='width:57pt'>10</td>
  <td class=xl66 width=76 style='width:57pt'>10</td>
  <td class=xl66 width=76 style='width:57pt'>10</td>
  <td class=xl66 width=76 style='width:57pt'>15</td>
  <td class=xl66 width=76 style='width:57pt'>15</td>
  <td class=xl66 width=76 style='width:57pt'></td>
  <td class=xl66 width=76 style='width:57pt'></td>
  <td class=xl66 width=64 style='width:48pt'><div class="col-sm-10
  col-sm-offset-1"><div class="row"><div
  class="col-sm-12"><div class="row
  extra-button"><button class="btn btn-lg btn-outline-primary
  btn-pill btn-xs full" data-toggle="modal"
  data-target="#modal-edit-manage-product" type="button"
  title="&#3649;&#3585;&#3657;&#3652;&#3586;"><span
  class="icon icon-lg
  icon-edit"></span></button></div></div></div></div></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 class=xl66 style='height:14.4pt'>B&amp;S</td>
  <td class=xl66>006*28</td>
  <td class=xl66>&#3604;&#3657;&#3634;&#3618;&#3650;&#3617;&#3650;&#3609;&#3605;&#3637;&#3648;&#3585;&#3621;&#3637;&#3618;&#3623;</td>
  <td class=xl66>2</td>
  <td class=xl66>0.06</td>
  <td class=xl66>28</td>
  <td class=xl66></td>
  <td class=xl66>CL</td>
  <td class=xl66>SK00</td>
  <td class=xl66></td>
  <td class=xl66>-</td>
  <td class=xl66>43332</td>
  <td class=xl66></td>
  <td class=xl66></td>
  <td class=xl66></td>
  <td class=xl66></td>
  <td class=xl66></td>
  <td class=xl66></td>
  <td class=xl66></td>
  <td class=xl66>0</td>
  <td class=xl66>36</td>
  <td class=xl66></td>
  <td class=xl66>18</td>
  <td class=xl66></td>
  <td class=xl66>2</td>
  <td class=xl66>36</td>
  <td class=xl66>15</td>
  <td class=xl66>15</td>
  <td class=xl66>15</td>
  <td class=xl66>15</td>
  <td class=xl66>15</td>
  <td class=xl66>15</td>
  <td class=xl66>15</td>
  <td class=xl66><div class="col-sm-10 col-sm-offset-1"><div
  class="row"><div class="col-sm-12"><div
  class="row extra-button"><button class="btn btn-lg
  btn-outline-primary btn-pill btn-xs full" data-toggle="modal"
  data-target="#modal-edit-manage-product" type="button"
  title="&#3649;&#3585;&#3657;&#3652;&#3586;"><span
  class="icon icon-lg
  icon-edit"></span></button></div></div></div></div></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 class=xl66 style='height:14.4pt'>B&amp;S</td>
  <td class=xl66>006*42</td>
  <td class=xl66>&#3651;&#3618;&#3650;&#3617;&#3650;&#3609;&#3619;&#3629;&#3605;&#3637;&#3648;&#3585;&#3621;&#3637;&#3618;&#3623;</td>
  <td class=xl66>2</td>
  <td class=xl66>0.06</td>
  <td class=xl66>42</td>
  <td class=xl66>&#3623;&#3633;&#3626;&#3604;&#3640;&#3614;&#3636;&#3648;&#3624;&#3625;</td>
  <td class=xl66>CL</td>
  <td class=xl66>SK06</td>
  <td class=xl66></td>
  <td class=xl66>43323</td>
  <td class=xl66>43338</td>
  <td class=xl66></td>
  <td class=xl66></td>
  <td class=xl66></td>
  <td class=xl66></td>
  <td class=xl66></td>
  <td class=xl66></td>
  <td class=xl66></td>
  <td class=xl66>0</td>
  <td class=xl66>266</td>
  <td class=xl66></td>
  <td class=xl66>14</td>
  <td class=xl66></td>
  <td class=xl66>19</td>
  <td class=xl66>266</td>
  <td class=xl66></td>
  <td class=xl66>5</td>
  <td class=xl66>5</td>
  <td class=xl66>5</td>
  <td class=xl66>5</td>
  <td class=xl66>5</td>
  <td class=xl66>5</td>
  <td class=xl66><div class="col-sm-10 col-sm-offset-1"><div
  class="row"><div class="col-sm-12"><div
  class="row extra-button"><button class="btn btn-lg
  btn-outline-primary btn-pill btn-xs full" data-toggle="modal"
  data-target="#modal-edit-manage-product" type="button"
  title="&#3649;&#3585;&#3657;&#3652;&#3586;"><span
  class="icon icon-lg
  icon-edit"></span></button></div></div></div></div></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 class=xl66 style='height:14.4pt'>B&amp;S</td>
  <td class=xl66>006*63</td>
  <td class=xl66>&#3650;&#3614;&#3621;&#3637;&#3648;&#3629;&#3626;&#3648;&#3605;&#3629;&#3619;&#3660;</td>
  <td class=xl66>2</td>
  <td class=xl66>0.06</td>
  <td class=xl66>63</td>
  <td class=xl66></td>
  <td class=xl66>CL</td>
  <td class=xl66>SK06</td>
  <td class=xl66></td>
  <td class=xl66>-</td>
  <td class=xl66>43338</td>
  <td class=xl66></td>
  <td class=xl66></td>
  <td class=xl66></td>
  <td class=xl66></td>
  <td class=xl66></td>
  <td class=xl66></td>
  <td class=xl66></td>
  <td class=xl66>0</td>
  <td class=xl66>160</td>
  <td class=xl66></td>
  <td class=xl66>20</td>
  <td class=xl66></td>
  <td class=xl66>8</td>
  <td class=xl66>160</td>
  <td class=xl66></td>
  <td class=xl66>30</td>
  <td class=xl66>30</td>
  <td class=xl66>30</td>
  <td class=xl66>30</td>
  <td class=xl66>30</td>
  <td class=xl66></td>
  <td class=xl66><div class="col-sm-10 col-sm-offset-1"><div
  class="row"><div class="col-sm-12"><div
  class="row extra-button"><button class="btn btn-lg
  btn-outline-primary btn-pill btn-xs full" data-toggle="modal"
  data-target="#modal-edit-manage-product" type="button"
  title="&#3649;&#3585;&#3657;&#3652;&#3586;"><span
  class="icon icon-lg
  icon-edit"></span></button></div></div></div></div></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 class=xl66 style='height:14.4pt'>B&amp;S</td>
  <td class=xl66>006*84</td>
  <td class=xl66>&#3629;&#3623;&#3609;&#3619;&#3640;&#3617;/&#3629;&#3623;&#3609;&#3627;&#3621;&#3634;&#3618;&#3594;&#3633;&#3657;&#3609;</td>
  <td class=xl66>2</td>
  <td class=xl66>0.06</td>
  <td class=xl66>84</td>
  <td class=xl66></td>
  <td class=xl66>CL</td>
  <td class=xl66>M206 &#3651;&#3618;&#3651;&#3627;&#3617;&#3656; SLX</td>
  <td class=xl66></td>
  <td class=xl66>-</td>
  <td class=xl66>43327</td>
  <td class=xl66></td>
  <td class=xl66></td>
  <td class=xl66></td>
  <td class=xl66></td>
  <td class=xl66></td>
  <td class=xl66></td>
  <td class=xl66></td>
  <td class=xl66>0</td>
  <td class=xl66>225</td>
  <td class=xl66></td>
  <td class=xl66>25</td>
  <td class=xl66></td>
  <td class=xl66>9</td>
  <td class=xl66>225</td>
  <td class=xl66>15</td>
  <td class=xl66>15</td>
  <td class=xl66>15</td>
  <td class=xl66>15</td>
  <td class=xl66>15</td>
  <td class=xl66>15</td>
  <td class=xl66>15</td>
  <td class=xl66><div class="col-sm-10 col-sm-offset-1"><div
  class="row"><div class="col-sm-12"><div
  class="row extra-button"><button class="btn btn-lg
  btn-outline-primary btn-pill btn-xs full" data-toggle="modal"
  data-target="#modal-edit-manage-product" type="button"
  title="&#3649;&#3585;&#3657;&#3652;&#3586;"><span
  class="icon icon-lg
  icon-edit"></span></button></div></div></div></div></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 class=xl66 style='height:14.4pt'>B&amp;S</td>
  <td class=xl66>80</td>
  <td class=xl66>&#3652;&#3609;&#3621;&#3656;&#3629;&#3609;</td>
  <td class=xl66>1</td>
  <td class=xl66>0.08</td>
  <td class=xl66>0</td>
  <td class=xl66></td>
  <td class=xl66>CL</td>
  <td class=xl66>SK06</td>
  <td class=xl66></td>
  <td class=xl66>-</td>
  <td class=xl66>43327</td>
  <td class=xl66></td>
  <td class=xl66></td>
  <td class=xl66></td>
  <td class=xl66></td>
  <td class=xl66></td>
  <td class=xl66></td>
  <td class=xl66></td>
  <td class=xl66>0</td>
  <td class=xl66>336</td>
  <td class=xl66></td>
  <td class=xl66>24</td>
  <td class=xl66></td>
  <td class=xl66>14</td>
  <td class=xl66>336</td>
  <td class=xl66>50</td>
  <td class=xl66>50</td>
  <td class=xl66>50</td>
  <td class=xl66>50</td>
  <td class=xl66>50</td>
  <td class=xl66>50</td>
  <td class=xl66>50</td>
  <td class=xl66><div class="col-sm-10 col-sm-offset-1"><div
  class="row"><div class="col-sm-12"><div
  class="row extra-button"><button class="btn btn-lg
  btn-outline-primary btn-pill btn-xs full" data-toggle="modal"
  data-target="#modal-edit-manage-product" type="button"
  title="&#3649;&#3585;&#3657;&#3652;&#3586;"><span
  class="icon icon-lg
  icon-edit"></span></button></div></div></div></div></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 class=xl66 style='height:14.4pt'>B&amp;S</td>
  <td class=xl66>84</td>
  <td class=xl66>&#3652;&#3609;&#3621;&#3656;&#3629;&#3609;&#3648;&#3614;&#3639;&#3656;&#3629;&#3629;&#3623;&#3609;&#3611;&#3633;&#3658;&#3617;</td>
  <td class=xl66>1</td>
  <td class=xl66>0.084</td>
  <td class=xl66>0</td>
  <td class=xl66></td>
  <td class=xl66>CL</td>
  <td class=xl66>SK06</td>
  <td class=xl66>&#3626;&#3637;&#3648;&#3586;&#3657;&#3617;</td>
  <td class=xl66>-</td>
  <td class=xl66>43340</td>
  <td class=xl66></td>
  <td class=xl66></td>
  <td class=xl66></td>
  <td class=xl66></td>
  <td class=xl66></td>
  <td class=xl66></td>
  <td class=xl66></td>
  <td class=xl66>0</td>
  <td class=xl66>80</td>
  <td class=xl66></td>
  <td class=xl66>20</td>
  <td class=xl66></td>
  <td class=xl66>4</td>
  <td class=xl66>80</td>
  <td class=xl66></td>
  <td class=xl66></td>
  <td class=xl66></td>
  <td class=xl66>30</td>
  <td class=xl66>30</td>
  <td class=xl66>30</td>
  <td class=xl66>30</td>
  <td class=xl66><div class="col-sm-10 col-sm-offset-1"><div
  class="row"><div class="col-sm-12"><div
  class="row extra-button"><button class="btn btn-lg
  btn-outline-primary btn-pill btn-xs full" data-toggle="modal"
  data-target="#modal-edit-manage-product" type="button"
  title="&#3649;&#3585;&#3657;&#3652;&#3586;"><span
  class="icon icon-lg
  icon-edit"></span></button></div></div></div></div></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 class=xl66 style='height:14.4pt'>B&amp;S</td>
  <td class=xl66>85</td>
  <td class=xl66>&#3650;&#3614;&#3621;&#3637;</td>
  <td class=xl66>1</td>
  <td class=xl66>0.085</td>
  <td class=xl66>0</td>
  <td class=xl66></td>
  <td class=xl66>CL</td>
  <td class=xl66>M206 &#3651;&#3618;&#3651;&#3627;&#3617;&#3656; SLX</td>
  <td class=xl66></td>
  <td class=xl66>-</td>
  <td class=xl66>43340</td>
  <td class=xl66></td>
  <td class=xl66></td>
  <td class=xl66></td>
  <td class=xl66></td>
  <td class=xl66></td>
  <td class=xl66></td>
  <td class=xl66></td>
  <td class=xl66>0</td>
  <td class=xl66>20</td>
  <td class=xl66></td>
  <td class=xl66>20</td>
  <td class=xl66></td>
  <td class=xl66>1</td>
  <td class=xl66>20</td>
  <td class=xl66></td>
  <td class=xl66></td>
  <td class=xl66></td>
  <td class=xl66>30</td>
  <td class=xl66>15</td>
  <td class=xl66>15</td>
  <td class=xl66>50</td>
  <td class=xl66><div class="col-sm-10 col-sm-offset-1"><div
  class="row"><div class="col-sm-12"><div
  class="row extra-button"><button class="btn btn-lg
  btn-outline-primary btn-pill btn-xs full" data-toggle="modal"
  data-target="#modal-edit-manage-product" type="button"
  title="&#3649;&#3585;&#3657;&#3652;&#3586;"><span
  class="icon icon-lg
  icon-edit"></span></button></div></div></div></div></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 class=xl66 style='height:14.4pt'>B&amp;S</td>
  <td class=xl66>90</td>
  <td class=xl66>&#3650;&#3614;&#3621;&#3637;&#3648;&#3614;&#3639;&#3656;&#3629;&#3629;&#3623;&#3609;&#3611;&#3633;&#3658;&#3617;</td>
  <td class=xl66>1</td>
  <td class=xl66>0.09</td>
  <td class=xl66>0</td>
  <td class=xl66></td>
  <td class=xl66>CL</td>
  <td class=xl66>SK06</td>
  <td class=xl66></td>
  <td class=xl66>-</td>
  <td class=xl66>43340</td>
  <td class=xl66></td>
  <td class=xl66></td>
  <td class=xl66></td>
  <td class=xl66></td>
  <td class=xl66></td>
  <td class=xl66></td>
  <td class=xl66></td>
  <td class=xl66>0</td>
  <td class=xl66>50</td>
  <td class=xl66></td>
  <td class=xl66>50</td>
  <td class=xl66></td>
  <td class=xl66>1</td>
  <td class=xl66>50</td>
  <td class=xl66></td>
  <td class=xl66></td>
  <td class=xl66></td>
  <td class=xl66>10</td>
  <td class=xl66>10</td>
  <td class=xl66>10</td>
  <td class=xl66>10</td>
  <td class=xl66><div class="col-sm-10 col-sm-offset-1"><div
  class="row"><div class="col-sm-12"><div
  class="row extra-button"><button class="btn btn-lg
  btn-outline-primary btn-pill btn-xs full" data-toggle="modal"
  data-target="#modal-edit-manage-product" type="button"
  title="&#3649;&#3585;&#3657;&#3652;&#3586;"><span
  class="icon icon-lg
  icon-edit"></span></button></div></div></div></div></td>
 </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
            </div>
        </div>










    </div>
</div>
<!--  -->
<? include('inc.footer.php');?>
<!--  -->
<? include('inc.footer.script.php');?>
<!--  -->
