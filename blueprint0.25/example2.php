
<!DOCTYPE html>
<head>
	<meta http-equiv="Content-type" content="text/html; charset=utf-8">
	<title>Timeline View Demo - dhtmlxScheduler</title>
	<meta name="description" content="This demo shows a JavaScript event calendar in Timeline View where the users can manage and track the ongoing tasks or projects.">
	<meta name="keywords" content="javascript, scheduler, event calendar, events calendar, event, calendar, ajax, google-like">
</head>
	<script src='https://dhtmlx.com/docs/products/dhtmlxScheduler/codebase/dhtmlxscheduler.js' type="text/javascript" charset="utf-8"></script>
	<script src='https://dhtmlx.com/docs/products/dhtmlxScheduler/codebase/ext/dhtmlxscheduler_timeline.js' type="text/javascript" charset="utf-8"></script>
	<script src='https://dhtmlx.com/docs/products/dhtmlxScheduler/codebase/ext/dhtmlxscheduler_daytimeline.js' type="text/javascript" charset="utf-8"></script>
	<script src='https://dhtmlx.com/docs/products/dhtmlxScheduler/codebase/ext/dhtmlxscheduler_treetimeline.js' type="text/javascript" charset="utf-8"></script>
    <script src='../../dhtmlxScheduler_v5.0.0/samples/common/dhtmlxTree/dhtmlxtree.js' type="text/javascript" charset="utf-8"></script>

	<link rel='STYLESHEET' type='text/css' href='https://dhtmlx.com/docs/products/dhtmlxScheduler/codebase/dhtmlxscheduler_material.css'>
	

<style type="text/css" media="screen">
	html, body{
		margin:0px;
		padding:0px;
		height:100%;
		overflow:hidden;
	}	
	.one_line{
		white-space:nowrap;
		overflow:hidden;
		padding-top:5px; padding-left:5px;
		text-align:left !important;
	}
</style>

<script type="text/javascript" charset="utf-8">
	function init() {
		window.resizeTo(950,700)
		modSchedHeight();
		
		scheduler.locale.labels.timeline_tab = "Timeline"
		scheduler.locale.labels.section_custom = "Section";
		scheduler.config.fix_tab_position = false;
		scheduler.config.details_on_create = true;
		scheduler.config.details_on_dblclick = true;
		scheduler.config.xml_date = "%Y-%m-%d %H:%i";
		
		scheduler.config.first_hour = 6;
		scheduler.config.last_hour = 23;
		//===============
		//Configuration
		//===============
		var sections=[
            {key:20, label:"Elizabeth Taylor"},
            {key:40, label:"John Williams"},
            {key:50, label:"David Miller"},
            {key:60, label:"Linda Brown"},
            {key:70, label:"George Lucas"},
            {key:80, label:"Kate Moss"},
            {key:90, label:"Dian Fossey"}
		];
		
		scheduler.createTimelineView({
			name:	"timeline",
			x_unit:	"minute",
			x_date:	"%H:%i",
			x_step:	60,
			x_size: 12,
			x_start: 8,
			x_length:	24,
			y_unit:	sections,
			y_property:	"section_id",
			render:"bar"
		});
        scheduler.locale.labels.treetimeline_tab = "Tree";
        var elements = [ // original hierarhical array to display
            {key:10, label:"Web Testing Dep.", open: true, children: [
                    {key:20, label:"Elizabeth Taylor"},
                    {key:30, label:"Managers",  children: [
                            {key:40, label:"John Williams"},
                            {key:50, label:"David Miller"}
                        ]},
                    {key:60, label:"Linda Brown"},
                    {key:70, label:"George Lucas"}
                ]},
            {key:110, label:"Human Relations Dep.", open:true, children: [
                    {key:80, label:"Kate Moss"},
                    {key:90, label:"Dian Fossey"}
                ]}
        ];
        scheduler.createTimelineView({
            section_autoheight: false,
            name:	"treetimeline",
            x_unit:	"minute",
            x_date:	"%H:%i",
            x_step:	30,
            x_size: 24,
            x_start: 16,
            x_length:	48,
            y_unit: elements,
            y_property:	"section_id",
            render: "tree",
            folder_dy:30,
            dy:60
        });

        scheduler.locale.labels.weektimeline_tab = "Week"
        scheduler.createTimelineView({
            name:	"weektimeline",
            x_unit:	"minute",
            x_date:	"%H:%i",
            x_step:	30,
            x_size: 24,
            x_start: 16,
            x_length:	48,

            render:"days",
            days:7
        });

        var dateToStr = scheduler.date.date_to_str("%j %F, %l");
        scheduler.templates["weektimeline_scale_label"] = function(section_id, section_label, section_options){
            return dateToStr(section_label);
        };

		//===============
		//Data loading
		//===============
		scheduler.config.lightbox.sections=[	
			{name:"description", height:130, map_to:"text", type:"textarea" , focus:true},
			{name:"custom", height:23, type:"select", options:sections, map_to:"section_id" },
			{name:"time", height:72, type:"time", map_to:"auto"}
		]

        scheduler.init('scheduler',new Date(2020,5,30),"treetimeline");
        scheduler.parse([
            { start_date: "2020-06-30 09:00", end_date: "2020-06-30 12:00", text:"Task A-12458", section_id:20},
            { start_date: "2020-06-30 10:00", end_date: "2020-06-30 16:00", text:"Task A-89411", section_id:20},
            { start_date: "2020-06-30 10:00", end_date: "2020-06-30 14:00", text:"Task A-64168", section_id:20},
            { start_date: "2020-06-30 16:00", end_date: "2020-06-30 17:00", text:"Task A-46598", section_id:20},

            { start_date: "2020-06-30 12:00", end_date: "2020-06-30 20:00", text:"Task B-48865", section_id:40},
            { start_date: "2020-06-30 14:00", end_date: "2020-06-30 16:00", text:"Task B-44864", section_id:40},
            { start_date: "2020-06-30 16:30", end_date: "2020-06-30 18:00", text:"Task B-46558", section_id:40},
            { start_date: "2020-06-30 18:30", end_date: "2020-06-30 20:00", text:"Task B-45564", section_id:40},

            { start_date: "2020-06-30 08:00", end_date: "2020-06-30 12:00", text:"Task C-32421", section_id:50},
            { start_date: "2020-06-30 14:30", end_date: "2020-06-30 16:45", text:"Task C-14244", section_id:50},

            { start_date: "2020-06-30 09:20", end_date: "2020-06-30 12:20", text:"Task D-52688", section_id:60},
            { start_date: "2020-06-30 11:40", end_date: "2020-06-30 16:30", text:"Task D-46588", section_id:60},
            { start_date: "2020-06-30 12:00", end_date: "2020-06-30 18:00", text:"Task D-12458", section_id:60}
        ],"json");
	}
</script>

<body onload="init();"  onresize="modSchedHeight()">
	
	
	
	
	
	<!-- info block
		href-prev
		href-next
		title
		desc-short
		desc-long
-->
    <style>
        a img{
            border: none;
        }
        li{
            list-style: none;
        }
    </style>
	<script>
		function modSchedHeight(){
			var headHeight = 100;
			var sch = document.getElementById("scheduler");
			sch.style.height = (parseInt(document.body.offsetHeight)-headHeight)+"px";
			var contbox = document.getElementById("contbox");
			contbox.style.width = (parseInt(document.body.offsetWidth)-300)+"px";
		}
	</script>
	<div style="position: relative; height:95px;background-color:#3D3D3D;border-bottom:5px solid #828282;">
		<a style="position: absolute; left: 25px; top: 22px; z-index: 10;" href="sample_units.shtml"><img src="images/btn-left.gif"></a>
		<div id="contbox" style="position: relative; padding: 22px 25px 0 75px; font: normal 17px Arial, Helvetica; color:white;">
			<div style="position: absolute; left: 75px; top: 22px; border-right:5px solid #2D8EB6;color:#2D8EB6;width:175px;height:50px;text-align:right;padding-right:25px;">Timeline View</div>
			<div style="padding-left: 205px; min-width: 400px;">
                <div style="font-size:12px;padding-left:20px;">See how you can manage and track the ongoing tasks or projects.</div>
    			<div style="font-size:12px;padding-left:20px;margin-top:5px;color:#949292;">You can add/edit/delete events, but changes will be available only until the demo is reloaded.</div>
            </div>
		</div>
		<a style="position: absolute; right: 25px; top: 22px;" href="sample_map.shtml"><img src="images/btn-right.gif"></a>
	</div>
	<!-- end. info block -->
    <ul>
        <li>
            <a></a>
            <span></span>
        </li>
    </ul>
	<!-- <div id="scheduler_here" class="dhx_cal_container" style='width:100%; height:100%;'>
		<div class="dhx_cal_navline">
			<div class="dhx_cal_prev_button">&nbsp;</div>
			<div class="dhx_cal_next_button">&nbsp;</div>
			<div class="dhx_cal_today_button"></div>
			<div class="dhx_cal_date"></div>
			<div class="dhx_cal_tab" name="timeline_tab" style="left:30px;"></div>
			<div class="dhx_cal_tab" name="treetimeline_tab" style="left:119px;"></div>
			<div class="dhx_cal_tab" name="weektimeline_tab" style="left:208px;"></div>
		</div>
		<div class="dhx_cal_header">
		</div>
		<div class="dhx_cal_data">
		</div>		
	</div> -->

    <div id="scheduler" class="dhx_cal_container" style='width:100%; height:400px;'>
                                                                    <div class="dhx_cal_navline" style="width:1085px;">
                                                                        <div class="dhx_cal_prev_button">&nbsp;</div>
                                                                        <div class="dhx_cal_next_button">&nbsp;</div>
                                                                        <div class="dhx_cal_today_button"></div>
                                                                        <div class="dhx_cal_date"></div>
                                                                        <div class="dhx_cal_tab" name="timeline_7days_tab" style="left: 0px;"></div>
                                                                        <div class="dhx_cal_tab" name="timeline_15days_tab" style="left: 70px;"></div>
                                                                        <div class="dhx_cal_tab" name="timeline_30days_tab" style="left: 140px;"></div>
                                                                    </div>
                                                                    <div class="dhx_cal_header">
                                                                    </div>
                                                                    <div class="dhx_cal_data">
                                                                    </div>
                                                                </div>

</body>