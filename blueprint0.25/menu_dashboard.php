<?php
    $select = 'open_menu_0';
    $select2 = 'menu_0';
    $select3 = 'menu_0';
    $select4 = 'menu_0';
	$select5 = 'menu_all';
	
	$typeSale = array('ขายใน', 'ขายต่าง');
	$orders = array('ออเดอร์', 'สอบถาม');

	$statusText = array('ผลิตเสร็จแล้ว', 'วางแผนแล้ว', 'กำลังผลิต', 'ปรับปรุงแผน<br>รออนุมัติ', 'รอวางแผน', 'ร่างแผนแล้ว<br>รออนุมัติ', 'ร่างแผนแล้ว<br>รอยืนยันสั่งซื้อ');
	$statusClass = array('success', 'info tone3', 'info tone2', 'warning tone2', 'default', 'warning', 'warning tone3');

	$productsName = array('1119P/013010400', '1ฑ090/013000402', '11190/013010400', '19090/013000402', '1ฌ094/013000502');
?>
<!--  -->
<? include('inc.header.php');?>
<!--  -->
<? include('inc.navbar.php');?>
<!--  -->
<? include('inc.menu.php');?>
<!-- <div class="layout-main"> -->
<!--  -->
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <h4 class="m-t-0">
                <!-- <span class="d-ib text-danger">--</span> <span class="icon icon-angle-double-right"></span> -->
                <!-- <span class="d-ib">ข้อมูลหลัก</span> -->
                <span class="d-ib text-primary">หน้าแรก
                <!-- <span class="d-ib">
                    <a class="title-bar-shortcut" href="#" title="Add to shortcut list" data-container="body" data-toggle-text="Remove from shortcut list" data-trigger="hover" data-placement="right" data-toggle="tooltip">
                        <span class="sr-only">Add to shortcut list</span>
                    </a>
                </span> -->
            </h4>
            <!-- <p class="title-bar-description">
                <small>Latest update on 01/01/2017 - 10.57 am.</small>
            </p> -->
        </div>

        <div class="row gutter-xs">
					<div class="col-xs-6 col-md-6">
                        <div class="card">
							<div class="card-header">
								<strong>ข้อมูลส่วนตัว</strong>
							</div>
                           <div class="card-body profile-header" data-toggle="match-height">
									<!-- <div class="col-sm-3 avatar">
											<img class="img-circle" src="img/2832982242.jpg" alt="ตะข่าย กว้างขวาง" style="max-width:65px;max-height:65px;">
									</div> -->
									<div class="col-sm-12 profile-info">
										<form class="form form-horizontal form-detail personal-detail">
											<div class="col-sm-12">
												<div class="col-sm-12">
													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label class="col-sm-6 control-label">รหัสพนักงาน</label>
																<div class="col-sm-6 n-p-l">
																	<p class="form-control-static">EM00001</p>
																</div>
															</div>
														</div>
														<div class="col-sm-6">
															<div class="form-group">
																<label class="col-sm-6 control-label">อีเมล</label>
																<div class="col-sm-6 n-p-l">
																	<p class="form-control-static">user@kkf.com</p>
																</div>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label class="col-sm-6 control-label">ชื่อ-นามสกุล</label>
																<div class="col-sm-6 n-p-l">
																	<p class="form-control-static">ชื่อผู้ใช้ นามสกุล</p>
																</div>
															</div>
														</div>
														<div class="col-sm-6">
															<div class="form-group">
																<label class="col-sm-6 control-label">แผนก</label>
																<div class="col-sm-6 n-p-l">
																	<p class="form-control-static">การตลาด</p>
																</div>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-6">
															<div class="form-group">
																<label class="col-sm-6 control-label">ตำแหน่ง</label>
																<div class="col-sm-6 n-p-l">
																	<p class="form-control-static">ผู้จัดการ</p>
																</div>
															</div>
														</div>
														<div class="col-sm-6">
															<div class="form-group">
																<label class="col-sm-6 control-label">สาขา</label>
																<div class="col-sm-6 n-p-l">
																	<p class="form-control-static">B&S สารคาม</p>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</form>
									</div>
								</div>
                        </div>
                    </div>

					<div class="col-xs-6 col-md-6">
                        <div class="card">
                                    <div class="card-header">
                                        <strong>การแจ้งเตือน</strong>
                                    </div>
                                    <div class="card-body notification" data-toggle="match-height">
										<div class="col-sm-12">
											<div class="col-sm-12">
												<div class="col-sm-12">
													<div class="row">
														<div class="col-md-8">
															<a href="#">ร่างแผนรายการสอบถามที่รอการอนุมัติ</a>
															<span class="icon-with-child">
																<span class="icon icon-bell-o icon-lg"></span>
																<span class="badge badge-danger badge-above right"><?=rand(1,10)?></span>
															</span>
														</div>
														<div class="col-sm-4">
															<a href="#">ข้อความถึงฉัน</a>
															<span class="icon-with-child">
																<span class="icon icon-bell-o icon-lg"></span>
																<span class="badge badge-danger badge-above right"><?=rand(1,10)?></span>
															</span>
														</div>
													</div>
													<div class="row">
														<div class="col-md-8">
															<a href="#">รายการออเดอร์ที่รอการอนุมัติ</a>
															<span class="icon-with-child">
																<span class="icon icon-bell-o icon-lg"></span>
																<span class="badge badge-danger badge-above right"><?=rand(1,10)?></span>
															</span>
														</div>
														<div class="col-sm-4">
															<a href="#">ข้อมูลข่าวสาร</a>
															<span class="icon-with-child">
																<span class="icon icon-bell-o icon-lg"></span>
																<span class="badge badge-danger badge-above right"><?=rand(1,10)?></span>
															</span>
														</div>
													</div>
													<div class="row">
														<div class="col-md-12">
															<a href="#">แผนการผลิตที่มีการเปลี่ยนแปลง รออนุมัติ</a>
															<span class="icon-with-child">
																<span class="icon icon-bell-o icon-lg"></span>
																<span class="badge badge-danger badge-above right"><?=rand(1,10)?></span>
															</span>
														</div>
													</div>
												</div>
											</div>
										</div>
                                    </div>
                                </div>
					</div>
					
					<div class="col-xs-6 col-md-12">
                        <div class="card">
							<div class="card-header">
								<strong>% ความคืบหน้า</strong>
							</div>
							<div class="card-body dashbord-graph-detail" data-toggle="match-height">
								<div class="media">
									<div class="col-sm-12">
										<a href="menu_mk_1.php">
											<img class="image" src="images/dashboard-graph.png">
										</a>
									</div>
								</div>
							</div>
						</div>
                    </div>

					<!-- <div class="col-xs-6 col-md-4">
                        <div class="card">
							<div class="card-header">
								<strong>% ความคืบหน้า</strong>
							</div>
							<div class="card-body dashbord-graph-detail" data-toggle="match-height">
								<div class="media">
									<div class="col-sm-4">
										<p><span>รายการสอบถาม</span></p>
										<div class="graph">
											<img src="images/graph01.jpg" style="max-width:60px;">
										</div>
									</div>
									<div class="col-sm-4">
										<p><span>รายการออเดอร์</span></p>
										<div class="graph">
											<img src="images/graph02.jpg" style="max-width:60px;">
										</div>
									</div>
									<div class="col-sm-4">
										<p><span>แผนเปลี่ยนแปลง</span></p>
										<div class="graph">
											<img src="images/graph03.jpg" style="max-width:60px;">
										</div>
									</div>
								</div>
							</div>
						</div>
                    </div> -->

					 <div class="col-xs-12">
                <!--  -->
                <div class="card">
                    <!-- <div class="card-header">
                        <a href="javascript:window.history.back(-1);" class="btn btn-sm btn-outline-primary"><span class="icon icon-long-arrow-left"></span>&nbsp;&nbsp;Back</a>&nbsp;&nbsp;
                        <strong class="">Latest update on 01/01/2017 - 10.57 am.</strong>
                    </div> -->
                    <div class="card-body">
						<!--  -->
						<div class="suggestion status">
							<div class="content">
								<div class="element">
									<a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="javascript:void(0)" target="_blank" type="button" title="แสดงแผน">
										<span class="icon icon-lg icon-file-text-o"></span>
									</a>
									<div class="text">แสดงแผน</div></div>
								<div class="element">
									<a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="javascript:void(0)" type="button" title="แก้ไข">
										<span class="icon icon-lg icon-edit"></span>
									</a>
									<div class="text">แก้ไข</div>
								</div>
								<div class="element">
									<button class="btn btn-outline-danger btn-pill btn-xs" type="button" title="ลบ">
										<span class="icon icon-lg icon-close"></span>
									</button>
									<div class="text">ลบ</div>
								</div>
							</div>
                        </div>

                        <div class="table-responsive">
                            <table class="table table-hover table-bordered text-center table-items" cellspacing="0" width="100%">
                                <thead>
									<tr class="at_bg_table_blue">
                                        <th class="text-center middle" rowspan="3">เลขที่</th>
                                        <th class="text-center middle" rowspan="3">ประเภท<br>รายการ</th>
                                        <th class="text-center middle" rowspan="3">ชื่อลูกค้า</th>
                                        <th class="text-center middle" rowspan="3">ประเภท<br>ขาย</th>
                                        <th class="text-center middle" rowspan="3">วันที่<br>สอบถาม /<br>สั่งซื้อ</th>
                                        <th class="text-center middle" colspan="4">วันที่ผลิต</th>
                                        <th class="text-center middle" colspan="2" rowspan="2">วันที่ส่งคลังสินค้า</th>
                                        <th class="text-center middle" rowspan="3">วันที่<br>จัดส่งสินค้า</th>
                                        <th class="text-center middle" rowspan="3">สถานะ</th>
                                        <th class="text-center middle" rowspan="3">ปรับแก้ไข</th>
                                    </tr>
                                    <tr class="at_bg_table_blue">
                                        <th class="text-center" colspan="2">ทอ</th>
                                        <th class="text-center" colspan="2">วงจรส่วนหลัง</th>
                                    </tr>
                                    <tr class="at_bg_table_blue">
                                        <th class="text-center">เริ่ม</th>
                                        <th class="text-center">เสร็จ</th>
                                        <th class="text-center">เริ่ม</th>
                                        <th class="text-center">เสร็จ</th>
                                        <th class="text-center">เริ่ม</th>
                                        <th class="text-center">เสร็จ</th>
                                    </tr>
                                </thead>
                                <tbody>
									<?php for($i=0; $i<10; $i++){ ?>
										<?php 
											$statusRand = rand(0,6);
											$typeOrderRand = rand(0,1);
										?>
										<tr>
											<td data-toggle="collapse" data-target="#item-<?=$i+1?>"><?=$typeOrderRand==0?"ORDER":"ENQUIRE"?>000<?=$i+1?></td>
											<td data-toggle="collapse" data-target="#item-<?=$i+1?>"><?=$orders[$typeOrderRand]?></td>
											<td data-toggle="collapse" data-target="#item-<?=$i+1?>">Customer00<?=$i+1?></td>
											<td data-toggle="collapse" data-target="#item-<?=$i+1?>"><?=$typeSale[rand(0,1)]?></td>
											<!-- <td>100</td> -->
											<td data-toggle="collapse" data-target="#item-<?=$i+1?>">20/07/2555</td>
											<td data-toggle="collapse" data-target="#item-<?=$i+1?>">20/07/2555</td>
											<td data-toggle="collapse" data-target="#item-<?=$i+1?>">20/07/2555</td>
											<td data-toggle="collapse" data-target="#item-<?=$i+1?>">20/07/2555</td>
											<td data-toggle="collapse" data-target="#item-<?=$i+1?>">20/07/2555</td>
											<td data-toggle="collapse" data-target="#item-<?=$i+1?>">20/07/2555</td>
											<td data-toggle="collapse" data-target="#item-<?=$i+1?>">20/07/2555</td>
											<td data-toggle="collapse" data-target="#item-<?=$i+1?>" class="top">20/07/2555</td>
											<?php
												$elementDiv = '';
												if($statusRand==3 || $statusRand==5){
													$elementDiv = "data-toggle=\"modal\" data-target=\"#approve-plan\"";
												}else if($statusRand==6){
													$elementDiv = "data-toggle=\"modal\" data-target=\"#modalConfirmInquiry\"";
												}
											?>
											<td class="left"><button class="btn btn-<?=$statusClass[$statusRand]?> status " type="button" <?=$elementDiv?>><?=$statusText[$statusRand]?></button></td>
											<td class="text-center" style="display: table-cell;">
												<a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="menu_mk_3.php" target="_blank" type="button" title="แสดงแผน">
													<span class="icon icon-lg icon-file-text-o"></span>
												</a>
												<a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="javascript:void(0)" type="button" title="แก้ไข" style="visibility:<?=$statusRand!=0?'visible':'hidden'?>">
													<span class="icon icon-lg icon-edit"></span>
												</a>
												<button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button" title="ลบ" style="visibility:<?=$statusRand!=0?'visible':'hidden'?>">
													<span class="icon icon-lg icon-close"></span>
												</button>
											</td>
										</tr>

										<tr class="cl">
                                            <td colspan="14" class="bg_item_in n-p-i">
                                                <div class="accordian-body collapse" id="item-<?=$i+1?>"> 
                                                    <table class="table table-bordered text-center table-hover" cellspacing="0" width="100%">
                                                        <thead>
                                                        <tr class="bg_table_in">
                                                            <th class="text-center middle" rowspan="3">หมายเลขร่างแผน</th>
                                                            <th class="text-center middle" rowspan="3">รหัสสินค้า</th>
                                                            <th class="text-center middle" rowspan="3">ประเภท<br>สินค้า</th>
                                                            <th class="text-center middle" colspan="2" rowspan="2">สั่งซื้อ</th>
                                                            <th class="text-center middle" colspan="4">วันที่ผลิต</th>
                                                            <th class="text-center middle" rowspan="3">จำนวน<br>ชุดทอ</th>
                                                            <th class="text-center middle" rowspan="3">จำนวน<br>ผืนทอ</th>
                                                            <th class="text-center middle" colspan="2" rowspan="2">วันที่ส่งคลังสินค้า</th>
                                                            <th class="text-center middle" rowspan="3">วันที่<br>จัดส่งสินค้า</th>
                                                            <th class="text-center middle" rowspan="3">สถานะในคลัง<br>สินค้าปัจจุบัน</th>
                                                        </tr>
                                                        <tr class="bg_table_in">
                                                            <th class="text-center" colspan="2">ทอ</th>
                                                            <th class="text-center" colspan="2">วงจรส่วนหลัง</th>
                                                        </tr>
                                                        <tr class="bg_table_in">
                                                            <th class="text-center">จำนวน</th>
                                                            <th class="text-center">หน่วย</th>
                                                            <th class="text-center">เริ่ม</th>
                                                            <th class="text-center">เสร็จ</th>
                                                            <th class="text-center">เริ่ม</th>
                                                            <th class="text-center">เสร็จ</th>
                                                            <th class="text-center">เริ่ม</th>
                                                            <th class="text-center">เสร็จ</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php for($j=0; $j<count($productsName); $j++){ ?>
                                                                <?php 
                                                                    $amountItem = rand(100, 300);
                                                                    $amountWovenDress = rand(10,20);
                                                                    $amountWeave = rand(300, 500);
                                                                ?>
                                                                <tr>
                                                                    <td class="<?=$j!=0?'f-white':''?>">DRAFT.ENQUIRE001R1</td>
                                                                    <td><?=$productsName[$j]?></td>
                                                                    <td>ข่ายเอ็น</td>
                                                                    <td><?=$amountItem?></td>
                                                                    <td>PC</td>
                                                                    <td>05/08/2556</td>
                                                                    <td>09/08/2556</td>
                                                                    <td>10/08/2556</td>
                                                                    <td>13/08/2556</td>
                                                                    <td><?=$amountWovenDress?></td>
                                                                    <td><?=$amountWeave?></td>
                                                                    <td>17/08/2556</td>
                                                                    <td>20/08/2556</td>
                                                                    <td>24/08/2556</td>
                                                                    <td><?=$amountWeave?></td>
                                                                </tr>
                                                            <?php } ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
									<?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
            </div>
        </div>
    </div>
</div>
<!--  -->
<? include('inc.footer.php');?>
<!--  -->
<? include('inc.footer.script.php');?>
<!--  -->
