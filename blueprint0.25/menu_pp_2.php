<?php
    $select = 'open_menu_5';
    $select2 = 'menu_pp_2';
?>

<!--  -->
<? include('inc.header.php');?>
<!--  -->
<? include('inc.navbar.php');?>
<!--  -->
<? include('inc.menu.php');?>
<!-- <div class="layout-main"> -->
<style>
.ui-draggable-dragging, .cell-highlight {
    z-index: 99;
    background-color: yellow;
}
</style>
<!--  -->
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <h4 class="m-t-0">
                <!-- <span class="d-ib text-danger">--</span> <span class="icon icon-angle-double-right"></span> -->
                <!-- <span class="d-ib">ข้อมูลหลัก</span> -->
                <span class="d-ib text-primary">การวางแผนการผลิต</span> <span class="icon icon-angle-double-right"></span> วางแผนการผลิต
                <!-- <span class="d-ib">
                    <a class="title-bar-shortcut" href="#" title="Add to shortcut list" data-container="body" data-toggle-text="Remove from shortcut list" data-trigger="hover" data-placement="right" data-toggle="tooltip">
                        <span class="sr-only">Add to shortcut list</span>
                    </a>
                </span> -->
            </h4>
            <!-- <p class="title-bar-description">
                <small>Latest update on 01/01/2017 - 10.57 am.</small>
            </p> -->
        </div>
        <!--  -->
        <!--  -->
        <!--  -->
        <!--  -->
        <!--  -->
        <div class="row gutter-xs">
            <div class="col-xs-12">
                <!--  -->
                <!--  -->
                <div class="card">
                    <!-- <div class="card-header">
                        <a href="javascript:window.history.back(-1);" class="btn btn-sm btn-outline-primary"><span class="icon icon-long-arrow-left"></span>&nbsp;&nbsp;Back</a>&nbsp;&nbsp;
                        <strong class="">Latest update on 01/01/2017 - 10.57 am.</strong>
                    </div> -->
                    <div class="card-body">
                        <div class="at_add_box">
                            <div class="row">
                                <div class="col-sm-6 m-b-sm">
                                    <h4 class="text-primary">การร่างแผนการผลิต</h4>
                                </div>
                                <div class="col-sm-6 text-right">
                                    <button class="btn btn-sm btn-primary btn-thick" type="submit">
                                    <span class="icon icon-download"></span>
                                    &nbsp;&nbsp;Save to not complete
                                    </button>
                                    <button class="btn btn-sm btn-primary btn-thick" type="submit">
                                    <span class="icon icon-download"></span>
                                    &nbsp;&nbsp;Save to plan completed
                                    </button>
                                    <!-- <button class="btn btn-info" data-toggle="modal" data-target="#Button_Addnew_Planning_Planning_list" type="button"><span class="icon icon-lg icon-plus"></span>&nbsp;&nbsp;Add order row</button> -->
                                    <!-- <button class="btn btn-info" type="button"><span class="icon icon-lg icon-refresh"></span>&nbsp;&nbsp;Update Data</button> -->
                                    <!-- <button class="btn btn-primary" type="button"><span class="icon icon-lg icon-file-excel-o"></span>&nbsp;&nbsp;Addition order file</button> -->
                                    <!-- <label class="btn btn-success file-upload-btn">
                                        <span class="icon icon-lg icon-file-excel-o"></span>&nbsp;&nbsp;Import
                                        <input class="file-upload-input" type="file" name="files[]" multiple="multiple">
                                    </label>
                                    <button class="btn btn-warning" type="button"><span class="icon icon-lg icon-file-text-o"></span>&nbsp;&nbsp;Export</button>
                                    <button class="btn btn-default" type="button"><span class="icon icon-lg icon-print"></span>&nbsp;&nbsp;Print</button> -->
                                </div>
                            </div>
                        </div>
                        <!--  -->
                        <div class="at_add_box">
                            <div class="table-responsive">
                        
                                <table id="planing-table" class="table table-bordered table-nowrap dataTable text-center no-footer" cellspacing="0" width="100%" aria-describedby="demo-datatables-2_info" role="grid" style="width: 100%;">
                                    <thead>
                                        <tr class="at_bg_table_blue">
                                            <th rowspan="2" class="text-center">Number</th>
                                            <th rowspan="2" class="text-center">Plan no.</th>
                                            <th rowspan="2" class="text-center">STATUS</th>
                                            <th rowspan="2" class="text-center">ORDER No.</th>
                                            <th rowspan="2" class="text-center">PROCESSCD</th>
                                            <th rowspan="2" class="text-center">AMOUNT</th>
                                            <th rowspan="2" class="text-center">MARK_DATE</th>
                                            <!-- <th colspan="4" class="text-center">OLD PLANED</th> -->
                                            <th colspan="4" class="text-center">NEW PLANED</th>
                                            <th rowspan="2" class="text-center">Edit date</th>
                                            <th rowspan="2" class="text-center">Planed by</th>
                                            <th rowspan="2" class="text-center">Approved date</th>
                                        </tr>
                                        <tr class="at_bg_table_blue">
                                            <th class="text-center">PROD_DATE</th>
                                            <th class="text-center">Estimate finish date</th>
                                            <th class="text-center">BRN_CODE</th>
                                            <th class="text-center">MACHINE</th>
                                        </tr>
                                    </thead>
                                    <tbody class="text-center">
                                        <tr data-name="KPI-17060120-C" data-duration="3" class="at_bg_table_orange_light">
                                            <td rowspan="12" valign="middle" class="at_bg_table_white_light">D-20170601-002</td>
                                            <td rowspan="3">D-20170601-002-A</td>
                                            <td class="column-status at_bg_table_orange">( 0% ) Planning</td>
                                            <td>KPI-17060120-C</td>
                                            <td>D04Y</td>
                                            <td>600 PC</td>
                                            <td>25-Aug-17</td>
                                            <!-- <td>15-Jul-17</td>
                                            <td>20-Aug-17</td>
                                            <td>BWC</td>
                                            <td>MC10</td> -->
                                            <td class="column-start">-</td>
                                            <td class="column-end">-</td>
                                            <td>-</td>
                                            <td>-</td>
                                            <td rowspan="3">-</td>
                                            <td rowspan="3">-</td>
                                            <td rowspan="3">-</td>
                                        </tr>
                                        <tr data-name="KPI-17061110-A" data-duration="4" class="at_bg_table_orange_light">
                                            <td class="column-status at_bg_table_orange">( 0% ) Planning</td>
                                            <td>KPI-17061110-A</td>
                                            <td>D04Y</td>
                                            <td>400 PC</td>
                                            <td>23-Sep-17</td>
                                            <!-- <td>15-Jul-17</td>
                                            <td>20-Aug-17</td>
                                            <td>BWC</td>
                                            <td>MC25</td> -->
                                            <td class="column-start">-</td>
                                            <td class="column-end">-</td>
                                            <td>-</td>
                                            <td>-</td>
                                        </tr>
                                        <tr data-name="KPI-17061456-B" data-duration="2" class="at_bg_table_orange_light">
                                            <td class="column-status at_bg_table_orange">( 0% ) Planning</td>
                                            <td>KPI-17061456-B</td>
                                            <td>D04Y</td>
                                            <td>100 PC</td>
                                            <td>22-Sep-17</td>
                                            <!-- <td>15-Jul-17</td>
                                            <td>20-Aug-17</td>
                                            <td>BWC</td>
                                            <td>MC15</td> -->
                                            <td class="column-start">-</td>
                                            <td class="column-end">-</td>
                                            <td>-</td>
                                            <td>-</td>
                                        </tr>
                                    </tbody>
                                </table>


                            </div>
                        </div>
                        <!--  -->
                        <div class="at_add_box">
                            <h4 class="at_text_black"><span class="icon icon-bar-chart-o"></span> Planning lead time by machine</h4>
                        </div>
                        <!--  -->
                        <div class="at_add_box">
                            <div id="scheduler" class="dhx_cal_container" style='width:100%; height:400px;'>
                                <div class="dhx_cal_navline">
                                    <div class="dhx_cal_prev_button">&nbsp;</div>
                                    <div class="dhx_cal_next_button">&nbsp;</div>
                                    <div class="dhx_cal_today_button"></div>
                                    <div class="dhx_cal_date"></div>
                                    <div class="dhx_cal_tab" name="timeline_7days_tab" style="left: 0px;"></div>
                                    <div class="dhx_cal_tab" name="timeline_15days_tab" style="left: 70px;"></div>
                                    <div class="dhx_cal_tab" name="timeline_30days_tab" style="left: 140px;"></div>
                                </div>
                                <div class="dhx_cal_header">
                                </div>
                                <div class="dhx_cal_data">
                                </div>
                            </div>
                        </div>
                        <!--  -->
                        <div class="at_add_box">
                            <h4 class="at_text_black"><span class="icon icon-calendar-check-o"></span> Production lead time</h4>
                        </div>
                        <!--  -->
                        <div class="at_add_box">
                            Chart 2
                        </div>
                        <!--  -->
                    </div>





                </div>
            </div>
        </div>
        <!--  -->
        <!--  -->
        <!--  -->
        <!--  -->
        <!--  -->
        <!--  -->
        <!--  -->
        <!--  -->
        <!--  -->
        <!--  -->
    </div>
</div>
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<div id="current_poduct_click" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12 m-b">
                            <label>Draft plan No.</label>
                            <input class="form-control" type="text" placeholder="R-20170801-001">
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-outline-primary" data-dismiss="modal" type="button">Leave</button>
                        <button class="btn btn-primary" data-dismiss="modal" type="button">Go to planning</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<div id="planning_create_new" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12 m-b">
                            <label>Revise plan No.</label>
                            <input class="form-control" type="text" placeholder="R-20170801-001">
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-outline-primary" data-dismiss="modal" type="button">Leave</button>
                        <button class="btn btn-primary" data-dismiss="modal" type="button">Go to revise plan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<div id="Planning_add_new_order" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Add new order</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12 m-b">
                            <label>Order No.</label>
                            <select id="form-control-21" class="custom-select custom-select-sm">
                                <option value="">Order No.</option>
                                <option value="">KPI-17061110-A</option>
                                <option value="">KPI-17061110-B</option>
                                <option value="">KPI-17061110-C</option>
                            </select>
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-outline-primary" data-dismiss="modal" type="button">Leave</button>
                        <button class="btn btn-primary" data-dismiss="modal" type="button">Add order</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<? include('inc.footer.php');?>
<!--  -->
<? include('inc.footer.script.php');?>
<!--  -->
<script src="https://code.jquery.com/jquery-latest.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
<script src="scheduler/codebase/dhtmlxscheduler.js"></script>
<script src="scheduler/codebase/ext/dhtmlxscheduler_timeline.js"></script>
<script src="js/timeline.js"></script>
<script></script>
