<?php
    $select = 'open_menu_5';
    $select2 = 'menu_pp_2';

    $index = array();
    $data = array();
    // $type = $_GET['type'];
    // $amount = $_GET['amount'];
    $type = 'all';
    $amount = 3;
    $hexColors = array(0x922B21, 0x6C3483, 0x1F618D, 0x0E6655, 0x0E6655, 0x9C640C, 0x797D7F, 0x212F3C);
    $bgColors = array('FADBD8', 'E8DAEF', 'D6EAF8', 'D1F2EB', 'FCF3CF', 'EAEDED', 'ffdfff');
    $fontColors = array();
    $bgTableFirst = array();
    $bgTableSecond = array();

    // echo 'value: ' . dechex($hexColors[0] + 1);
    
    foreach($hexColors as $item){
        $bgTableFirst[] = dechex($hexColors[0] + 1);
    }

    // echo "<div style='position:absolute;top:0;z-index:9999;color:#fff;'>";
    // print_r($bgTableFirst);

    // echo "</div>";
    
    // exit();

    $dataAll = array(
        array('IQR001', 'CUSTOMER001', 'ขายใน', '12/07/2556'),
        array('IQR002', 'CUSTOMER002', 'ขายต่าง', '12/07/2556'),
        array('IQR003', 'CUSTOMER003', 'ขายใน', '12/07/2556'),
        array('IQR004', 'CUSTOMER004', 'ขายใน', '12/07/2556'),
        array('IQR005', 'CUSTOMER005', 'ขายใน', '12/07/2556'),
        array('IQR006', 'CUSTOMER006', 'ขายใน', '12/07/2556'),
        array('IQR007', 'CUSTOMER007', 'ขายใน', '12/07/2556'),
        array('IQR008', 'CUSTOMER008', 'ขายต่าง', '12/07/2556'),
        array('IQR009', 'CUSTOMER009', 'ขายใน', '12/07/2556'),
        array('ORDER001', 'CUSTOMER001', 'ขายใน', '12/07/2556'),
        array('ORDER002', 'CUSTOMER002', 'ขายต่าง', '06/08/2556'),
        array('ORDER003', 'CUSTOMER003', 'ขายใน', '18/08/2556'),
        array('ORDER004', 'CUSTOMER004', 'ขายใน', '12/07/2556'),
        array('ORDER005', 'CUSTOMER005', 'ขายใน', '06/08/2556'),
        array('ORDER006', 'CUSTOMER006', 'ขายใน', '18/08/2556'),
        array('ORDER007', 'CUSTOMER007', 'ขายใน', '12/07/2556'),
        array('ORDER008', 'CUSTOMER008', 'ขายต่าง', '06/08/2556'),
        array('ORDER009', 'CUSTOMER009', 'ขายใน', '18/08/2556')
    );

    $dataOrder = array(
        array('ORDER001', 'CUSTOMER001', 'ขายใน', '12/07/2556'),
        array('ORDER002', 'CUSTOMER002', 'ขายต่าง', '06/08/2556'),
        array('ORDER003', 'CUSTOMER003', 'ขายใน', '18/08/2556'),
        array('ORDER004', 'CUSTOMER004', 'ขายใน', '12/07/2556'),
        array('ORDER005', 'CUSTOMER005', 'ขายใน', '06/08/2556'),
        array('ORDER006', 'CUSTOMER006', 'ขายใน', '18/08/2556'),
        array('ORDER007', 'CUSTOMER007', 'ขายใน', '12/07/2556'),
        array('ORDER008', 'CUSTOMER008', 'ขายต่าง', '06/08/2556'),
        array('ORDER009', 'CUSTOMER009', 'ขายใน', '18/08/2556')
    );

    $dataInquiry = array(
        array('IQR001', 'CUSTOMER001', 'ขายใน', '12/07/2556'),
        array('IQR002', 'CUSTOMER002', 'ขายต่าง', '12/07/2556'),
        array('IQR003', 'CUSTOMER003', 'ขายใน', '12/07/2556'),
        array('IQR004', 'CUSTOMER004', 'ขายใน', '12/07/2556'),
        array('IQR005', 'CUSTOMER005', 'ขายใน', '12/07/2556'),
        array('IQR006', 'CUSTOMER006', 'ขายใน', '12/07/2556'),
        array('IQR007', 'CUSTOMER007', 'ขายใน', '12/07/2556'),
        array('IQR008', 'CUSTOMER008', 'ขายต่าง', '12/07/2556'),
        array('IQR009', 'CUSTOMER009', 'ขายใน', '12/07/2556')
    );

    for($i=0; $i<$amount; $i++){
        
        $id = 'id' . ($i+1);
        // $index = $_GET[$id] - 1;
        $index = $i;

        if($type == 'all'){
            $data[] = $dataAll[$index];
        }else if($type == '1'){
            $data[] = $dataOrder[$index];
        }else{
            $data[] = $dataInquiry[$index];
        }
    }
?>

<!--  -->
<? include('inc.header.php');?>
<!--  -->
<? include('inc.navbar.php');?>
<!--  -->
<? include('inc.menu.php');?>
<!-- <div class="layout-main"> -->
<style>
.ui-draggable-dragging, .cell-highlight {
    z-index: 99;
    background-color: yellow;
}
</style>
<!--  -->
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <h4 class="m-t-0">
                <!-- <span class="d-ib text-danger">--</span> <span class="icon icon-angle-double-right"></span> -->
                <!-- <span class="d-ib">ข้อมูลหลัก</span> -->
                <span class="d-ib text-primary">การวางแผนการผลิต</span> <span class="icon icon-angle-double-right"></span> รายละเอียดวางแผนการผลิต
                <!-- <span class="d-ib">
                    <a class="title-bar-shortcut" href="#" title="Add to shortcut list" data-container="body" data-toggle-text="Remove from shortcut list" data-trigger="hover" data-placement="right" data-toggle="tooltip">
                        <span class="sr-only">Add to shortcut list</span>
                    </a>
                </span> -->
            </h4>
            <!-- <p class="title-bar-description">
                <small>Latest update on 01/01/2017 - 10.57 am.</small>
            </p> -->
        </div>
        <div class="row gutter-xs">
            <div class="col-xs-12">
                <div class="card">
                    <div class="card-body">
                        <div class="at_add_box">

                            <div class="card planning" aria-expanded="true" style="display:block;">
                                <div class="card-header at_bg_table_blue">
                                    <div class="card-actions">
                                        <button type="button" class="card-action card-toggler" title="Collapse" aria-expanded="true"></button>
                                    </div>
                                    <strong>วางแผนการผลิต</strong>
                                </div>  
                                <div class="card-body" style="display: block;">
                                    <form id="form-planning" method="get" class="form form-horizontal">
                                        <?php for($i=0; $i<$amount; $i++){ ?>
                                            <div class="card planning-item" aria-expanded="true" data-color="<?=$bgColors[$i]?>">
                                                <div class="card-header" style="background:#<?=$bgColors[$i]?>;color:#333;">
                                                    <div class="card-actions">
                                                            <button type="button" class="card-action card-toggler" title="Collapse" aria-expanded="true"></button>
                                                    </div>
                                                    <strong>รายการ <?=$data[$i][0]?></strong>
                                                </div>
                                                <div class="card-body item" style="display: block;">
                                                    <div class="row relative">
                                                        <div class="col-sm-6">
                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <div class="col-sm-12">
                                                                        <strong>รหัสออเดอร์ : </strong><span id="order-id"><?=$data[$i][0]?></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <div class="col-sm-12">
                                                                        <strong>ชื่อลูกค้า : </strong><span id="customer-name"><?=$data[$i][1]?></span>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <div class="col-sm-12">
                                                                        <strong>ประเภทขาย : </strong><span id="type-sale"><?=$data[$i][2]?></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    <div class="col-sm-12">
                                                                        <strong>วันที่สั่งซื้อ : </strong><span id="date-buy"><?=$data[$i][3]?></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <button class="btn btn-sm btn-primary btn-thick pull-right-bottom plan-all" type="button" onclick="onPlanAll(this)">
                                                            <span class="icon icon-calendar"></span>
                                                            <!-- <span class="spinner spinner-default spinner-sm input-icon"></span> -->
                                                            &nbsp;&nbsp;วางแผนอัตโนมัติ
                                                        </button>
                                                    </div>

                                                    <div class="table-responsive">
                                                        <table id="planing-table" class="table table-hover table-bordered table-nowrap dataTable text-center no-footer choose" cellspacing="0" width="100%" aria-describedby="demo-datatables-2_info" role="grid" style="width: 100%;">
                                                            <thead>
                                                                <tr class="at_bg_table_light_gray">
                                                                    <th class="text-center"></th>
                                                                    <th class="text-center"></th>
                                                                    <th class="text-center" colspan="2">วันที่ผลิต</th>
                                                                    <th class="text-center"></th>
                                                                    <th class="text-center"></th>
                                                                </tr>
                                                                <tr class="at_bg_table_light_gray">
                                                                    <th class="text-center">รหัสสินค้า</th>
                                                                    <th class="text-center">ประเภทสินค้า</th>
                                                                    <th class="text-center" style="width:100px;">เริ่ม</th>
                                                                    <th class="text-center" style="width:100px;">เสร็จ</th>
                                                                    <th class="text-center" style="width: 120px;">สถานะ</th>
                                                                    <th class="text-center" style="width: 120px;">ควบคุม</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody class="text-center">
                                                                <?php 
                                                                    $amountRand = rand(5,8);
                                                                    for($j=0; $j<$amountRand; $j++){
                                                                        $producId = RandomProductId();
                                                                ?>
                                                                <tr data-name="<?=$producId?>" data-duration="3" style="background:#<?=$bgColors[$i]?>;color:#333;">
                                                                    <td><?=$producId?></td>
                                                                    <td>ข่ายเอ็น</td>
                                                                    <td class="column-start"></td>
                                                                    <td class="column-end"></td>
                                                                    <td class="column-status at_bg_table_orange">( 0% ) วางแผน</td>
                                                                    <td class="control">
                                                                        <button class="btn btn-sm  btn-outline-primary outline btn-thick width-fit" type="button" name="plan-custom" data-toggle="modal" data-target="#modalPlanningCustom" onclick="onPlanItem(this, 'custom')">
                                                                            วางแผน
                                                                        </button>
                                                                        <button class="btn btn-sm btn-primary btn-thick width-fit" type="button" name="plan-auto" onclick="onPlanItem(this, 'auto')">
                                                                            อัตโนมัติ
                                                                        </button>
                                                                    </td>
                                                                </tr>
                                                                <?php } ?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>

                                        <div class="at_add_box">
                                            <div class="card" aria-expanded="true">
                                                <!-- <div class="card-header at_bg_table_light_gray">
                                                    <div class="card-actions">
                                                            <button type="button" class="card-action card-toggler" title="Collapse" aria-expanded="true"></button>
                                                    </div>
                                                    <strong>เลือกรายการ</strong>
                                                </div> -->
                                                <div class="card-body" style="display: block;">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="at_add_box relative">
                                                                <button class="btn btn-sm btn-primary btn-thick start-plan width-fit save-plan" type="button"  data-toggle="modal" data-target="#modalSavePlanning">
                                                                    <span class="icon icon-save"></span>
                                                                    &nbsp;&nbsp;บันทึกแผน
                                                                </button>
                                                                <div id="scheduler" class="dhx_cal_container" style='width:100%; height:1000px;'>
                                                                    <div class="dhx_cal_navline" style="width:1085px;">
                                                                        <div class="dhx_cal_prev_button">&nbsp;</div>
                                                                        <div class="dhx_cal_next_button">&nbsp;</div>
                                                                        <div class="dhx_cal_today_button"></div>
                                                                        <div class="dhx_cal_date"></div>
                                                                        <div class="dhx_cal_tab" name="timeline_7days_tab" style="left: 0px;"></div>
                                                                        <div class="dhx_cal_tab" name="timeline_15days_tab" style="left: 70px;"></div>
                                                                        <div class="dhx_cal_tab" name="timeline_30days_tab" style="left: 140px;"></div>
                                                                    </div>
                                                                    <div class="dhx_cal_header">
                                                                    </div>
                                                                    <div class="dhx_cal_data">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
    function RandomString()
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randstring = '';
        for ($i = 0; $i < 5; $i++) {
            $randstring .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randstring;
    }

    function RandomProductId()
    {
        $characters = '0123456789';
        $randstring = '';
        for ($i = 0; $i < 5; $i++) {
            $randstring .= $characters[rand(0, strlen($characters) - 1)];
        }
        return 'P' . $randstring;
    }
?>

<? include('inc.footer.php');?>
<!--  -->
<? include('inc.footer.script.php');?>

<script src="js/planning.js"></script>

<script src="js/jquery/ui/1.12.1/jquery-ui.min.js"></script>
<script src="scheduler/codebase/dhtmlxscheduler.js"></script>
<script src="scheduler/codebase/ext/dhtmlxscheduler_timeline.js"></script>
<script src='scheduler/codebase/dhtmlxscheduler-v5.js' type="text/javascript" charset="utf-8"></script>
<script src='scheduler/codebase/ext/dhtmlxscheduler_timeline.js' type="text/javascript" charset="utf-8"></script>
<script src='scheduler/codebase/ext/dhtmlxscheduler_daytimeline.js' type="text/javascript" charset="utf-8"></script>
<script src='scheduler/codebase/ext/dhtmlxscheduler_treetimeline.js' type="text/javascript" charset="utf-8"></script>



<!-- <script src="js/jquery/ui/1.12.1/jquery-ui.min.js"></script>
<script src="scheduler/codebase/dhtmlxscheduler.js"></script>
<script src="scheduler/codebase/ext/dhtmlxscheduler_timeline.js"></script>
<script src='scheduler/codebase/dhtmlxscheduler-v5.js' type="text/javascript" charset="utf-8"></script>
<script src='scheduler/codebase/ext/dhtmlxscheduler_timeline.js' type="text/javascript" charset="utf-8"></script>
<script src='scheduler/codebase/ext/dhtmlxscheduler_daytimeline.js' type="text/javascript" charset="utf-8"></script>
<script src='scheduler/codebase/ext/dhtmlxscheduler_treetimeline.js' type="text/javascript" charset="utf-8"></script>
 -->

<script src="js/timeline-plan.js"></script>
<script src="js/timeline-plan-detail.js"></script>

<script>

</script>
