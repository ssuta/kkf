<?php
    $select = 'open_menu_1';
    $select2 = 'menu_9';
    $select3 = 'menu_9';
    $select4 = 'menu_2_8';
?>
<!--  -->
<? include('inc.header.php');?>
<!--  -->
<? include('inc.navbar.php');?>
<!--  -->
<? include('inc.menu.php');?>
<!-- <div class="layout-main"> -->
<!--  -->
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <h4 class="m-t-0">
                <!-- <span class="d-ib text-danger">--</span> <span class="icon icon-angle-double-right"></span> -->
                <!-- <span class="d-ib">ข้อมูลหลัก</span> -->
                <span class="d-ib text-primary">การตั้งค่า</span> <span class="icon icon-angle-double-right"></span> ข้อมูลอ้างอิง <span class="icon icon-angle-double-right"></span> เบอร์ใย
                <!-- <span class="d-ib">
                    <a class="title-bar-shortcut" href="#" title="Add to shortcut list" data-container="body" data-toggle-text="Remove from shortcut list" data-trigger="hover" data-placement="right" data-toggle="tooltip">
                        <span class="sr-only">Add to shortcut list</span>
                    </a>
                </span> -->
            </h4>
            <!-- <p class="title-bar-description">
                <small>Latest update on 01/01/2017 - 10.57 am.</small>
            </p> -->
        </div>

        <div class="row gutter-xs">
            <div class="col-xs-12">
                <!--  -->
                <div class="card">
                    <!-- <div class="card-header">
                        <a href="javascript:window.history.back(-1);" class="btn btn-sm btn-outline-primary"><span class="icon icon-long-arrow-left"></span>&nbsp;&nbsp;Back</a>&nbsp;&nbsp;
                        <strong class="">Latest update on 01/01/2017 - 10.57 am.</strong>
                    </div> -->
                    <div class="card-body">
                        <!--  -->
                        <? include('inc.home_top_filter.php');?>
                        <!--  -->
                        <div class="table-responsive">
                         <table id="demo-datatables-2" class="table table-striped table-bordered table-nowrap dataTable text-center no-footer" cellspacing="0" width="100%">
                                <thead>
<tr class="at_bg_table_blue">
    <th class="text-center">รหัสเบอร์ใย</th>
    <th class="text-center">รายละเอียดเบอร์ใย</th>
    <th class="text-center">รหัสประเภทการฉีด</th>
    <th class="text-center">ประเภทการขาย</th>
    <th class="text-center">ประเภทวัตถุดิบ</th>
    <th class="text-center">เบอร์ใยจัดเรียง</th>
    <th class="text-center">จำนวนเส้นจัดเรียง</th>
    <th class="text-center">อักขระพิเศษ</th>
    <th class="text-center">ประเภทวัสดุพิเศษ</th>
    <th class="text-center">เริ่มใช้งาน</th>
    <th class="text-center">ปรับแก้ไข</th>
</tr>
                                </thead>
                                <tbody>

<tr>
    <td>006*21</td>
    <td>0.06*21</td>
    <td>2</td>
    <td>0</td>
    <td>T</td>
    <td>0.06</td>
    <td>21</td>
    <td></td>
    <td>วัสดุพิเศษ</td>
    <td>6/02/2017 15:36 น.</td>
    <td class="text-center" style="display: table-cell;">
         <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_2_8.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>


<tr>
    <td>006*28</td>
    <td></td>
    <td>2</td>
    <td>0</td>
    <td>T</td>
    <td>0.06</td>
    <td>28</td>
    <td></td>
    <td>วัสดุพิเศษ</td>
    <td>23/12/2009 15:48 น.</td>
    <td class="text-center" style="display: table-cell;">
         <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_2_8.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

<tr>
    <td>006*42</td>
    <td></td>
    <td>2</td>
    <td>0</td>
    <td>T</td>
    <td>0.06</td>
    <td>42</td>
    <td></td>
    <td>วัสดุพิเศษ</td>
    <td>23/12/2009 15:48 น.</td>
    <td class="text-center" style="display: table-cell;">
         <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_2_8.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

<tr>
    <td>006*63</td>
    <td>006*63</td>
    <td>2</td>
    <td>0</td>
    <td>T</td>
    <td>0.06</td>
    <td>63</td>
    <td></td>
    <td>วัสดุพิเศษ</td>
    <td>NULL</td>
    <td class="text-center" style="display: table-cell;">
         <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_2_8.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

<tr>
    <td>006*84</td>
    <td>006*84</td>
    <td>2</td>
    <td>0</td>
    <td>T</td>
    <td>0.06</td>
    <td>84</td>
    <td></td>
    <td></td>
    <td>NULL</td>
    <td class="text-center" style="display: table-cell;">
         <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_2_8.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

<tr>
    <td>80</td>
    <td></td>
    <td>1</td>
    <td>0</td>
    <td>M</td>
    <td>0.08</td>
    <td>0</td>
    <td></td>
    <td></td>
    <td>25/12/2009 12:08 น.</td>
    <td class="text-center" style="display: table-cell;">
         <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_2_8.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

<tr>
    <td>84</td>
    <td></td>
    <td>1</td>
    <td>0</td>
    <td>M</td>
    <td>0.084</td>
    <td>0</td>
    <td></td>
    <td></td>
    <td>25/12/2009 12:08 น.</td>
    <td class="text-center" style="display: table-cell;">
         <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_2_8.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

<tr>
    <td>85</td>
    <td></td>
    <td>1</td>
    <td>0</td>
    <td>M</td>
    <td>0.085</td>
    <td>0</td>
    <td></td>
    <td></td>
    <td>25/12/2009 12:08 น.</td>
    <td class="text-center" style="display: table-cell;">
         <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_2_8.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

<tr>
    <td>90</td>
    <td></td>
    <td>1</td>
    <td>0</td>
    <td>M</td>
    <td>0.09</td>
    <td>0</td>
    <td></td>
    <td></td>
    <td>25/12/2009 12:08 น.</td>
    <td class="text-center" style="display: table-cell;">
         <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_2_8.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

<tr>
    <td>009A</td>
    <td>009A</td>
    <td>1</td>
    <td>0</td>
    <td>M</td>
    <td>0.09</td>
    <td>0</td>
    <td>A</td>
    <td></td>
    <td>10/01/2018 15:03 น.</td>
    <td class="text-center" style="display: table-cell;">
         <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_2_8.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

<tr>
    <td>95</td>
    <td></td>
    <td>1</td>
    <td>0</td>
    <td>M</td>
    <td>0.095</td>
    <td>0</td>
    <td></td>
    <td></td>
    <td>25/12/2009 12:08 น.</td>
    <td class="text-center" style="display: table-cell;">
         <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_2_8.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

<tr>
    <td>10</td>
    <td></td>
    <td>1</td>
    <td>0</td>
    <td>M</td>
    <td>0.1</td>
    <td>0</td>
    <td></td>
    <td></td>
    <td>25/12/2009 12:08 น.</td>
    <td class="text-center" style="display: table-cell;">
         <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_2_8.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

<tr>
    <td>010 Q</td>
    <td>010 ใยแบน</td>
    <td>1</td>
    <td>0</td>
    <td>M</td>
    <td>0.1</td>
    <td>0</td>
    <td>Q</td>
    <td></td>
    <td>9/11/2017 9:26 น.</td>
    <td class="text-center" style="display: table-cell;">
         <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_2_8.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

<tr>
    <td>010A</td>
    <td>010 A</td>
    <td>1</td>
    <td>0</td>
    <td>M</td>
    <td>0.1</td>
    <td>0</td>
    <td>A</td>
    <td></td>
    <td>10/01/2018 15:03 น.</td>
    <td class="text-center" style="display: table-cell;">
         <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_2_8.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

<tr>
    <td>010*2</td>
    <td>010*2</td>
    <td>2</td>
    <td>0</td>
    <td>T</td>
    <td>0.1</td>
    <td>2</td>
    <td></td>
    <td></td>
    <td>20/03/2014 15:56 น.</td>
    <td class="text-center" style="display: table-cell;">
         <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_2_8.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

<tr>
    <td>010*3</td>
    <td>010*3</td>
    <td>2</td>
    <td>0</td>
    <td>T</td>
    <td>0.1</td>
    <td>3</td>
    <td></td>
    <td></td>
    <td>6/03/2014 17:29 น.</td>
    <td class="text-center" style="display: table-cell;">
         <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_2_8.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

<tr>
    <td>010*6</td>
    <td></td>
    <td>2</td>
    <td>0</td>
    <td>T</td>
    <td>0.1</td>
    <td>6</td>
    <td></td>
    <td></td>
    <td>23/12/2009 15:48 น.</td>
    <td class="text-center" style="display: table-cell;">
         <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_2_8.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

<tr>
    <td>010*8</td>
    <td>010*8</td>
    <td>2</td>
    <td>0</td>
    <td>T</td>
    <td>0.1</td>
    <td>8</td>
    <td></td>
    <td></td>
    <td>12/06/2017 16:14 น.</td>
    <td class="text-center" style="display: table-cell;">
         <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_2_8.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
            </div>
        </div>










    </div>
</div>
<!--  -->
<? include('inc.footer.php');?>
<!--  -->
<? include('inc.footer.script.php');?>
<!--  -->
