<?php
    $select = 'open_menu_1';
    $select2 = 'menu_5';
    $select3 = 'menu_5_2';
    $select4 = 'menu_3_4';
?>
<!--  -->
<? include('inc.header.php');?>
<!--  -->
<? include('inc.navbar.php');?>
<!--  -->
<? include('inc.menu.php');?>
<!-- <div class="layout-main"> -->
<!--  -->
<div class="layout-content">
    <div class="layout-content-body">
		<div class="title-bar">
            <h1 class="title-bar-title">
              <span class="d-ib">เครื่องทอ</span>
            </h1>
            <div class="title-bar-description">
				 <span class="d-ib text-primary">การตั้งค่า</span> <span class="icon icon-angle-double-right"></span> กระบวนการผลิต <span class="icon icon-angle-double-right"></span> เครื่องทอ <span class="icon icon-angle-double-right"></span> แก้ไข
			</div>
		</div>
		<div class="row">
		
            <div class="col-md-12">
				 
                <!--  -->
                <div class="demo-form-wrapper">
                        <!--  -->
					<form class="form form-horizontal">
						<div class="panel m-b-lg">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#information" data-toggle="tab" aria-expanded="true">ข้อมูลทั่วไป</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane fade active in" id="home">
								<div class="card" aria-expanded="true">
                            <div class="card-header at_bg_table_light_gray">
                               <div class="card-actions">
                                    <button type="button" class="card-action card-toggler" title="Collapse" aria-expanded="true"></button>
                                    <!-- <button type="button" class="card-action card-reload" title="Reload"></button>
                                    <button type="button" class="card-action card-remove" title="Remove"></button> -->
                                </div>
                                <strong>เครื่องทอ</strong>
                            </div>
                            <div class="card-body" style="display: block;">
								<form class="form horizontal">
									<div class="form-group">
										<label class="col-sm-2 control-label" for="form-control-1">รหัสเครื่องทอ</label>
										<div class="col-sm-4">
											<input id="form-control-1" class="form-control" type="text" value="A001">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="form-control-2">ชื่อเครื่องทอ</label>
										<div class="col-sm-4">
											<input id="form-control-2" class="form-control" type="text" value="ED  7.5 * 130 * 785">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="form-control-2">รหัสกลุ่มเครื่องทอ</label>
										<div class="col-sm-4">
											<input id="form-control-2" class="form-control" type="text" value="CN">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="form-control-2">จน. กระสวย</label>
										<div class="col-sm-4">
											<input id="form-control-2" class="form-control" type="text" value="785">
										</div>
									</div>
									<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">โรงทอ</label>
												<div class="col-sm-4">
													<select id="form-control-6" class="form-control">
														<option value="1">M : โรงทอ M</option>
														<option value="2">S : โรงทอ S</option>
														<option value="3">C : โรงทอ C</option>
														<option value="4">T : โรงทอ T</option>
													</select>
												</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="form-control-1">เริ่มใช้งาน</label>
										<div class="col-sm-4">
											 <div class=" input-with-icon">
											  <input class="form-control" type="text" data-provide="datepicker" value="11/13/2009  5:09:08 PM">
												<span class="icon icon-calendar input-icon"></span>
											 </div>
										</div>
									</div>
								</form>
                            </div>

							</div>
							</div>
							</div>
						</div>
						<div class="m-t text-center">
						<a class="btn btn-lg btn-primary" href="menu_4_3.php" type="submit">บันทึก</a>
                        <a class="btn btn-lg btn-default" href="menu_4_3.php" type="button>">ยกเลิก</a>
						</div>
					</form>
                    
                        <!--  -->
                    </div>
                </div>
                <!--  -->
            </div>
        </div>







    </div>
</div>
<!--  -->
<? include('inc.footer.php');?>
<!--  -->
<? include('inc.footer.script.php');?>
<!--  -->