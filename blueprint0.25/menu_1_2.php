<?php
    $select = 'open_menu_1';
    $select2 = 'menu_1';
    $select3 = 'menu_1';
    $select4 = 'menu_1_2';
?>
<!--  -->
<? include('inc.header.php');?>
<!--  -->
<? include('inc.navbar.php');?>
<!--  -->
<? include('inc.menu.php');?>
<!-- <div class="layout-main"> -->
<!--  -->
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <h4 class="m-t-0">
                <!-- <span class="d-ib text-danger">--</span> <span class="icon icon-angle-double-right"></span> -->
                <!-- <span class="d-ib">ข้อมูลหลัก</span> -->
                <span class="d-ib text-primary">ข้อมูลหลัก</span> <span class="icon icon-angle-double-right"></span> โรงงาน <span class="icon icon-angle-double-right"></span> โรงทอ
                <!-- <span class="d-ib">
                    <a class="title-bar-shortcut" href="#" title="Add to shortcut list" data-container="body" data-toggle-text="Remove from shortcut list" data-trigger="hover" data-placement="right" data-toggle="tooltip">
                        <span class="sr-only">Add to shortcut list</span>
                    </a>
                </span> -->
            </h4>
            <!-- <p class="title-bar-description">
                <small>Latest update on 01/01/2017 - 10.57 am.</small>
            </p> -->
        </div>

        <div class="row gutter-xs">
            <div class="col-xs-12">
                <!--  -->
                <div class="card">
                    <!-- <div class="card-header">
                        <a href="javascript:window.history.back(-1);" class="btn btn-sm btn-outline-primary"><span class="icon icon-long-arrow-left"></span>&nbsp;&nbsp;Back</a>&nbsp;&nbsp;
                        <strong class="">Latest update on 01/01/2017 - 10.57 am.</strong>
                    </div> -->
                    <div class="card-body">
                        <!--  -->
                        <? include('inc.home_top_filter.php');?>
                        <!--  -->
                        <div class="table-responsive">
                            <table id="demo-datatables-2" class="table table-striped table-bordered table-nowrap dataTable text-center no-footer" cellspacing="0" width="100%">
                                <thead>
                                    <tr class="at_bg_table_blue">
                                        <th class="text-center">รหัสโรงทอ</th>
                                        <th class="text-center">ชื่อโรงทอ</th>
                                        <th class="text-center">รหัสสาขา</th>
                                        <th class="text-center">เริ่มใช้งาน</th>
                                        <th class="text-center">ปรับแก้ไข</th>
                                    </tr>
                                </thead>
                                <tbody>
<tr>
    <td>M</td>
    <td>โรงทอ M</td>
    <td>B&amp;S</td>
    <td>21/07/2549 14:46 น.</td>
	<td class="text-center" style="display: table-cell;">
         <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_1_2.php" type="button">
	 <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
<tr>
    <td>S</td>
    <td>โรงทอ S</td>
    <td>B&amp;S</td>
    <td>1/07/2552 18:01 น.</td>
    <td class="text-center" style="display: table-cell;">
         <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_1_2.php" type="button">
	 <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
<tr>
    <td>C</td>
    <td>โรงทอ C</td>
    <td>BWC</td>
    <td>21/07/2549 14:43 น.</td>
    <td class="text-center" style="display: table-cell;">
       <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_1_2.php" type="button">
	 <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
<tr>
    <td>T</td>
    <td>โรงทอ T</td>
    <td>CY</td>
    <td>8/09/2552 20:50 น.</td>
    <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_1_2.php" type="button">
	 <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
<tr>
    <td>V</td>
    <td>โรงทอ V</td>
    <td>CY</td>
    <td>6/10/2549 16:15 น.</td>
    <td class="text-center" style="display: table-cell;">
         <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_1_2.php" type="button">
	 <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
<tr>
    <td>F</td>
    <td>โรงทอ F</td>
    <td>FM</td>
    <td>10/10/2560 16:28 น.</td>
    <td class="text-center" style="display: table-cell;">
         <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_1_2.php" type="button">
	 <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
<tr>
    <td>L</td>
    <td>โรงทอ L</td>
    <td>FM1</td>
    <td>9/01/2557 11:12 น.</td>
    <td class="text-center" style="display: table-cell;">
       <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_1_2.php" type="button">
	 <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
<tr>
    <td>E</td>
    <td>โรงทอ E</td>
    <td>KKF</td>
    <td>21/07/2549 14:43 น.</td>
    <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_1_2.php" type="button">
	 <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
<tr>
    <td>P</td>
    <td>อวนปั๊ม</td>
    <td>KKF1</td>
    <td>22/09/2552 15:30 น.</td>
    <td class="text-center" style="display: table-cell;">
         <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_1_2.php" type="button">
	 <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
<tr>
    <td>N</td>
    <td>โรงทอด้าย N</td>
    <td>KKF2</td>
    <td>5/04/2560 14:45 น.</td>
    <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_1_2.php" type="button">
	 <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
<tr>
    <td>A</td>
    <td>โรงทอ A</td>
    <td>NR</td>
    <td>21/07/2549 14:45 น.</td>
    <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_1_2.php" type="button">
	 <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
<tr>
    <td>X</td>
    <td>โรงทอ สาขา เมืองจีน</td>
    <td>XFF</td>
    <td>28/02/2555 17:03 น.</td>
    <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_1_2.php" type="button">
	 <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
            </div>
        </div>










    </div>
</div>
<!--  -->
<? include('inc.footer.php');?>
<!--  -->
<? include('inc.footer.script.php');?>
<!--  -->