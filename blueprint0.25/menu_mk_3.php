﻿<?php
    $select = 'open_menu_2';
    $select2 = 'menu_mk_3';
    $select3 = 'menu_mk_3';
    $select4 = 'menu_mk_3';
?>
<!--  -->
<? include('inc.header.php');?>
<!--  -->
<? include('inc.navbar.php');?>
<!--  -->
<? include('inc.menu.php');?>
<!-- <div class="layout-main"> -->
<!--  -->
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <h4 class="m-t-0">
                <!-- <span class="d-ib text-danger">--</span> <span class="icon icon-angle-double-right"></span> -->
                <!-- <span class="d-ib">ข้อมูลหลัก</span> -->
                <span class="d-ib text-primary">การตลาด</span> <span class="icon icon-angle-double-right"></span> รายละเอียดออเดอร์ <span class="icon icon-angle-double-right"></span> แสดงแผนรายการ ORDER0003
                <!-- <span class="d-ib">
                    <a class="title-bar-shortcut" href="#" title="Add to shortcut list" data-container="body" data-toggle-text="Remove from shortcut list" data-trigger="hover" data-placement="right" data-toggle="tooltip">
                        <span class="sr-only">Add to shortcut list</span>
                    </a>
                </span> -->
            </h4>
            <!-- <p class="title-bar-description">
                <small>Latest update on 01/01/2017 - 10.57 am.</small>
            </p> -->
        </div>

        <div class="row gutter-xs">
            <div class="col-xs-12">
                <!--  -->
                <div class="card">
                    <!-- <div class="card-header">
                        <a href="javascript:window.history.back(-1);" class="btn btn-sm btn-outline-primary"><span class="icon icon-long-arrow-left"></span>&nbsp;&nbsp;Back</a>&nbsp;&nbsp;
                        <strong class="">Latest update on 01/01/2017 - 10.57 am.</strong>
                    </div> -->
                    <div class="card-body">
                        <!--  -->
                        <? 
                            // include('inc.home_top_filter_extra_order.php');
                        ?>
                        <div class="at_add_box">
                            <div class="card" aria-expanded="true">
                                <div class="card-header at_bg_table_light_gray">
                                    <div class="card-actions">
                                            <button type="button" class="card-action card-toggler" title="Collapse" aria-expanded="true"></button>
                                            <!-- <button type="button" class="card-action card-reload" title="Reload"></button>
                                            <button type="button" class="card-action card-remove" title="Remove"></button> -->
                                    </div>
                                    <strong>รายละเอียดออเดอร์</strong>
                                </div>
                                <div class="card-body" style="display: block;">
                                    <div class="row">

                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="col-sm-6 control-label text-right" for="form-control-14">เลขที่ออเดอร์ : </label>
                                                <label class="col-sm-6 control-label text-left" for="form-control-14">ORDER0001</label>
                                            </div>
                                        </div>


                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="col-sm-6 control-label text-right" for="form-control-14">เลขที่ใบสั่งผลิต : </label>
                                                <label class="col-sm-6 control-label text-left" for="form-control-14">18/04124-A</label>
                                            </div>
                                        </div>
                                        
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="col-sm-6 control-label text-right" for="form-control-14">ลูกค้า/กลุ่ม : </label>
                                                <label class="col-sm-6 control-label text-left" for="form-control-14">A0210</label>
                                            </div>
                                        </div>
                                        
                                        
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="col-sm-6 control-label text-right" for="form-control-14">ประเทศ : </label>
                                                <label class="col-sm-6 control-label text-left" for="form-control-14">มาเลเซีย</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="col-sm-6 control-label text-right" for="form-control-14">วันที่สั่งผลิต : </label>
                                                <label class="col-sm-6 control-label text-left" for="form-control-14">07/05/2556</label>
                                            </div>
                                        </div>


                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="col-sm-6 control-label text-right" for="form-control-14">วันที่ปิด LOT : </label>
                                                <label class="col-sm-6 control-label text-left" for="form-control-14">31/07/2556</label>
                                            </div>
                                        </div>

                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="col-sm-6 control-label text-right" for="form-control-14">วันที่โอนข้อมูลสั่งผลิต : </label>
                                                <label class="col-sm-6 control-label text-left" for="form-control-14">07/05/2556</label>
                                            </div>
                                        </div>


                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label class="col-sm-6 control-label text-right" for="form-control-14">Shipping Mark : </label>
                                                <label class="col-sm-6 control-label text-left" for="form-control-14">A021001 - SION LEE B/No.</label>
                                            </div>
                                        </div>
                                    </div>

                                   
                                </div>
                            </div>
                        </div>

						<div class="suggestion">
							<div class="content">
								<div class="element"><div class="square gray"></div><div class="text">แผนที่วาง</div></div>
								<div class="element"><div class="square black"></div><div class="text">ตามที่ผลิตจริง</div></div>
								<div class="element"><div class="square red"></div><div class="text">ตามที่ผลิตจริงและไม่ทันกำหนดส่ง</div></div>
							</div>
                        </div>
                        
						<div id="section-1">
							<div class="table-responsive">
								<table id="table-detail-order" class="table table-hover table-bordered table-items text-center" cellspacing="0" width="100%">
									<thead>
										<tr class="at_bg_table_blue">
											<th class="text-center middle" rowspan="3">รหัสสินค้า</th>
											<th class="text-center middle" rowspan="3">ประเภทสินค้า</th>
											<th class="text-center middle" colspan="2" rowspan="2">สั่งซื้อ</th>
											<th class="text-center middle" colspan="4">วันที่ผลิต</th>
											<th class="text-center middle" rowspan="3">จำนวนชุดทอ</th>
											<th class="text-center middle" rowspan="3">จำนวนผืนทอ</th>
											<th class="text-center middle" colspan="2" rowspan="2">วันที่ส่งคลังสินค้า</th>
											<th class="text-center middle" rowspan="3">วันที่<br>จัดส่งสินค้า</th>
											<th class="text-center middle" rowspan="3">สถานะในคลัง<br>สินค้าปัจจุบัน</th>
										</tr>
                                        <tr class="at_bg_table_blue">
                                            <th class="text-center" colspan="2">ทอ</th>
                                            <th class="text-center" colspan="2">วงจรส่วนหลัง</th>
                                        </tr>
                                        <tr class="at_bg_table_blue">
                                            <th class="text-center">จำนวน</th>
                                            <th class="text-center">หน่วย</th>
                                            <th class="text-center">เริ่ม</th>
                                            <th class="text-center">เสร็จ</th>
                                            <th class="text-center">เริ่ม</th>
                                            <th class="text-center">เสร็จ</th>
                                            <th class="text-center">เริ่ม</th>
                                            <th class="text-center">เสร็จ</th>
                                        </tr>
									</thead>
									<tbody>

									<tr height=19 style='height:14.4pt' data-toggle="collapse" data-target="#item-1" onclick="showGraphDetail(1)">
                                            <td class="black" width=186 style='width:139pt'>1119P/013010400</td>
  <td class="black text-center" width=138 style='text-align:center;'>&#3586;&#3656;&#3634;&#3618;&#3648;&#3629;&#3655;&#3609;</td>
  <td class="black" align=right width=138 style='width:103pt'>100</td>
  <td class="black" width=138 style='width:103pt'>PC</td>
  <td class="black" align=right width=173 style='width:130pt'>7/20/2012</td>
  <td class="black" align=right width=173 style='width:130pt'>7/20/2012</td>
  <td class="black" align=right width=173 style='width:130pt'>7/20/2012</td>
  <td class="black" align=right width=173 style='width:130pt'>7/20/2012</td>
  <td class="black" align=right width=138 style='width:103pt'>10</td>
  <td class="black" align=right width=138 style='width:103pt'>345</td>
  <td class="black" align=right width=174 style='width:131pt'>7/20/2012</td>
  <td class="black" align=right width=174 style='width:131pt'>7/20/2012</td>
  <td class="black" align=right width=138 style='width:103pt'>7/20/2012</td>
  <td class="black" align=center width=178 style='width:134pt'>345</td>
 </tr>

<tr class="cl">
    <td colspan="14" class="bg_item_in n-p-i">
        <div class="accordian-body collapse" id="item-1"> 
            <div id="graph1"></div>
        </div>
    </td>
</tr>


 <tr height=19 style='height:14.4pt' data-toggle="collapse" data-target="#item-2" onclick="showGraphDetail(2)">
  
        <td class="black">1&#3601;090/013000402</td>
  <td class="black">&#3586;&#3656;&#3634;&#3618;&#3648;&#3629;&#3655;&#3609;</td>
  <td class="black" align=right>100</td>
  <td class="black">PC</td>
  <td class="black" align=right>7/20/2012</td>
  <td class="gray" align=right>7/20/2012</td>
  <td class="gray" align=right>7/20/2012</td>
  <td class="gray" align=right>7/20/2012</td>
  <td class="black" align=right>10</td>
  <td class="black" align=right>345</td>
  <td class="gray" align=right>7/20/2012</td>
  <td class="gray" align=right>7/20/2012</td>  
  <td class="black"></td>
  <td class="black">-</td>
 </tr>

<tr class="cl">
    <td colspan="14" class="bg_item_in n-p-i">
        <div class="accordian-body collapse" id="item-2"> 
            <div id="graph2"></div>
        </div>
    </td>
</tr>

 <tr height=19 style='height:14.4pt' data-toggle="collapse" data-target="#item-3" onclick="showGraphDetail(3)">
  
        <td class="black">11190/013010400</td>
        <td class="black">&#3586;&#3656;&#3634;&#3618;&#3648;&#3629;&#3655;&#3609;</td>
  <td class="black" align=right>100</td>
  <td class="black">PC</td>
  <td class="black" align=right>7/20/2012</td>
  <td class="gray" align=right>7/20/2012</td>
  <td class="black" align=right>7/20/2012</td>
  <td class="gray" align=right>7/20/2012</td>
  <td class="black" align=right>10</td>
  <td class="black" align=right>345</td>
  <td class="black" align=right>7/20/2012</td>
  <td class="gray" align=right>7/20/2012</td>
  <td class=""></td>
  <td class="black"></td> 
 </tr>

 <tr class="cl">
    <td colspan="14" class="bg_item_in n-p-i">
        <div class="accordian-body collapse" id="item-3"> 
            <div id="graph3"></div>
        </div>
    </td>
</tr>

 <tr height=19 style='height:14.4pt' data-toggle="collapse" data-target="#item-4" onclick="showGraphDetail(4)">
  
        <td class="black">19090/013000402</td>
        <td class="black">&#3586;&#3656;&#3634;&#3618;&#3648;&#3629;&#3655;&#3609;</td>
  <td class="black" align=right>100</td>
  <td class="black">PC</td>
  <td class="gray" align=right>7/20/2012</td>
  <td class="gray" align=right>7/20/2012</td>
  <td class="gray" align=right>7/20/2012</td>
  <td class="gray" align=right>7/20/2012</td>
  <td class="black" align=right>10</td>
  <td class="black" align=right>345</td>
  <td class="gray" align=right>7/20/2012</td>
  <td class="gray" align=right>7/20/2012</td>  
  <td class=""></td>
  <td class="black">-</td>
 </tr>
 
 <tr class="cl">
    <td colspan="14" class="bg_item_in n-p-i">
        <div class="accordian-body collapse" id="item-4"> 
            <div id="graph4"></div>
        </div>
    </td>
</tr>

 <tr height=19 style='height:14.4pt' data-toggle="collapse" data-target="#item-5" onclick="showGraphDetail(5)">
  
        <td class="black">1&#3596;094/013000502</td>
        <td class="black">&#3586;&#3656;&#3634;&#3618;&#3648;&#3629;&#3655;&#3609;</td>
  <td class="black" align=right>100</td>
  <td class="black">PC</td>
  <td class="black" align=right>7/20/2012</td>
  <td class="gray" align=right>7/20/2012</td>
  <td class="black" align=right>7/20/2012</td>
  <td class="gray" align=right>7/20/2012</td>
  <td class="black" align=right>10</td>
  <td class="black" align=right>345</td>
  <td class="gray" align=right>7/20/2012</td>
  <td class="gray" align=right>7/20/2012</td>  
  <td class=""></td>
  <td class="black">-</td>
 </tr>
 
 <tr class="cl">
    <td colspan="14" class="bg_item_in n-p-i">
        <div class="accordian-body collapse" id="item-5"> 
            <div id="graph5"></div>
        </div>
    </td>
</tr>

									</tbody>
								</table>
							</div>
						</div>
                    </div>
                </div>
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
            </div>
        </div>
    </div>
</div>

<!--  -->
<? include('inc.footer.php');?>
<!--  -->
<? include('inc.footer.script.php');?>
<!--  -->

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/xrange.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

<script src="js/graph.js"></script>
