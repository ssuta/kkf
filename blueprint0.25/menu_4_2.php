<?php
    $select = 'open_menu_1';
    $select2 = 'menu_3';
    $select3 = 'menu_3';
    $select4 = 'menu_3_3';
?>
<!--  -->
<? include('inc.header.php');?>
<!--  -->
<? include('inc.navbar.php');?>
<!--  -->
<? include('inc.menu.php');?>
<!-- <div class="layout-main"> -->
<!--  -->
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <h4 class="m-t-0">
                <!-- <span class="d-ib text-danger">--</span> <span class="icon icon-angle-double-right"></span> -->
                <!-- <span class="d-ib">ข้อมูลหลัก</span> -->
                <span class="d-ib text-primary">ข้อมูลหลัก</span> <span class="icon icon-angle-double-right"></span> เครื่องจักร <span class="icon icon-angle-double-right"></span> คุณสมบัติกลุ่มเครื่องทอ
                <!-- <span class="d-ib">
                    <a class="title-bar-shortcut" href="#" title="Add to shortcut list" data-container="body" data-toggle-text="Remove from shortcut list" data-trigger="hover" data-placement="right" data-toggle="tooltip">
                        <span class="sr-only">Add to shortcut list</span>
                    </a>
                </span> -->
            </h4>
            <!-- <p class="title-bar-description">
                <small>Latest update on 01/01/2017 - 10.57 am.</small>
            </p> -->
        </div>

        <div class="row gutter-xs">
            <div class="col-xs-12">
                <!--  -->
                <div class="card">
                    <!-- <div class="card-header">
                        <a href="javascript:window.history.back(-1);" class="btn btn-sm btn-outline-primary"><span class="icon icon-long-arrow-left"></span>&nbsp;&nbsp;Back</a>&nbsp;&nbsp;
                        <strong class="">Latest update on 01/01/2017 - 10.57 am.</strong>
                    </div> -->
                    <div class="card-body">
                        <!--  -->
                        <? include('inc.home_top_filter.php');?>
                        <!--  -->
                        <div class="table-responsive">
                            <table id="demo-datatables-2" class="table table-striped table-bordered table-nowrap dataTable text-center no-footer" cellspacing="0" width="100%">
                                <thead>
                                    <tr class="at_bg_table_blue">
                                        <th class="text-center">รหัสกลุ่มเครื่องทอ</th>
                                        <th class="text-center">เบอร์ใยต่ำสุด</th>
                                        <th class="text-center">เบอร์ใยสูงสุด</th>
                                        <th class="text-center">รหัสเงื่อน</th>
                                        <th class="text-center">ขนาดตาต่ำสุด</th>
                                        <th class="text-center">ขนาดตาสูงสุด</th>
                                        <th class="text-center">จน. ตา ต่ำสุด</th>
                                        <th class="text-center">จน. ตา สูงสุด</th>
                                        <th class="text-center">ความยาวสูงสุด</th>
                                        <th class="text-center">ปภ.กลุ่มวัตถุดิบ</th>
                                        <th class="text-center">เริ่มต้นใช้งาน</th>
                                        <th class="text-center">ปรับแก้ไข</th>
                                    </tr>
                                </thead>
                                <tbody>
                                
                                <tr>
    <td>C1</td>
    <td>012*8</td>
    <td>012*12</td>
    <td>DK</td>
    <td>3</td>
    <td>120</td>
    <td>1.5</td>
    <td>520</td>
    <td>999</td>
    <td>T</td>
    <td>11/13/2009  5:09:08 PM</td>


    <td class="text-center" style="display: table-cell;">
         <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_4_2.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

                            <tr>
    <td>C1</td>
    <td>020*3</td>    
    <td>020*4</td>
    <td>DK</td>
    <td>1</td>
    <td>120</td>
    <td>1.5</td>
    <td>520</td>
    <td>999</td>
    <td>T</td>
    <td>11/28/2011  11:47:08 AM</td>


    <td class="text-center" style="display: table-cell;">
         <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_4_2.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

                            <tr>
    <td>C1</td>
    <td>210/12</td>    
    <td>210/12</td>
    <td>SK</td>
    <td>1.8</td>
    <td>80</td>
    <td>600</td>
    <td>600</td>
    <td>999</td>
    <td>N</td>
    <td>12/07/2016  12:24:08 PM</td>


    <td class="text-center" style="display: table-cell;">
         <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_4_2.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

<tr>
    <td>C3</td>
    <td>020*5</td>    
    <td>020*6</td>
    <td>DK</td>
    <td>5</td>
    <td>120</td>
    <td>1</td>
    <td>430</td>
    <td>999</td>
    <td>T</td>
    <td>09/18/2017  11:36:08 AM</td>


    <td class="text-center" style="display: table-cell;">
         <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_4_2.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

<tr>
    <td>C3</td>
    <td>210/12</td>    
    <td>210/15</td>
    <td>DK</td>
    <td>5</td>
    <td>120</td>
    <td>1</td>
    <td>430</td>
    <td>999</td>
    <td>N</td>
    <td>03/26/2014  09:29:08 AM</td>


    <td class="text-center" style="display: table-cell;">
         <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_4_2.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
        
<tr>
    <td>C5</td>
    <td>020*8</td>    
    <td>020*8</td>
    <td>DK</td>
    <td>3.5</td>
    <td>120</td>
    <td>2</td>
    <td>430</td>
    <td>999</td>
    <td>T</td>
    <td>03/08/2017  10:13:08 AM</td>


    <td class="text-center" style="display: table-cell;">
         <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_4_2.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>   

<tr>
    <td>C5</td>
    <td>023*3</td>    
    <td>023*3</td>
    <td>DK</td>
    <td>3.5</td>
    <td>120</td>
    <td>2</td>
    <td>430</td>
    <td>999</td>
    <td>T</td>
    <td>10/12/2006  08:05:08 AM</td>


    <td class="text-center" style="display: table-cell;">
         <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_4_2.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>   


<tr>
    <td>C5</td>
    <td>044</td>    
    <td>052</td>
    <td>DK</td>
    <td>1.5</td>
    <td>120</td>
    <td>1.5</td>
    <td>430</td>
    <td>999</td>
    <td>M</td>
    <td>10/24/2014  12:09:08 AM</td>


    <td class="text-center" style="display: table-cell;">
         <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_4_2.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>               

<tr>
    <td>C6</td>
    <td>080 Q</td>    
    <td>150 Q</td>
    <td>DK</td>
    <td>3</td>
    <td>100</td>
    <td>1</td>
    <td>210</td>
    <td>999.99</td>
    <td>M</td>
    <td>01/09/2015  11:24:08 AM</td>


    <td class="text-center" style="display: table-cell;">
         <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_4_2.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>


<tr>
    <td>C6</td>
    <td>080</td>    
    <td>150</td>
    <td>DK</td>
    <td>3</td>
    <td>120</td>
    <td>1</td>
    <td>210</td>
    <td>999.99</td>
    <td>M</td>
    <td>10/25/2013  14:34:08 PM</td>


    <td class="text-center" style="display: table-cell;">
         <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_4_2.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

<tr>
    <td>C7</td>
    <td>055</td>    
    <td>057</td>
    <td>DK</td>
    <td>1.5</td>
    <td>120</td>
    <td>2</td>
    <td>400</td>
    <td>999</td>
    <td>M</td>
    <td>11/09/2013  10:06:08 AM</td>


    <td class="text-center" style="display: table-cell;">
         <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_4_2.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>


<tr>
    <td>C7</td>
    <td>060</td>    
    <td>060</td>
    <td>DK</td>
    <td>1.5</td>
    <td>30</td>
    <td>271</td>
    <td>400</td>
    <td>999</td>
    <td>M</td>
    <td>25/10/2013  14:26:08 PM</td>


    <td class="text-center" style="display: table-cell;">
         <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_4_2.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>


<tr>
    <td>C8</td>
    <td>015*15</td>    
    <td>015*15</td>
    <td>DK</td>
    <td>56</td>
    <td>56</td>
    <td>1</td>
    <td>350</td>
    <td>999</td>
    <td>T</td>
    <td>08/02/2011  14:59:08 PM</td>


    <td class="text-center" style="display: table-cell;">
         <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_4_2.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>



<tr>
    <td>C8</td>
    <td>020*10</td>    
    <td>020*10</td>
    <td>DK</td>
    <td>4</td>
    <td>120</td>
    <td>2</td>
    <td>350</td>
    <td>999</td>
    <td>T</td>
    <td>22/10/2016  11:57:08 AM</td>


    <td class="text-center" style="display: table-cell;">
         <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_4_2.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>


<tr>
    <td>C8</td>
    <td>023*6</td>    
    <td>023*9</td>
    <td>DK</td>
    <td>4</td>
    <td>120</td>
    <td>2</td>
    <td>350</td>
    <td>999</td>
    <td>T</td>
    <td>11/11/2014  11:05:08 AM</td>


    <td class="text-center" style="display: table-cell;">
         <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_4_2.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>


<tr>
    <td>C9</td>
    <td>020*12</td>    
    <td>020*12</td>
    <td>DK</td>
    <td>4</td>
    <td>120</td>
    <td>2</td>
    <td>250</td>
    <td>999</td>
    <td>T</td>
    <td>06/08/2015  14:39:08 PM</td>


    <td class="text-center" style="display: table-cell;">
         <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_4_2.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>


<tr>
    <td>C9</td>
    <td>020*14</td>    
    <td>020*14</td>
    <td>DK</td>
    <td>4</td>
    <td>120</td>
    <td>2</td>
    <td>250</td>
    <td>999</td>
    <td>T</td>
    <td>10/11/2017  15:24:08 PM</td>


    <td class="text-center" style="display: table-cell;">
         <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_4_2.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>


<tr>
    <td>C9</td>
    <td>020*15</td>    
    <td>020*16</td>
    <td>DK</td>
    <td>4</td>
    <td>120</td>
    <td>2</td>
    <td>250</td>
    <td>999</td>
    <td>T</td>
    <td>09/18/2017  14:38:08 PM</td>


    <td class="text-center" style="display: table-cell;">
         <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_4_2.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
            </div>
        </div>










    </div>
</div>
<!--  -->
<? include('inc.footer.php');?>
<!--  -->
<? include('inc.footer.script.php');?>
<!--  -->
