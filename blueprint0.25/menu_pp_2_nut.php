<?php
    $select = 'open_menu_5';
    $select2 = 'menu_pp_2';
?>

<!--  -->
<? include('inc.header.php');?>
<!--  -->
<? include('inc.navbar.php');?>
<!--  -->
<? include('inc.menu.php');?>
<!-- <div class="layout-main"> -->
<style>
.ui-draggable-dragging, .cell-highlight {
    z-index: 99;
    background-color: yellow;
}
</style>
<!--  -->
<div class="layout-content">
    <div class="layout-content-body">
		<div class="title-bar">
            <h1 class="title-bar-title">
              <span class="d-ib">วางแผนการผลิต</span>
            </h1>
            <div class="title-bar-description">
				 <span class="d-ib text-primary">การวางแผนการผลิต</span> <span class="icon icon-angle-double-right"></span> วางแผนการผลิต
			</div>
		</div>
        <!--  -->
        <!--  -->
       <div class="row gutter-xs">
            <div class="col-xs-12">
                <div class="card">
                    <div class="card-body">
                        <div class="at_add_box">
                            <div class="card" aria-expanded="true">
                                <div class="card-header at_bg_table_blue">
                                    <div class="card-actions">
                                            <button type="button" class="card-action card-toggler" title="Collapse" aria-expanded="true"></button>
                                    </div>
                                    <strong>เลือกรายการ</strong>
                                </div>
                                <div class="card-body" style="display: block;">
                                    <form id="form-filter" method="get" class="form form-horizontal">
                                        <div class="row">
											 <div class="col-sm-5">
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label text-right" for="form-control-14">รายการ : </label>
                                                    <div class="col-sm-9 has-feedback">
                                                        <!--<select id="demo-select2-1" class="form-control" name="branch">
                                                            <option value="all">ทั้งหมด</option>
                                                            <option value="1">ออเดอร์</option>
															<option value="2">รายการสอบถาม</option>
                                                        </select>-->
														  <div class="custom-controls-stacked m-t">
														    <label class="custom-control custom-control-primary custom-radio">
                                                            <input class="custom-control-input" type="radio" name="type_order" value="all">
                                                            <span class="custom-control-indicator"></span>
                                                            <span class="custom-control-label">ทั้งหมด</span>
                                                            </label>
                                                            <label class="custom-control custom-control-primary custom-radio">
                                                            <input class="custom-control-input" type="radio" name="type_order" value="order" checked="checcked">
                                                            <span class="custom-control-indicator"></span>
                                                            <span class="custom-control-label">รายการออเดอร์</span>
                                                            </label>
                                                            
                                                            <label class="custom-control custom-control-primary custom-radio">
                                                            <input class="custom-control-input" type="radio" name="type_order" value="inquiry">
                                                            <span class="custom-control-indicator"></span>
                                                            <span class="custom-control-label">รายการใบสอบถาม</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-7 text-right">
                                                <div class="form-group">
                                                    <div class="col-sm-12">
                                                        <button id="save-filter" class="btn btn-outline-primary btn-sm buttons-collection buttons-colvis width-fit" type="submit">
                                                            <span class="icon icon-save"></span>
                                                            &nbsp;&nbsp;บันทึก
                                                        </button>

                                                        <button class="btn btn-sm btn-primary btn-thick width-fit" type="submit">
                                                            <span class="icon icon-search"></span>
                                                            &nbsp;&nbsp;ค้นหา
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label text-right" for="form-control-14">สาขา : </label>
                                                    <div class="col-sm-9 has-feedback">
                                                        <select id="demo-select2-1" class="form-control" name="branch">
                                                            <option value="all">ทั้งหมด</option>
                                                            <option value="1">ขอนแก่นแหอวน</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label class="col-sm-5 control-label text-right" for="form-control-14">ประเภทการขาย : </label>
                                                    <div class="col-sm-7 has-feedback">
                                                        <select id="demo-select2-2" class="form-control" name="type_sale">
                                                            <option value="all">ทั้งหมด</option>
                                                            <option value="1">ขายใน</option>
                                                            <option value="2">ขายต่าง</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label text-right" for="form-control-14">สถานะ</label>
                                                    <div class="col-sm-9 has-feedback">
                                                        <select id="demo-select2-3" class="form-control">
                                                            <option value="0">ทั้งหมด</option>
                                                            <option value="0">รอร่างแผน</option>
                                                            <option value="0">มีการเปลี่ยนแปลง รอร่างแผนใหม่</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 control-label text-right" for="form-control-14">ตามช่วงเวลา</label>
                                                        <div class="col-sm-7 has-feedback">
                                                            <select id="demo-select2-3-2" class="form-control">
                                                                <option value="1">สั่งซื้อ</option>
                                                                <option value="0">จัดส่งสินค้า</option>
                                                                <option value="0">ผลิต</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="input-group input-daterange" data-provide="datepicker" data-date-autoclose="true" data-date-format="dd-M-yyyy">
                                                    <input class="form-control" type="text" value="เริ่มต้น">
                                                    <span class="input-group-addon">ถึง</span>
                                                    <input class="form-control" type="text" value="สิ้นสุด">
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>

								 <div class="card-body" style="display: block;">
                                    <section id="section-order" data-type="order">
                                        <form id="form-order" method="get" class="form form-horizontal">
                                            <button id="start-plan-order" class="btn btn-sm btn-primary btn-thick start-plan width-fit" type="submit" onclick="openToPlaning('order')">
                                                <span class="icon icon-calendar"></span>
                                                &nbsp;&nbsp;เริ่มวางแผน
                                            </button>
                                            
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="table-responsive">
                                                        <table id="planing-table" class="datatables-custom-full table table-bordered table-nowrap dataTable text-center no-footer choose tablesorter-blue" cellspacing="0" width="100%" aria-describedby="demo-datatables-2_info" role="grid" style="width: 100%;">
                                                            <thead>
                                                                <tr class="at_bg_table_blue">
                                                                    <th class="text-center" style="width:100px;">ออเดอร์</th>
                                                                    <th class="text-center">ชื่อลูกค้า</th>
                                                                    <th class="text-center">วันที่สั่งซื้อ</th>
																	<th class="text-center">สินค้า</th>
																	<th class="text-center">วันที่ผลิต</th>
																	<th class="text-center">วันที่ผลิตเสร็จ</th>
                                                                    <th class="text-center no-sort" style="width:50px;">สถานะ</th>
																	<th class="text-center no-sort" style="width:80px;">วางแผน</th>
                                                                </tr>
                                                            </thead>
															<tbody class="order text-center">
                                                                <tr>
                                                                    <td>
                                                                        <span class="order_no">ORDER001</span>
                                                                    </td>
                                                                    <td>CUSTOMER001</td>
																	<td>12/07/2556</td>
                                                                    <td></td>
																	<td></td>
																	<td></td>
                                                                    <td><span class="icon icon-lg icon-hourglass-half" title="รอร่างแผน"></span></td>
																	<td>
																		<div class="order-actions">
																			<button class="btn btn-md btn-default btnplan" type="button" onclick="onPlanAll(this)">วางแผน</button>
																			<button type="button" class="order-action order-toggler collapsed" title="Collapse" aria-expanded="true" data-toggle="collapse" data-target="#order_detail1"></button>
																		</div>
																	</td>
                                                                </tr>
																<tbody class="text-center order_detail collapse" id="order_detail1" aria-expanded="false">
                                                                <tr data-name="PD01 : แห A">
                                                                    <td colspan="4" >PD01 : แห A</td>
                                                                    <td class="column-start"></td>
																	<td class="column-end"></td>
                                                                    <td class="column-status at_bg_table_orange">( 0% ) วางแผน</td>
																	<td>
																		<div class="order-actions">
																			<button class="btn btn-md btn-default btnplan" type="button" name="plan-item" onclick="onPlanItem(this)">วางแผน</button>
																		</div>
																	</td>
                                                                </tr>
                                                                <tr data-name="PD02 : แห B">
                                                                    <td colspan="4" >PD02 : แห B</td>
                                                                    <td class="column-start"></td>
																	<td class="column-end"></td>
                                                                    <td class="column-status at_bg_table_orange">( 0% ) วางแผน</td>
																	<td>
																		<div class="order-actions">
																			<button class="btn btn-md btn-default btnplan" type="button" name="plan-item" onclick="onPlanItem(this)">วางแผน</button>
																		</div>
																	</td>
                                                                </tr>
																</tbody>
															</tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </section>

                                    <section id="section-inquiry" data-type="inquiry">
                                        <form id="form-inquiry" method="get" class="form form-horizontal">
                                            <!-- <div class="row top-header">
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" placeholder="ค้นหา...">
                                                </div>
                                                <div class="col-sm-4 text-right text-fade">
                                                    <span class="icon icon-lg icon-hourglass-half" title="รอร่างแผน"></span> = รอร่างแผน,
                                                    <span class="icon icon-lg icon-edit" style="margin-left:15px;"></span> = มีการเปลี่ยนแปลง รอร่างแผนใหม่
                                                </div>
                                                <div class="col-sm-4 text-right">
                                                    <button id="start-plan-inquiry" class="btn btn-sm btn-primary btn-thick" type="submit" onclick="openToPlaning('order')">
                                                        <span class="icon icon-download"></span>
                                                        &nbsp;&nbsp;เริ่มวางแผน
                                                    </button>
                                                </div>
                                            </div> -->
                                            <button id="start-plan-inquiry" class="btn btn-sm btn-primary btn-thick start-plan width-fit" type="submit" onclick="openToPlaning('order')">
                                                <span class="icon icon-calendar"></span>
                                                &nbsp;&nbsp;เริ่มวางแผน
                                            </button>
                                            
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="table-responsive">
                                                        <table class="table table-striped table-bordered table-nowrap dataTable text-center no-footer choose datatables-custom-full" cellspacing="0" width="100%" aria-describedby="demo-datatables-2_info" role="grid" style="width: 100%;">
                                                            <thead>
                                                                <tr class="at_bg_table_blue">
                                                                    <th class="text-center no-sort" style="width:50px;">เลือก</th>
                                                                    <th class="text-center" style="width:200px;">รหัสออเดอร์</th>
                                                                    <th class="text-center">ชื่อลูกค้า</th>
                                                                    <th class="text-center">ประเภทขาย</th>
                                                                    <th class="text-center">วันที่สั่งซื้อ</th>
                                                                    <th class="text-center no-sort" style="width:50px;">สถานะ</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody class="text-center">
                                                                <tr>
                                                                    <td>
                                                                        <label class="custom-control custom-control-primary custom-checkbox">
                                                                            <input class="custom-control-input" type="checkbox" name="select_checkbox[]" onclick="onSelectItem(this)" value="1">
                                                                            <span class="custom-control-indicator"></span>
                                                                        </label>
                                                                    </td>
                                                                    <td>IQR001</td>
                                                                    <td>CUSTOMER001</td>
                                                                    <td>ขายใน</td>
                                                                    <td>12/07/2556</td>
                                                                    <td><span class="icon icon-lg icon-hourglass-half" title="รอร่างแผน"></span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <label class="custom-control custom-control-primary custom-checkbox">
                                                                            <input class="custom-control-input" type="checkbox" name="select_checkbox[]" onclick="onSelectItem(this)" value="2">
                                                                            <span class="custom-control-indicator"></span>
                                                                        </label>
                                                                    </td>
                                                                    <td>IQR002</td>
                                                                    <td>CUSTOMER002</td>
                                                                    <td>ขายต่าง</td>
                                                                    <td>06/08/2556</td>
                                                                    <td><span class="icon icon-lg icon-hourglass-half" title="รอร่างแผน"></span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <label class="custom-control custom-control-primary custom-checkbox">
                                                                            <input class="custom-control-input" type="checkbox" name="select_checkbox[]" onclick="onSelectItem(this)" value="3">
                                                                            <span class="custom-control-indicator"></span>
                                                                        </label>
                                                                    </td>
                                                                    <td>IQR003</td>
                                                                    <td>CUSTOMER003</td>
                                                                    <td>ขายใน</td>
                                                                    <td>18/08/2556</td>
                                                                    <td><span class="icon icon-lg icon-hourglass-half" title="รอร่างแผน"></span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <label class="custom-control custom-control-primary custom-checkbox">
                                                                            <input class="custom-control-input" type="checkbox" name="select_checkbox[]" onclick="onSelectItem(this)" value="4">
                                                                            <span class="custom-control-indicator"></span>
                                                                        </label>
                                                                    </td>
                                                                    <td>IQR004</td>
                                                                    <td>CUSTOMER004</td>
                                                                    <td>ขายใน</td>
                                                                    <td>12/07/2556</td>
                                                                    <td><span class="icon icon-lg icon-edit" title="มีการเปลี่ยนแปลง รอร่างแผนใหม่"></span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <label class="custom-control custom-control-primary custom-checkbox">
                                                                            <input class="custom-control-input" type="checkbox" name="select_checkbox[]" onclick="onSelectItem(this)" value="5">
                                                                            <span class="custom-control-indicator"></span>
                                                                        </label>
                                                                    </td>
                                                                    <td>IQR005</td>
                                                                    <td>CUSTOMER005</td>
                                                                    <td>ขายต่าง</td>
                                                                    <td>06/08/2556</td>
                                                                    <td><span class="icon icon-lg icon-edit" title="มีการเปลี่ยนแปลง รอร่างแผนใหม่"></span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <label class="custom-control custom-control-primary custom-checkbox">
                                                                            <input class="custom-control-input" type="checkbox" name="select_checkbox[]" onclick="onSelectItem(this)" value="6">
                                                                            <span class="custom-control-indicator"></span>
                                                                        </label>
                                                                    </td>
                                                                    <td>IQR006</td>
                                                                    <td>CUSTOMER006</td>
                                                                    <td>ขายใน</td>
                                                                    <td>18/08/2556</td>
                                                                    <td><span class="icon icon-lg icon-hourglass-half" title="รอร่างแผน"></span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <label class="custom-control custom-control-primary custom-checkbox">
                                                                            <input class="custom-control-input" type="checkbox" name="select_checkbox[]" onclick="onSelectItem(this)" value="7">
                                                                            <span class="custom-control-indicator"></span>
                                                                        </label>
                                                                    </td>
                                                                    <td>IQR007</td>
                                                                    <td>CUSTOMER007</td>
                                                                    <td>ขายใน</td>
                                                                    <td>12/07/2556</td>
                                                                    <td><span class="icon icon-lg icon-edit" title="มีการเปลี่ยนแปลง รอร่างแผนใหม่"></span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <label class="custom-control custom-control-primary custom-checkbox">
                                                                            <input class="custom-control-input" type="checkbox" name="select_checkbox[]" onclick="onSelectItem(this)" value="8">
                                                                            <span class="custom-control-indicator"></span>
                                                                        </label>
                                                                    </td>
                                                                    <td>IQR008</td>
                                                                    <td>CUSTOMER008</td>
                                                                    <td>ขายต่าง</td>
                                                                    <td>06/08/2556</td>
                                                                    <td><span class="icon icon-lg icon-hourglass-half" title="รอร่างแผน"></span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <label class="custom-control custom-control-primary custom-checkbox">
                                                                            <input class="custom-control-input" type="checkbox" name="select_checkbox[]" onclick="onSelectItem(this)" value="9">
                                                                            <span class="custom-control-indicator"></span>
                                                                        </label>
                                                                    </td>
                                                                    <td>IQR009</td>
                                                                    <td>CUSTOMER009</td>
                                                                    <td>ขายใน</td>
                                                                    <td>18/08/2556</td>
                                                                    <td><span class="icon icon-lg icon-hourglass-half" title="รอร่างแผน"></span></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </section>
                                </div>
                            </div>
                            
                            <div class="card" aria-expanded="true">
                               
                            </div>
                        </div>
                    </div>
                </div>

				 <div class="card">
                    <div class="card-body">
						   <div class="at_add_box">
                            <h4 class="at_text_black"><span class="icon icon-bar-chart-o"></span> แผนการทำงานของเครื่องจักร</h4>
                         <div class="row">
						  <div class="col-sm-3 text-left">
								 <form class="form form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-4 control-label text-right" for="form-control-14">แผนร่าง</label>
                            <div class="col-sm-8 has-feedback">
                                <select id="demo-select2-4" class="form-control">
                                  						<option value="1">PL01 : แผนร่างที่1</option>
														<option value="2">PL02 : แผนร่างที่2</option>
                                </select>
                            </div>
                        </div>
                    </form>
                                </div>
								<div class="col-sm-2 text-left">
                                    <button class="btn btn-sm btn-primary btn-thick" type="submit">
                                    <span class="icon icon-search-plus"></span>
										เรียกดู
                                    </button>
                                </div>
                           <div class="col-sm-7 text-right">
                                    <button class="btn btn-sm btn-primary btn-thick" type="submit">
                                    <span class="icon icon-save"></span>
										บันทึกแบบร่าง
                                    </button>
                                    <button class="btn btn-sm btn-primary btn-thick" type="submit">
                                    <span class="icon icon-download"></span>
										บันทึกแผนจริง
                                    </button>
									 <button class="btn btn-sm btn-primary btn-thick" type="submit">
                                    <span class="icon icon-trash"></span>
										ยกเลิกแผน
                                    </button>
                                </div>
                            </div>
						</div>
						
                        <!--  -->
                        <div class="at_add_box">
                            <div id="scheduler" class="dhx_cal_container" style='width:100%; height:400px;'>
                                <div class="dhx_cal_navline">
                                    <div class="dhx_cal_prev_button">&nbsp;</div>
                                    <div class="dhx_cal_next_button">&nbsp;</div>
                                    <div class="dhx_cal_today_button"></div>
                                    <div class="dhx_cal_date"></div>
                                    <div class="dhx_cal_tab" name="timeline_7days_tab" style="left: 0px;"></div>
                                    <div class="dhx_cal_tab" name="timeline_15days_tab" style="left: 70px;"></div>
                                    <div class="dhx_cal_tab" name="timeline_30days_tab" style="left: 140px;"></div>
                                </div>
                                <div class="dhx_cal_header">
                                </div>
                                <div class="dhx_cal_data">
                                </div>
                            </div>
                        </div>
                        <!--  -->
                    </div>
					</div>
				 </div>
            </div>
        </div>
        <!--  -->
    </div>
</div>
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<div id="current_poduct_click" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12 m-b">
                            <label>Draft plan No.</label>
                            <input class="form-control" type="text" placeholder="R-20170801-001">
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-outline-primary" data-dismiss="modal" type="button">Leave</button>
                        <button class="btn btn-primary" data-dismiss="modal" type="button">Go to planning</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<div id="planning_create_new" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12 m-b">
                            <label>Revise plan No.</label>
                            <input class="form-control" type="text" placeholder="R-20170801-001">
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-outline-primary" data-dismiss="modal" type="button">Leave</button>
                        <button class="btn btn-primary" data-dismiss="modal" type="button">Go to revise plan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<div id="Planning_add_new_order" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Add new order</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12 m-b">
                            <label>Order No.</label>
                            <select id="form-control-21" class="custom-select custom-select-sm">
                                <option value="">Order No.</option>
                                <option value="">KPI-17061110-A</option>
                                <option value="">KPI-17061110-B</option>
                                <option value="">KPI-17061110-C</option>
                            </select>
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-outline-primary" data-dismiss="modal" type="button">Leave</button>
                        <button class="btn btn-primary" data-dismiss="modal" type="button">Add order</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--  -->
<!--  -->
<!--  -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<? include('inc.footer.php');?>
<!--  -->
<? include('inc.footer.script.php');?>
<!--  -->
<script src="https://code.jquery.com/jquery-latest.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
<script src="scheduler/codebase/dhtmlxscheduler.js"></script>
<script src="scheduler/codebase/ext/dhtmlxscheduler_timeline.js"></script>
<script src="js/timeline-plan.js"></script>


<script></script>
