﻿<?php
    $select = 'open_menu_3';
    $select2 = 'menu_mk_4';
    $select3 = 'menu_mk_4';
    $select4 = 'menu_mk_4';
?>
<!--  -->
<? include('inc.header.php');?>
<!--  -->
<? include('inc.navbar.php');?>
<!--  -->
<? include('inc.menu.php');?>
<!-- <div class="layout-main"> -->
<!--  -->
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <h4 class="m-t-0">
                <!-- <span class="d-ib text-danger">--</span> <span class="icon icon-angle-double-right"></span> -->
                <!-- <span class="d-ib">ข้อมูลหลัก</span> -->
                <span class="d-ib text-primary">การตลาด</span> <span class="icon icon-angle-double-right"></span> แผนการผลิตโดยรวม
                <!-- <span class="d-ib">
                    <a class="title-bar-shortcut" href="#" title="Add to shortcut list" data-container="body" data-toggle-text="Remove from shortcut list" data-trigger="hover" data-placement="right" data-toggle="tooltip">
                        <span class="sr-only">Add to shortcut list</span>
                    </a>
                </span> -->
            </h4>
            <!-- <p class="title-bar-description">
                <small>Latest update on 01/01/2017 - 10.57 am.</small>
            </p> -->
        </div>

        <div class="row gutter-xs">
            <div class="col-xs-12">
                <!--  -->
                <div class="card">
                    <!-- <div class="card-header">
                        <a href="javascript:window.history.back(-1);" class="btn btn-sm btn-outline-primary"><span class="icon icon-long-arrow-left"></span>&nbsp;&nbsp;Back</a>&nbsp;&nbsp;
                        <strong class="">Latest update on 01/01/2017 - 10.57 am.</strong>
                    </div> -->
                    <div class="card-body">
                        <!--  -->

						
                        <? include('inc.home_top_filter_extra_order.php');?>
						
                        <!--  -->
                        <div class="table-responsive">
                            <table id="datatables-custom" class="table table-hover table-bordered text-center" cellspacing="0" width="100%">
                                <thead>
									<tr class="at_bg_table_blue">
                                        <th class="text-center middle" rowspan="3">เลขที่ออเดอร์</th>
                                        <th class="text-center middle" rowspan="3">ชื่อลูกค้า</th>
                                        <th class="text-center middle" rowspan="3">ประเภทขาย</th>
                                        <!-- <th class="text-center middle" colspan="2" rowspan="2">ประเภทขาย</th> -->
                                        <th class="text-center middle" rowspan="3">วันที่สั่งซื้อ</th>
                                        <th class="text-center middle" colspan="4">วันที่ผลิต</th>
                                        <th class="text-center middle" colspan="2" rowspan="2">วันที่ส่งฝ่ายการตลาด</th>
                                        <th class="text-center middle" rowspan="3">วันที่<br>จัดส่งสินค้า</th>
                                        <th class="text-center middle" rowspan="3">สถานะ</th>
                                        <th class="text-center middle" rowspan="3">แสดง</th>
                                    </tr>
                                    <tr class="at_bg_table_blue">
                                        <th class="text-center" colspan="2">ทอ</th>
                                        <th class="text-center" colspan="2">วงจรส่วนหลัง</th>
                                    </tr>
                                    <tr class="at_bg_table_blue">
                                        <!-- <th class="text-center">ขายใน</th>
                                        <th class="text-center">ขายต่าง</th> -->
                                        <th class="text-center">เริ่ม</th>
                                        <th class="text-center">เสร็จ</th>
                                        <th class="text-center">เริ่ม</th>
                                        <th class="text-center">เสร็จ</th>
                                        <th class="text-center">เริ่ม</th>
                                        <th class="text-center">เสร็จ</th>
                                    </tr>
                                </thead>
                                <tbody>
									<tr class="success">
										<td class="top">ORDER0001</td>
										<td>Customer001</td>
										<td>ขายใน</td>
										<!-- <td>100</td> -->
										<td>20/07/2555</td>
										<td>20/07/2555</td>
										<td>20/07/2555</td>
										<td>20/07/2555</td>
										<td>20/07/2555</td>
										<td>20/07/2555</td>
										<td>20/07/2555</td>
										<td class="top">20/07/2555</td>
										<td class="left"><button class="btn btn-success status " type="button">ผลิตเสร็จแล้ว</button></td>
										<td class="text-center" style="display: table-cell;">
											<a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="menu_mk_3.php" type="button">
												<span class="icon icon-lg icon-file-text-o"></span>
											</a>
											<!--<a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="#" type="button">
												<span class="icon icon-lg icon-edit"></span>
											</a>-->
											<!-- <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
												<span class="icon icon-lg icon-close"></span>
											</button> -->
										</td>
									</tr>
									
									<tr>
										<td class="top">ORDER0002</td>
										<td>Customer002</td>
										<td>ขายต่าง</td>
										<!-- <td>100</td> -->
										<td>20/07/2555</td>
										<td>20/07/2555</td>
										<td>20/07/2555</td>
										<td>20/07/2555</td>
										<td>20/07/2555</td>
										<td>20/07/2555</td>
										<td>20/07/2555</td>
										<td class="top">20/07/2555</td>
										<td class="left"><button class="btn btn-info status " type="button">กำลังผลิต</button></td>
										<td class="text-center" style="display: table-cell;">
											<a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="menu_mk_3.php" type="button">
												<span class="icon icon-lg icon-file-text-o"></span>
											</a>
											<!-- <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
												<span class="icon icon-lg icon-close"></span>
											</button> -->
										</td>
									</tr>
									
									<tr>
										<td class="top">ORDER0003</td>
										<td>Customer003</td>
										<td>ขายใน</td>
										<!-- <td>100</td> -->
										<td>20/07/2555</td>
										<td>20/07/2555</td>
										<td>20/07/2555</td>
										<td>20/07/2555</td>
										<td>20/07/2555</td>
										<td>20/07/2555</td>
										<td>20/07/2555</td>
										<td class="top">20/07/2555</td>
										<td class="left"><button class="btn btn-warning status " type="button"  data-toggle="modal" data-target="#approve-plan">ปรับปรุงแผน รออนุมัติ</button><br>
										</td>
										<td class="text-center" style="display: table-cell;">
											<a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="menu_mk_3.php" type="button">
												<span class="icon icon-lg icon-file-text-o"></span>
											</a>
											<!-- <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
												<span class="icon icon-lg icon-close"></span>
											</button> -->
										</td>
									</tr>

									<tr>
										<td>ORDER0004</td>
										<td>Customer004</td>
										<td>ขายต่าง</td>
										<!-- <td>100</td> -->
										<td>20/07/2555</td>
										<td>20/07/2555</td>
										<td>20/07/2555</td>
										<td>20/07/2555</td>
										<td>20/07/2555</td>
										<td>20/07/2555</td>
										<td>20/07/2555</td>
										<td>20/07/2555</td>
										<td><button class="btn btn-default status " type="button">รอวางแผน</button></td>
										<td class="text-center" style="display: table-cell;">
											<a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="menu_mk_3.php" type="button">
												<span class="icon icon-lg icon-file-text-o"></span>
											</a>
											<!-- <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
												<span class="icon icon-lg icon-close"></span>
											</button> -->
										</td>
									</tr>
									<tr>
										<td>ORDER0005</td>
										<td>Customer005</td>
										<td>ขายใน</td>
										<!-- <td>100</td> -->
										<td>20/07/2555</td>
										<td>20/07/2555</td>
										<td>20/07/2555</td>
										<td>20/07/2555</td>
										<td>20/07/2555</td>
										<td>20/07/2555</td>
										<td>20/07/2555</td>
										<td>20/07/2555</td>
										<td><button class="btn btn-success status " type="button">วางแผนแล้ว</button></td>
										<td class="text-center" style="display: table-cell;">
												<a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="menu_mk_3.php" type="button">
												<span class="icon icon-lg icon-file-text-o"></span>
											</a>
											<!-- <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
												<span class="icon icon-lg icon-close"></span>
											</button> -->
										</td>
									</tr>
									<tr class="success">
										<td>ORDER0006</td>
										<td>Customer006</td>
										<td>ขายต่าง</td>
										<!-- <td>100</td> -->
										<td>20/07/2555</td>
										<td>20/07/2555</td>
										<td>20/07/2555</td>
										<td>20/07/2555</td>
										<td>20/07/2555</td>
										<td>20/07/2555</td>
										<td>20/07/2555</td>
										<td>20/07/2555</td>
										<td><button class="btn btn-success status " type="button">ผลิตเสร็จแล้ว</button></td>
										<td class="text-center" style="display: table-cell;">
											<a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="menu_mk_3.php" type="button">
												<span class="icon icon-lg icon-file-text-o"></span>
											</a>
											<!-- <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
												<span class="icon icon-lg icon-close"></span>
											</button> -->
										</td>
									</tr>
									<tr>
										<td>ORDER0007</td>
										<td>Customer007</td>
										<td>ขายใน</td>
										<!-- <td>100</td> -->
										<td>20/07/2555</td>
										<td>20/07/2555</td>
										<td>20/07/2555</td>
										<td>20/07/2555</td>
										<td>20/07/2555</td>
										<td>20/07/2555</td>
										<td>20/07/2555</td>
										<td>20/07/2555</td>
										<td><button class="btn btn-warning status " type="button" data-toggle="modal" data-target="#approve-plan">รออนุมัติ</button><br>
										</td>
										<td class="text-center" style="display: table-cell;">
											<a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="menu_mk_3.php" type="button">
												<span class="icon icon-lg icon-file-text-o"></span>
											</a>
											<!-- <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
												<span class="icon icon-lg icon-close"></span>
											</button> -->
										</td>
									</tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
            </div>
        </div>
    </div>
</div>


<!--  -->
<? include('inc.footer.php');?>
<!--  -->
<? include('inc.footer.script.php');?>
<!--  -->
