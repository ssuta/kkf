<?php
    $select = 'open_menu_3';
    $select2 = 'menu_5_2';
    $select3 = 'menu_5_2';
    $select4 = 'menu_5_2';
?>
<!--  -->
<? include('inc.header.php');?>
<!--  -->
<? include('inc.navbar.php');?>
<!--  -->
<? include('inc.menu.php');?>
<!-- <div class="layout-main"> -->
<!--  -->
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <h4 class="m-t-0">
                <!-- <span class="d-ib text-danger">--</span> <span class="icon icon-angle-double-right"></span> -->
                <!-- <span class="d-ib">ข้อมูลหลัก</span> -->
                <span class="d-ib text-primary">ข้อมูลหลัก</span> <span class="icon icon-angle-double-right"></span> กระบวนการผลิต <span class="icon icon-angle-double-right"></span> แสดงผลแผนการใช้ใย
                <!-- <span class="d-ib">
                    <a class="title-bar-shortcut" href="#" title="Add to shortcut list" data-container="body" data-toggle-text="Remove from shortcut list" data-trigger="hover" data-placement="right" data-toggle="tooltip">
                        <span class="sr-only">Add to shortcut list</span>
                    </a>
                </span> -->
            </h4>
            <!-- <p class="title-bar-description">
                <small>Latest update on 01/01/2017 - 10.57 am.</small>
            </p> -->
        </div>

        <div class="row gutter-xs">
            <div class="col-xs-12">
                <!--  -->
                <div class="card">
                    <!-- <div class="card-header">
                        <a href="javascript:window.history.back(-1);" class="btn btn-sm btn-outline-primary"><span class="icon icon-long-arrow-left"></span>&nbsp;&nbsp;Back</a>&nbsp;&nbsp;
                        <strong class="">Latest update on 01/01/2017 - 10.57 am.</strong>
                    </div> -->
                    <div class="card-body">
                        <!--  -->
                        <? include('inc.home_top_filter.php');?>
                        <!--  -->
                        <div class="table-responsive">
                            <table id="demo-datatables-2" class="table table-striped table-bordered table-nowrap dataTable text-center no-footer" cellspacing="0" width="100%">
                                <thead>
<tr class="at_bg_table_blue">
<th rowspan="2">รหัสสาขา</th>
<th rowspan="2">รหัสประเภทใย</th>
<th rowspan="2">รายละเอียดประเภทใย</th>
<th rowspan="2">รหัสเบอร์ใย</th>
<th rowspan="2">รหัสประเภทการฉีด</th>
<th rowspan="2">เบอร์ใยจัดเรียง</th>
<th rowspan="2">จำนวนเส้นจัดเรียง</th>
<th rowspan="2">ประเภทวัสดุพิเศษ</th>
<th rowspan="2">วันที่ต้องสั่งซื้อวัตถุดิบ</th>
<th rowspan="2">วันที่ต้องเริ่มฉีดใยล่วงหน้า</th>
<th colspan="7" class="text-center">ปริมาณใยที่ต้องฉีดอย่างน้อย (กิโลกรัม) ต่อวัน                       </th>
</tr>
<tr class="at_bg_table_blue">
<th>25/08/2018</th>
<th>26/08/2018</th>
<th>27/08/2018</th>
<th>28/08/2018</th>
<th>29/08/2018</th>
<th>30/08/2018</th>
<th>31/08/2018</th>
</tr>
                                </thead>
                                <tbody>
<tr>
    <td>B&amp;S</td>
    <td>1</td>
    <td>โมโน</td>
    <td>006*21</td>
    <td>2</td>
    <td>0.06</td>
    <td>21</td>
    <td></td>
    <td>-</td>
    <td>25/08/2561</td>
    <td>1,000</td>
    <td>1,000</td>
    <td>1,000</td>
    <td>1,500</td>
    <td>1,500</td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>B&amp;S</td>
    <td>2</td>
    <td>ด้ายโมโนตีเกลียว</td>
    <td>006*28</td>
    <td>2</td>
    <td>0.06</td>
    <td>28</td>
    <td></td>
    <td>-</td>
    <td>20/08/2561</td>
    <td>1,500</td>
    <td>1,500</td>
    <td>1,500</td>
    <td>1,500</td>
    <td>1,500</td>
    <td>1,500</td>
    <td>1,500</td>
</tr>
<tr class="at_bg_table_orange_hight">
    <td>B&amp;S</td>
    <td>7</td>
    <td>ใยโมโนรอตีเกลียว</td>
    <td>006*42</td>
    <td>2</td>
    <td>0.06</td>
    <td>42</td>
    <td>วัสดุพิเศษ</td>
    <td>11/08/2561</td>
    <td class="at_bg_table_white_hight">26/08/2561</td>
    <td class="at_bg_table_white_hight"></td>
    <td class="at_bg_table_white_hight">500</td>
    <td class="at_bg_table_white_hight">500</td>
    <td class="at_bg_table_white_hight">500</td>
    <td class="at_bg_table_white_hight">500</td>
    <td class="at_bg_table_white_hight">500</td>
    <td class="at_bg_table_white_hight">500</td>
</tr>
<tr>
    <td>B&amp;S</td>
    <td>8</td>
    <td>โพลีเอสเตอร์</td>
    <td>006*63</td>
    <td>2</td>
    <td>0.06</td>
    <td>63</td>
    <td></td>
    <td>-</td>
    <td>26/08/2561</td>
    <td></td>
    <td>3,000</td>
    <td>3,000</td>
    <td>3,000</td>
    <td>3,000</td>
    <td>3,000</td>
    <td></td>
</tr>
<tr>
    <td>B&amp;S</td>
    <td>9</td>
    <td>อวนรุม/อวนหลายชั้น</td>
    <td>006*84</td>
    <td>2</td>
    <td>0.06</td>
    <td>84</td>
    <td></td>
    <td>-</td>
    <td>15/08/2561</td>
    <td>1,500</td>
    <td>1,500</td>
    <td>1,500</td>
    <td>1,500</td>
    <td>1,500</td>
    <td>1,500</td>
    <td>1,500</td>
</tr>
<tr>
    <td>B&amp;S</td>
    <td>4</td>
    <td>ไนล่อน</td>
    <td>80</td>
    <td>1</td>
    <td>0.08</td>
    <td>0</td>
    <td></td>
    <td>-</td>
    <td>15/08/2561</td>
    <td>5,000</td>
    <td>5,000</td>
    <td>5,000</td>
    <td>5,000</td>
    <td>5,000</td>
    <td>5,000</td>
    <td>5,000</td>
</tr>
<tr>
    <td>B&amp;S</td>
    <td>5</td>
    <td>ไนล่อนเพื่ออวนปั๊ม</td>
    <td>84</td>
    <td>1</td>
    <td>0.084</td>
    <td>0</td>
    <td></td>
    <td>-</td>
    <td>28/08/2561</td>
    <td></td>
    <td></td>
    <td></td>
    <td>3,000</td>
    <td>3,000</td>
    <td>3,000</td>
    <td>3,000</td>
</tr>
<tr>
    <td>B&amp;S</td>
    <td>6</td>
    <td>โพลี</td>
    <td>85</td>
    <td>1</td>
    <td>0.085</td>
    <td>0</td>
    <td></td>
    <td>-</td>
    <td>28/08/2561</td>
    <td></td>
    <td></td>
    <td></td>
    <td>3,000</td>
    <td>1,500</td>
    <td>1,500</td>
    <td>5,000</td>
</tr>
<tr>
    <td>B&amp;S</td>
    <td>3</td>
    <td>โพลีเพื่ออวนปั๊ม</td>
    <td>90</td>
    <td>1</td>
    <td>0.09</td>
    <td>0</td>
    <td></td>
    <td>-</td>
    <td>28/08/2561</td>
    <td></td>
    <td></td>
    <td></td>
    <td>1,000</td>
    <td>1,000</td>
    <td>1,000</td>
    <td>1,000</td>
</tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
            </div>
        </div>










    </div>
</div>
<!--  -->
<? include('inc.footer.php');?>
<!--  -->
<? include('inc.footer.script.php');?>
<!--  -->
