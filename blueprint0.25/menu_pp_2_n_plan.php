<?php
    $select = 'open_menu_5';
    $select2 = 'menu_pp_2';

    $index = array();
    $data = array();
    $type = $_GET['type'];
    $amount = $_GET['amount'];

    $dataAll = array(
        array('IQR001', 'CUSTOMER001', 'ขายใน', '12/07/2556'),
        array('IQR002', 'CUSTOMER002', 'ขายต่าง', '12/07/2556'),
        array('IQR003', 'CUSTOMER003', 'ขายใน', '12/07/2556'),
        array('IQR004', 'CUSTOMER004', 'ขายใน', '12/07/2556'),
        array('IQR005', 'CUSTOMER005', 'ขายใน', '12/07/2556'),
        array('IQR006', 'CUSTOMER006', 'ขายใน', '12/07/2556'),
        array('IQR007', 'CUSTOMER007', 'ขายใน', '12/07/2556'),
        array('IQR008', 'CUSTOMER008', 'ขายต่าง', '12/07/2556'),
        array('IQR009', 'CUSTOMER009', 'ขายใน', '12/07/2556'),
        array('ORDER001', 'CUSTOMER001', 'ขายใน', '12/07/2556'),
        array('ORDER002', 'CUSTOMER002', 'ขายต่าง', '06/08/2556'),
        array('ORDER003', 'CUSTOMER003', 'ขายใน', '18/08/2556'),
        array('ORDER004', 'CUSTOMER004', 'ขายใน', '12/07/2556'),
        array('ORDER005', 'CUSTOMER005', 'ขายใน', '06/08/2556'),
        array('ORDER006', 'CUSTOMER006', 'ขายใน', '18/08/2556'),
        array('ORDER007', 'CUSTOMER007', 'ขายใน', '12/07/2556'),
        array('ORDER008', 'CUSTOMER008', 'ขายต่าง', '06/08/2556'),
        array('ORDER009', 'CUSTOMER009', 'ขายใน', '18/08/2556')
    );

    $dataOrder = array(
        array('ORDER001', 'CUSTOMER001', 'ขายใน', '12/07/2556'),
        array('ORDER002', 'CUSTOMER002', 'ขายต่าง', '06/08/2556'),
        array('ORDER003', 'CUSTOMER003', 'ขายใน', '18/08/2556'),
        array('ORDER004', 'CUSTOMER004', 'ขายใน', '12/07/2556'),
        array('ORDER005', 'CUSTOMER005', 'ขายใน', '06/08/2556'),
        array('ORDER006', 'CUSTOMER006', 'ขายใน', '18/08/2556'),
        array('ORDER007', 'CUSTOMER007', 'ขายใน', '12/07/2556'),
        array('ORDER008', 'CUSTOMER008', 'ขายต่าง', '06/08/2556'),
        array('ORDER009', 'CUSTOMER009', 'ขายใน', '18/08/2556')
    );

    $dataInquiry = array(
        array('IQR001', 'CUSTOMER001', 'ขายใน', '12/07/2556'),
        array('IQR002', 'CUSTOMER002', 'ขายต่าง', '12/07/2556'),
        array('IQR003', 'CUSTOMER003', 'ขายใน', '12/07/2556'),
        array('IQR004', 'CUSTOMER004', 'ขายใน', '12/07/2556'),
        array('IQR005', 'CUSTOMER005', 'ขายใน', '12/07/2556'),
        array('IQR006', 'CUSTOMER006', 'ขายใน', '12/07/2556'),
        array('IQR007', 'CUSTOMER007', 'ขายใน', '12/07/2556'),
        array('IQR008', 'CUSTOMER008', 'ขายต่าง', '12/07/2556'),
        array('IQR009', 'CUSTOMER009', 'ขายใน', '12/07/2556')
    );

    


    for($i=0; $i<$amount; $i++){
        
        $id = 'id' . ($i+1);
        $index = $_GET[$id] - 1;

        if($type == 'all'){
            $data[] = $dataAll[$index];
        }else if($type == '1'){
            $data[] = $dataOrder[$index];
        }else{
            $data[] = $dataInquiry[$index];
        }
    }

   

    // echo '<pre>';
    // print_r($data);
    // echo '</pre>';
    // exit();
?>

<!--  -->
<? include('inc.header.php');?>
<!--  -->
<? include('inc.navbar.php');?>
<!--  -->
<? include('inc.menu.php');?>
<!-- <div class="layout-main"> -->
<style>
.ui-draggable-dragging, .cell-highlight {
    z-index: 99;
    background-color: yellow;
}
</style>
<!--  -->
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <h4 class="m-t-0">
                <span class="d-ib text-primary">การวางแผนการผลิต</span> <span class="icon icon-angle-double-right"></span> วางแผนการผลิต
            </h4>
        </div>
        <div class="row gutter-xs">
            <div class="col-xs-12">
                <div class="card">
                    <div class="card-body">

                        <?php for($i=0; $i<$amount; $i++){ ?>
                        <div class="at_add_box">
                            <div class="card" aria-expanded="true">
                                <!-- <div class="card-header at_bg_table_light_gray">
                                    <div class="card-actions">
                                            <button type="button" class="card-action card-toggler" title="Collapse" aria-expanded="true"></button>
                                    </div>
                                    <strong>วางแผนการผลิต</strong>
                                </div> -->
                                <div class="card-body item" style="display: block;">
                                    <div class="row relative">
                                        <form id="form-product" class="form form-horizontal">
                                            <div class="col-sm-6">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <strong>รหัสออเดอร์ : </strong><span id="order-id"><?=$data[$i][0]?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <strong>ชื่อลูกค้า : </strong><span id="customer-name"><?=$data[$i][1]?></span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <strong>ประเภทขาย : </strong><span id="type-sale"><?=$data[$i][2]?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <strong>วันที่สั่งซื้อ : </strong><span id="date-buy"><?=$data[$i][3]?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>

                                        <button class="btn btn-sm btn-primary btn-thick pull-right-bottom plan-all" type="button" onclick="onPlanAll(this)">
                                            <span class="icon icon-calendar"></span>
                                            <!-- <span class="spinner spinner-default spinner-sm input-icon"></span> -->
                                            &nbsp;&nbsp;วางแผนอัตโนมัติ
                                        </button>
                                    </div>

                                    <div class="table-responsive">
                                        <table id="planing-table" class="table table-striped table-bordered table-nowrap dataTable text-center no-footer choose" cellspacing="0" width="100%" aria-describedby="demo-datatables-2_info" role="grid" style="width: 100%;">
                                            <thead>
                                                <tr class="header-table">
                                                    <th class="text-center"></th>
                                                    <th class="text-center"></th>
                                                    <th class="text-center" colspan="2">วันที่ผลิต</th>
                                                    <th class="text-center"></th>
                                                    <th class="text-center"></th>
                                                </tr>
                                                <tr class="header-table">
                                                    <th class="text-center">รหัสสินค้า</th>
                                                    <th class="text-center">ประเภทสินค้า</th>
                                                    <th class="text-center" style="width:100px;">เริ่ม</th>
                                                    <th class="text-center" style="width:100px;">เสร็จ</th>
                                                    <th class="text-center" style="width: 120px;">สถานะ</th>
                                                    <th class="text-center" style="width: 120px;">ควบคุม</th>
                                                </tr>
                                            </thead>
                                            <tbody class="text-center">
                                                <?php 
                                                    $amountRand = rand(3,6);
                                                    for($j=0; $j<$amountRand; $j++){
                                                        $producId = RandomString();
                                                ?>
                                                <tr data-name="<?=$producId?>" data-duration="3">
                                                    <td><?=$producId?></td>
                                                    <td>ข่ายเอ็น</td>
                                                    <td class="column-start"></td>
                                                    <td class="column-end"></td>
                                                    <td class="column-status at_bg_table_orange">( 0% ) วางแผน</td>
                                                    <td class="control">
                                                        <button class="btn btn-sm btn-primary btn-thick width-fit" type="button" name="plan-item" onclick="onPlanItem(this)">
                                                            วางแผน
                                                        </button>
                                                    </td>
                                                </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>

                        <div class="at_add_box" >
                            <div class="card" aria-expanded="true">
                                <!-- <div class="card-header at_bg_table_light_gray">
                                    <div class="card-actions">
                                            <button type="button" class="card-action card-toggler" title="Collapse" aria-expanded="true"></button>
                                    </div>
                                    <strong>เลือกรายการ</strong>
                                </div> -->
                                <div class="card-body" style="display: block;">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="at_add_box relative">
                                                <button class="btn btn-sm btn-primary btn-thick start-plan width-fit save-plan" type="button" onclick="">
                                                    <span class="icon icon-save"></span>
                                                    &nbsp;&nbsp;บันทึกแผน
                                                </button>
                                                <div id="scheduler" class="dhx_cal_container" style='width:100%; height:400px;'>
                                                    <div class="dhx_cal_navline">
                                                        <div class="dhx_cal_prev_button">&nbsp;</div>
                                                        <div class="dhx_cal_next_button">&nbsp;</div>
                                                        <div class="dhx_cal_today_button"></div>
                                                        <div class="dhx_cal_date"></div>
                                                        <div class="dhx_cal_tab" name="timeline_7days_tab" style="left: 0px;"></div>
                                                        <div class="dhx_cal_tab" name="timeline_15days_tab" style="left: 70px;"></div>
                                                        <div class="dhx_cal_tab" name="timeline_30days_tab" style="left: 140px;"></div>
                                                    </div>
                                                    <div class="dhx_cal_header">
                                                    </div>
                                                    <div class="dhx_cal_data">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
    function RandomString()
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randstring = '';
        for ($i = 0; $i < 10; $i++) {
            $randstring .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randstring;
    }
?>

<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<? include('inc.footer.php');?>
<!--  -->
<? include('inc.footer.script.php');?>
<!--  -->
<!-- <script src="https://code.jquery.com/jquery-latest.min.js"></script> -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
<script src="scheduler/codebase/dhtmlxscheduler.js"></script>
<script src="scheduler/codebase/ext/dhtmlxscheduler_timeline.js"></script>
<script src="js/timeline-plan.js"></script>
<script>

</script>
