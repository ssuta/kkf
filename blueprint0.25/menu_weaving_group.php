<?php
    $select = 'open_menu_1';
    $select2 = 'menu_3';
    $select3 = 'menu_3';
    $select4 = 'menu_3_2';
?>
<!--  -->
<? include('inc.header.php');?>
<!--  -->
<? include('inc.navbar.php');?>
<!--  -->
<? include('inc.menu.php');?>
<!-- <div class="layout-main"> -->
<!--  -->
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <h4 class="m-t-0">
                <!-- <span class="d-ib text-danger">--</span> <span class="icon icon-angle-double-right"></span> -->
                <!-- <span class="d-ib">ข้อมูลหลัก</span> -->
                <span class="d-ib text-primary">การตั้งค่า</span> <span class="icon icon-angle-double-right"></span> เครื่องจักร <span class="icon icon-angle-double-right"></span> กลุ่มเครื่องทอ
                <!-- <span class="d-ib">
                    <a class="title-bar-shortcut" href="#" title="Add to shortcut list" data-container="body" data-toggle-text="Remove from shortcut list" data-trigger="hover" data-placement="right" data-toggle="tooltip">
                        <span class="sr-only">Add to shortcut list</span>
                    </a>
                </span> -->
            </h4>
            <!-- <p class="title-bar-description">
                <small>Latest update on 01/01/2017 - 10.57 am.</small>
            </p> -->
        </div>

        <div class="row gutter-xs">
            <div class="col-xs-12">
                <!--  -->
                <div class="card">
                    <!-- <div class="card-header">
                        <a href="javascript:window.history.back(-1);" class="btn btn-sm btn-outline-primary"><span class="icon icon-long-arrow-left"></span>&nbsp;&nbsp;Back</a>&nbsp;&nbsp;
                        <strong class="">Latest update on 01/01/2017 - 10.57 am.</strong>
                    </div> -->
                    <div class="card-body">
                        <!--  -->
                        <? include('inc.home_top_filter.php');?>
                        <!--  -->
                        <div class="table-responsive">
                            <table id="demo-datatables-2" class="table table-hover table-bordered table-nowrap dataTable text-center no-footer" cellspacing="0" width="100%">
                                <thead>
                                    <tr class="at_bg_table_blue">
                                        <th class="text-center">รหัสกลุ่มเครื่องทอ</th>
                                        <th class="text-center">ชื่อกลุ่มเครื่องทอ</th>
                                        <th class="text-center">รหัสสาขา</th>
                                        <th class="text-center">จน. กระสวย</th>
                                        <th class="text-center">มตฐ.กำไร/ค/ว</th>
                                        <th class="text-center">% ปสภ.มตฐ.</th>
                                        <th class="text-center">% ปสภ.คิดมิเตอร์</th>
                                        <th class="text-center">% ปสภ.<br>ในการวางแผน</th>
                                        <th class="text-center">ระยะพิท</th>
                                        <th class="text-center">Flag อวนปั๊ม</th>
                                        <th class="text-center">เบอร์ใยต่ำสุด</th>
                                        <th class="text-center">เบอร์ใยสูงสุด</th>
                                        <th class="text-center">รหัสเงื่อน</th>
                                        <th class="text-center">ขนาดตาต่ำสุด</th>
                                        <th class="text-center">ขนาดตาสูงสุด</th>
                                        <th class="text-center">จน. ตา ต่ำสุด</th>
                                        <th class="text-center">จน. ตา สูงสุด</th>
                                        <th class="text-center">ความยาวสูงสุด</th>
                                        <th class="text-center">ปภ.กลุ่มวัตถุดิบ</th>
                                        <th class="text-center">เริ่มใช้งาน</th>
                                        <th class="text-center">ปรับแก้ไข</th>
                                    </tr>
                                </thead>
                                <tbody>
                                
                                <tr height=19 style='height:14.4pt'>
  <td height=19 width=68 style='height:14.4pt;width:51pt'>DH 1</td>
  <td align="left" width=313 style='width:235pt'>010 - 012 DK std</td>
  <td width=36 style='width:27pt'>B&amp;S</td>
  <td align=right width=36 style='width:27pt'>561</td>
  <td align=right width=36 style='width:27pt'>3500</td>
  <td align=right width=29 style='width:22pt'>100</td>
  <td align=right width=29 style='width:22pt'>95</td>
  <td align=right width=29 style='width:22pt'>95</td>
  <td align=right width=36 style='width:27pt'>6</td>
  <td width=17 style='width:13pt'>N</td>
  <td width=63 style='width:47pt'>012*8</td>
  <td width=63 style='width:47pt'>012*12</td>
  <td width=23 style='width:17pt'>DK</td>
  <td align=right width=29 style='width:22pt'>3</td>
  <td align=right width=29 style='width:22pt'>120</td>
  <td align=right width=29 style='width:22pt'>1.5</td>
  <td align=right width=29 style='width:22pt'>520</td>
  <td align=right width=50 style='width:38pt'>999</td>
  <td width=19 style='width:14pt'>T</td>
  <td align=right width=130 style='width:97pt'>40130.71468</td>
  <td colspan=31 width=1984 style='mso-ignore:colspan;width:1488pt'><a
  class="btn btn-lg btn-outline-primary btn-pill btn-xs"
  href="edit_menu_4_1.php" type="button"><span
  class="icon icon-lg icon-edit"></span></a><button
  class="btn btn-outline-danger btn-pill btn-xs"
  data-toggle="modal" data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 style='height:14.4pt'>DH 2</td>
  <td align="left">020 DK</td>
  <td>B&amp;S</td>
  <td align=right>460</td>
  <td align=right>1800</td>
  <td align=right>0</td>
  <td align=right>0</td>
  <td align=right>0</td>
  <td align=right>8</td>
  <td>N</td>
  <td>020*3</td>
  <td>020*4</td>
  <td>DK</td>
  <td align=right>1</td>
  <td align=right>120</td>
  <td align=right>1.5</td>
  <td align=right>520</td>
  <td align=right>999</td>
  <td>T</td>
  <td align=right>40875.49122</td>
  <td colspan=31 style='mso-ignore:colspan'><a class="btn btn-lg
  btn-outline-primary btn-pill btn-xs" href="edit_menu_4_1.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 style='height:14.4pt'>DH10</td>
  <td align="left">021-023</td>
  <td>B&amp;S</td>
  <td align=right>810</td>
  <td align=right>3500</td>
  <td align=right>95</td>
  <td align=right>115</td>
  <td align=right>95</td>
  <td align=right>7.5</td>
  <td>N</td>
  <td>210/12</td>
  <td>210/12</td>
  <td>SK</td>
  <td align=right>1.8</td>
  <td align=right>80</td>
  <td align=right>600</td>
  <td align=right>600</td>
  <td align=right>999</td>
  <td>N</td>
  <td align=right>42711.51725</td>
  <td colspan=31 style='mso-ignore:colspan'><a class="btn btn-lg
  btn-outline-primary btn-pill btn-xs" href="edit_menu_4_1.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 style='height:14.4pt'>C1</td>
  <td align="left">9.0DK 033-040 012-015*8,10,12 210/9</td>
  <td>BWC</td>
  <td align=right>430</td>
  <td align=right>4000</td>
  <td align=right>100</td>
  <td align=right>110</td>
  <td align=right>95</td>
  <td align=right>9</td>
  <td>N</td>
  <td>020*5</td>
  <td>020*6</td>
  <td>DK</td>
  <td align=right>5</td>
  <td align=right>120</td>
  <td align=right>1</td>
  <td align=right>430</td>
  <td align=right>999</td>
  <td>T</td>
  <td align=right>42996.48388</td>
  <td colspan=31 style='mso-ignore:colspan'><a class="btn btn-lg
  btn-outline-primary btn-pill btn-xs" href="edit_menu_4_1.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 style='height:14.4pt'>C5</td>
  <td align="left">DK12.7 045-055 210/15-18</td>
  <td>BWC</td>
  <td align=right>431</td>
  <td align=right>4500</td>
  <td align=right>100</td>
  <td align=right>110</td>
  <td align=right>95</td>
  <td align=right>0</td>
  <td>N</td>
  <td>210/12</td>
  <td>210/15</td>
  <td>DK</td>
  <td align=right>5</td>
  <td align=right>120</td>
  <td align=right>1</td>
  <td align=right>430</td>
  <td align=right>999</td>
  <td>N</td>
  <td align=right>41724.39554</td>
  <td colspan=31 style='mso-ignore:colspan'><a class="btn btn-lg
  btn-outline-primary btn-pill btn-xs" href="edit_menu_4_1.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 style='height:14.4pt'>C6</td>
  <td align="left">DK 19.0 070-117 210/36-48</td>
  <td>BWC</td>
  <td align=right>210</td>
  <td align=right>5000</td>
  <td align=right>100</td>
  <td align=right>110</td>
  <td align=right>95</td>
  <td align=right>22</td>
  <td>N</td>
  <td>020*8</td>
  <td>020*8</td>
  <td>DK</td>
  <td align=right>3.5</td>
  <td align=right>120</td>
  <td align=right>2</td>
  <td align=right>430</td>
  <td align=right>999</td>
  <td>T</td>
  <td align=right>42802.42624</td>
  <td colspan=31 style='mso-ignore:colspan'><a class="btn btn-lg
  btn-outline-primary btn-pill btn-xs" href="edit_menu_4_1.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 style='height:14.4pt'>SK10</td>
  <td align="left">&#3585;&#3621;&#3640;&#3656;&#3617; 013 - 016 >30 &#3605;&#3634;</td>
  <td>CY</td>
  <td align=right>461</td>
  <td align=right>1000</td>
  <td align=right>100</td>
  <td align=right>105</td>
  <td align=right>95</td>
  <td align=right>0</td>
  <td>N</td>
  <td>023*3</td>
  <td>023*3</td>
  <td>DK</td>
  <td align=right>3.5</td>
  <td align=right>120</td>
  <td align=right>2</td>
  <td align=right>430</td>
  <td align=right>999</td>
  <td>T</td>
  <td align=right>40463.33729</td>
  <td colspan=31 style='mso-ignore:colspan'><a class="btn btn-lg
  btn-outline-primary btn-pill btn-xs" href="edit_menu_4_1.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 style='height:14.4pt'>SK3</td>
  <td align="left">&#3585;&#3621;&#3640;&#3656;&#3617; 010 <= 30 &#3605;&#3634;</td>
  <td>CY</td>
  <td align=right>461</td>
  <td align=right>1000</td>
  <td align=right>100</td>
  <td align=right>105</td>
  <td align=right>95</td>
  <td align=right>0</td>
  <td>N</td>
  <td>044</td>
  <td>052</td>
  <td>DK</td>
  <td align=right>1.5</td>
  <td align=right>120</td>
  <td align=right>1.5</td>
  <td align=right>430</td>
  <td align=right>999</td>
  <td>M</td>
  <td align=right>41936.50637</td>
  <td colspan=31 style='mso-ignore:colspan'><a class="btn btn-lg
  btn-outline-primary btn-pill btn-xs" href="edit_menu_4_1.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 style='height:14.4pt'>SK4</td>
  <td align="left">&#3585;&#3621;&#3640;&#3656;&#3617; 010 > 30 &#3605;&#3634;</td>
  <td>CY</td>
  <td align=right>461</td>
  <td align=right>1000</td>
  <td align=right>100</td>
  <td align=right>105</td>
  <td align=right>95</td>
  <td align=right>0</td>
  <td>N</td>
  <td>080 Q</td>
  <td>150 Q</td>
  <td>DK</td>
  <td align=right>3</td>
  <td align=right>100</td>
  <td align=right>1</td>
  <td align=right>210</td>
  <td align=right>999.99</td>
  <td>M</td>
  <td align=right>42013.47502</td>
  <td colspan=31 style='mso-ignore:colspan'><a class="btn btn-lg
  btn-outline-primary btn-pill btn-xs" href="edit_menu_4_1.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 style='height:14.4pt'>CTLS 12.7</td>
  <td align="left">380/12-380/15 &#3605;&#3634;&#3606;&#3637;&#3656; (1.9-2.3 cm)</td>
  <td>FM1</td>
  <td align=right>480</td>
  <td align=right>100</td>
  <td align=right>90</td>
  <td align=right>105</td>
  <td align=right>105</td>
  <td align=right>12.7</td>
  <td>N</td>
  <td>080</td>
  <td>150</td>
  <td>DK</td>
  <td align=right>3</td>
  <td align=right>120</td>
  <td align=right>1</td>
  <td align=right>210</td>
  <td align=right>999.99</td>
  <td>M</td>
  <td align=right>41572.60762</td>
  <td colspan=31 style='mso-ignore:colspan'><a class="btn btn-lg
  btn-outline-primary btn-pill btn-xs" href="edit_menu_4_1.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 style='height:14.4pt'>ZRS 11.0A</td>
  <td align="left">380/6 &#3605;&#3634;&#3627;&#3656;&#3634;&#3591; (2.31 cm Up)</td>
  <td>FM1</td>
  <td align=right>630</td>
  <td align=right>100</td>
  <td align=right>90</td>
  <td align=right>105</td>
  <td align=right>105</td>
  <td align=right>11</td>
  <td>N</td>
  <td>055</td>
  <td>057</td>
  <td>DK</td>
  <td align=right>1.5</td>
  <td align=right>120</td>
  <td align=right>2</td>
  <td align=right>400</td>
  <td align=right>999</td>
  <td>M</td>
  <td align=right>41587.42139</td>
  <td colspan=31 style='mso-ignore:colspan'><a class="btn btn-lg
  btn-outline-primary btn-pill btn-xs" href="edit_menu_4_1.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 style='height:14.4pt'>ZRS 12.6A</td>
  <td align="left">380/9 &#3605;&#3634;&#3627;&#3656;&#3634;&#3591; (2.31 cm Up)</td>
  <td>FM1</td>
  <td align=right>550</td>
  <td align=right>100</td>
  <td align=right>90</td>
  <td align=right>110</td>
  <td align=right>110</td>
  <td align=right>12.6</td>
  <td>N</td>
  <td>060</td>
  <td>060</td>
  <td>DK</td>
  <td align=right>1.5</td>
  <td align=right>30</td>
  <td align=right>271</td>
  <td align=right>400</td>
  <td align=right>999</td>
  <td>M</td>
  <td align=right>41572.60157</td>
  <td colspan=31 style='mso-ignore:colspan'><a class="btn btn-lg
  btn-outline-primary btn-pill btn-xs" href="edit_menu_4_1.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 style='height:14.4pt'>EA</td>
  <td align="left">SK 5.334*460</td>
  <td>KKF</td>
  <td align=right>460</td>
  <td align=right>1200</td>
  <td align=right>100</td>
  <td align=right>110</td>
  <td align=right>95</td>
  <td align=right>5.34</td>
  <td>N</td>
  <td>015*15</td>
  <td>015*15</td>
  <td>DK</td>
  <td align=right>56</td>
  <td align=right>56</td>
  <td align=right>1</td>
  <td align=right>350</td>
  <td align=right>999</td>
  <td>T</td>
  <td align=right>40757.62425</td>
  <td colspan=31 style='mso-ignore:colspan'><a class="btn btn-lg
  btn-outline-primary btn-pill btn-xs" href="edit_menu_4_1.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 style='height:14.4pt'>EC</td>
  <td align="left">SH7.0,SK (020sk,210/2-3 6-30cm,210/4-6 1.27-4m)</td>
  <td>KKF</td>
  <td align=right>525</td>
  <td align=right>1600</td>
  <td align=right>100</td>
  <td align=right>120</td>
  <td align=right>95</td>
  <td align=right>525</td>
  <td>N</td>
  <td>020*10</td>
  <td>020*10</td>
  <td>DK</td>
  <td align=right>4</td>
  <td align=right>120</td>
  <td align=right>2</td>
  <td align=right>350</td>
  <td align=right>999</td>
  <td>T</td>
  <td align=right>42665.49814</td>
  <td colspan=31 style='mso-ignore:colspan'><a class="btn btn-lg
  btn-outline-primary btn-pill btn-xs" href="edit_menu_4_1.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 style='height:14.4pt'>EP1</td>
  <td align="left">&#3648;&#3588;&#3619;&#3639;&#3656;&#3629;&#3591;&#3607;&#3629;&#3629;&#3623;&#3609;&#3611;&#3633;&#3657;&#3617;
  110/4,210/2</td>
  <td>KKF1</td>
  <td align=right>1200</td>
  <td align=right>0</td>
  <td align=right>0</td>
  <td align=right>0</td>
  <td align=right>0</td>
  <td align=right>0</td>
  <td>Y</td>
  <td>023*6</td>
  <td>023*9</td>
  <td>DK</td>
  <td align=right>4</td>
  <td align=right>120</td>
  <td align=right>2</td>
  <td align=right>350</td>
  <td align=right>999</td>
  <td>T</td>
  <td align=right>41954.46192</td>
  <td colspan=31 style='mso-ignore:colspan'><a class="btn btn-lg
  btn-outline-primary btn-pill btn-xs" href="edit_menu_4_1.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 style='height:14.4pt'>EP3</td>
  <td align="left">&#3648;&#3588;&#3619;&#3639;&#3656;&#3629;&#3591;&#3607;&#3629;&#3629;&#3623;&#3609;&#3611;&#3633;&#3657;&#3617;
  210/5-210/10</td>
  <td>KKF1</td>
  <td align=right>1200</td>
  <td align=right>0</td>
  <td align=right>0</td>
  <td align=right>0</td>
  <td align=right>0</td>
  <td align=right>0</td>
  <td>Y</td>
  <td>020*12</td>
  <td>020*12</td>
  <td>DK</td>
  <td align=right>4</td>
  <td align=right>120</td>
  <td align=right>2</td>
  <td align=right>250</td>
  <td align=right>999</td>
  <td>T</td>
  <td align=right>42163.61051</td>
  <td colspan=31 style='mso-ignore:colspan'><a class="btn btn-lg
  btn-outline-primary btn-pill btn-xs" href="edit_menu_4_1.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 style='height:14.4pt'>CAD</td>
  <td align="left">021SK</td>
  <td>NR</td>
  <td align=right>650</td>
  <td align=right>2000</td>
  <td align=right>100</td>
  <td align=right>100</td>
  <td align=right>100</td>
  <td align=right>8</td>
  <td>N</td>
  <td>020*14</td>
  <td>020*14</td>
  <td>DK</td>
  <td align=right>4</td>
  <td align=right>120</td>
  <td align=right>2</td>
  <td align=right>250</td>
  <td align=right>999</td>
  <td>T</td>
  <td align=right>43019.64176</td>
  <td colspan=31 style='mso-ignore:colspan'><a class="btn btn-lg
  btn-outline-primary btn-pill btn-xs" href="edit_menu_4_1.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 style='height:14.4pt'>CN</td>
  <td align="left">021- 023 ,024-029(543-800&#3605;&#3634;),030(400,650-800&#3605;&#3634;)</td>
  <td>NR</td>
  <td align=right>810</td>
  <td align=right>3500</td>
  <td align=right>95</td>
  <td align=right>115</td>
  <td align=right>95</td>
  <td align=right>7.5</td>
  <td>N</td>
  <td>020*15</td>
  <td>020*16</td>
  <td>DK</td>
  <td align=right>4</td>
  <td align=right>120</td>
  <td align=right>2</td>
  <td align=right>250</td>
  <td align=right>999</td>
  <td>T</td>
  <td align=right>42996.61015</td>
  <td colspan=31 style='mso-ignore:colspan'><a class="btn btn-lg
  btn-outline-primary btn-pill btn-xs" href="edit_menu_4_1.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
                                

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
            </div>
        </div>










    </div>
</div>
<!--  -->
<? include('inc.footer.php');?>
<!--  -->
<? include('inc.footer.script.php');?>
<!--  -->
