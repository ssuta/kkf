<div class="layout-main">
    <div class="layout-sidebar">
        <div class="layout-sidebar-backdrop"></div>
        <div class="layout-sidebar-body">
            <div class="custom-scrollbar">
                <nav id="sidenav" class="sidenav-collapse collapse">
                    <ul class="sidenav m-t-md">
                    <!-- <ul class="sidenav sidenav-collapsed" aria-expanded="false"> -->
                        <li class="sidenav-search hidden-md hidden-lg">
                            <form class="sidenav-form" action="/">
                                <div class="form-group form-group-sm">
                                    <div class="input-with-icon">
                                        <input class="form-control" type="text" placeholder="Search…">
                                        <span class="icon icon-search input-icon"></span>
                                    </div>
                                </div>
                            </form>
                        </li>
                        <!--  -->
                        <? if ($select2 == 'open_master'){?>
                        <li class="sidenav-item has-subnav open">
                        <? }else{?>
                        <li class="sidenav-item has-subnav">
                        <? } ?>
                            <a href="#" aria-haspopup="true" class="no_active">
                                <span class="sidenav-icon icon icon-database"></span>
                                <span class="sidenav-label">Master Data</span>
                            </a>
                            <? if ($select2 == 'open_master'){?>
                            <ul class="sidenav-subnav collapse in">
                            <? }else{?>
                            <ul class="sidenav-subnav collapse">
                            <? } ?>
                                <!--  -->
                                <? if ($select == 'master-v3-site'){?>
                                <li class="sidenav-item active">
                                    <a href="master-v3-site.php" target="" style="color: #0288d1;">
                                        <span class="sidenav-icon icon icon-sitemap"></span>
                                        <span class="sidenav-label">Site</span>
                                    </a>
                                </li>
                                <? }else{?>
                                <li class="sidenav-item">
                                    <a href="master-v3-site.php" target="">
                                        <span class="sidenav-icon icon icon-sitemap"></span>
                                        <span class="sidenav-label">Site</span>
                                    </a>
                                </li>
                                <? } ?>
                                <!--  -->
                                <? if ($select == 'master-v3-machine'){?>
                                <li class="sidenav-item active">
                                    <a href="master-v3-machine.php" target="" style="color: #0288d1;">
                                        <span class="sidenav-icon icon icon-cubes"></span>
                                        <span class="sidenav-label">Machine</span>
                                    </a>
                                </li>
                                <? }else{?>
                                <li class="sidenav-item">
                                    <a href="master-v3-machine.php" target="">
                                        <span class="sidenav-icon icon icon-cubes"></span>
                                        <span class="sidenav-label">Machine</span>
                                    </a>
                                </li>
                                <? } ?>
                                <!--  -->
                                <? if ($select == 'master-v3-cycle'){?>
                                <li class="sidenav-item active">
                                    <a href="master-v3-cycle.php" target="" style="color: #0288d1;">
                                        <span class="sidenav-icon icon icon-industry"></span>
                                        <span class="sidenav-label">Cycle</span>
                                    </a>
                                </li>
                                <? }else{?>
                                <li class="sidenav-item">
                                    <a href="master-v3-cycle.php" target="">
                                        <span class="sidenav-icon icon icon-industry"></span>
                                        <span class="sidenav-label">Cycle</span>
                                    </a>
                                </li>
                                <? } ?>
                                <!--  -->
                                <? if ($select == 'master-v3-product'){?>
                                <li class="sidenav-item active">
                                    <a href="master-v3-product.php" target="" style="color: #0288d1;">
                                        <span class="sidenav-icon icon icon-cube"></span>
                                        <span class="sidenav-label">Product</span>
                                    </a>
                                </li>
                                <? }else{?>
                                <li class="sidenav-item">
                                    <a href="master-v3-product.php" target="">
                                        <span class="sidenav-icon icon icon-cube"></span>
                                        <span class="sidenav-label">Product</span>
                                    </a>
                                </li>
                                <? } ?>
                                <!--  -->
                            </ul>
                        </li>
                        <!--  -->
                        <hr class="at-hr">
                        <!--  -->
                        <? if ($select2 == 'open_production'){?>
                        <li class="sidenav-item has-subnav open">
                        <? }else{?>
                        <li class="sidenav-item has-subnav">
                        <? } ?>
                            <a href="#" aria-haspopup="true">
                                <span class="sidenav-icon icon icon-calendar-check-o"></span>
                                <span class="sidenav-label">Production Plan</span>
                            </a>
                            <? if ($select2 == 'open_production'){?>
                            <ul class="sidenav-subnav collapse in">
                            <? }else{?>
                            <ul class="sidenav-subnav collapse">
                            <? } ?>
                                <!--  -->
                                <? if ($select == 'Current'){?>
                                <li class="sidenav-item active">
                                    <a href="current-poduct.php" target="" style="color: #0288d1;">
                                        <span class="sidenav-icon icon icon-cube"></span>
                                        <span class="sidenav-label">Current Poduct</span>
                                    </a>
                                </li>
                                <? }else{?>
                                <li class="sidenav-item">
                                    <a href="current-poduct.php" target="">
                                        <span class="sidenav-icon icon icon-cube"></span>
                                        <span class="sidenav-label">Current Poduct</span>
                                    </a>
                                </li>
                                <? } ?>
                                <!--  -->
                                <? if ($select == 'planing'){?>
                                <li class="sidenav-item active">
                                    <a href="planing-new-1.php" target="" style="color: #0288d1;">
                                        <span class="sidenav-icon icon icon-object-group"></span>
                                        <span class="sidenav-label">Planning</span>
                                    </a>
                                </li>
                                <? }else{?>
                                <li class="sidenav-item">
                                    <a href="planing-new-1.php" target="">
                                        <span class="sidenav-icon icon icon-object-group"></span>
                                        <span class="sidenav-label">Planning</span>
                                    </a>
                                </li>
                                <? } ?>
                                <!--  -->
                                <? if ($select == 'State-Material'){?>
                                <li class="sidenav-item active">
                                    <a href="state-material-1-list.php" target="" style="color: #ef6c00;">
                                        <span class="sidenav-icon icon icon-shopping-basket"></span>
                                        <span class="sidenav-label">Material state</span>
                                    </a>
                                </li>
                                <? }else{?>
                                <li class="sidenav-item">
                                    <a href="state-material-1-list.php" target="" class="no_active">
                                        <span class="sidenav-icon icon icon-shopping-basket"></span>
                                        <span class="sidenav-label">Material state</span>
                                    </a>
                                </li>
                                <? } ?>
                                <!--  -->
                                <? if ($select == 'State-Machine'){?>
                                <li class="sidenav-item active">
                                    <a href="state-machine.php" target="" style="color: #43a047;">
                                        <span class="sidenav-icon icon icon-cubes"></span>
                                        <span class="sidenav-label">Machine state</span>
                                    </a>
                                </li>
                                <? }else{?>
                                <li class="sidenav-item">
                                    <a href="state-machine.php" target="" class="no_active">
                                        <span class="sidenav-icon icon icon-cubes"></span>
                                        <span class="sidenav-label">Machine state</span>
                                    </a>
                                </li>
                                <? } ?>
                                <!--  -->
                            </ul>
                        </li>
                        <!--  -->
                        <hr class="at-hr">
                        <!--  -->
                        <? if ($select2 == 'open_report'){?>
                        <li class="sidenav-item has-subnav open">
                        <? }else{?>
                        <li class="sidenav-item has-subnav">
                        <? } ?>
                            <a href="#" aria-haspopup="true" class="no_active">
                                <span class="sidenav-icon icon icon-bar-chart-o"></span>
                                <span class="sidenav-label">Report & Analysis</span>
                            </a>
                            <? if ($select2 == 'open_report'){?>
                            <ul class="sidenav-subnav collapse in">
                            <? }else{?>
                            <ul class="sidenav-subnav collapse">
                            <? } ?>
                                <!--  -->
                                <? if ($select == 'OEE'){?>
                                <li class="sidenav-item active">
                                    <a href="oee.php" target="" style="color: #0288d1;">
                                        <span class="sidenav-icon icon icon-line-chart"></span>
                                        <span class="sidenav-label">OEE</span>
                                    </a>
                                </li>
                                <? }else{?>
                                <li class="sidenav-item">
                                    <a href="oee.php" target="">
                                        <span class="sidenav-icon icon icon-line-chart"></span>
                                        <span class="sidenav-label">OEE</span>
                                    </a>
                                </li>
                                <? } ?>
                                <!--  -->
                                <? if ($select == 'Losses'){?>
                                <li class="sidenav-item active">
                                    <a href="losses.php" target="" style="color: #0288d1;">
                                        <span class="sidenav-icon icon icon-flash"></span>
                                        <span class="sidenav-label">Losses</span>
                                    </a>
                                </li>
                                <? }else{?>
                                <li class="sidenav-item">
                                    <a href="losses.php" target="">
                                        <span class="sidenav-icon icon icon-flash"></span>
                                        <span class="sidenav-label">Losses</span>
                                    </a>
                                </li>
                                <? } ?>
                                <!--  -->
                                <? if ($select == 'IndividualReport'){?>
                                <li class="sidenav-item active">
                                    <a href="individualreport.php" target="" style="color: #0288d1;">
                                        <span class="sidenav-icon icon icon-file-text-o"></span>
                                        <span class="sidenav-label">Individual Report</span>
                                    </a>
                                </li>
                                <? }else{?>
                                <li class="sidenav-item">
                                    <a href="individualreport.php" target="">
                                        <span class="sidenav-icon icon icon-file-text-o"></span>
                                        <span class="sidenav-label">Individual Report</span>
                                    </a>
                                </li>
                                <? } ?>
                                <!--  -->
                            </ul>
                        </li>
                        <!--  -->
                        <hr class="at-hr">
                        <!--  -->
                        <? if ($select2 == 'open_config'){?>
                        <li class="sidenav-item has-subnav open">
                        <? }else{?>
                        <li class="sidenav-item has-subnav">
                        <? } ?>
                            <a href="#" aria-haspopup="true" class="no_active">
                                <span class="sidenav-icon icon icon-clone"></span>
                                <span class="sidenav-label">Config Data</span>
                            </a>
                            <? if ($select2 == 'open_config'){?>
                            <ul class="sidenav-subnav collapse in">
                            <? }else{?>
                            <ul class="sidenav-subnav collapse">
                            <? } ?>
                                <!--  -->
                                <? if ($select == 'group'){?>
                                <li class="sidenav-item active">
                                    <a href="group.php" target="" style="color: #0288d1;">
                                        <span class="sidenav-icon icon icon-sitemap"></span>
                                        <span class="sidenav-label">Group</span>
                                    </a>
                                </li>
                                <? }else{?>
                                <li class="sidenav-item">
                                    <a href="group.php" target="">
                                        <span class="sidenav-icon icon icon-sitemap"></span>
                                        <span class="sidenav-label">Group</span>
                                    </a>
                                </li>
                                <? } ?>
                                <!--  -->
                                <? if ($select == 'work'){?>
                                <li class="sidenav-item active">
                                    <a href="config-3-process-3-work.php" target="" style="color: #D70000;">
                                        <span class="sidenav-icon icon icon-smile-o"></span>
                                        <span class="sidenav-label">Work</span>
                                    </a>
                                </li>
                                <? }else{?>
                                <li class="sidenav-item">
                                    <a href="config-3-process-3-work.php" target="">
                                        <span class="sidenav-icon icon icon-smile-o"></span>
                                        <span class="sidenav-label">Work</span>
                                    </a>
                                </li>
                                <? } ?>
                                <!--  -->
                                <? if ($select == 'config-4-unit'){?>
                                <li class="sidenav-item active">
                                    <a href="config-4-unit-1-unit.php" target="" style="color: #43a047;">
                                        <span class="sidenav-icon icon icon-hourglass-2"></span>
                                        <span class="sidenav-label">Unit</span>
                                    </a>
                                </li>
                                <? }else{?>
                                <li class="sidenav-item">
                                    <a href="config-4-unit-1-unit.php" target="">
                                        <span class="sidenav-icon icon icon-hourglass-2"></span>
                                        <span class="sidenav-label">Unit</span>
                                    </a>
                                </li>
                                <? } ?>
                                <!--  -->
                                <? if ($select == 'package'){?>
                                <li class="sidenav-item active">
                                    <a href="config-1-product-4-package.php" target="" style="color: #0288d1;">
                                        <span class="sidenav-icon icon icon-cube"></span>
                                        <span class="sidenav-label">Package</span>
                                    </a>
                                </li>
                                <? }else{?>
                                <li class="sidenav-item">
                                    <a href="config-1-product-4-package.php" target="">
                                        <span class="sidenav-icon icon icon-cube"></span>
                                        <span class="sidenav-label">Package</span>
                                    </a>
                                </li>
                                <? } ?>
                                <!--  -->
                                <? if ($select == 'config-1-product'){?>
                                <li class="sidenav-item active">
                                    <a href="config-1-product-2-color.php" target="" style="color: #0288d1;">
                                        <span class="sidenav-icon icon icon-eyedropper"></span>
                                        <span class="sidenav-label">Color</span>
                                    </a>
                                </li>
                                <? }else{?>
                                <li class="sidenav-item">
                                    <a href="config-1-product-2-color.php" target="">
                                        <span class="sidenav-icon icon icon-eyedropper"></span>
                                        <span class="sidenav-label">Color</span>
                                    </a>
                                </li>
                                <? } ?>
                                <!--  -->
                                <? if ($select == 'Cycle-time'){?>
                                <li class="sidenav-item active">
                                    <a href="config-3-process-5-type.php" target="" style="color: #d50000;">
                                        <span class="sidenav-icon icon icon-industry"></span>
                                        <span class="sidenav-label">Cycle time</span>
                                    </a>
                                </li>
                                <? }else{?>
                                <li class="sidenav-item">
                                    <a href="config-3-process-5-type.php" target="">
                                        <span class="sidenav-icon icon icon-industry"></span>
                                        <span class="sidenav-label">Cycle time</span>
                                    </a>
                                </li>
                                <? } ?>
                                <!--  -->
                                <? if ($select == 'status'){?>
                                <li class="sidenav-item active">
                                    <a href="config-4-status-new.php" target="" style="color: #43a047;">
                                        <span class="sidenav-icon icon icon-flag-o"></span>
                                        <span class="sidenav-label">Status</span>
                                    </a>
                                </li>
                                <? }else{?>
                                <li class="sidenav-item">
                                    <a href="config-4-status-new.php" target="">
                                        <span class="sidenav-icon icon icon-flag-o"></span>
                                        <span class="sidenav-label">Status</span>
                                    </a>
                                </li>
                                <? } ?>
                                <!--  -->
                                <? if ($select == 'remark'){?>
                                <li class="sidenav-item active">
                                    <a href="config-4-planing-2-remark.php" target="" style="color: #0288d1;">
                                        <span class="sidenav-icon icon icon-tags"></span>
                                        <span class="sidenav-label">Remark</span>
                                    </a>
                                </li>
                                <? }else{?>
                                <li class="sidenav-item">
                                    <a href="config-4-planing-2-remark.php" target="">
                                        <span class="sidenav-icon icon icon-tags"></span>
                                        <span class="sidenav-label">Remark</span>
                                    </a>
                                </li>
                                <? } ?>
                                <!--  -->
                                <? if ($select == 'warning'){?>
                                <li class="sidenav-item active">
                                    <a href="config-4-planing-3-warning.php" target="" style="color: #0288d1;">
                                        <span class="sidenav-icon icon icon-volume-up"></span>
                                        <span class="sidenav-label">Warning</span>
                                    </a>
                                </li>
                                <? }else{?>
                                <li class="sidenav-item">
                                    <a href="config-4-planing-3-warning.php" target="">
                                        <span class="sidenav-icon icon icon-volume-up"></span>
                                        <span class="sidenav-label">Warning</span>
                                    </a>
                                </li>
                                <? } ?>
                                <!--  -->
                                <? if ($select == 'alarm'){?>
                                <li class="sidenav-item active">
                                    <a href="alarm.php" target="" style="color: #d50000;">
                                        <span class="sidenav-icon icon icon-warning"></span>
                                        <span class="sidenav-label">Alarm</span>
                                    </a>
                                </li>
                                <? }else{?>
                                <li class="sidenav-item">
                                    <a href="alarm.php" target="">
                                        <span class="sidenav-icon icon icon-warning"></span>
                                        <span class="sidenav-label">Alarm</span>
                                    </a>
                                </li>
                                <? } ?>
                                <!--  -->
                                <? if ($select == 'config-5-user'){?>
                                <li class="sidenav-item active">
                                    <a href="config-5-user.php" target="" style="color: #000000;">
                                        <span class="sidenav-icon icon icon-user"></span>
                                        <span class="sidenav-label">Program user</span>
                                    </a>
                                </li>
                                <? }else{?>
                                <li class="sidenav-item">
                                    <a href="config-5-user.php" target="">
                                        <span class="sidenav-icon icon icon-user"></span>
                                        <span class="sidenav-label">Program user</span>
                                    </a>
                                </li>
                                <? } ?>
                            </ul>
                        </li>
                        <!--  -->
                        <hr class="at-hr">
                        <!--  -->
                        <!--  -->
                        <!--  -->
                        <!--  -->
                        <!--  -->
                        <!--  -->






























                        <?php /*<!--  -->
                        <? if ($select == 'planing'){?>
                        <li class="sidenav-item active">
                            <a href="planing.php" target="" style="color: #0288d1;">
                                <span class="sidenav-icon icon icon-object-group"></span>
                                <span class="sidenav-label">Planing</span>
                            </a>
                        </li>
                        <? }else{?>
                        <li class="sidenav-item">
                            <a href="planing.php" target="">
                                <span class="sidenav-icon icon icon-object-group"></span>
                                <span class="sidenav-label">Planing</span>
                            </a>
                        </li>
                        <? } ?>
                        <!--  -->
                        <? if ($select == 'Current'){?>
                        <li class="sidenav-item active">
                            <a href="current-poduct.php" target="" style="color: #0288d1;">
                                <span class="sidenav-icon icon icon-cube"></span>
                                <span class="sidenav-label">Current Poduct</span>
                            </a>
                        </li>
                        <? }else{?>
                        <li class="sidenav-item">
                            <a href="current-poduct.php" target="">
                                <span class="sidenav-icon icon icon-cube"></span>
                                <span class="sidenav-label">Current Poduct</span>
                            </a>
                        </li>
                        <? } ?>
                        <!--  -->
                        <? if ($select == 'OEE'){?>
                        <li class="sidenav-item active">
                            <a href="oee.php" target="" style="color: #0288d1;">
                                <span class="sidenav-icon icon icon-line-chart"></span>
                                <span class="sidenav-label">OEE</span>
                            </a>
                        </li>
                        <? }else{?>
                        <li class="sidenav-item">
                            <a href="oee.php" target="">
                                <span class="sidenav-icon icon icon-line-chart"></span>
                                <span class="sidenav-label">OEE</span>
                            </a>
                        </li>
                        <? } ?>
                        <!--  -->
                        <? if ($select == 'Losses'){?>
                        <li class="sidenav-item active">
                            <a href="losses.php" target="" style="color: #0288d1;">
                                <span class="sidenav-icon icon icon-flash"></span>
                                <span class="sidenav-label">Losses</span>
                            </a>
                        </li>
                        <? }else{?>
                        <li class="sidenav-item">
                            <a href="losses.php" target="">
                                <span class="sidenav-icon icon icon-flash"></span>
                                <span class="sidenav-label">Losses</span>
                            </a>
                        </li>
                        <? } ?>
                        <!--  -->
                        <? if ($select == 'IndividualReport'){?>
                        <li class="sidenav-item active">
                            <a href="individualreport.php" target="" style="color: #0288d1;">
                                <span class="sidenav-icon icon icon-file-text-o"></span>
                                <span class="sidenav-label">Individual Report</span>
                            </a>
                        </li>
                        <? }else{?>
                        <li class="sidenav-item">
                            <a href="individualreport.php" target="">
                                <span class="sidenav-icon icon icon-file-text-o"></span>
                                <span class="sidenav-label">Individual Report</span>
                            </a>
                        </li>
                        <? } ?>
                        <hr class="at-hr">
                        <!--  -->
                        <li class="sidenav-heading">State</li>
                        <!--  -->
                        <? if ($select == 'State-Material'){?>
                        <li class="sidenav-item active">
                            <a href="state-material-1-list.php" target="" style="color: #ef6c00;">
                                <span class="sidenav-icon icon icon-shopping-basket"></span>
                                <span class="sidenav-label">Material</span>
                            </a>
                        </li>
                        <? }else{?>
                        <li class="sidenav-item">
                            <a href="state-material-1-list.php" target="">
                                <span class="sidenav-icon icon icon-shopping-basket"></span>
                                <span class="sidenav-label">Material</span>
                            </a>
                        </li>
                        <? } ?>
                        <!--  -->
                        <? if ($select == 'State-Machine'){?>
                        <li class="sidenav-item active">
                            <a href="state-machine.php" target="" style="color: #43a047;">
                                <span class="sidenav-icon icon icon-cubes"></span>
                                <span class="sidenav-label">Machine</span>
                            </a>
                        </li>
                        <? }else{?>
                        <li class="sidenav-item">
                            <a href="state-machine.php" target="">
                                <span class="sidenav-icon icon icon-cubes"></span>
                                <span class="sidenav-label">Machine</span>
                            </a>
                        </li>
                        <? } ?>
                        <hr class="at-hr">
                        <!--  -->
                        <li class="sidenav-heading">Config Data</li>
                        <!--  -->
                        <? if ($select == 'group'){?>
                        <li class="sidenav-item active">
                            <a href="group.php" target="" style="color: #0288d1;">
                                <span class="sidenav-icon icon icon-sitemap"></span>
                                <span class="sidenav-label">Group</span>
                            </a>
                        </li>
                        <? }else{?>
                        <li class="sidenav-item">
                            <a href="group.php" target="">
                                <span class="sidenav-icon icon icon-sitemap"></span>
                                <span class="sidenav-label">Group</span>
                            </a>
                        </li>
                        <? } ?>
                        <!--  -->
                        <? if ($select == 'work'){?>
                        <li class="sidenav-item active">
                            <a href="config-3-process-3-work.php" target="" style="color: #D70000;">
                                <span class="sidenav-icon icon icon-smile-o"></span>
                                <span class="sidenav-label">Work</span>
                            </a>
                        </li>
                        <? }else{?>
                        <li class="sidenav-item">
                            <a href="config-3-process-3-work.php" target="">
                                <span class="sidenav-icon icon icon-smile-o"></span>
                                <span class="sidenav-label">Work</span>
                            </a>
                        </li>
                        <? } ?>
                        <!--  -->
                        <? if ($select == 'config-4-unit'){?>
                        <li class="sidenav-item active">
                            <a href="config-4-unit-1-unit.php" target="" style="color: #43a047;">
                                <span class="sidenav-icon icon icon-hourglass-2"></span>
                                <span class="sidenav-label">Unit</span>
                            </a>
                        </li>
                        <? }else{?>
                        <li class="sidenav-item">
                            <a href="config-4-unit-1-unit.php" target="">
                                <span class="sidenav-icon icon icon-hourglass-2"></span>
                                <span class="sidenav-label">Unit</span>
                            </a>
                        </li>
                        <? } ?>
                        <!--  -->
                        <? if ($select == 'package'){?>
                        <li class="sidenav-item active">
                            <a href="config-1-product-4-package.php" target="" style="color: #0288d1;">
                                <span class="sidenav-icon icon icon-cube"></span>
                                <span class="sidenav-label">Package</span>
                            </a>
                        </li>
                        <? }else{?>
                        <li class="sidenav-item">
                            <a href="config-1-product-4-package.php" target="">
                                <span class="sidenav-icon icon icon-cube"></span>
                                <span class="sidenav-label">Package</span>
                            </a>
                        </li>
                        <? } ?>
                        <!--  -->
                        <? if ($select == 'config-1-product'){?>
                        <li class="sidenav-item active">
                            <a href="config-1-product-2-color.php" target="" style="color: #0288d1;">
                                <span class="sidenav-icon icon icon-eyedropper"></span>
                                <span class="sidenav-label">Color</span>
                            </a>
                        </li>
                        <? }else{?>
                        <li class="sidenav-item">
                            <a href="config-1-product-2-color.php" target="">
                                <span class="sidenav-icon icon icon-eyedropper"></span>
                                <span class="sidenav-label">Color</span>
                            </a>
                        </li>
                        <? } ?>
                        <!--  -->
                        <? if ($select == 'Cycle-time'){?>
                        <li class="sidenav-item active">
                            <a href="config-3-process-5-type.php" target="" style="color: #d50000;">
                                <span class="sidenav-icon icon icon-industry"></span>
                                <span class="sidenav-label">Cycle time</span>
                            </a>
                        </li>
                        <? }else{?>
                        <li class="sidenav-item">
                            <a href="config-3-process-5-type.php" target="">
                                <span class="sidenav-icon icon icon-industry"></span>
                                <span class="sidenav-label">Cycle time</span>
                            </a>
                        </li>
                        <? } ?>
                        <!--  -->
                        <? if ($select == 'status'){?>
                        <li class="sidenav-item active">
                            <a href="config-4-status-new.php" target="" style="color: #43a047;">
                                <span class="sidenav-icon icon icon-flag-o"></span>
                                <span class="sidenav-label">Status</span>
                            </a>
                        </li>
                        <? }else{?>
                        <li class="sidenav-item">
                            <a href="config-4-status-new.php" target="">
                                <span class="sidenav-icon icon icon-flag-o"></span>
                                <span class="sidenav-label">Status</span>
                            </a>
                        </li>
                        <? } ?>
                        <!--  -->
                        <? if ($select == 'remark'){?>
                        <li class="sidenav-item active">
                            <a href="config-4-planing-2-remark.php" target="" style="color: #0288d1;">
                                <span class="sidenav-icon icon icon-tags"></span>
                                <span class="sidenav-label">Remark</span>
                            </a>
                        </li>
                        <? }else{?>
                        <li class="sidenav-item">
                            <a href="config-4-planing-2-remark.php" target="">
                                <span class="sidenav-icon icon icon-tags"></span>
                                <span class="sidenav-label">Remark</span>
                            </a>
                        </li>
                        <? } ?>
                        <!--  -->
                        <? if ($select == 'warning'){?>
                        <li class="sidenav-item active">
                            <a href="config-4-planing-3-warning.php" target="" style="color: #0288d1;">
                                <span class="sidenav-icon icon icon-volume-up"></span>
                                <span class="sidenav-label">Warning</span>
                            </a>
                        </li>
                        <? }else{?>
                        <li class="sidenav-item">
                            <a href="config-4-planing-3-warning.php" target="">
                                <span class="sidenav-icon icon icon-volume-up"></span>
                                <span class="sidenav-label">Warning</span>
                            </a>
                        </li>
                        <? } ?>
                        <!--  -->
                        <? if ($select == 'alarm'){?>
                        <li class="sidenav-item active">
                            <a href="alarm.php" target="" style="color: #d50000;">
                                <span class="sidenav-icon icon icon-warning"></span>
                                <span class="sidenav-label">Alarm</span>
                            </a>
                        </li>
                        <? }else{?>
                        <li class="sidenav-item">
                            <a href="alarm.php" target="">
                                <span class="sidenav-icon icon icon-warning"></span>
                                <span class="sidenav-label">Alarm</span>
                            </a>
                        </li>
                        <? } ?>
                        <!--  -->
                        <? if ($select == 'config-3-process'){?>
                        <li class="sidenav-item active">
                            <a href="config-3-process.php" target="" style="color: #d50000;">
                                <span class="sidenav-icon icon icon-industry"></span>
                                <span class="sidenav-label">Process</span>
                            </a>
                        </li>
                        <? }else{?>
                        <li class="sidenav-item">
                            <a href="config-3-process.php" target="">
                                <span class="sidenav-icon icon icon-industry"></span>
                                <span class="sidenav-label">Process</span>
                            </a>
                        </li>
                        <? } ?>
                        <!--  -->
                        <? if ($select == 'config-4-machine'){?>
                        <li class="sidenav-item active">
                            <a href="config-4-machine.php" target="" style="color: #43a047;">
                                <span class="sidenav-icon icon icon-cubes"></span>
                                <span class="sidenav-label">Machine</span>
                            </a>
                        </li>
                        <? }else{?>
                        <li class="sidenav-item">
                            <a href="config-4-machine.php" target="">
                                <span class="sidenav-icon icon icon-cubes"></span>
                                <span class="sidenav-label">Machine</span>
                            </a>
                        </li>
                        <? } ?>
                        <!--  -->
                        <? if ($select == 'config-4-planing'){?>
                        <li class="sidenav-item active">
                            <a href="config-4-planing.php" target="" style="color: #0288d1;">
                                <span class="sidenav-icon icon icon-list-ul"></span>
                                <span class="sidenav-label">Planing</span>
                            </a>
                        </li>
                        <? }else{?>
                        <li class="sidenav-item">
                            <a href="config-4-planing.php" target="">
                                <span class="sidenav-icon icon icon-list-ul"></span>
                                <span class="sidenav-label">Planing</span>
                            </a>
                        </li>
                        <? } ?>
                        <!--  -->
                        <? if ($select == 'config-5-user'){?>
                        <li class="sidenav-item active">
                            <a href="config-5-user.php" target="" style="color: #000000;">
                                <span class="sidenav-icon icon icon-user"></span>
                                <span class="sidenav-label">Program user</span>
                            </a>
                        </li>
                        <? }else{?>
                        <li class="sidenav-item">
                            <a href="config-5-user.php" target="">
                                <span class="sidenav-icon icon icon-user"></span>
                                <span class="sidenav-label">Program user</span>
                            </a>
                        </li>
                        <? } ?>
                        <hr class="at-hr">
                        <!--  -->
                        <li class="sidenav-heading">Master Data</li>
                        <!--  -->
                        <? if ($select == 'master-3-process'){?>
                        <li class="sidenav-item active">
                            <a href="master-3-process-1-line.php" target="" style="color: #d50000;">
                                <span class="sidenav-icon icon icon-industry"></span>
                                <span class="sidenav-label">Process</span>
                            </a>
                        </li>
                        <? }else{?>
                        <li class="sidenav-item">
                            <a href="master-3-process-1-line.php" target="">
                                <span class="sidenav-icon icon icon-industry"></span>
                                <span class="sidenav-label">Process</span>
                            </a>
                        </li>
                        <? } ?>
                        <!--  -->
                        <? if ($select == 'master-2-material'){?>
                        <li class="sidenav-item active">
                            <a href="master-2-material.php" target="" style="color: #ef6c00;">
                                <span class="sidenav-icon icon icon-shopping-basket"></span>
                                <span class="sidenav-label">Material</span>
                            </a>
                        </li>
                        <? }else{?>
                        <li class="sidenav-item">
                            <a href="master-2-material.php" target="">
                                <span class="sidenav-icon icon icon-shopping-basket"></span>
                                <span class="sidenav-label">Material</span>
                            </a>
                        </li>
                        <? } ?>
                        <!--  -->
                        <? if ($select == 'master-1-product'){?>
                        <li class="sidenav-item active">
                            <a href="master-1-product-1-list.php" target="" style="color: #0288d1;">
                                <span class="sidenav-icon icon icon-cube"></span>
                                <span class="sidenav-label">Product</span>
                            </a>
                        </li>
                        <? }else{?>
                        <li class="sidenav-item">
                            <a href="master-1-product-1-list.php" target="">
                                <span class="sidenav-icon icon icon-cube"></span>
                                <span class="sidenav-label">Product</span>
                            </a>
                        </li>
                        <? } ?>
                        <!--  -->
                        <? if ($select == 'master-4-machine'){?>
                        <li class="sidenav-item active">
                            <a href="master-4-machine-1-list.php" target="" style="color: #43a047;">
                                <span class="sidenav-icon icon icon-cubes"></span>
                                <span class="sidenav-label">Machine</span>
                            </a>
                        </li>
                        <? }else{?>
                        <li class="sidenav-item">
                            <a href="master-4-machine-1-list.php" target="">
                                <span class="sidenav-icon icon icon-cubes"></span>
                                <span class="sidenav-label">Machine</span>
                            </a>
                        </li>
                        <? } ?>
                        <hr class="at-hr">
                        <!--  -->*/ ?>
                    </ul>
                </nav>
            </div>
        </div>
    </div>