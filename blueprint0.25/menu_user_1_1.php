<?php
    $select = 'open_menu_9';
    $select2 = 'menu_9_1';
	$select3 = 'menu_9_1';
	$select4 = 'menu_9_1';
?>
<!--  -->
<? include('inc.header.php');?>
<!--  -->
<? include('inc.navbar.php');?>
<!--  -->
<? include('inc.menu.php');?>
<!-- <div class="layout-main"> -->
<!--  -->
<div class="layout-content">
    <div class="layout-content-body">
		<div class="title-bar">
            <h1 class="title-bar-title">
              <span class="d-ib">แก้ไขสิทธิผู้ใช้งานระบบ</span>
            </h1>
            <div class="title-bar-description">
				  <span class="d-ib text-primary"> ผู้ดูแลระบบ </span> <span class="icon icon-angle-double-right"> แก้ไขสิทธิผู้ใช้งานระบบ</span>  
			</div>
		</div>
        <div class="row gutter-xs">
            <div class="col-xs-12">
                <!--  -->
                <div class="card">
                    <div class="card-body">
					   <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-6 m-b">
                            <label>ชื่อสิทธิ</label>
                            <input class="form-control" type="text" value="Administrator">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>คำอธิบาย</label>
                            <input class="form-control" type="text" value="Administrator Role">
                        </div>
						 <div class="col-xs-6 m-b">
                            <label>สถานะการใช้งาน</label>
							<div class="custom-controls-stacked">
                      <label class="switch switch-primary">
                        <input class="switch-input" type="checkbox" checked="checked">
                        <span class="switch-track"></span>
                        <span class="switch-thumb"></span>
                      </label>
                      </div>
                        </div>

						 <div class="col-xs-6 m-b">
                            <label>วันหมดอายุ</label>

							<div class="custom-controls-stacked left">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input" type="checkbox" name="mode"  id="hasExpired" checked="checked">
                          <span class="custom-control-indicator"></span>
                          <span class="custom-control-label">ไม่มีวันหมดอายุ</span>
                        </label>
							</div>		

							 <div id="expiredDate" style="display:none;" class=" m-t">
								<div  class="col-xs-3 m-t">
								<label>กำหนดวันหมดอายุ</label>
								</div>
								<div class="col-xs-9 m-b">
													<div class="input-with-icon">
											  <input class="form-control" type="text" data-provide="datepicker" value="11/13/2009">
												<span class="icon icon-calendar input-icon"></span>
											 </div>
											 </div>
											  </div>
                        </div>
						<div class="col-xs-12 m-b">
						 <label>เมนู</label>

						  <div class="table-responsive">
                            <table class="table table-bordered table-nowrap dataTable menu_customize text-center no-footer" cellspacing="0" width="100%">
                                <thead>
                                    <tr class="at_bg_table_blue">
                                        <th rowspan="2" class="text-center">เมนู</th>
                                        <th class="text-center">แสดง</th>
										<th class="text-center">เพิ่ม</th>
										<th class="text-center">แก้ไข</th>
										<th class="text-center">ลบ</th>
                                    </tr>
									 <tr class="at_bg_table_blue">
										<th class="text-center"><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></th>
					   <th class="text-center"><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></th>
					   <th class="text-center"><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></th>
					   <th class="text-center"><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></th>
                                    </tr>
                                </thead>
								<!-- -->
<tbody>
<tr class="header">
    <td class="text-left">การตลาด</td>
  <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
</tr>
<!-- -->
<tr class="sub_menu">
    <td class="text-left">รายการออเดอร์</td>
    <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
</tr>
<!-- -->
<tr class="sub_menu">
    <td class="text-left">รายการสอบถาม</td>
     <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
</tr>
<!-- -->
<!-- -->
<tr class="sub_menu">
    <td class="text-left">รายการรออนุมัติ</td>
     <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
</tr>
<!-- -->
<!-- -->
<tr class="sub_menu">
    <td class="text-left">แผนการผลิตโดยรวม</td>
   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
</tr>
</tbody>
<!-- -->
<tbody>
<tr class="header">
    <td class="text-left">การวางแผนการผลิต</td>
     <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
</tr>
<!-- -->
<tr class="sub_menu"> 
    <td class="text-left">แผนการผลิตปัจจุบัน</td>
     <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
</tr>
<!-- -->
<tr class="sub_menu">
    <td class="text-left">วางแผนการผลิต</td>
    <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
</tr>
<!-- -->
<tr class="sub_menu">
    <td class="text-left">รายการออเดอร์ที่รอการวางแผน</td>
     <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
</tr>
<!-- -->
<tr class="sub_menu">
    <td class="text-left">รายการออเดอร์ที่รอการอนุมัติ</td>
      <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
</tr>
</tbody>
<!-- -->
<tbody>
<tr class="header">
    <td class="text-left">การผลิต</td>
     <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
</tr>
<!-- -->
<tr class="sub_menu">
    <td class="text-left">แผนการผลิตปัจจุบัน</td>
     <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
</tr>
<!-- -->
<tr class="sub_menu">
    <td class="text-left">การผลิตส่วนหน้า</td>
     <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
</tr>
<!-- -->
<tr class="sub_menu">
    <td class="text-left">การผลิตส่วนหลัง</td>
    <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
</tr class="sub_menu">
</tbody>
<!-- -->

<!-- -->
<tbody>
<tr class="header">
    <td class="text-left">การซ่อมบำรุง</td>
    <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
</tr>
<!-- -->
<tr class="sub_menu">
    <td class="text-left">ตารางการซ่อมบำรุง</td>
      <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
</tr>
<!-- -->
<tr class="sub_menu">
    <td class="text-left">บันทึกการจอดเครื่อง</td>
     <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
</tr>
<!-- -->
<tr class="sub_menu">
    <td class="text-left">ข้อมูลการจอดเครื่องจาก PLC</td>
   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
</tr>
</tbody>
<!-- -->
<tbody>
<tr class="header">
    <td class="text-left">การวิเคราะห์และรายงาน</td>
     <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
</tr>
<!-- -->
<tr class="sub_menu">
    <td class="text-left">ภาพรวมด้านการตลาด</td>
     <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
</tr>
<!-- -->
<tr class="sub_menu">
    <td class="text-left">ภาพรวมด้านการผลิต</td>
     <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
</tr>
<!-- -->
<tr class="sub_menu">
    <td class="text-left">รายงานสรุปการผลิต</td>
     <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
</tr>
<!-- -->
<tr class="sub_menu">
    <td class="text-left">รายงานประสิทธิภาพการผลิต</td>
    <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
</tr>
<!-- -->
<tr class="sub_menu">
    <td class="text-left">ฝ่ายการตลาด</td>
    <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
</tr>
<!-- -->
<tr class="sub_menu">
    <td class="text-left">ฝ่ายการวางแผนการผลิต</td>
     <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
</tr>
<!-- -->
<tr class="sub_menu">
    <td class="text-left">ฝ่ายการผลิต</td>
     <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
</tr>
<!-- -->
<tr class="sub_menu">
    <td class="text-left">ฝ่ายซ่อมบำรุง</td>
      <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
</tr>
<!-- -->
<tr class="sub_menu">
    <td class="text-left">ฝ่ายการบริหารและฝ่ายอื่นๆ</td>
      <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
</tr>
</tbody>

<tbody>
<tr class="header">
    <td class="text-left">การตั้งค่า</td>
    <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
</tr>
<!-- -->
<tr class="sub_menu">
    <td class="text-left">สาขา/โรงทอ</td>
    <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
</tr>
<!-- -->
<tr class="sub_menu">
    <td class="text-left">สินค้า</td>
    <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
</tr>
<!-- -->
<tr class="sub_menu">
    <td class="text-left">ลูกค้า</td>
      <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
</tr>
<!-- -->
<tr class="sub_menu">
    <td class="text-left">เครื่องชักใย</td>
  <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
</tr>
<!-- -->
<tr class="sub_menu">
    <td class="text-left">กลุ่มเครื่องทอ</td>
     <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
</tr>
<!-- -->
<tr class="sub_menu">
    <td class="text-left">เครื่องทอ</td>
     <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
</tr>
<!-- -->
<tr class="sub_menu">
    <td class="text-left">กระบวนการผลิต</td>
    <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
</tr>
<!-- -->
<tr class="sub_menu">
    <td class="text-left">ขั้นตอนการผลิต</td>
    <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
</tr>
<!-- -->
<tr class="sub_menu">
    <td class="text-left">ลำดับขั้นตอนการผลิต</td>
      <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
</tr>
<!-- -->
<tr class="sub_menu">
    <td class="text-left">วงจรการผลิต</td>
    <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
</tr>
<!-- -->
<tr class="sub_menu">
    <td class="text-left">สถานี</td>
     <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
</tr>
<!-- -->
<tr class="sub_menu">
    <td class="text-left">ประเภทงานซ่อมบำรุง</td>
      <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
</tr>
<!-- -->
<tr class="sub_menu">
    <td class="text-left">กิจกรรมงานซ่อมบำรุง</td>
    <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
</tr>
<!-- -->
<tr class="sub_menu">
    <td class="text-left">กะทำงาน</td>
     <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
</tr>
<!-- -->
<tr class="sub_menu">
    <td class="text-left">วันหยุด</td>
      <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
</tr>
<!-- -->
<tr class="sub_menu">
    <td class="text-left">ฤดูกาล</td>
    <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
</tr>
<!-- -->
<tr class="sub_menu">
    <td class="text-left">มาตรฐานการตอบเป้าหมาย</td>
     <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
</tr>
<!-- -->
<tr class="sub_menu">
    <td class="text-left">ประเภทการขาย</td>
    <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
</tr>
<!-- -->
<tr class="sub_menu">
    <td class="text-left">ระดับคุณภาพ</td>
   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
</tr>
<!-- -->
<tr class="sub_menu">
    <td class="text-left">กลุ่มวัตถุดิบ</td>
     <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
</tr>
<!-- -->
<tr class="sub_menu">
    <td class="text-left">เงื่อน</td>
     <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
</tr>
<!-- -->
<tr class="sub_menu">
    <td class="text-left">การอบ</td>
     <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
</tr>
<!-- -->
<tr class="sub_menu">
    <td class="text-left">หู</td>
    <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
</tr>
<!-- -->
<tr class="sub_menu">
    <td class="text-left">เบอร์ใย</td>
   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
</tr>
<!-- -->
<tr class="sub_menu">
    <td class="text-left">กำหนดชักใย</td>
     <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
</tr>
<!-- -->
<tr class="sub_menu">
    <td class="text-left">ชุบนุ่ม</td>
     <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
</tr>
<!-- -->
<tr class="sub_menu">
    <td class="text-left">สูตรคำนวณและเงื่อนไข</td>
     <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
</tr>
<!-- -->
<tr class="sub_menu">
    <td class="text-left">ตั้งค่าระบบ</td>
    <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
</tr>
<!-- -->
<tr class="sub_menu">
    <td class="text-left">ภาษา</td>
     <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
</tr>
<!-- -->
<tr class="sub_menu">
    <td class="text-left">กำหนดสิทธิ์ผู้ใช้งานระบบ</td>
      <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
</tr>
<!-- -->
<tr class="sub_menu">
    <td class="text-left">จัดการผู้ใช้งานระบบ</td>
   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
					   <td><div class="custom-controls-stacked">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                        </label>
                      </div></td>
</tr>
</tbody>

                            </table>
                        </div>
					
					    

						</div>

                    </div>
                    <div class="m-t text-right">
						<button class="btn btn-lg btn-primary" type="button">บันทึก</button>
                        <button class="btn btn-lg btn-default" type="button">ยกเลิก</button>
                    </div>
                </form>
                    </div>
                </div>
                <!--  -->
            </div>
        </div>










    </div>
</div>
<!--  -->
<? include('inc.footer.php');?>
<!--  -->

<? include('inc.footer.script.php');?>
<!--  -->
