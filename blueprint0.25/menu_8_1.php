<?php
    $select = 'open_menu_1';
    $select2 = 'menu_8';
    $select3 = 'menu_8';
    $select4 = 'menu_8_1';
	
?>
<!--  -->
<? include('inc.header.php');?>
<!--  -->
<? include('inc.navbar.php');?>
<!--  -->
<? include('inc.menu.php');?>
<!-- <div class="layout-main"> -->
<!--  -->
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <h4 class="m-t-0">
                <!-- <span class="d-ib text-danger">--</span> <span class="icon icon-angle-double-right"></span> -->
                <!-- <span class="d-ib">ข้อมูลหลัก</span> -->
                <span class="d-ib text-primary">การตั้งค่า</span> <span class="icon icon-angle-double-right"></span> ข้อมูลหลักอื่นๆ <span class="icon icon-angle-double-right"></span> กะทำงาน
                <!-- <span class="d-ib">
                    <a class="title-bar-shortcut" href="#" title="Add to shortcut list" data-container="body" data-toggle-text="Remove from shortcut list" data-trigger="hover" data-placement="right" data-toggle="tooltip">
                        <span class="sr-only">Add to shortcut list</span>
                    </a>
                </span> -->
            </h4>
            <!-- <p class="title-bar-description">
                <small>Latest update on 01/01/2017 - 10.57 am.</small>
            </p> -->
        </div>

        <div class="row gutter-xs">
            <div class="col-xs-12">
                <!--  -->
                <div class="card">
                    <!-- <div class="card-header">
                        <a href="javascript:window.history.back(-1);" class="btn btn-sm btn-outline-primary"><span class="icon icon-long-arrow-left"></span>&nbsp;&nbsp;Back</a>&nbsp;&nbsp;
                        <strong class="">Latest update on 01/01/2017 - 10.57 am.</strong>
                    </div> -->
                    <div class="card-body">
                        <!--  -->
                        <? include('inc.home_top_filter.php');?>
                        <!--  -->
                        <div class="table-responsive">
                            <table id="demo-datatables-2" class="table table-striped table-bordered table-nowrap dataTable text-center no-footer" cellspacing="0" width="100%">
                                <thead>
                                    <tr class="at_bg_table_blue">
                                        <th class="text-center">สาขา</th>
                                        <th class="text-center">รหัสกะ</th>
                                        <th class="text-center">ชื่อกะ</th>
                                        <th class="text-center">วันที่เริ่ม</th>
                                        <th class="text-center">เวลาที่สิ้นสุด</th>
                                        <th class="text-center">วันที่เริ่มใช้งาน</th>
                                        <th class="text-center">ปรับแก้ไข</th>
                                    </tr>
                                </thead>

<?php $column1 = array(
" B&S",
" B&S",
" B&S",
" BWC",
" BWC",
" CY",
" CY",
" FM",
" FM",
" FM",
" FM1",
" FM1",
" KKF",
" KKF",
" KKF1",
" KKF1",
" NR",
" NR",
);

$arrlength = count($column1);

$column2 = array(
"A",
"B",
"C",
"A",
"B",
"A",
"B",
"A",
"B",
"C",
"A",
"B",
"A",
"B",
"A",
"B",
"A",
"B",

);

$column3 = array(
"กะเช้า",
"กะบ่าย",
"กะดึก",
"กะเช้า",
"กะบ่าย",
"กะเช้า",
"กะบ่าย",
"กะเช้า",
"กะบ่าย",
"กะดึก",
"กะเช้า",
"กะบ่าย",
"กะเช้า",
"กะบ่าย",
"กะเช้า",
"กะบ่าย",
"กะเช้า",
"กะบ่าย",

);

$column4 = array(

"8:01",
"16:01",
"0:01",
"8:01",
"20:01",
"8:00",
"20:01",
"8:00",
"16:31",
"1:01",
"8:01",
"20:01",
"8:01",
"20:01",
"8:00",
"20:00",
"8:01",
"20:01",

);

$column5 = array(
"16:00",
"0:00",
"8:00",
"20:00",
"8:00",
"20:00",
"7:59",
"16:30",
"1:00",
"7:59",
"20:00",
"8:00",
"20:00",
"8:00",
"19:59",
"7:59",
"20:00",
"8:00",

);

$column6 = array(
"24/08/2561"
);
?>

                                <tbody>
                                  <?php for($i=0;$i<$arrlength;$i++){?>
                                    <!--html.1 xsl.3-->
                                    <tr>
                                      <td><?php echo $column1[$i];?></td>
                                      <td><?php echo $column2[$i];?></td>
                                      <td><?php echo $column3[$i];?></td>
                                      <td><?php echo $column4[$i];?></td>
                                      <td><?php echo $column5[$i];?></td>
                                      <td><?php echo $column6[0];?></td>
                                      <td class="text-center" style="display: table-cell;">
                                         <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_8_1.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                                        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                                            <span class="icon icon-lg icon-close"></span>
                                        </button>
                                    </td>
                                </tr>
                               <?}?>

                        </tbody>
                    </table>
                        </div>
                    </div>
                </div>
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
            </div>
        </div>










    </div>
</div>
<!--  -->
<? include('inc.footer.php');?>
<!--  -->
<? include('inc.footer.script.php');?>
<!--  -->
