<?php
$select = 'open_menu_9';
$select2 = 'menu_9_2';
$select3 = 'menu_9_2';
$select4 = 'menu_9_2';
?>
<!--  -->
<?include 'inc.header.php';?>
<!--  -->
<?include 'inc.navbar.php';?>
<!--  -->
<?include 'inc.menu.php';?>
<!-- <div class="layout-main"> -->
<!--  -->
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <h1 class="title-bar-title">
                <span class="d-ib">แก้ไขผู้ใช้งานระบบ</span>
            </h1>
            <div class="title-bar-description">
                <span class="d-ib text-primary">การตั้งค่า</span> <span class="icon icon-angle-double-right"></span> ผู้ดูแลระบบ <span class="icon icon-angle-double-right"> จัดการผู้ใช้งานระบบ</span><span class="icon icon-angle-double-right"> แก้ไขผู้ใช้งานระบบ</span>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <!--  -->
                <div class="demo-form-wrapper">
                    <form class="form form-horizontal">
                        <div class="panel m-b-lg">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#tab-1" data-toggle="tab" aria-expanded="true">ข้อมุลทั่วไป</a></li>
                                <li class=""><a href="#tab-2" data-toggle="tab" aria-expanded="false">กำหนดสิทธิ์การใช้งาน</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane fade active in" id="tab-1">
                                    <div class="card" aria-expanded="true">
                                        <div class="card-header at_bg_table_light_gray">
                                            <div class="card-actions">
                                                <button type="button" class="card-action card-toggler" title="Collapse" aria-expanded="true"></button>
                                            </div>
                                            <strong>ข้อมุลพนักงาน</strong>
                                        </div>
                                        <div class="card-body a-p-t a-p-b" style="display: block;">
                                            <form class="form horizontal">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 control-label" for="form-control-1">รหัสพนักงาน</label>
                                                        <div class="col-sm-7">
                                                            <input id="form-control-1" class="form-control" type="text" value="MK001">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-5 control-label" for="form-control-1">อีเมล์</label>
                                                        <div class="col-sm-7">
                                                            <input id="form-control-1" class="form-control" type="text" value="test@kkf.com">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-5 control-label" for="form-control-1">รหัสผ่าน</label>
                                                        <div class="col-sm-7">
                                                            <input id="form-control-1" class="form-control" type="password" value="1234">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-5 control-label" for="form-control-1">ชื่อ</label>
                                                        <div class="col-sm-7">
                                                            <input id="form-control-1" class="form-control" type="text" value="ชื่อพนักงาน">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-5 control-label" for="form-control-1">นามสกุล</label>
                                                        <div class="col-sm-7">
                                                            <input id="form-control-1" class="form-control" type="text" value="ทดสอบระบบ">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-5 control-label" for="form-control-1">วันเกิด</label>
                                                        <div class="col-sm-3">
                                                            <div class=" input-with-icon">
                                                                <input class="form-control" type="text" data-provide="datepicker" value="29/12/2018">
                                                                <span class="icon icon-calendar input-icon"></span>
                                                            </div>
                                                        </div>
                                                        <label class="col-sm-2 control-label" for="form-control-1">อายุ</label>
                                                        <div class="col-sm-2">
                                                            <div class=" input-with-icon">
                                                                <input class="form-control" type="text" value="30">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-5 control-label" for="form-control-1">แผนก</label>
                                                        <div class="col-sm-7">
                                                            <select class="custom-select">
                                                                <option value="1">การตลาด</option>
                                                                <option value="2">วางแผนการผลิต</option>
                                                                <option value="3">การผลิต</option>
                                                                <option value="4">การซ่อมบำรุง</option>
                                                                <option value="5">ไอที</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-5 control-label" for="form-control-1">ตำแหน่ง</label>
                                                        <div class="col-sm-7">
                                                            <select class="custom-select">
                                                                <option value="1">พนักงานทั่วไป</option>
                                                                <option value="2">ผู้จัดการ</option>
                                                                <option value="3">ผู้บริหาร</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-5 control-label" for="form-control-1">สาขา</label>
                                                        <div class="col-sm-7">
                                                            <select class="custom-select">
                                                                <option value="1">B&S : สารคาม</option>
                                                                <option value="2">BWC : ศูนย์บวร</option>
                                                                <option value="3">CY : เชียงยืน</option>
                                                                <option value="4">EXT : ภายนอก</option>
                                                                <option value="5">XFF : สาขาเมืองจีน</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-5 control-label" for="form-control-1">สถานะการใช้งาน</label>
                                                        <div class="col-sm-7 custom-controls-stacked">
                                                            <label class="switch switch-primary">
                                                                <input class="switch-input" type="checkbox" checked="checked">
                                                                <span class="switch-track"></span>
                                                                <span class="switch-thumb"></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <div class="image-profile">
                                                                <img class="image" src="images/default-profile.jpeg">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label" for="form-control-1">อัปโหลดรูป</label>
                                                        <div class="col-sm-7">
                                                            <input class="form-control" type="file">
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>



                                </div>
                                <!-- -->
                                <div class="tab-pane fade" id="tab-2">

                                    <div class="card" aria-expanded="true">
                                        <div class="card-header at_bg_table_light_gray">
                                            <div class="card-actions">
                                                <button type="button" class="card-action card-toggler" title="Collapse" aria-expanded="true"></button>
                                                <!-- <button type="button" class="card-action card-reload" title="Reload"></button>
                                                <button type="button" class="card-action card-remove" title="Remove"></button> -->
                                            </div>
                                            <strong>สิทธิ์การใช้งาน</strong>
                                        </div>
                                        <div class="card-body" style="display: block;">
                                            <form class="form horizontal">
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="form-control-1">สิทธิ์การใช้งาน</label>
                                                    <div class="col-sm-4">
                                                        <select class="custom-select">
                                                            <option value="1">Administrator</option>
                                                            <option value="2">Manager</option>
                                                            <option value="3">IT</option>
                                                            <option value="4">หัวหน้าฝ่ายวางแผน</option>
                                                            <option value="5">พนักงานฝ่ายวางแผน</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="form-control-1">สิทธิ์การใช้งานสำรอง</label>
                                                    <div class="col-sm-4">
                                                        <select class="custom-select">
                                                            <option value="0">ไม่มี</option>
                                                            <option value="1">Administrator</option>
                                                            <option value="3">IT</option>
                                                            <option value="4">หัวหน้าฝ่ายวางแผน</option>
                                                            <option value="5">พนักงานฝ่ายวางแผน</option>
                                                        </select>
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="form-control-1">เลือกสาขาเพิ่มเติม</label>
                                                    <div class="col-sm-4">
                                                        <select id="demo-select2-2" class="form-control select2-hidden-accessible" multiple="" tabindex="-1" aria-hidden="true">
                                                            <option value="B&S : สารคาม">B&S : สารคาม</option>
                                                            <option value="BWC : ศูนย์บวร">BWC : ศูนย์บวร</option>
                                                            <option value="CY : เชียงยืน">CY : เชียงยืน</option>
                                                            <option value="EXT : ภายนอก">EXT : ภายนอก</option>
                                                            <option value="XFF : สาขาเมืองจีน">XFF : สาขาเมืองจีน</option>
                                                        </select>
                                                        <i class="">สิทธิ์ในการจัดการข้อมูลจากสาขาอื่นๆ</i>
                                                    </div>
                                                </div>

                                            </form>
                                        </div>
                                    </div>

                                    <div class="card" aria-expanded="true">
                                        <div class="card-header at_bg_table_light_gray">
                                            <div class="card-actions">
                                                <button type="button" class="card-action card-toggler" title="Collapse" aria-expanded="true"></button>
                                                <!-- <button type="button" class="card-action card-reload" title="Reload"></button>
                                                <button type="button" class="card-action card-remove" title="Remove"></button> -->
                                            </div>
                                            <strong>ตั้งค่าเมนู</strong>
                                        </div>
                                        <form class="form form-horizontal card-body" style="display: block;">
                                            <div class="panel m-b-lg">
                                                <ul class="nav nav-tabs">
                                                    <li class="active"><a href="#branch-1" data-toggle="tab" aria-expanded="true">สาขาหลัก</a></li>
                                                    <li class="hide" id="extra-tab"><a href="#branch-2" data-toggle="tab" aria-expanded="false">สาขาเพิ่มเติม</a></li>
                                                </ul>
                                                <div class="tab-content">
                                                    <!-- -->
                                                    <div class="tab-pane fade active in" id="branch-1">
                                                        <div class="col-xs-12 m-b">
                                                            <div class="table-responsive">
                                                                <table class="table table-bordered table-nowrap dataTable menu_customize text-center no-footer" cellspacing="0" width="100%">
                                                                    <thead>
                                                                        <tr class="at_bg_table_blue">
                                                                            <th rowspan="2" class="text-center">เมนู</th>
                                                                            <th class="text-center">แสดง</th>
                                                                            <th class="text-center">เพิ่ม</th>
                                                                            <th class="text-center">แก้ไข</th>
                                                                            <th class="text-center">ลบ</th>
                                                                        </tr>
                                                                        <tr class="at_bg_table_blue">
                                                                            <th class="text-center">
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </th>
                                                                            <th class="text-center">
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </th>
                                                                            <th class="text-center">
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </th>
                                                                            <th class="text-center">
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </th>
                                                                        </tr>
                                                                    </thead>
                                                                    <!-- -->
                                                                    <tbody>
                                                                        <tr class="header">
                                                                            <td class="text-left">การตลาด</td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <!-- -->
                                                                        <tr class="sub_menu">
                                                                            <td class="text-left">รายการออเดอร์</td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <!-- -->
                                                                        <tr class="sub_menu">
                                                                            <td class="text-left">รายการสอบถาม</td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <!-- -->
                                                                        <!-- -->
                                                                        <tr class="sub_menu">
                                                                            <td class="text-left">รายการรออนุมัติ</td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <!-- -->
                                                                        <!-- -->
                                                                        <tr class="sub_menu">
                                                                            <td class="text-left">แผนการผลิตโดยรวม</td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                    <!-- -->
                                                                    <tbody>
                                                                        <tr class="header">
                                                                            <td class="text-left">การวางแผนการผลิต</td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <!-- -->
                                                                        <tr class="sub_menu">
                                                                            <td class="text-left">แผนการผลิตปัจจุบัน</td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <!-- -->
                                                                        <tr class="sub_menu">
                                                                            <td class="text-left">วางแผนการผลิต</td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <!-- -->
                                                                        <tr class="sub_menu">
                                                                            <td class="text-left">รายการออเดอร์ที่รอการวางแผน</td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <!-- -->
                                                                        <tr class="sub_menu">
                                                                            <td class="text-left">รายการออเดอร์ที่รอการอนุมัติ</td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                    <!-- -->
                                                                    <tbody>
                                                                        <tr class="header">
                                                                            <td class="text-left">การผลิต</td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <!-- -->
                                                                        <tr class="sub_menu">
                                                                            <td class="text-left">แผนการผลิตปัจจุบัน</td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <!-- -->
                                                                        <tr class="sub_menu">
                                                                            <td class="text-left">การผลิตส่วนหน้า</td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <!-- -->
                                                                        <tr class="sub_menu">
                                                                            <td class="text-left">การผลิตส่วนหลัง</td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                    <!-- -->
                                                                    <!-- -->
                                                                    <tbody>
                                                                        <tr class="header">
                                                                            <td class="text-left">การซ่อมบำรุง</td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <!-- -->
                                                                        <tr class="sub_menu">
                                                                            <td class="text-left">ตารางการซ่อมบำรุง</td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <!-- -->
                                                                        <tr class="sub_menu">
                                                                            <td class="text-left">บันทึกการจอดเครื่อง</td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <!-- -->
                                                                        <tr class="sub_menu">
                                                                            <td class="text-left">ข้อมูลการจอดเครื่องจาก PLC</td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                    <!-- -->
                                                                    <tbody>
                                                                        <tr class="header">
                                                                            <td class="text-left">การวิเคราะห์และรายงาน</td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <!-- -->
                                                                        <tr class="sub_menu">
                                                                            <td class="text-left">ภาพรวมด้านการตลาด</td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <!-- -->
                                                                        <tr class="sub_menu">
                                                                            <td class="text-left">ภาพรวมด้านการผลิต</td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <!-- -->
                                                                        <tr class="sub_menu">
                                                                            <td class="text-left">รายงานสรุปการผลิต</td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <!-- -->
                                                                        <tr class="sub_menu">
                                                                            <td class="text-left">รายงานประสิทธิภาพการผลิต</td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <!-- -->
                                                                        <tr class="sub_menu">
                                                                            <td class="text-left">ฝ่ายการตลาด</td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <!-- -->
                                                                        <tr class="sub_menu">
                                                                            <td class="text-left">ฝ่ายการวางแผนการผลิต</td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <!-- -->
                                                                        <tr class="sub_menu">
                                                                            <td class="text-left">ฝ่ายการผลิต</td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <!-- -->
                                                                        <tr class="sub_menu">
                                                                            <td class="text-left">ฝ่ายซ่อมบำรุง</td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <!-- -->
                                                                        <tr class="sub_menu">
                                                                            <td class="text-left">ฝ่ายการบริหารและฝ่ายอื่นๆ</td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>

                                                                    <tbody>
                                                                        <tr class="header">
                                                                            <td class="text-left">การตั้งค่า</td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <!-- -->
                                                                        <tr class="sub_menu">
                                                                            <td class="text-left">สาขา/โรงทอ</td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <!-- -->
                                                                        <tr class="sub_menu">
                                                                            <td class="text-left">สินค้า</td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <!-- -->
                                                                        <tr class="sub_menu">
                                                                            <td class="text-left">ลูกค้า</td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <!-- -->
                                                                        <tr class="sub_menu">
                                                                            <td class="text-left">เครื่องชักใย</td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <!-- -->
                                                                        <tr class="sub_menu">
                                                                            <td class="text-left">กลุ่มเครื่องทอ</td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <!-- -->
                                                                        <tr class="sub_menu">
                                                                            <td class="text-left">เครื่องทอ</td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <!-- -->
                                                                        <tr class="sub_menu">
                                                                            <td class="text-left">กระบวนการผลิต</td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <!-- -->
                                                                        <tr class="sub_menu">
                                                                            <td class="text-left">ขั้นตอนการผลิต</td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <!-- -->
                                                                        <tr class="sub_menu">
                                                                            <td class="text-left">ลำดับขั้นตอนการผลิต</td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <!-- -->
                                                                        <tr class="sub_menu">
                                                                            <td class="text-left">วงจรการผลิต</td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <!-- -->
                                                                        <tr class="sub_menu">
                                                                            <td class="text-left">สถานี</td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <!-- -->
                                                                        <tr class="sub_menu">
                                                                            <td class="text-left">ประเภทงานซ่อมบำรุง</td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <!-- -->
                                                                        <tr class="sub_menu">
                                                                            <td class="text-left">กิจกรรมงานซ่อมบำรุง</td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <!-- -->
                                                                        <tr class="sub_menu">
                                                                            <td class="text-left">กะทำงาน</td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <!-- -->
                                                                        <tr class="sub_menu">
                                                                            <td class="text-left">วันหยุด</td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <!-- -->
                                                                        <tr class="sub_menu">
                                                                            <td class="text-left">ฤดูกาล</td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <!-- -->
                                                                        <tr class="sub_menu">
                                                                            <td class="text-left">มาตรฐานการตอบเป้าหมาย</td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <!-- -->
                                                                        <tr class="sub_menu">
                                                                            <td class="text-left">ประเภทการขาย</td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <!-- -->
                                                                        <tr class="sub_menu">
                                                                            <td class="text-left">ระดับคุณภาพ</td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <!-- -->
                                                                        <tr class="sub_menu">
                                                                            <td class="text-left">กลุ่มวัตถุดิบ</td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <!-- -->
                                                                        <tr class="sub_menu">
                                                                            <td class="text-left">เงื่อน</td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <!-- -->
                                                                        <tr class="sub_menu">
                                                                            <td class="text-left">การอบ</td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <!-- -->
                                                                        <tr class="sub_menu">
                                                                            <td class="text-left">หู</td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <!-- -->
                                                                        <tr class="sub_menu">
                                                                            <td class="text-left">เบอร์ใย</td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <!-- -->
                                                                        <tr class="sub_menu">
                                                                            <td class="text-left">กำหนดชักใย</td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <!-- -->
                                                                        <tr class="sub_menu">
                                                                            <td class="text-left">ชุบนุ่ม</td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <!-- -->
                                                                        <tr class="sub_menu">
                                                                            <td class="text-left">สูตรคำนวณและเงื่อนไข</td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <!-- -->
                                                                        <tr class="sub_menu">
                                                                            <td class="text-left">ตั้งค่าระบบ</td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <!-- -->
                                                                        <tr class="sub_menu">
                                                                            <td class="text-left">ภาษา</td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <!-- -->
                                                                        <tr class="sub_menu">
                                                                            <td class="text-left">กำหนดสิทธิ์ผู้ใช้งานระบบ</td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <!-- -->
                                                                        <tr class="sub_menu">
                                                                            <td class="text-left">จัดการผู้ใช้งานระบบ</td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input select" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input add" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input edit" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                            <td>
                                                                                <div class="custom-controls-stacked">
                                                                                    <label class="custom-control custom-control-primary custom-checkbox">
                                                                                        <input class="custom-control-input delete" type="checkbox" name="mode" checked="checked">
                                                                                        <span class="custom-control-indicator"></span>
                                                                                    </label>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>

                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- -->
                                                    <div class="tab-pane fade" id="branch-2">

                                                    </div>
                                                    <!-- -->
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <!-- -->
                            </div>
                        </div>
                        <div class="m-t text-center">
                            <a class="btn btn-lg btn-primary" href="menu_2_1.php" type="submit">บันทึก</a>
                            <a class="btn btn-lg btn-default" href="menu_2_1.php" type="button>">ยกเลิก</a>
                        </div>
                    </form>
                </div>
                <!--  -->
            </div>
        </div>


    </div>
</div>
<!--  -->
<?include 'inc.footer.php';?>
<!--  -->
<?include 'inc.footer.script.php';?>
<!--  -->