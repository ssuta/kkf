const startPlanOrder = 'start-plan-order';
const startPlanInquiry = 'start-plan-inquiry';
var isClickFistBefore = false;
var amountOrder = 0;
var amountInquiry = 0;

$(function(){

    // var width = window.innerWidth;
    // if(width < 1600){
    //     $('.card.noti-menu').hide();
    //     $('.sidenav-toggler.hidden-xs').trigger('click');
    // }

    // $('.sidenav-toggler.hidden-xs').on('click', function(){
    //     var navbarWidth = $('.layout-sidebar-body').width();
    //     if(navbarWidth == 220){
    //         $('.card.noti-menu').hide();
    //     }else{
    //         $('.card.noti-menu').show();
    //     }
    // });

    // $('#section-order tbody tr').on('click', function(){
    //     console.log('9999');
    //     onCheckboxChecked(this, 'order');
    // });

    // $('#section-inquiry tbody tr').on('click', function(){
    //     onCheckboxChecked(this, 'inquiry');
    // });

    // var values = [];
    // $('#section-order tbody tr').on('click', function(){
    //     values = [];
    //     var items = $(this).find('td');

    //     items.each(function(i, e){
    //         var item = $(e).html();
    //         values.push(item);
    //     });

    //     var formGroup = $('#form-product').find('.form-group');
    //     for(var i=0; i<items.length; i++){
    //         var span = $(formGroup[i]).find('span');
    //         span.html(values[i]);
    //     }
    //     console.log(values);
    // });
});

function openToPlaning(type){
    // $( "#form-order" ).on( "submit", function( event ) {
    //     event.preventDefault();
    //     console.log( $( this ).serialize() );
    //   });
}

function onSelectItem(element){
    var type = $(element).parents('section').attr('data-type');
    onCheckboxChecked(element, type);
}


function onCheckboxChecked(element, type){
    // var checkbox = $(element).find('input[type="checkbox"]');
    var checkbox = $(element);
    if(checkbox.prop("checked")){
        // checkbox.prop("checked", false);

        if(type == 'order'){
            amountOrder++

            if(amountOrder > 0){
                $('#' + startPlanOrder).show();
            }
        }else{
            amountInquiry++

            if(amountInquiry > 0){
                $('#' + startPlanInquiry).show();
            }
        }
    }else{
        // checkbox.prop("checked", true);

        if(type == 'order'){
            amountOrder--;
            
            if(amountOrder == 0){
                $('#' + startPlanOrder).hide();
            }
        }else{
            amountInquiry--;
            
            if(amountInquiry == 0){
                $('#' + startPlanInquiry).hide();
            }
        }
    }
}

function selectOption(element){
    var value = $(element).val();

    // clear
    $('#section-1, #section-2, #section-3').hide();

    // set
    $('#section-' + value).show();

    if(value == 3){
        $('#search-main').hide();
        $('#date-filter').hide();
        $('#search-filter').removeClass('hide');
        $('#tb-search-filter').focus();
    }else{
        $('#search-filter').addClass('hide');
        $('#search-main').show();
        $('#date-filter').show();
    }
}

function sentProduct(value){
    if(value == 1){
        $('#no-1').html('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ORDER001 : 100 PC');
        $('#no-2').html('NO.1 : ORDER002 : 100 PC');
        $('#no-3').html('NO.2 : ORDER003 : 200 PC');
        $('#no-4').html('NO.3 : ORDER004 : 600 PC');
        $('#no-5').html('NO.4 : ORDER005 : 700 PC');
        $('#status1').html('ทำการจัดส่งสินค้าแล้ว');
        isClickFistBefore = true;
    }else if(value == 2){
        $('#no-2').html('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ORDER002 : 100 PC');
        if(isClickFistBefore){
            $('#no-3').html('NO.1 : ORDER003 : 200 PC');
            $('#no-4').html('NO.2 : ORDER004 : 600 PC');
            $('#no-5').html('NO.3 : ORDER005 : 700 PC');
        }else{
            $('#no-3').html('NO.2 : ORDER003 : 200 PC');
            $('#no-4').html('NO.3 : ORDER004 : 600 PC');
            $('#no-5').html('NO.4 : ORDER005 : 700 PC');
        }
        $('#status2').html('ทำการจัดส่งสินค้าแล้ว');
    }
}

function saveGraph(element){
    $(element).parent().hide();
}

function cancelGraph(element){
    $(element).parent().hide();
}

function selectShowing(element){
    var value = $(element).val();
    if(value == 1){
        $('#section2').addClass('hide');
        $('#section1').removeClass('hide');
    }else{
        $('#section1').addClass('hide');
        $('#section2').removeClass('hide');
    }
}

function showGraphDetail(index){
    switch(index){
        case 1:
            $('#graph' + index).highcharts(getChartOptions1());
            break;
        case 2:
            $('#graph' + index).highcharts(getChartOptions2());
            break;
        case 3:
            $('#graph' + index).highcharts(getChartOptions3());
            break;
        case 4:
            $('#graph' + index).highcharts(getChartOptions4());
            break;
        case 5:
            $('#graph' + index).highcharts(getChartOptions5());
            break;
    }
}



function changeRadioType(element){
    console.log(amountOrder);
    console.log(amountInquiry);
    var value = $(element).val();
    if(value == 'order'){
        $('#' + startPlanInquiry).hide();
        if(amountOrder == 0){
            $('#' + startPlanOrder).hide();
        }else{
            $('#' + startPlanOrder).show();
        }
        
        $('#section-inquiry').hide();
        $('#section-order').show();
    }else if(value == 'inquiry'){
        $('#' + startPlanOrder).hide();
        if(amountInquiry == 0){
            $('#' + startPlanInquiry).hide();
        }else{
            $('#' + startPlanInquiry).show();
        }

        $('#section-order').hide();
        $('#section-inquiry').show();
    }
}