const schedulerId = 'scheduler';
const tableId = 'planing-table';
const defaultDuration = 4;

jQuery(document).ready(function ($) {

    scheduler.locale.labels.timeline_tab = "Timeline";
    scheduler.locale.labels.section_custom = "Section";
    scheduler.config.details_on_create = true;
    scheduler.config.details_on_dblclick = true;
    scheduler.config.xml_date = "%Y-%m-%d %H:%i";

    //===============
    //Configuration
    //===============
    var sections = [{
        key: 1,
        label: "MC 10"
    }, {
        key: 2,
        label: "MC 20"
    }, {
        key: 3,
        label: "MC 30"
    }, {
        key: 4,
        label: "MC 40"
    }, ];

    scheduler.createTimelineView({
        name: "timeline_7days",
        x_unit: "day",
        x_date: "%d",
        x_step: 1,
        x_size: 7,
        x_start: 0,
        x_length: 48,
        y_unit: sections,
        y_property: "section_id",
        render: "bar",
        section_autoheight: false
    });

    scheduler.createTimelineView({
        name: "timeline_15days",
        x_unit: "day",
        x_date: "%d",
        x_step: 1,
        x_size: 15,
        x_start: 0,
        x_length: 48,
        y_unit: sections,
        y_property: "section_id",
        render: "bar",
        section_autoheight: false
    });

    scheduler.createTimelineView({
        name: "timeline_30days",
        x_unit: "day",
        x_date: "%d",
        x_step: 1,
        x_size: 30,
        x_start: 0,
        x_length: 48,
        y_unit: sections,
        y_property: "section_id",
        render: "bar",
        section_autoheight: false
    });

    //===============
    //Data loading
    //===============
    scheduler.config.lightbox.sections = [{
            name: "description",
            height: 30,
            map_to: "text",
            type: "textarea",
            focus: true
        },
        {
            name: "custom",
            height: 23,
            type: "select",
            options: sections,
            map_to: "section_id"
        },
        {
            name: "time",
            height: 72,
            type: "time",
            map_to: "auto"
        }
    ];
    scheduler.config.time_step = 1440;
    scheduler.config.readonly_form = true;
    scheduler.config.drag_create = false;
    scheduler.config.fix_tab_position = false;

    scheduler.locale.labels.timeline_7days_tab = "7 Days"
    scheduler.locale.labels.timeline_15days_tab = "15 Days"
    scheduler.locale.labels.timeline_30days_tab = "30 Days";

    scheduler.date.timeline_7days_start = function (date) {
        return scheduler.date.day_start(date); //
    }
    scheduler.date.timeline_15days_start = function (date) {
        return scheduler.date.day_start(date); //
    }
    scheduler.date.timeline_30days_start = function (date) {
        return scheduler.date.day_start(date); //
    }

    scheduler.date.add_timeline_7days = function (date, inc) {
        return scheduler.date.add(date, inc * 7, "day");
    }
    scheduler.date.add_timeline_15days = function (date, inc) {
        return scheduler.date.add(date, inc * 15, "day");
    }
    scheduler.date.add_timeline_30days = function (date, inc) {
        return scheduler.date.add(date, inc * 30, "day");
    }

    scheduler.date.subtract_timeline_7days = function (date, inc) {
        return scheduler.date.subtract(date, inc * -7, "day");
    }
    scheduler.date.subtract_timeline_15days = function (date, inc) {
        return scheduler.date.subtract(date, inc * -15, "day");
    }
    scheduler.date.subtract_timeline_30days = function (date, inc) {
        return scheduler.date.subtract(date, inc * -30, "day");
    }

    scheduler.templates.timeline_7days_date = scheduler.templates.timeline_7days_date;
    scheduler.templates.timeline_15days_date = scheduler.templates.timeline_15days_date;
    scheduler.templates.timeline_30days_date = scheduler.templates.timeline_30days_date;

    scheduler.templates.timeline_7days_scale_date = scheduler.templates.timeline_7days_scale_date;
    scheduler.templates.timeline_15days_scale_date = scheduler.templates.timeline_15days_scale_date;
    scheduler.templates.timeline_30days_scale_date = scheduler.templates.timeline_30days_scale_date;

    scheduler.attachEvent("onSchedulerReady", onSchedulerReady);
    scheduler.attachEvent("onDataRender", updateDroppable);
    scheduler.attachEvent("onEventChanged", onSchedulerEventChanged);
    scheduler.attachEvent("onBeforeEventDelete", onSchedulerBeforeEventDelete);

    scheduler.init(schedulerId, new Date(), "timeline_7days");


});

function padNum(number, digit = 2) {
    return ("0".repeat(digit) + number).substr(digit * -1);
}

function onSchedulerReady() {
    // const rows = Array.from(document.querySelectorAll(`#${tableId} tbody tr`));
    // if (!rows)
    //     return;

    const now = new Date();
    const date = `${padNum(now.getDate())}-${padNum(now.getMonth() + 1)}-${now.getFullYear()}`;

    jQuery(`#${tableId} tbody tr`).each(function () {
        const r = jQuery(this);
        r.draggable({
            cancel: "a.ui-icon", // clicking an icon won't initiate dragging
            revert: "invalid", // when not dropped, the item will revert back to its initial position
            helper: "clone",
            cursor: "move"
        });
    });
}

function updateDroppable() {
    setTimeout(function () {
        jQuery(`#${schedulerId} .dhx_matrix_cell`).droppable({
            accept: '.ui-draggable',
            hoverClass: "cell-highlight",
            tolerance: "pointer",
            drop: function (event, ui) {
                const obj = ui.draggable;
                if (!obj)
                    return;

                const duration = obj.data('duration') || defaultDuration;

                const columnIndex = jQuery(this).index();
                const sectionIndex = jQuery(this).closest('table').closest('tr').index();

                const today = moment().startOf('day');
                const startMoment = moment(today).add(columnIndex, 'day');
                const endMoment = moment(startMoment).add(duration, 'day');
                const start = startMoment.format('DD-MM-YYYY 00:00');
                const end = endMoment.format('DD-MM-YYYY 00:00');
                const startShort = startMoment.format('DD-MMM-YYYY');
                const endShort = moment(endMoment).subtract(1, 'day').format('DD-MMM-YYYY');


                const row = jQuery(`#${tableId} tbody tr[data-name="${obj.data('name')}"]`);
                row.find('.column-status')
                    .removeClass('at_bg_table_orange')
                    .addClass('at_bg_table_green')
                    .text('(100%) Planning');

                row.find('.column-start')
                    .text(startShort);

                row.find('.column-end')
                    .text(endShort);

                scheduler.addEvent({
                    id: obj.data('name'),
                    start_date: start,
                    end_date: end,
                    text: obj.data('name'),
                    section_id: sectionIndex + 1,
                    row: row
                });

                updateDroppable();
            }
        });
    }, 100);
}

function onSchedulerEventChanged(id, ev) {
    if (ev.row) {
        const startDate = moment(ev.start_date);
        const endDate = moment(ev.end_date).subtract(1, 'day');
        const start = startDate.format('DD-MMM-YYYY');
        const end = endDate.format('DD-MMM-YYYY');

        ev.row.find('.column-start')
            .text(start);

        ev.row.find('.column-end')
            .text(end);
    }
}

function onSchedulerBeforeEventDelete(id, ev) {
    if (ev.row) {
        ev.row.find('.column-status')
            .removeClass('at_bg_table_green')
            .addClass('at_bg_table_orange')
            .text('(0%) Planning');

        ev.row.find('.column-start')
            .text('-');

        ev.row.find('.column-end')
            .text('-');
    }
    return true;
}