$(function(){
    // create event
    scheduler.init(schedulerDivId, new Date(), "timeline_15days");
    
    for(var i=0; i<15; i++){
        var randSectionId = getRandomInt(1, 20);
        var startDay = day;
        // var startDay = getRandomInt(day, day + 2);
        var startDate = startDay + '-' +  (month + 1) + '-' + year + ' 00:00';
        var endDate = getRandomInt(startDay + 1, day + 5) + '-' +  (month + 1) + '-' + year + ' 00:00';
    
        // console.log(startDate);
        // console.log(endDate);
    
        var productId = 'P' + getRandomInt(10000, 99999);
        scheduler.addEvent({
            id: productId,
            start_date: startDate,
            end_date: endDate,
            text: productId,
            section_id: randSectionId,
            type: 'current',
            // color: color,
            // row: $($('tbody tr')[0])
        });
    }
});