$(function () {
    /*menu role*/
    $('#hasExpired').change(onHasExpire);
    /*menu role*/

    /*menu user*/
    $('#extra-tab a').click(onExtraBranchClick);
    $('#demo-select2-2').change(onSelectBranch);
    //$('#select-role').click(onSelectRoleClick);
    /*menu user*/
    initCheckboxEvent();
});

function onSelectRoleClick(e) {
    if ($('#user-role').val() == 2)
        $('#second-role').show();
    else
        $('#second-role').hide();
}


function initCheckboxEvent() {
    $('.table thead input[type=checkbox]').change(onHeaderCheckboxClick);
    $('.table .header input[type=checkbox]').change(onCheckboxHeaderClick);
    $('.table .sub_menu input[type=checkbox]').change(onCheckboxChildrenClick);
    $('.table .sub_menu input[type=checkbox].select').change(onSelectCheck);
}

function onSelectCheck(e) {
    if (!this.checked) {
        $(this).parents('tr').find('input[type=checkbox]:not(.select)').prop('checked', false);
        $(this).parents('tr').find('input[type=checkbox]:not(.select)').attr('disabled', 'disabled');
    } else {
        $(this).parents('tr').find('input[type=checkbox]:not(.select)').removeAttr('disabled');
    }
}

function onExtraBranchClick(e) {
    $('#branch-2').html('');
    $('#branch-1').clone().appendTo($('#branch-2'));
    $('#branch-2').find('input[type=checkbox].select').attr('disabled', 'disabled');
    $('#branch-2').find('input[type=checkbox].select:not(:checked)').parents('tr').find('input[type=checkbox]').attr('disabled', 'disabled');
    $('#branch-2').find('input[type=checkbox]:not(.select)').prop('checked',false);
    initCheckboxEvent();
}

function onHasExpire() {
    if (this.checked)
        $('#expiredDate').hide();
    else
        $('#expiredDate').show();
}

function onSelectBranch(e) {
    if ($('.select2-selection__rendered').children('.select2-selection__choice').length > 0)
        $('.nav-tabs #extra-tab').removeClass('hide');
    else
        $('.nav-tabs #extra-tab').addClass('hide');
}

function onHeaderCheckboxClick(e) {
    e.stopPropagation();
    $(this).parents('table').find('tbody input[type=checkbox]' + getType($(this))).prop('checked', this.checked);
    if (!this.checked && $(this).hasClass('select')) {
        $('.table input[type=checkbox]:not(.select)').prop('checked', false);
        $('.table input[type=checkbox]:not(.select)').attr('disabled', 'disabled');
    } else
        $('.table input[type=checkbox]:not(.select)').removeAttr('disabled');
}

function onCheckboxHeaderClick(e) {
    e.stopPropagation();
    var menuGroup = $(this).parents('.header').closest('tbody');
    menuGroup.find('.sub_menu input[type=checkbox]' + getType($(this))).prop('checked', this.checked);
    if (!this.checked)
        $('thead input[type=checkbox]' + getType($(this))).prop('checked', false);
    else {
        if ($('table').find('tbody input[type=checkbox]' + getType($(this))).length == $('table').find('tbody input[type=checkbox]:checked' + getType($(this))).length)
            $('thead input[type=checkbox]' + getType($(this))).prop('checked', true);
    }
}

function onCheckboxChildrenClick(e) {
    e.stopPropagation();

    var menuGroup = $(this).parents('.sub_menu').closest('tbody');
    if (!this.checked) {
        menuGroup.find('.header input[type=checkbox]' + getType($(this))).prop('checked', this.checked);
        $('thead input[type=checkbox]' + getType($(this))).prop('checked', false);
    }
    else {
        if (menuGroup.find('.sub_menu input[type=checkbox]' + getType($(this))).length == menuGroup.find('.sub_menu input[type=checkbox]:checked' + getType($(this))).length)
            menuGroup.find('.header input[type=checkbox]' + getType($(this))).prop('checked', true);
        if ($('table').find('tbody input[type=checkbox]' + getType($(this))).length == $('table').find('tbody input[type=checkbox]:checked' + getType($(this))).length)
            $('thead input[type=checkbox]' + getType($(this))).prop('checked', true);
    }
}

function getType(input) {
    if (input.hasClass('delete'))
        return '.delete';
    else if (input.hasClass('select'))
        return '.select';
    else if (input.hasClass('edit'))
        return '.edit';
    else if (input.hasClass('add'))
        return '.add';
    else
        return '';
}