const schedulerDivId = 'scheduler';
const tableDivId = 'planing-table';
const defaultDuration = 4;
var typeAuto = 'auto';
var typePlanning = 'planing';

var d = new Date();
var day = d.getDate();
var month = d.getMonth();
var year = d.getFullYear();

jQuery(document).ready(function ($) {

    scheduler.locale.labels.timeline_tab = "Timeline";
    scheduler.locale.labels.section_custom = "Section";
    scheduler.config.details_on_create = true;
    scheduler.config.details_on_dblclick = true;
    scheduler.config.xml_date = "%Y-%m-%d %H:%i";
    scheduler.config.drag_resize= false;

    //===============
    //Configuration
    //===============
    var sections = [{
        key: 1,
        label: "MC 10"
    }, {
        key: 2,
        label: "MC 20"
    }, {
        key: 3,
        label: "MC 30"
    }, {
        key: 4,
        label: "MC 40"
    }, ];

    var elements = [ // original hierarhical array to display
        {key:'b1', label:"สาขาที่ 1", open: true, children: [
                {key:'g1-1', label:"กลุ่มเครื่องทอที่ 1", open: true, children: [
                        {key: 1, label: "เครื่องทอที่ 1" },
                        {key: 2, label: "เครื่องทอที่ 2"},
                        {key: 3, label: "เครื่องทอที่ 3"},
                        {key: 4, label: "เครื่องทอที่ 4"},
                        {key: 5, label: "เครื่องทอที่ 5"},
                    ]},
                {key:'g1-2', label:"กลุ่มเครื่องทอที่ 2", open: true,  children: [
                        {key: 6, label: "เครื่องทอที่ 1" },
                        {key: 7, label: "เครื่องทอที่ 2"},
                        {key: 8, label: "เครื่องทอที่ 3"},
                        {key: 9, label: "เครื่องทอที่ 4"},
                        {key: 10, label: "เครื่องทอที่ 5"},
                    ]},
            ]},
            {key:'b2', label:"สาขาที่ 2", open: true, children: [
                    {key:'g2-1', label:"กลุ่มเครื่องทอที่ 3", open: true, children: [
                            {key: 11, label: "เครื่องทอที่ 1" },
                            {key: 12, label: "เครื่องทอที่ 2"},
                            {key: 13, label: "เครื่องทอที่ 3"},
                            {key: 14, label: "เครื่องทอที่ 4"},
                            {key: 15, label: "เครื่องทอที่ 5"},
                        ]},
                    {key:'g2-2', label:"กลุ่มเครื่องทอที่ 4", open: true,  children: [
                            {key: 16, label: "เครื่องทอที่ 1" },
                            {key: 17, label: "เครื่องทอที่ 2"},
                            {key: 18, label: "เครื่องทอที่ 3"},
                            {key: 19, label: "เครื่องทอที่ 4"},
                            {key: 20, label: "เครื่องทอที่ 5"},
                        ]},
            ]},
    ];

    scheduler.createTimelineView({
        name: "timeline_7days",
        x_unit: "day",
        x_date: "%d",
        x_step: 1,
        x_size: 7,
        x_start: 0,
        x_length: 48,
        y_unit: elements,
        y_property: "section_id",
        render: "tree",
        folder_dy: 25,
        dy:23,
        section_autoheight: false
    });

    
    scheduler.createTimelineView({
        name: "timeline_15days",
        x_unit: "day",
        x_date: "%d",
        x_step: 1,
        x_size: 15,
        x_start: 0,
        x_length: 48,
        y_unit: elements,
        y_property: "section_id",
        render: "tree",
        folder_dy: 25,
        dy:23,
        section_autoheight: false
    });

    scheduler.createTimelineView({
        name: "timeline_30days",
        x_unit: "day",
        x_date: "%d",
        x_step: 1,
        x_size: 30,
        x_start: 0,
        x_length: 48,
        y_unit: elements,
        y_property: "section_id",
        render: "tree",
        folder_dy: 25,
        dy:23,
        section_autoheight: false
    });

    //===============
    //Data loading
    //===============
    scheduler.config.lightbox.sections = [{
            name: "description",
            height: 30,
            map_to: "text",
            type: "textarea",
            focus: true
        },
        {
            name: "custom",
            height: 23,
            type: "select",
            options: sections,
            map_to: "section_id"
        },
        {
            name: "time",
            height: 72,
            type: "time",
            map_to: "auto"
        }
    ];
    scheduler.config.time_step = 1440;
    scheduler.config.readonly_form = true;
    scheduler.config.drag_create = false;
    scheduler.config.fix_tab_position = false;

    scheduler.locale.labels.timeline_7days_tab = "7 วัน"
    scheduler.locale.labels.timeline_15days_tab = "15 วัน"
    scheduler.locale.labels.timeline_30days_tab = "30 วัน";
    scheduler.locale.labels.today_tab = "วันนี้";

    scheduler.date.timeline_7days_start = function (date) {
        return scheduler.date.day_start(date); //
    }
    scheduler.date.timeline_15days_start = function (date) {
        return scheduler.date.day_start(date); //
    }
    scheduler.date.timeline_30days_start = function (date) {
        return scheduler.date.day_start(date); //
    }

    scheduler.date.add_timeline_7days = function (date, inc) {
        return scheduler.date.add(date, inc * 7, "day");
    }
    scheduler.date.add_timeline_15days = function (date, inc) {
        return scheduler.date.add(date, inc * 15, "day");
    }
    scheduler.date.add_timeline_30days = function (date, inc) {
        return scheduler.date.add(date, inc * 30, "day");
    }

    scheduler.date.subtract_timeline_7days = function (date, inc) {
        return scheduler.date.subtract(date, inc * -7, "day");
    }
    scheduler.date.subtract_timeline_15days = function (date, inc) {
        return scheduler.date.subtract(date, inc * -15, "day");
    }
    scheduler.date.subtract_timeline_30days = function (date, inc) {
        return scheduler.date.subtract(date, inc * -30, "day");
    }

    scheduler.templates.timeline_7days_date = scheduler.templates.timeline_7days_date;
    scheduler.templates.timeline_15days_date = scheduler.templates.timeline_15days_date;
    scheduler.templates.timeline_30days_date = scheduler.templates.timeline_30days_date;

    scheduler.templates.timeline_7days_scale_date = scheduler.templates.timeline_7days_scale_date;
    scheduler.templates.timeline_15days_scale_date = scheduler.templates.timeline_15days_scale_date;
    scheduler.templates.timeline_30days_scale_date = scheduler.templates.timeline_30days_scale_date;

    scheduler.attachEvent("onSchedulerReady", onSchedulerReady);
    scheduler.attachEvent("onDataRender", updateDroppable);
    scheduler.attachEvent("onEventChanged", onSchedulerEventChanged);
    scheduler.attachEvent("onBeforeEventDelete", onSchedulerBeforeEventDelete);

    // scheduler.init(schedulerDivId, new Date(), "timeline_7days");


    // scheduler.addEvent({
    //     id: 'efxTf5s3TH',
    //     start_date: '23-08-2018 00:00',
    //     end_date: '26-08-2018 00:00',
    //     text: 'efxTf5s3TH',
    //     section_id: 1,
    //     // row: $($('tbody tr')[0])
    // });

    // scheduler.addEvent({
    //     id: 'Ou3Fd2JdnX',
    //     start_date: '23-08-2018 00:00',
    //     end_date: '24-08-2018 00:00',
    //     text: 'Ou3Fd2JdnX',
    //     section_id: 2,
    //     // row: $($('tbody tr')[0])
    // });

    // scheduler.addEvent({
    //     id: 'j7vSki4DvE',
    //     start_date: '22-08-2018 00:00',
    //     end_date: '24-08-2018 00:00',
    //     text: 'j7vSki4DvE',
    //     section_id: 3,
    //     // row: $($('tbody tr')[0])
    // });

    // scheduler.addEvent({
    //     id: 'pu7ojUe2Rf',
    //     start_date: '24-08-2018 00:00',
    //     end_date: '26-08-2018 00:00',
    //     text: 'pu7ojUe2Rf',
    //     section_id: 3,
    //     // row: $($('tbody tr')[0])
    // });

    // scheduler.addEvent({
    //     id: '8gIjvEmCh2',
    //     start_date: '27-08-2018 00:00',
    //     end_date: '30-08-2018 00:00',
    //     text: '8gIjvEmCh2',
    //     section_id: 3,
    //     // row: $($('tbody tr')[0])
    // });


    scheduler.attachEvent("onConfirmedBeforeEventDelete", function(id, event){
        // your custom code
    });
    
    scheduler.attachEvent("onEventChanged", function (id, event) {
            // your custom code
    });
    
    // scheduler.attachEvent("onEventAdded", function (id, event) {
    //         // your custom code
    //         console.log(event);
    // });

    scheduler.templates.event_class = function (start, end, event) {
        if (event.type == 'current') return "current_event";
        return "planning_event"; 
    };

    scheduler.attachEvent("onBeforeDrag", function (id, mode, e){
        if(e.target.className.indexOf('current_event') != -1){
            return false;
        }else{
            return true;
        }
    });
});

function padNum(number, digit = 2) {
    return ("0".repeat(digit) + number).substr(digit * -1);
}

function onSchedulerReady() {
    // const rows = Array.from(document.querySelectorAll(`#${tableDivId} tbody tr`));
    // if (!rows)
    //     return;

    const now = new Date();
    const date = `${padNum(now.getDate())}-${padNum(now.getMonth() + 1)}-${now.getFullYear()}`;

    jQuery(`#${tableDivId} tbody tr`).each(function () {
        const r = jQuery(this);
        r.draggable({
            cancel: "a.ui-icon", // clicking an icon won't initiate dragging
            revert: "invalid", // when not dropped, the item will revert back to its initial position
            helper: "clone",
            cursor: "move"
        });
    });
}

function updateDroppable() {
    setTimeout(function () {
        jQuery(`#${schedulerDivId} .dhx_matrix_cell`).droppable({
            accept: '.ui-draggable',
            hoverClass: "cell-highlight",
            tolerance: "pointer",
            drop: function (event, ui) {
                const obj = ui.draggable;
                if (!obj)
                    return;


                const duration = obj.data('duration') || defaultDuration;

                const columnIndex = jQuery(this).index();
                const sectionIndex = jQuery(this).closest('table').closest('tr').index();

                const today = moment().startOf('day');
                const startMoment = moment(today).add(columnIndex, 'day');
                const endMoment = moment(startMoment).add(duration, 'day');
                const start = startMoment.format('DD-MM-YYYY 00:00');
                const end = endMoment.format('DD-MM-YYYY 00:00');
                // const startShort = startMoment.format('DD-MMM-YYYY');
                // const endShort = moment(endMoment).subtract(1, 'day').format('DD-MMM-YYYY');
                const startShort = startMoment.format('DD/MM/YYYY');
                const endShort = moment(endMoment).subtract(1, 'day').format('DD/MM/YYYY');


                const row = jQuery(`#${tableDivId} tbody tr[data-name="${obj.data('name')}"]`);

                var hhh = `${obj.data('name')}`;
                console.log("row: ", row);

                row.find('.column-status')
                    .removeClass('at_bg_table_orange')
                    .addClass('at_bg_table_green')
                    .text('(100%) วางแผน');

                row.find('.column-start')
                    .text(startShort);

                row.find('.column-end')
                    .text(endShort);

                    
                console.log("id: ", obj.data('name'));
                console.log("start_date: ", start);
                console.log("end_date: ", end);
                console.log("text: ", obj.data('name'));
                console.log("section_id: ", sectionIndex + 1);
                console.log("row: ", row);

                scheduler.addEvent({
                    id: obj.data('name'),
                    start_date: start,
                    end_date: end,
                    text: obj.data('name'),
                    section_id: sectionIndex + 1,
                    row: row
                });

                updateDroppable();

                var tr = $('#' + tableDivId + ' tbody tr[data-name="'+obj.data('name')+'"]')[0];
                console.log('tr',tr);
                $(tr).find('button[name="plan-auto"]').addClass('hide');
            }
        });
    }, 100);
}

function onSchedulerEventChanged(id, ev) {
    if (ev.row) {
        const startDate = moment(ev.start_date);
        const endDate = moment(ev.end_date).subtract(1, 'day');
        const start = startDate.format('DD-MMM-YYYY');
        const end = endDate.format('DD-MMM-YYYY');

        ev.row.find('.column-start')
            .text(start);

        ev.row.find('.column-end')
            .text(end);
    }
}

function onSchedulerBeforeEventDelete(id, ev) {
    if (ev.row) {
        ev.row.find('.column-status')
            .removeClass('at_bg_table_green')
            .addClass('at_bg_table_orange')
            .text('(0%) วางแผน');

        ev.row.find('.column-start')
            .text('-');

        ev.row.find('.column-end')
            .text('-');
    }
    return true;
}

function getRandomInt(min, max){
    return Math.floor(Math.random() * (max - min + 1)) + min;
}


function onPlanItem(element, type){
    if(type == typeAuto){
        addPlanningEvent(element, type);
        $(element).addClass('hide');
        $(element).prev().addClass('hide');
    }

    var items = $('.card-body.item');
    var temp = 1;
    $.each(items, function(i, v){
        var planAll = $(v).find('.plan-all')[0];
        // console.log(planAll);
        var table = $(v).find('table');
        var button = table.find('td[class="control"]').find('button[name="plan-auto"]');
        var amountButton = button.length;
        var amountHide = 0;


        $.each(button, function(i, btn){
            if($(btn).hasClass('hide'))
                amountHide++;
        });

        if(amountHide > 0){
            $(planAll).hide();
        }else{
            $(planAll).show();
        }
        temp++;
    });

    // console.log('elm', element);

    
}

function onPlanAll(element){
    var td = $(element).parents('.card-body.item').find('button[name="plan-auto"]');
    var time = 100;
    $.each(td, function(i, v){
        //setTimeout(function(){
            $(v).trigger('click');
        //}, i * 1000);
    });

    $(element).addClass('hide');
}

function addPlanningEvent(element, type){
    var parent = $(element).parents('.card.planning-item');
    var color = '#' + $(parent).attr('data-color');
    var id = $(element).parents('tr').attr('data-name');

    var randSectionId = getRandomInt(1, 20);
    var startDay = getRandomInt(day, day + 7);
    var startDate = startDay + '-' +  (month + 1) + '-' + year + ' 00:00';
    var endDate = getRandomInt(startDay + 1, day + 7) + '-' +  (month + 1) + '-' + year + ' 00:00';

    // console.log(startDate);
    // console.log(endDate);

    
    scheduler.addEvent({
        id: id,
        start_date: startDate,
        end_date: endDate,
        text: id,
        section_id: randSectionId,
        color: color,
        textColor: '#333',
        borderColor: '#333',
        type: 'planning',
        // row: $($('tbody tr')[0])
    });

    var colPercent = $(element).parents('td').prev();
    colPercent.removeClass('at_bg_table_orange');
    colPercent.addClass('at_bg_table_green');
    colPercent.html('(100%) วางแผน');
}