var getChartOptions1 = function () {

  var options = {
    chart: {
      type: 'xrange'
    },
    title: {
      text: 'ORDER0001 : 119P/013010400'
    },  
  credits: {
      enabled: false
    }, exporting: {
      enabled: false
    },
    xAxis: {
      type: 'datetime'
    },
    yAxis: {
      title: {
        text: ''
      },
      categories: ['เครื่องทอ MC.01', 'เครื่องทอ MC.02', 'วงจร D02Y','ส่งเข้าคลังสินค้า'],
      reversed: true
    },legend: {
      enabled: false
    },series: [{
      name: 'รหัสสินค้า 1119P/0130400',
      // pointPadding: 0,
      // groupPadding: 0,
      borderColor: 'gray',
      pointWidth: 20,
      data: [{
        x: Date.UTC(2014, 10, 21),
        x2: Date.UTC(2014, 11, 2),
        y: 0,
        partialFill: 1,
        color: '#66FF00'
      }, {
        x: Date.UTC(2014, 10, 25),
        x2: Date.UTC(2014, 11, 5),
        y: 1,
        partialFill: 1,
        color: '#66FF00'
      }, {
        x: Date.UTC(2014, 10, 23),
        x2: Date.UTC(2014, 11, 15),
        y: 2,
        partialFill: 1,
        color: '#3399FF'
      }, {
        x: Date.UTC(2014, 10, 30),
        x2: Date.UTC(2014, 11, 25),
        y: 3,
        partialFill: 1,
        color: '#FFCC00'
      }],
      dataLabels: {
        enabled: true
      }
    }]
  };

  return options;
}

var getChartOptions2 = function () {

  var options = {
      chart: {
          type: 'xrange'
        },
        title: {
          text: 'ORDER0001 : 1ฑ090/013000402'
        },  
      credits: {
          enabled: false
        }, exporting: {
          enabled: false
        },
        xAxis: {
          type: 'datetime'
        },
        yAxis: {
          title: {
            text: ''
          },
          categories: ['เครื่องทอ MC.12', 'วงจร C01T','ส่งเข้าคลังสินค้า'],
          reversed: true
        },legend: {
          enabled: false
        },series: [{
          name: 'รหัสสินค้า 1ฑ090/013000402',
          // pointPadding: 0,
          // groupPadding: 0,
          borderColor: 'gray',
          pointWidth: 20,
          data: [{
            x: Date.UTC(2014, 10, 21),
            x2: Date.UTC(2014, 11, 2),
            y: 0,
            partialFill: .1,
            color: '#66FF00'
          }, {
            x: Date.UTC(2014, 10, 23),
            x2: Date.UTC(2014, 11, 15),
            y: 1,
            partialFill: 0,
            color: '#3399FF'
          }, {
            x: Date.UTC(2014, 10, 30),
            x2: Date.UTC(2014, 11, 25),
            y: 2,
            partialFill: 0,
            color: '#FFCC00'
          }],
          dataLabels: {
            enabled: true
          }
        }]
  };

  return options;
}

var getChartOptions3 = function () {

  var options = {
      chart: {
          type: 'xrange'
        },
        title: {
          text: 'ORDER0001 : 11190/013010400'
        },  
      credits: {
          enabled: false
        }, exporting: {
          enabled: false
        },
        xAxis: {
          type: 'datetime'
        },
        yAxis: {
          title: {
            text: ''
          },
          categories: ['เครื่องทอ MC.35', 'วงจร D04T','ส่งเข้าคลังสินค้า'],
          reversed: true
        },legend: {
          enabled: false
        },series: [{
          name: 'รหัสสินค้า 11190/013010400',
          // pointPadding: 0,
          // groupPadding: 0,
          borderColor: 'gray',
          pointWidth: 20,
          data: [{
            x: Date.UTC(2014, 10, 21),
            x2: Date.UTC(2014, 11, 2),
            y: 0,
            partialFill: .9,
            color: '#66FF00'
          }, {
            x: Date.UTC(2014, 10, 23),
            x2: Date.UTC(2014, 11, 15),
            y: 1,
            partialFill: .35,
            color: '#3399FF'
          }, {
            x: Date.UTC(2014, 10, 30),
            x2: Date.UTC(2014, 11, 25),
            y: 2,
            partialFill: .05,
            color: '#FFCC00'
          }],
          dataLabels: {
            enabled: true
          }
        }]
  };

  return options;
}

var getChartOptions4 = function () {

  var options = {
      chart: {
          type: 'xrange'
        },
        title: {
          text: 'ORDER0001 : 19090/013000402'
        },  
      credits: {
          enabled: false
        }, exporting: {
          enabled: false
        },
        xAxis: {
          type: 'datetime'
        },
        yAxis: {
          title: {
            text: ''
          },
          categories: ['เครื่องทอ MC.105','วงจร S02Y','ส่งเข้าคลังสินค้า'],
          reversed: true
        },legend: {
          enabled: false
        },series: [{
          name: 'รหัสสินค้า 19090/013000402',
          // pointPadding: 0,
          // groupPadding: 0,
          borderColor: 'gray',
          pointWidth: 20,
          data: [{
            x: Date.UTC(2014, 10, 21),
            x2: Date.UTC(2014, 11, 2),
            y: 0,
            partialFill: 0,
            color: '#66FF00'
          }, {
            x: Date.UTC(2014, 10, 23),
            x2: Date.UTC(2014, 11, 15),
            y: 1,
            partialFill: 0,
            color: '#3399FF'
          }, {
            x: Date.UTC(2014, 10, 30),
            x2: Date.UTC(2014, 11, 25),
            y: 2,
            partialFill: 0,
            color: '#FFCC00'
          }],
          dataLabels: {
            enabled: true
          }
        }]
  };

  return options;
}

var getChartOptions5 = function () {

  var options = {
      chart: {
          type: 'xrange'
        },
        title: {
          text: 'ORDER0001 : 1ฌ094/013000502'
        },  
      credits: {
          enabled: false
        }, exporting: {
          enabled: false
        },
        xAxis: {
          type: 'datetime'
        },
        yAxis: {
          title: {
            text: ''
          },
          categories: ['เครื่องทอ MC.257', 'วงจร S99T','ส่งเข้าคลังสินค้า'],
          reversed: true
        },legend: {
          enabled: false
        },series: [{
          name: 'รหัสสินค้า 1ฌ094/013000502',
          // pointPadding: 0,
          // groupPadding: 0,
          borderColor: 'gray',
          pointWidth: 20,
          data: [{
            x: Date.UTC(2014, 10, 21),
            x2: Date.UTC(2014, 11, 2),
            y: 0,
            partialFill: 0.4,
            color: '#66FF00'
          }, {
            x: Date.UTC(2014, 10, 23),
            x2: Date.UTC(2014, 11, 15),
            y: 1,
            partialFill: 0.12,
            color: '#3399FF'
          }, {
            x: Date.UTC(2014, 10, 30),
            x2: Date.UTC(2014, 11, 25),
            y: 2,
            partialFill: 0,
            color: '#FFCC00'
          }],
          dataLabels: {
            enabled: true
          }
        }]
  };

  return options;
}


var getChartOptions6 = function () {

  var options = {
    chart: {
      type: 'xrange',
      events: {
        click: function () {
          showGraphItem();
        }
      }
    },
    title: {
      text: null
    },  
  credits: {
      enabled: false
    }, exporting: {
      enabled: false
    },
    xAxis: {
      type: 'datetime'
    },
    yAxis: {
      title: {
        text: ''
      },
      categories: ['กลุ่มเครื่องทอ CN', 'กลุ่มเครื่องทอ CAD 11.0  ','กลุ่มเครื่องทอ ED 10.0','กลุ่มเครื่องทอ FM1','','วงจรส่วนหลัง'],
      reversed: true
    },legend: {
      enabled: false
    },series: [{
      name: '',
      events: {
        click: function () {
          showGraphItem();
        }
      },
      // pointPadding: 0,
      // groupPadding: 0,
      borderColor: 'gray',
      pointWidth: 20,
      data: [{
        x: Date.UTC(2014, 10, 25),
        x2: Date.UTC(2014, 10, 26),
        y: 0,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 26),
        x2: Date.UTC(2014, 10, 27),
        y: 0,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 27),
        x2: Date.UTC(2014, 10, 28),
        y: 0,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 28),
        x2: Date.UTC(2014, 10, 29),
        y: 0,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'95%'}
      },{
        x: Date.UTC(2014, 10, 29),
        x2: Date.UTC(2014, 10, 30),
        y: 0,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'85%'}
      },{
        x: Date.UTC(2014, 10, 30),
        x2: Date.UTC(2014, 10, 31),
        y: 0,
        partialFill: 0,
        color: '#D2B48C',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'75%'}
      },{
        x: Date.UTC(2014, 11, 1),
        x2: Date.UTC(2014, 11, 2),
        y: 0,
        partialFill: 0,
        color: '#D2B48C',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'65%'}
      },{
        x: Date.UTC(2014, 11, 2),
        x2: Date.UTC(2014, 11, 3),
        y: 0,
        partialFill: 0,
        color: '#F9967A',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'55%'}
      },{
        x: Date.UTC(2014, 11, 3),
        x2: Date.UTC(2014, 11, 4),
        y: 0,
        partialFill: 0,
        color: '#F9967A',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'45%'}
      },{
        x: Date.UTC(2014, 11, 4),
        x2: Date.UTC(2014, 11, 5),
        y: 0,
        partialFill: 0,
        color: '#FF8C00',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'35%'}
      },{
        x: Date.UTC(2014, 11, 5),
        x2: Date.UTC(2014, 11, 6),
        y: 0,
        partialFill: 0,
        color: '#FF8C00',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'25%'}
      },{
        x: Date.UTC(2014, 11, 6),
        x2: Date.UTC(2014, 11, 7),
        y: 0,
        partialFill: 0,
        color: '#FF4500',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'15%'}
      },{
        x: Date.UTC(2014, 11, 7),
        x2: Date.UTC(2014, 11, 8),
        y: 0,
        partialFill: 0,
        color: '#FF4500',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'5%'}
      },{
        x: Date.UTC(2014, 11, 8),
        x2: Date.UTC(2014, 11, 9),
        y: 0,
        partialFill: 0,
        color: '#FF4500',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'0%'}
      },{
        x: Date.UTC(2014, 10, 25),
        x2: Date.UTC(2014, 10, 26),
        y: 1,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 26),
        x2: Date.UTC(2014, 10, 27),
        y: 1,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 27),
        x2: Date.UTC(2014, 10, 28),
        y: 1,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 28),
        x2: Date.UTC(2014, 10, 29),
        y: 1,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 29),
        x2: Date.UTC(2014, 10, 30),
        y: 1,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 30),
        x2: Date.UTC(2014, 10, 31),
        y: 1,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 1),
        x2: Date.UTC(2014, 11, 2),
        y: 1,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 2),
        x2: Date.UTC(2014, 11, 3),
        y: 1,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 3),
        x2: Date.UTC(2014, 11, 4),
        y: 1,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 4),
        x2: Date.UTC(2014, 11, 5),
        y: 1,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 5),
        x2: Date.UTC(2014, 11, 6),
        y: 1,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 6),
        x2: Date.UTC(2014, 11, 7),
        y: 1,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 7),
        x2: Date.UTC(2014, 11, 8),
        y: 1,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 8),
        x2: Date.UTC(2014, 11, 9),
        y: 1,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 25),
        x2: Date.UTC(2014, 10, 26),
        y: 2,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 26),
        x2: Date.UTC(2014, 10, 27),
        y: 2,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 27),
        x2: Date.UTC(2014, 10, 28),
        y: 2,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 28),
        x2: Date.UTC(2014, 10, 29),
        y: 2,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 29),
        x2: Date.UTC(2014, 10, 30),
        y: 2,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 30),
        x2: Date.UTC(2014, 10, 31),
        y: 2,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 1),
        x2: Date.UTC(2014, 11, 2),
        y: 2,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 2),
        x2: Date.UTC(2014, 11, 3),
        y: 2,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 3),
        x2: Date.UTC(2014, 11, 4),
        y: 2,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 4),
        x2: Date.UTC(2014, 11, 5),
        y: 2,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 5),
        x2: Date.UTC(2014, 11, 6),
        y: 2,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 6),
        x2: Date.UTC(2014, 11, 7),
        y: 2,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 7),
        x2: Date.UTC(2014, 11, 8),
        y: 2,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 8),
        x2: Date.UTC(2014, 11, 9),
        y: 2,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 25),
        x2: Date.UTC(2014, 10, 26),
        y: 3,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 26),
        x2: Date.UTC(2014, 10, 27),
        y: 3,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 27),
        x2: Date.UTC(2014, 10, 28),
        y: 3,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 28),
        x2: Date.UTC(2014, 10, 29),
        y: 3,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 29),
        x2: Date.UTC(2014, 10, 30),
        y: 3,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 30),
        x2: Date.UTC(2014, 10, 31),
        y: 3,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 1),
        x2: Date.UTC(2014, 11, 2),
        y: 3,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 2),
        x2: Date.UTC(2014, 11, 3),
        y: 3,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 3),
        x2: Date.UTC(2014, 11, 4),
        y: 3,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 4),
        x2: Date.UTC(2014, 11, 5),
        y: 3,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 5),
        x2: Date.UTC(2014, 11, 6),
        y: 3,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 6),
        x2: Date.UTC(2014, 11, 7),
        y: 3,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 7),
        x2: Date.UTC(2014, 11, 8),
        y: 3,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 8),
        x2: Date.UTC(2014, 11, 9),
        y: 3,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 25),
        x2: Date.UTC(2014, 10, 26),
        y: 5,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 26),
        x2: Date.UTC(2014, 10, 27),
        y: 5,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 27),
        x2: Date.UTC(2014, 10, 28),
        y: 5,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 28),
        x2: Date.UTC(2014, 10, 29),
        y: 5,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 29),
        x2: Date.UTC(2014, 10, 30),
        y: 5,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 30),
        x2: Date.UTC(2014, 10, 31),
        y: 5,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'99%'}
      },{
        x: Date.UTC(2014, 11, 1),
        x2: Date.UTC(2014, 11, 2),
        y: 5,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'98%'}
      },{
        x: Date.UTC(2014, 11, 2),
        x2: Date.UTC(2014, 11, 3),
        y: 5,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'97%'}
      },{
        x: Date.UTC(2014, 11, 3),
        x2: Date.UTC(2014, 11, 4),
        y: 5,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'96%'}
      },{
        x: Date.UTC(2014, 11, 4),
        x2: Date.UTC(2014, 11, 5),
        y: 5,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'95%'}
      },{
        x: Date.UTC(2014, 11, 5),
        x2: Date.UTC(2014, 11, 6),
        y: 5,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'94%'}
      },{
        x: Date.UTC(2014, 11, 6),
        x2: Date.UTC(2014, 11, 7),
        y: 5,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'93%'}
      },{
        x: Date.UTC(2014, 11, 7),
        x2: Date.UTC(2014, 11, 8),
        y: 5,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'92%'}
      },{
        x: Date.UTC(2014, 11, 8),
        x2: Date.UTC(2014, 11, 9),
        y: 5,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'91%'}
      }],
      dataLabels: {
        enabled: false
      }
    }]
  };

  return options;
}



var getChartOptions7 = function () {

  var options = {
    chart: {
      type: 'xrange',
      events: {
        click: function () {
          showGraphList();
        }
      }
    },
    title: {
      text: null
    },  
  credits: {
      enabled: false
    }, exporting: {
      enabled: false
    },
    xAxis: {
      type: 'datetime'
    },
    yAxis: {
      title: {
        text: ''
      },
      categories: ['กลุ่มเครื่องทอ CN','เครื่องทอ A001','เครื่องทอ A002','เครื่องทอ A003','เครื่องทอ A004','เครื่องทอ A005', 'กลุ่มเครื่องทอ CAD 11.0  ','กลุ่มเครื่องทอ ED 10.0   ','กลุ่มเครื่องทอ FM1','','วงจรส่วนหลัง'],
      reversed: true
    },legend: {
      enabled: false
    },series: [{
      name: '',
      events: {
        click: function () {
          showGraphList();
        }
      },
      // pointPadding: 0,
      // groupPadding: 0,
      borderColor: 'gray',
      pointWidth: 20,
      data: [{
        x: Date.UTC(2014, 10, 25),
        x2: Date.UTC(2014, 10, 26),
        y: 0,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 26),
        x2: Date.UTC(2014, 10, 27),
        y: 0,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 27),
        x2: Date.UTC(2014, 10, 28),
        y: 0,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 28),
        x2: Date.UTC(2014, 10, 29),
        y: 0,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'95%'}
      },{
        x: Date.UTC(2014, 10, 29),
        x2: Date.UTC(2014, 10, 30),
        y: 0,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'85%'}
      },{
        x: Date.UTC(2014, 10, 30),
        x2: Date.UTC(2014, 10, 31),
        y: 0,
        partialFill: 0,
        color: '#D2B48C',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'75%'}
      },{
        x: Date.UTC(2014, 11, 1),
        x2: Date.UTC(2014, 11, 2),
        y: 0,
        partialFill: 0,
        color: '#D2B48C',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'65%'}
      },{
        x: Date.UTC(2014, 11, 2),
        x2: Date.UTC(2014, 11, 3),
        y: 0,
        partialFill: 0,
        color: '#F9967A',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'55%'}
      },{
        x: Date.UTC(2014, 11, 3),
        x2: Date.UTC(2014, 11, 4),
        y: 0,
        partialFill: 0,
        color: '#F9967A',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'45%'}
      },{
        x: Date.UTC(2014, 11, 4),
        x2: Date.UTC(2014, 11, 5),
        y: 0,
        partialFill: 0,
        color: '#FF8C00',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'35%'}
      },{
        x: Date.UTC(2014, 11, 5),
        x2: Date.UTC(2014, 11, 6),
        y: 0,
        partialFill: 0,
        color: '#FF8C00',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'25%'}
      },{
        x: Date.UTC(2014, 11, 6),
        x2: Date.UTC(2014, 11, 7),
        y: 0,
        partialFill: 0,
        color: '#FF4500',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'15%'}
      },{
        x: Date.UTC(2014, 11, 7),
        x2: Date.UTC(2014, 11, 8),
        y: 0,
        partialFill: 0,
        color: '#FF4500',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'5%'}
      },{
        x: Date.UTC(2014, 11, 8),
        x2: Date.UTC(2014, 11, 9),
        y: 0,
        partialFill: 0,
        color: '#FF4500',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'0%'}
      },{
        x: Date.UTC(2014, 10, 25),
        x2: Date.UTC(2014, 10, 28),
        y: 1,
        partialFill: 0,
        color: '#7FFFD4',
        dataLabels: {
          enabled: true,
          align: 'left',
         format:'ORDER001'}
      },{
        x: Date.UTC(2014, 10, 28),
        x2: Date.UTC(2014, 11, 3),
        y: 1,
        partialFill: 0,
        color: '#7FFFD4',
        dataLabels: {
          enabled: true,
          align: 'left',
         format:'ORDER002'}
      },{
        x: Date.UTC(2014, 11, 3),
        x2: Date.UTC(2014, 11, 8),
        y: 1,
        partialFill: 0,
        color: '#7FFFD4',
        dataLabels: {
          enabled: true,
          align: 'left',
         format:'ORDER003'}
      },{
        x: Date.UTC(2014, 10, 25),
        x2: Date.UTC(2014, 10, 27),
        y: 2,
        partialFill: 0,
        color: '#7FFFD4',
        dataLabels: {
          enabled: true,
          align: 'left',
         format:'ORDER004'}
      },{
        x: Date.UTC(2014, 10, 27),
        x2: Date.UTC(2014, 11, 1),
        y: 2,
        partialFill: 0,
        color: '#7FFFD4',
        dataLabels: {
          enabled: true,
          align: 'left',
         format:'ORDER005'}
      },{
        x: Date.UTC(2014, 11, 1),
        x2: Date.UTC(2014, 11, 5),
        y: 2,
        partialFill: 0,
        color: '#7FFFD4',
        dataLabels: {
          enabled: true,
          align: 'left',
         format:'ORDER006'}
      },{
        x: Date.UTC(2014, 11, 5),
        x2: Date.UTC(2014, 11, 7),
        y: 2,
        partialFill: 0,
        color: '#7FFFD4',
        dataLabels: {
          enabled: true,
          align: 'left',
         format:'ORDER007'}
      },{
        x: Date.UTC(2014, 11, 7),
        x2: Date.UTC(2014, 11, 9),
        y: 2,
        partialFill: 0,
        color: '#FF0000',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'ปรับปรุงเครื่องจักร'}
      },{
        x: Date.UTC(2014, 10, 25),
        x2: Date.UTC(2014, 10, 30),
        y: 3,
        partialFill: 0,
        color: '#7FFFD4',
        dataLabels: {
          enabled: true,
          align: 'left',
         format:'ORDER008'}
      },{
        x: Date.UTC(2014, 11, 2),
        x2: Date.UTC(2014, 11, 6),
        y: 3,
        partialFill: 0,
        color: '#7FFFD4',
        dataLabels: {
          enabled: true,
          align: 'left',
         format:'ORDER009'}
      },{
        x: Date.UTC(2014, 10, 25),
        x2: Date.UTC(2014, 10, 28),
        y: 4,
        partialFill: 0,
        color: '#7FFFD4',
        dataLabels: {
          enabled: true,
          align: 'left',
         format:'ORDER010'}
      },{
        x: Date.UTC(2014, 11, 1),
        x2: Date.UTC(2014, 11, 5),
        y: 4,
        partialFill: 0,
        color: '#7FFFD4',
        dataLabels: {
          enabled: true,
          align: 'left',
         format:'ORDER011'}
      },{
        x: Date.UTC(2014, 10, 25),
        x2: Date.UTC(2014, 10, 28),
        y: 5,
        partialFill: 0,
        color: '#7FFFD4',
        dataLabels: {
          enabled: true,
          align: 'left',
         format:'ORDER012'}
      },{
        x: Date.UTC(2014, 10, 28),
        x2: Date.UTC(2014, 11, 4),
        y: 5,
        partialFill: 0,
        color: '#7FFFD4',
        dataLabels: {
          enabled: true,
          align: 'left',
         format:'ORDER013'}
      },{
        x: Date.UTC(2014, 10, 25),
        x2: Date.UTC(2014, 10, 26),
        y: 6,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 26),
        x2: Date.UTC(2014, 10, 27),
        y: 6,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 27),
        x2: Date.UTC(2014, 10, 28),
        y: 6,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 28),
        x2: Date.UTC(2014, 10, 29),
        y: 6,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 29),
        x2: Date.UTC(2014, 10, 30),
        y: 6,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 30),
        x2: Date.UTC(2014, 10, 31),
        y: 6,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 1),
        x2: Date.UTC(2014, 11, 2),
        y: 6,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 2),
        x2: Date.UTC(2014, 11, 3),
        y: 6,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 3),
        x2: Date.UTC(2014, 11, 4),
        y: 6,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 4),
        x2: Date.UTC(2014, 11, 5),
        y: 6,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 5),
        x2: Date.UTC(2014, 11, 6),
        y: 6,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 6),
        x2: Date.UTC(2014, 11, 7),
        y: 6,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 7),
        x2: Date.UTC(2014, 11, 8),
        y: 6,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 8),
        x2: Date.UTC(2014, 11, 9),
        y: 6,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 25),
        x2: Date.UTC(2014, 10, 26),
        y: 7,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 26),
        x2: Date.UTC(2014, 10, 27),
        y: 7,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 27),
        x2: Date.UTC(2014, 10, 28),
        y: 7,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 28),
        x2: Date.UTC(2014, 10, 29),
        y: 7,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 29),
        x2: Date.UTC(2014, 10, 30),
        y: 7,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 30),
        x2: Date.UTC(2014, 10, 31),
        y: 7,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 1),
        x2: Date.UTC(2014, 11, 2),
        y: 7,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 2),
        x2: Date.UTC(2014, 11, 3),
        y: 7,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 3),
        x2: Date.UTC(2014, 11, 4),
        y: 7,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 4),
        x2: Date.UTC(2014, 11, 5),
        y: 7,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 5),
        x2: Date.UTC(2014, 11, 6),
        y: 7,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 6),
        x2: Date.UTC(2014, 11, 7),
        y: 7,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 7),
        x2: Date.UTC(2014, 11, 8),
        y: 7,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 8),
        x2: Date.UTC(2014, 11, 9),
        y: 7,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 25),
        x2: Date.UTC(2014, 10, 26),
        y: 8,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 26),
        x2: Date.UTC(2014, 10, 27),
        y: 8,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 27),
        x2: Date.UTC(2014, 10, 28),
        y: 8,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 28),
        x2: Date.UTC(2014, 10, 29),
        y: 8,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 29),
        x2: Date.UTC(2014, 10, 30),
        y: 8,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 30),
        x2: Date.UTC(2014, 10, 31),
        y: 8,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 1),
        x2: Date.UTC(2014, 11, 2),
        y: 8,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 2),
        x2: Date.UTC(2014, 11, 3),
        y: 8,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 3),
        x2: Date.UTC(2014, 11, 4),
        y: 8,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 4),
        x2: Date.UTC(2014, 11, 5),
        y: 8,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 5),
        x2: Date.UTC(2014, 11, 6),
        y: 8,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 6),
        x2: Date.UTC(2014, 11, 7),
        y: 8,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 7),
        x2: Date.UTC(2014, 11, 8),
        y: 8,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 8),
        x2: Date.UTC(2014, 11, 9),
        y: 8,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 25),
        x2: Date.UTC(2014, 10, 26),
        y: 10,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 26),
        x2: Date.UTC(2014, 10, 27),
        y: 10,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 27),
        x2: Date.UTC(2014, 10, 28),
        y: 10,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 28),
        x2: Date.UTC(2014, 10, 29),
        y: 10,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 29),
        x2: Date.UTC(2014, 10, 30),
        y: 10,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 30),
        x2: Date.UTC(2014, 10, 31),
        y: 10,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'99%'}
      },{
        x: Date.UTC(2014, 11, 1),
        x2: Date.UTC(2014, 11, 2),
        y: 10,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'98%'}
      },{
        x: Date.UTC(2014, 11, 2),
        x2: Date.UTC(2014, 11, 3),
        y: 10,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'97%'}
      },{
        x: Date.UTC(2014, 11, 3),
        x2: Date.UTC(2014, 11, 4),
        y: 10,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'96%'}
      },{
        x: Date.UTC(2014, 11, 4),
        x2: Date.UTC(2014, 11, 5),
        y: 10,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'95%'}
      },{
        x: Date.UTC(2014, 11, 5),
        x2: Date.UTC(2014, 11, 6),
        y: 10,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'94%'}
      },{
        x: Date.UTC(2014, 11, 6),
        x2: Date.UTC(2014, 11, 7),
        y: 10,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'93%'}
      },{
        x: Date.UTC(2014, 11, 7),
        x2: Date.UTC(2014, 11, 8),
        y: 10,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'92%'}
      },{
        x: Date.UTC(2014, 11, 8),
        x2: Date.UTC(2014, 11, 9),
        y: 10,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'91%'}
      }],
      dataLabels: {
        enabled: false
      }
    }]
  };

  return options;
}

function showGraphItem(){
  // $('#graph-1').highcharts(getChartOptions7());

  var chart = Highcharts.chart('graph-1', {
    chart: {
      type: 'xrange',
      events: {
        click: function () {
          showGraphList();
        }
      }
    },
    title: {
      text: null
    },  
  credits: {
      enabled: false
    }, exporting: {
      enabled: false
    },
    xAxis: {
      type: 'datetime'
    },
    yAxis: {
      title: {
        text: ''
      },
      categories: ['กลุ่มเครื่องทอ CN','เครื่องทอ A001','เครื่องทอ A002','เครื่องทอ A003','เครื่องทอ A004','เครื่องทอ A005', 'กลุ่มเครื่องทอ CAD 11.0  ','กลุ่มเครื่องทอ ED 10.0   ','กลุ่มเครื่องทอ FM1','','วงจรส่วนหลัง'],
      reversed: true
    },legend: {
      enabled: false
    },series: [{
      name: '',
      events: {
        click: function () {
          showGraphList();
        }
      },
      // pointPadding: 0,
      // groupPadding: 0,
      borderColor: 'gray',
      pointWidth: 20,
      data: [{
        x: Date.UTC(2014, 10, 25),
        x2: Date.UTC(2014, 10, 26),
        y: 0,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 26),
        x2: Date.UTC(2014, 10, 27),
        y: 0,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 27),
        x2: Date.UTC(2014, 10, 28),
        y: 0,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 28),
        x2: Date.UTC(2014, 10, 29),
        y: 0,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'95%'}
      },{
        x: Date.UTC(2014, 10, 29),
        x2: Date.UTC(2014, 10, 30),
        y: 0,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'85%'}
      },{
        x: Date.UTC(2014, 10, 30),
        x2: Date.UTC(2014, 10, 31),
        y: 0,
        partialFill: 0,
        color: '#D2B48C',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'75%'}
      },{
        x: Date.UTC(2014, 11, 1),
        x2: Date.UTC(2014, 11, 2),
        y: 0,
        partialFill: 0,
        color: '#D2B48C',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'65%'}
      },{
        x: Date.UTC(2014, 11, 2),
        x2: Date.UTC(2014, 11, 3),
        y: 0,
        partialFill: 0,
        color: '#F9967A',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'55%'}
      },{
        x: Date.UTC(2014, 11, 3),
        x2: Date.UTC(2014, 11, 4),
        y: 0,
        partialFill: 0,
        color: '#F9967A',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'45%'}
      },{
        x: Date.UTC(2014, 11, 4),
        x2: Date.UTC(2014, 11, 5),
        y: 0,
        partialFill: 0,
        color: '#FF8C00',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'35%'}
      },{
        x: Date.UTC(2014, 11, 5),
        x2: Date.UTC(2014, 11, 6),
        y: 0,
        partialFill: 0,
        color: '#FF8C00',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'25%'}
      },{
        x: Date.UTC(2014, 11, 6),
        x2: Date.UTC(2014, 11, 7),
        y: 0,
        partialFill: 0,
        color: '#FF4500',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'15%'}
      },{
        x: Date.UTC(2014, 11, 7),
        x2: Date.UTC(2014, 11, 8),
        y: 0,
        partialFill: 0,
        color: '#FF4500',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'5%'}
      },{
        x: Date.UTC(2014, 11, 8),
        x2: Date.UTC(2014, 11, 9),
        y: 0,
        partialFill: 0,
        color: '#FF4500',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'0%'}
      },{
        x: Date.UTC(2014, 10, 25),
        x2: Date.UTC(2014, 10, 28),
        y: 1,
        partialFill: 0,
        color: '#7FFFD4',
        dataLabels: {
          enabled: true,
          align: 'left',
         format:'ORDER001'}
      },{
        x: Date.UTC(2014, 10, 28),
        x2: Date.UTC(2014, 11, 3),
        y: 1,
        partialFill: 0,
        color: '#7FFFD4',
        dataLabels: {
          enabled: true,
          align: 'left',
         format:'ORDER002'}
      },{
        x: Date.UTC(2014, 11, 3),
        x2: Date.UTC(2014, 11, 8),
        y: 1,
        partialFill: 0,
        color: '#7FFFD4',
        dataLabels: {
          enabled: true,
          align: 'left',
         format:'ORDER003'}
      },{
        x: Date.UTC(2014, 10, 25),
        x2: Date.UTC(2014, 10, 27),
        y: 2,
        partialFill: 0,
        color: '#7FFFD4',
        dataLabels: {
          enabled: true,
          align: 'left',
         format:'ORDER004'}
      },{
        x: Date.UTC(2014, 10, 27),
        x2: Date.UTC(2014, 11, 1),
        y: 2,
        partialFill: 0,
        color: '#7FFFD4',
        dataLabels: {
          enabled: true,
          align: 'left',
         format:'ORDER005'}
      },{
        x: Date.UTC(2014, 11, 1),
        x2: Date.UTC(2014, 11, 5),
        y: 2,
        partialFill: 0,
        color: '#7FFFD4',
        dataLabels: {
          enabled: true,
          align: 'left',
         format:'ORDER006'}
      },{
        x: Date.UTC(2014, 11, 5),
        x2: Date.UTC(2014, 11, 7),
        y: 2,
        partialFill: 0,
        color: '#7FFFD4',
        dataLabels: {
          enabled: true,
          align: 'left',
         format:'ORDER007'}
      },{
        x: Date.UTC(2014, 11, 7),
        x2: Date.UTC(2014, 11, 9),
        y: 2,
        partialFill: 0,
        color: '#FF0000',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'ปรับปรุงเครื่องจักร'}
      },{
        x: Date.UTC(2014, 10, 25),
        x2: Date.UTC(2014, 10, 30),
        y: 3,
        partialFill: 0,
        color: '#7FFFD4',
        dataLabels: {
          enabled: true,
          align: 'left',
         format:'ORDER008'}
      },{
        x: Date.UTC(2014, 11, 2),
        x2: Date.UTC(2014, 11, 6),
        y: 3,
        partialFill: 0,
        color: '#7FFFD4',
        dataLabels: {
          enabled: true,
          align: 'left',
         format:'ORDER009'}
      },{
        x: Date.UTC(2014, 10, 25),
        x2: Date.UTC(2014, 10, 28),
        y: 4,
        partialFill: 0,
        color: '#7FFFD4',
        dataLabels: {
          enabled: true,
          align: 'left',
         format:'ORDER010'}
      },{
        x: Date.UTC(2014, 11, 1),
        x2: Date.UTC(2014, 11, 5),
        y: 4,
        partialFill: 0,
        color: '#7FFFD4',
        dataLabels: {
          enabled: true,
          align: 'left',
         format:'ORDER011'}
      },{
        x: Date.UTC(2014, 10, 25),
        x2: Date.UTC(2014, 10, 28),
        y: 5,
        partialFill: 0,
        color: '#7FFFD4',
        dataLabels: {
          enabled: true,
          align: 'left',
         format:'ORDER012'}
      },{
        x: Date.UTC(2014, 10, 28),
        x2: Date.UTC(2014, 11, 4),
        y: 5,
        partialFill: 0,
        color: '#7FFFD4',
        dataLabels: {
          enabled: true,
          align: 'left',
         format:'ORDER013'}
      },{
        x: Date.UTC(2014, 10, 25),
        x2: Date.UTC(2014, 10, 26),
        y: 6,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 26),
        x2: Date.UTC(2014, 10, 27),
        y: 6,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 27),
        x2: Date.UTC(2014, 10, 28),
        y: 6,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 28),
        x2: Date.UTC(2014, 10, 29),
        y: 6,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 29),
        x2: Date.UTC(2014, 10, 30),
        y: 6,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 30),
        x2: Date.UTC(2014, 10, 31),
        y: 6,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 1),
        x2: Date.UTC(2014, 11, 2),
        y: 6,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 2),
        x2: Date.UTC(2014, 11, 3),
        y: 6,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 3),
        x2: Date.UTC(2014, 11, 4),
        y: 6,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 4),
        x2: Date.UTC(2014, 11, 5),
        y: 6,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 5),
        x2: Date.UTC(2014, 11, 6),
        y: 6,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 6),
        x2: Date.UTC(2014, 11, 7),
        y: 6,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 7),
        x2: Date.UTC(2014, 11, 8),
        y: 6,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 8),
        x2: Date.UTC(2014, 11, 9),
        y: 6,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 25),
        x2: Date.UTC(2014, 10, 26),
        y: 7,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 26),
        x2: Date.UTC(2014, 10, 27),
        y: 7,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 27),
        x2: Date.UTC(2014, 10, 28),
        y: 7,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 28),
        x2: Date.UTC(2014, 10, 29),
        y: 7,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 29),
        x2: Date.UTC(2014, 10, 30),
        y: 7,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 30),
        x2: Date.UTC(2014, 10, 31),
        y: 7,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 1),
        x2: Date.UTC(2014, 11, 2),
        y: 7,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 2),
        x2: Date.UTC(2014, 11, 3),
        y: 7,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 3),
        x2: Date.UTC(2014, 11, 4),
        y: 7,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 4),
        x2: Date.UTC(2014, 11, 5),
        y: 7,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 5),
        x2: Date.UTC(2014, 11, 6),
        y: 7,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 6),
        x2: Date.UTC(2014, 11, 7),
        y: 7,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 7),
        x2: Date.UTC(2014, 11, 8),
        y: 7,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 8),
        x2: Date.UTC(2014, 11, 9),
        y: 7,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 25),
        x2: Date.UTC(2014, 10, 26),
        y: 8,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 26),
        x2: Date.UTC(2014, 10, 27),
        y: 8,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 27),
        x2: Date.UTC(2014, 10, 28),
        y: 8,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 28),
        x2: Date.UTC(2014, 10, 29),
        y: 8,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 29),
        x2: Date.UTC(2014, 10, 30),
        y: 8,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 30),
        x2: Date.UTC(2014, 10, 31),
        y: 8,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 1),
        x2: Date.UTC(2014, 11, 2),
        y: 8,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 2),
        x2: Date.UTC(2014, 11, 3),
        y: 8,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 3),
        x2: Date.UTC(2014, 11, 4),
        y: 8,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 4),
        x2: Date.UTC(2014, 11, 5),
        y: 8,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 5),
        x2: Date.UTC(2014, 11, 6),
        y: 8,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 6),
        x2: Date.UTC(2014, 11, 7),
        y: 8,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 7),
        x2: Date.UTC(2014, 11, 8),
        y: 8,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 11, 8),
        x2: Date.UTC(2014, 11, 9),
        y: 8,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 25),
        x2: Date.UTC(2014, 10, 26),
        y: 10,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 26),
        x2: Date.UTC(2014, 10, 27),
        y: 10,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 27),
        x2: Date.UTC(2014, 10, 28),
        y: 10,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 28),
        x2: Date.UTC(2014, 10, 29),
        y: 10,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 29),
        x2: Date.UTC(2014, 10, 30),
        y: 10,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'100%'}
      },{
        x: Date.UTC(2014, 10, 30),
        x2: Date.UTC(2014, 10, 31),
        y: 10,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'99%'}
      },{
        x: Date.UTC(2014, 11, 1),
        x2: Date.UTC(2014, 11, 2),
        y: 10,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'98%'}
      },{
        x: Date.UTC(2014, 11, 2),
        x2: Date.UTC(2014, 11, 3),
        y: 10,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'97%'}
      },{
        x: Date.UTC(2014, 11, 3),
        x2: Date.UTC(2014, 11, 4),
        y: 10,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'96%'}
      },{
        x: Date.UTC(2014, 11, 4),
        x2: Date.UTC(2014, 11, 5),
        y: 10,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'95%'}
      },{
        x: Date.UTC(2014, 11, 5),
        x2: Date.UTC(2014, 11, 6),
        y: 10,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'94%'}
      },{
        x: Date.UTC(2014, 11, 6),
        x2: Date.UTC(2014, 11, 7),
        y: 10,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'93%'}
      },{
        x: Date.UTC(2014, 11, 7),
        x2: Date.UTC(2014, 11, 8),
        y: 10,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'92%'}
      },{
        x: Date.UTC(2014, 11, 8),
        x2: Date.UTC(2014, 11, 9),
        y: 10,
        partialFill: 0,
        color: '#32CD32',
        dataLabels: {
          enabled: true,
          align: 'center',
         format:'91%'}
      }],
      dataLabels: {
        enabled: false
      }
    }]
  });

  chart.yAxis[0].labelGroup.element.childNodes.forEach(function(label)
  {
    label.style.cursor = "pointer";
    label.onclick = function(){
      showGraphList()
    }
  });

}

function showGraphList(){
  // $('#graph-1').highcharts(getChartOptions6());

  var chart = Highcharts.chart('graph-1', {
    chart: {
type: 'xrange',
events: {
click: function () {
  showGraphItem();
}
}
},
title: {
text: null
},  
credits: {
enabled: false
}, exporting: {
enabled: false
},
xAxis: {
type: 'datetime'
},
yAxis: {
title: {
text: ''
},
categories: ['กลุ่มเครื่องทอ CN', 'กลุ่มเครื่องทอ CAD 11.0  ','กลุ่มเครื่องทอ ED 10.0','กลุ่มเครื่องทอ FM1','','วงจรส่วนหลัง'],
reversed: true
},legend: {
enabled: false
},series: [{
name: '',
events: {
click: function () {
  showGraphItem();
}
},
// pointPadding: 0,
// groupPadding: 0,
borderColor: 'gray',
pointWidth: 20,
data: [{
x: Date.UTC(2014, 10, 25),
x2: Date.UTC(2014, 10, 26),
y: 0,
partialFill: 0,
color: '#32CD32',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'100%'}
},{
x: Date.UTC(2014, 10, 26),
x2: Date.UTC(2014, 10, 27),
y: 0,
partialFill: 0,
color: '#32CD32',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'100%'}
},{
x: Date.UTC(2014, 10, 27),
x2: Date.UTC(2014, 10, 28),
y: 0,
partialFill: 0,
color: '#32CD32',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'100%'}
},{
x: Date.UTC(2014, 10, 28),
x2: Date.UTC(2014, 10, 29),
y: 0,
partialFill: 0,
color: '#32CD32',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'95%'}
},{
x: Date.UTC(2014, 10, 29),
x2: Date.UTC(2014, 10, 30),
y: 0,
partialFill: 0,
color: '#32CD32',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'85%'}
},{
x: Date.UTC(2014, 10, 30),
x2: Date.UTC(2014, 10, 31),
y: 0,
partialFill: 0,
color: '#D2B48C',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'75%'}
},{
x: Date.UTC(2014, 11, 1),
x2: Date.UTC(2014, 11, 2),
y: 0,
partialFill: 0,
color: '#D2B48C',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'65%'}
},{
x: Date.UTC(2014, 11, 2),
x2: Date.UTC(2014, 11, 3),
y: 0,
partialFill: 0,
color: '#F9967A',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'55%'}
},{
x: Date.UTC(2014, 11, 3),
x2: Date.UTC(2014, 11, 4),
y: 0,
partialFill: 0,
color: '#F9967A',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'45%'}
},{
x: Date.UTC(2014, 11, 4),
x2: Date.UTC(2014, 11, 5),
y: 0,
partialFill: 0,
color: '#FF8C00',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'35%'}
},{
x: Date.UTC(2014, 11, 5),
x2: Date.UTC(2014, 11, 6),
y: 0,
partialFill: 0,
color: '#FF8C00',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'25%'}
},{
x: Date.UTC(2014, 11, 6),
x2: Date.UTC(2014, 11, 7),
y: 0,
partialFill: 0,
color: '#FF4500',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'15%'}
},{
x: Date.UTC(2014, 11, 7),
x2: Date.UTC(2014, 11, 8),
y: 0,
partialFill: 0,
color: '#FF4500',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'5%'}
},{
x: Date.UTC(2014, 11, 8),
x2: Date.UTC(2014, 11, 9),
y: 0,
partialFill: 0,
color: '#FF4500',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'0%'}
},{
x: Date.UTC(2014, 10, 25),
x2: Date.UTC(2014, 10, 26),
y: 1,
partialFill: 0,
color: '#32CD32',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'100%'}
},{
x: Date.UTC(2014, 10, 26),
x2: Date.UTC(2014, 10, 27),
y: 1,
partialFill: 0,
color: '#32CD32',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'100%'}
},{
x: Date.UTC(2014, 10, 27),
x2: Date.UTC(2014, 10, 28),
y: 1,
partialFill: 0,
color: '#32CD32',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'100%'}
},{
x: Date.UTC(2014, 10, 28),
x2: Date.UTC(2014, 10, 29),
y: 1,
partialFill: 0,
color: '#32CD32',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'100%'}
},{
x: Date.UTC(2014, 10, 29),
x2: Date.UTC(2014, 10, 30),
y: 1,
partialFill: 0,
color: '#32CD32',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'100%'}
},{
x: Date.UTC(2014, 10, 30),
x2: Date.UTC(2014, 10, 31),
y: 1,
partialFill: 0,
color: '#32CD32',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'100%'}
},{
x: Date.UTC(2014, 11, 1),
x2: Date.UTC(2014, 11, 2),
y: 1,
partialFill: 0,
color: '#32CD32',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'100%'}
},{
x: Date.UTC(2014, 11, 2),
x2: Date.UTC(2014, 11, 3),
y: 1,
partialFill: 0,
color: '#32CD32',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'100%'}
},{
x: Date.UTC(2014, 11, 3),
x2: Date.UTC(2014, 11, 4),
y: 1,
partialFill: 0,
color: '#32CD32',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'100%'}
},{
x: Date.UTC(2014, 11, 4),
x2: Date.UTC(2014, 11, 5),
y: 1,
partialFill: 0,
color: '#32CD32',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'100%'}
},{
x: Date.UTC(2014, 11, 5),
x2: Date.UTC(2014, 11, 6),
y: 1,
partialFill: 0,
color: '#32CD32',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'100%'}
},{
x: Date.UTC(2014, 11, 6),
x2: Date.UTC(2014, 11, 7),
y: 1,
partialFill: 0,
color: '#32CD32',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'100%'}
},{
x: Date.UTC(2014, 11, 7),
x2: Date.UTC(2014, 11, 8),
y: 1,
partialFill: 0,
color: '#32CD32',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'100%'}
},{
x: Date.UTC(2014, 11, 8),
x2: Date.UTC(2014, 11, 9),
y: 1,
partialFill: 0,
color: '#32CD32',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'100%'}
},{
x: Date.UTC(2014, 10, 25),
x2: Date.UTC(2014, 10, 26),
y: 2,
partialFill: 0,
color: '#32CD32',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'100%'}
},{
x: Date.UTC(2014, 10, 26),
x2: Date.UTC(2014, 10, 27),
y: 2,
partialFill: 0,
color: '#32CD32',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'100%'}
},{
x: Date.UTC(2014, 10, 27),
x2: Date.UTC(2014, 10, 28),
y: 2,
partialFill: 0,
color: '#32CD32',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'100%'}
},{
x: Date.UTC(2014, 10, 28),
x2: Date.UTC(2014, 10, 29),
y: 2,
partialFill: 0,
color: '#32CD32',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'100%'}
},{
x: Date.UTC(2014, 10, 29),
x2: Date.UTC(2014, 10, 30),
y: 2,
partialFill: 0,
color: '#32CD32',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'100%'}
},{
x: Date.UTC(2014, 10, 30),
x2: Date.UTC(2014, 10, 31),
y: 2,
partialFill: 0,
color: '#32CD32',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'100%'}
},{
x: Date.UTC(2014, 11, 1),
x2: Date.UTC(2014, 11, 2),
y: 2,
partialFill: 0,
color: '#32CD32',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'100%'}
},{
x: Date.UTC(2014, 11, 2),
x2: Date.UTC(2014, 11, 3),
y: 2,
partialFill: 0,
color: '#32CD32',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'100%'}
},{
x: Date.UTC(2014, 11, 3),
x2: Date.UTC(2014, 11, 4),
y: 2,
partialFill: 0,
color: '#32CD32',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'100%'}
},{
x: Date.UTC(2014, 11, 4),
x2: Date.UTC(2014, 11, 5),
y: 2,
partialFill: 0,
color: '#32CD32',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'100%'}
},{
x: Date.UTC(2014, 11, 5),
x2: Date.UTC(2014, 11, 6),
y: 2,
partialFill: 0,
color: '#32CD32',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'100%'}
},{
x: Date.UTC(2014, 11, 6),
x2: Date.UTC(2014, 11, 7),
y: 2,
partialFill: 0,
color: '#32CD32',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'100%'}
},{
x: Date.UTC(2014, 11, 7),
x2: Date.UTC(2014, 11, 8),
y: 2,
partialFill: 0,
color: '#32CD32',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'100%'}
},{
x: Date.UTC(2014, 11, 8),
x2: Date.UTC(2014, 11, 9),
y: 2,
partialFill: 0,
color: '#32CD32',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'100%'}
},{
x: Date.UTC(2014, 10, 25),
x2: Date.UTC(2014, 10, 26),
y: 3,
partialFill: 0,
color: '#32CD32',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'100%'}
},{
x: Date.UTC(2014, 10, 26),
x2: Date.UTC(2014, 10, 27),
y: 3,
partialFill: 0,
color: '#32CD32',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'100%'}
},{
x: Date.UTC(2014, 10, 27),
x2: Date.UTC(2014, 10, 28),
y: 3,
partialFill: 0,
color: '#32CD32',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'100%'}
},{
x: Date.UTC(2014, 10, 28),
x2: Date.UTC(2014, 10, 29),
y: 3,
partialFill: 0,
color: '#32CD32',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'100%'}
},{
x: Date.UTC(2014, 10, 29),
x2: Date.UTC(2014, 10, 30),
y: 3,
partialFill: 0,
color: '#32CD32',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'100%'}
},{
x: Date.UTC(2014, 10, 30),
x2: Date.UTC(2014, 10, 31),
y: 3,
partialFill: 0,
color: '#32CD32',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'100%'}
},{
x: Date.UTC(2014, 11, 1),
x2: Date.UTC(2014, 11, 2),
y: 3,
partialFill: 0,
color: '#32CD32',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'100%'}
},{
x: Date.UTC(2014, 11, 2),
x2: Date.UTC(2014, 11, 3),
y: 3,
partialFill: 0,
color: '#32CD32',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'100%'}
},{
x: Date.UTC(2014, 11, 3),
x2: Date.UTC(2014, 11, 4),
y: 3,
partialFill: 0,
color: '#32CD32',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'100%'}
},{
x: Date.UTC(2014, 11, 4),
x2: Date.UTC(2014, 11, 5),
y: 3,
partialFill: 0,
color: '#32CD32',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'100%'}
},{
x: Date.UTC(2014, 11, 5),
x2: Date.UTC(2014, 11, 6),
y: 3,
partialFill: 0,
color: '#32CD32',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'100%'}
},{
x: Date.UTC(2014, 11, 6),
x2: Date.UTC(2014, 11, 7),
y: 3,
partialFill: 0,
color: '#32CD32',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'100%'}
},{
x: Date.UTC(2014, 11, 7),
x2: Date.UTC(2014, 11, 8),
y: 3,
partialFill: 0,
color: '#32CD32',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'100%'}
},{
x: Date.UTC(2014, 11, 8),
x2: Date.UTC(2014, 11, 9),
y: 3,
partialFill: 0,
color: '#32CD32',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'100%'}
},{
x: Date.UTC(2014, 10, 25),
x2: Date.UTC(2014, 10, 26),
y: 5,
partialFill: 0,
color: '#32CD32',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'100%'}
},{
x: Date.UTC(2014, 10, 26),
x2: Date.UTC(2014, 10, 27),
y: 5,
partialFill: 0,
color: '#32CD32',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'100%'}
},{
x: Date.UTC(2014, 10, 27),
x2: Date.UTC(2014, 10, 28),
y: 5,
partialFill: 0,
color: '#32CD32',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'100%'}
},{
x: Date.UTC(2014, 10, 28),
x2: Date.UTC(2014, 10, 29),
y: 5,
partialFill: 0,
color: '#32CD32',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'100%'}
},{
x: Date.UTC(2014, 10, 29),
x2: Date.UTC(2014, 10, 30),
y: 5,
partialFill: 0,
color: '#32CD32',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'100%'}
},{
x: Date.UTC(2014, 10, 30),
x2: Date.UTC(2014, 10, 31),
y: 5,
partialFill: 0,
color: '#32CD32',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'99%'}
},{
x: Date.UTC(2014, 11, 1),
x2: Date.UTC(2014, 11, 2),
y: 5,
partialFill: 0,
color: '#32CD32',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'98%'}
},{
x: Date.UTC(2014, 11, 2),
x2: Date.UTC(2014, 11, 3),
y: 5,
partialFill: 0,
color: '#32CD32',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'97%'}
},{
x: Date.UTC(2014, 11, 3),
x2: Date.UTC(2014, 11, 4),
y: 5,
partialFill: 0,
color: '#32CD32',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'96%'}
},{
x: Date.UTC(2014, 11, 4),
x2: Date.UTC(2014, 11, 5),
y: 5,
partialFill: 0,
color: '#32CD32',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'95%'}
},{
x: Date.UTC(2014, 11, 5),
x2: Date.UTC(2014, 11, 6),
y: 5,
partialFill: 0,
color: '#32CD32',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'94%'}
},{
x: Date.UTC(2014, 11, 6),
x2: Date.UTC(2014, 11, 7),
y: 5,
partialFill: 0,
color: '#32CD32',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'93%'}
},{
x: Date.UTC(2014, 11, 7),
x2: Date.UTC(2014, 11, 8),
y: 5,
partialFill: 0,
color: '#32CD32',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'92%'}
},{
x: Date.UTC(2014, 11, 8),
x2: Date.UTC(2014, 11, 9),
y: 5,
partialFill: 0,
color: '#32CD32',
dataLabels: {
  enabled: true,
  align: 'center',
 format:'91%'}
}],
dataLabels: {
enabled: false
}
}]
});

chart.yAxis[0].labelGroup.element.childNodes.forEach(function(label)
{
label.style.cursor = "pointer";
label.onclick = function(){
  showGraphItem();
}
});

}