'use strict';

(function($) {
    'use strict';

    function datatablesFull(){
        var datatablesButtons = [];
        var elementName = '.datatables-custom-full';
        var element = $(elementName);

        $.each( element, function( i, val ) {
            // search
            var DataTable = $.fn.dataTable;
            $.extend(true, DataTable.ext.classes, {
                sWrapper: "dataTables_wrapper dt-bootstrap"
            });
        
            $(val).find('tfoot th').each(function () {
                var title = $(this).text();
                if(!$(this).hasClass('no-sort')){
                    $(this).html('<input class="form-control input-sm" type="text" placeholder="' + title + '" />');
                }else{
                    $(this).html('<input class="form-control input-sm hide" type="text" placeholder="' + title + '" />');
                }
            });

            var $dtb = $(val).DataTable({
                dom: "<'row'<'col-sm-6'i><'col-sm-6'f>>" + "<'table-responsive'tr>" + "<'row'<'col-sm-6'l><'col-sm-6'p>>",
                buttons: [
                    {
                        extend: 'colvis',
                        text: 'กรอง',
                        className: 'btn-outline-primary btn-extra'
                    }
                ],
                lengthChange: true,
                colReorder: true,
                fixedHeader: false,
                info:false,
                columnDefs: [ {
                    targets: 'no-sort',
                    orderable: false,
                },
                {
                    targets: 1,
                    orderable: true  
                }],
                order: [[ 1, 'desc' ]],
                // ordering:  true,
                pageLength: 50,
                // scrollY: 400,
                // scrollCollapse: true,
                // scroller: true,
                language: {
                    // decimal:        "",
                    // emptyTable:     "No data available in table",
                    info:           "แสดง _START_ ถึง _END_ ของ _TOTAL_ แถว",
                    // infoEmpty:      "Showing 0 to 0 of 0 entries",
                    // infoFiltered:   "(filtered from _MAX_ total entries)",
                    // infoPostFix:    "",
                    // thousands:      ",",
                    lengthMenu:     "แสดง _MENU_ แถว",
                    // loadingRecords: "Loading...",
                    // processing:     "Processing...",
                    // search:         "Search:",
                    // zeroRecords:    "No matching records found",,
                    // aria: {
                    //     "sortAscending":  ": activate to sort column ascending",
                    //     "sortDescending": ": activate to sort column descending"
                    // },
                    paginate: {
                        previous: '&laquo;',
                        next: '&raquo;'
                    },
                    search: "_INPUT_",
                    searchPlaceholder: "ค้นหา…"
                },
            });
            // datatablesButtons.push($dtb);
            // $dtb.buttons().container().appendTo('#DataTables_Table_'+i+'_wrapper .col-sm-6:eq(0)');

            // Apply the search
            $dtb.columns().every(function () {
                var that = this;
        
                $('input', this.footer()).on('keyup change', function () {
                if (that.search() !== this.value) {
                    that.search(this.value).draw();
                }
                });
            });

        });
        
        // var elementName = '.datatables-custom-full';
        // var element2 = $($(elementName)[0]);

        // var DataTable = $.fn.dataTable;
        //     $.extend(true, DataTable.ext.classes, {
        //         sWrapper: "dataTables_wrapper dt-bootstrap"
        //     });
        
        //     $(elementName+' tfoot th').each(function () {
        //         var title = $(this).text();
        //         $(this).html('<input class="form-control input-sm" type="text" placeholder="' + title + '" />');
        //     });

        // var $datatables = element2.DataTable({
        //     dom: "<'row'<'col-sm-12'i>>" + "<'table-responsive'tr>" + "<'row'<'col-sm-6'l><'col-sm-6'p>>",
        //     language: {
        //     paginate: {
        //         previous: '&laquo;',
        //         next: '&raquo;'
        //     },
        //     search: "_INPUT_",
        //     searchPlaceholder: "Search…"
        //     },
        //     order: [[2, "desc"]]
        // });
    
        // // Apply the search
        // $datatables.columns().every(function () {
        //     var that = this;
    
        //     $('input', this.footer()).on('keyup change', function () {
        //         console.log('ssssssssssss');
        //     if (that.search() !== this.value) {
        //         that.search(this.value).draw();
        //     }
        //     });
        // });
    }

    function datatablesIndividualColumnSearchingExample1() {
        var $datatables = $('#demo-datatables-3-1');
        if ($datatables.length) {
            var DataTable = $.fn.dataTable;
            $.extend(true, DataTable.ext.classes, {
                sWrapper: "dataTables_wrapper dt-bootstrap"
            });
        
            $('#demo-datatables-3-1 tfoot th').each(function () {
                var title = $(this).text();
                $(this).html('<input class="form-control input-sm" type="text" placeholder="' + title + '" />');
            });
        
            // DataTable
            var $datatables = $datatables.DataTable({
                dom: "<'row'<'col-sm-12'i>>" + "<'table-responsive'tr>" + "<'row'<'col-sm-6'l><'col-sm-6'p>>",
                language: {
                paginate: {
                    previous: '&laquo;',
                    next: '&raquo;'
                },
                search: "_INPUT_",
                searchPlaceholder: "Search…"
                },
                order: [[2, "desc"]]
            });
        
            // Apply the search
            $datatables.columns().every(function () {
                var that = this;
        
                $('input', this.footer()).on('keyup change', function () {
                if (that.search() !== this.value) {
                    that.search(this.value).draw();
                }
                });
            });
        }
    }

    function datatablesComplexHeaderExampleCustom() {

        var $datatables = $('#datatables-custom');
        $datatables.DataTable({
            dom: "<'row'<'col-sm-6'i><'col-sm-6'f>>" + "<'table-responsive'tr>" + "<'row'<'col-sm-6'l><'col-sm-6'p>>",
            ordering:  false,
            pageLength: 25,
            // scrollY: 400,
            // scrollCollapse: true,
            // scroller: true,
            language: {
                // decimal:        "",
                // emptyTable:     "No data available in table",
                info:           "แสดง _START_ ถึง _END_ ของ _TOTAL_ แถว",
                // infoEmpty:      "Showing 0 to 0 of 0 entries",
                // infoFiltered:   "(filtered from _MAX_ total entries)",
                // infoPostFix:    "",
                // thousands:      ",",
                // lengthMenu:     "Show _MENU_ entries",
                // loadingRecords: "Loading...",
                // processing:     "Processing...",
                // search:         "Search:",
                // zeroRecords:    "No matching records found",,
                // aria: {
                //     "sortAscending":  ": activate to sort column ascending",
                //     "sortDescending": ": activate to sort column descending"
                // },
                paginate: {
                    previous: '&laquo;',
                    next: '&raquo;'
                },
                search: "_INPUT_",
                searchPlaceholder: "ค้นหา…"
            },
            // order: [
            //     [1, "desc"]
            // ]
        });

        var $datatables2 = $('#datatables-base1');
        $datatables2.DataTable({
            dom: "<'row'<'col-sm-6'i><'col-sm-6'f>>" + "<'table-responsive'tr>" + "<'row'<'col-sm-6'l><'col-sm-6'p>>",
            ordering:  true,
            pageLength: 25,
            // scrollY: 400,
            // scrollCollapse: true,
            // scroller: true,
            language: {
                // decimal:        "",
                // emptyTable:     "No data available in table",
                info:           "แสดง _START_ ถึง _END_ ของ _TOTAL_ แถว",
                // infoEmpty:      "Showing 0 to 0 of 0 entries",
                // infoFiltered:   "(filtered from _MAX_ total entries)",
                // infoPostFix:    "",
                // thousands:      ",",
                // lengthMenu:     "Show _MENU_ entries",
                // loadingRecords: "Loading...",
                // processing:     "Processing...",
                // search:         "Search:",
                // zeroRecords:    "No matching records found",,
                // aria: {
                //     "sortAscending":  ": activate to sort column ascending",
                //     "sortDescending": ": activate to sort column descending"
                // },
                paginate: {
                    previous: '&laquo;',
                    next: '&raquo;'
                },
                search: "_INPUT_",
                searchPlaceholder: "ค้นหา…"
            },
            // order: [
            //     [1, "desc"]
            // ]
        });

        var $datatables3 = $('#datatables-base2');
        $datatables3.DataTable({
            dom: "<'row'<'col-sm-6'i><'col-sm-6'f>>" + "<'table-responsive'tr>" + "<'row'<'col-sm-6'l><'col-sm-6'p>>",
            ordering:  true,
            pageLength: 25,
            // scrollY: 400,
            // scrollCollapse: true,
            // scroller: true,
            language: {
                // decimal:        "",
                // emptyTable:     "No data available in table",
                info:           "แสดง _START_ ถึง _END_ ของ _TOTAL_ แถว",
                // infoEmpty:      "Showing 0 to 0 of 0 entries",
                // infoFiltered:   "(filtered from _MAX_ total entries)",
                // infoPostFix:    "",
                // thousands:      ",",
                // lengthMenu:     "Show _MENU_ entries",
                // loadingRecords: "Loading...",
                // processing:     "Processing...",
                // search:         "Search:",
                // zeroRecords:    "No matching records found",,
                // aria: {
                //     "sortAscending":  ": activate to sort column ascending",
                //     "sortDescending": ": activate to sort column descending"
                // },
                paginate: {
                    previous: '&laquo;',
                    next: '&raquo;'
                },
                search: "_INPUT_",
                searchPlaceholder: "ค้นหา…"
            },
            // order: [
            //     [1, "desc"]
            // ]
        });

        var $datatables3 = $('#datatables-base-no-sort');
        $datatables3.DataTable({
            dom: "<'row'<'col-sm-6'i><'col-sm-6'f>>" + "<'table-responsive'tr>" + "<'row'<'col-sm-6'l><'col-sm-6'p>>",
            ordering:  false,
            pageLength: 25,
            // scrollY: 400,
            // scrollCollapse: true,
            // scroller: true,
            language: {
                // decimal:        "",
                // emptyTable:     "No data available in table",
                info:           "แสดง _START_ ถึง _END_ ของ _TOTAL_ แถว",
                // infoEmpty:      "Showing 0 to 0 of 0 entries",
                // infoFiltered:   "(filtered from _MAX_ total entries)",
                // infoPostFix:    "",
                // thousands:      ",",
                // lengthMenu:     "Show _MENU_ entries",
                // loadingRecords: "Loading...",
                // processing:     "Processing...",
                // search:         "Search:",
                // zeroRecords:    "No matching records found",,
                // aria: {
                //     "sortAscending":  ": activate to sort column ascending",
                //     "sortDescending": ": activate to sort column descending"
                // },
                paginate: {
                    previous: '&laquo;',
                    next: '&raquo;'
                },
                search: "_INPUT_",
                searchPlaceholder: "ค้นหา…"
            },
            // order: [
            //     [1, "desc"]
            // ]
        });
    }

    datatablesFull();
    datatablesComplexHeaderExampleCustom();
    datatablesIndividualColumnSearchingExample1();

})(jQuery);

