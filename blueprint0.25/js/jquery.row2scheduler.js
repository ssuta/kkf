(function ($) {
    $.fn.row2scheduler = function (options) {
        const settings = $.extend({
            scheduler: '#scheduler',
            rows: 'tr',
            drop: function(rowData, scheduler)
        }, options);
        return this.each(function () {
            // Reference to this table.
            const $table = this;

            // Create tbody tag
            let $tbody = $('tbody', $table);
            if ($tbody.length == 0) {
                $tbody = $('tbody').appendTo($table);
            }

            // Create drag start event for table rows.
            let $rows = $(settings.rows, $tbody);
            $rows.on('dragstart', function (ev) {
                const $this = $(this);
                $this.prop('draggable', true);
                ev.dataTransfer.setData('rowData', {
                    name: $this.data('name'),
                    start: $this.data('start'),
                    end: $this.data('end'),
                    element: $this
                });
            });

            $(settings.scheduler)
                .on('dragover', function (ev) {
                    ev.preventDefault();
                    $(this).addClass('on-drag-over');
                })
                .on('mouseleave', function (ev) {
                    ev.preventDefault();
                    $(this).removeClass('on-drag-over');
                })
                .on('drop', function (ev) {
                    ev.preventDefault();
                    $(this).removeClass('on-drag-over');
                    const rowData = ev.dataTransfer.getData('rowData');
                    if(settings.drop && typeof(settings.drop) == 'function') {
                        settings.drop(rowData, scheduler);
                    }
                });
        });
    };
}(jQuery));