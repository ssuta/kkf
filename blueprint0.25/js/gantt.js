const schedulerId = 'scheduler';
const tableId = 'planing-table';

const ganttData = {
    data: [{
            "id": 1,
            "text": "Machine 1",
            "unscheduled": true,
            "open": true
        },
        {
            "id": 2,
            "text": "Machine 2",
            "unscheduled": true,
            "open": true
        },
        {
            "id": 3,
            "text": "Machine 3",
            "unscheduled": true,
            "open": true
        },
        {
            "id": 4,
            "text": "Machine 4",
            "unscheduled": true,
            "open": true
        }
    ]
};

jQuery(document).ready(function ($) {

    $(".gantt-1").dhx_gantt({
        data: ganttData
    });

    initTableDragDrop();

});

function padNum(number, digit = 2) {
    return ("0".repeat(digit) + number).substr(digit * -1);
}

function initTableDragDrop() {
    // const rows = Array.from(document.querySelectorAll(`#${tableId} tbody tr`));
    // if (!rows)
    //     return;

    const now = new Date();
    const date = `${padNum(now.getDate())}-${padNum(now.getMonth() + 1)}-${now.getFullYear()}`;

    jQuery(`#${tableId} tbody tr`).each(function () {
        const r = jQuery(this);

        const duration = Math.round(Math.random() * 10) + 1;
        const start = new Date(now.getFullYear(), now.getMonth(), now.getDate());
        const end = new Date(now.getFullYear(), now.getMonth(), now.getDate() + duration);
        
        const name = r.data('name');
        r.data('start', start);
        r.data('end', end);
        r.draggable({
            cancel: "a.ui-icon", // clicking an icon won't initiate dragging
            revert: "invalid", // when not dropped, the item will revert back to its initial position
            containment: "document",
            helper: "clone",
            cursor: "move"
        });
    });


    jQuery('.gantt-1').droppable({
        drop: function (event, ui) {
            const obj = ui.draggable;
            if (!obj)
                return;

            const startDate = obj.data('start');
            const timeDiff = Math.abs(obj.data('end').getTime() - obj.data('start').getTime());
            const duration = Math.ceil(timeDiff / (1000 * 3600 * 24));
            const startDateStr = `${padNum(startDate.getDate())}-${padNum(startDate.getMonth() + 1)}-${startDate.getFullYear()}`;

            gantt.addTask({
                start_date: startDate,
                duration: duration,
                text: obj.data('name'),
                id: obj.data('name')
            }, 1);
        }
    });
}