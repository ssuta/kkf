<?php
    $select = 'open_menu_1';
    $select2 = 'menu_2';
    $select3 = 'menu_2';
    $select4 = 'menu_2_1';
?>
<!--  -->
<? include('inc.header.php');?>
<!--  -->
<? include('inc.navbar.php');?>
<!--  -->
<? include('inc.menu.php');?>
<!-- <div class="layout-main"> -->
<!--  -->
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <h4 class="m-t-0">
                <span class="d-ib text-primary">การตั้งค่า</span> <span class="icon icon-angle-double-right"></span> สินค้า
            </h4>
        </div>

        <div class="row gutter-xs">
            <div class="col-xs-12">
                <!--  -->
                <div class="card">
                    <div class="card-body">
                        <!--  -->
<div class="at_add_box">
<div class="card" aria-expanded="true">
										<div class="card-header at_bg_table_light_gray">
											<div class="card-actions">
												 <button type="button" class="card-action card-toggler" title="Collapse" aria-expanded="true"></button>
												 <!-- <button type="button" class="card-action card-reload" title="Reload"></button>
												 <button type="button" class="card-action card-remove" title="Remove"></button> -->
											</div>
											<strong>ตัวเลือกการค้นหา</strong>
										</div>
										<div class="card-body" style="display: block;">
										<form class="form horizontal">
											 <div class="col-sm-4">
            <form class="row form form-horizontal">
                <div class="form-group">
                    <label class="col-sm-4 control-label text-right" for="form-control-14">ประเภทการขาย</label>
                    <div class="col-sm-8 has-feedback">
                        <select id="demo-select2-1" class="form-control">
                            <option value="0">ทั้งหมด</option>
                            <option value="1">ภายในประเทศ</option>
                            <option value="2">ต่างประเทศ</option>
                        </select>
                    </div>
                </div>
            </form>
        </div>
		<div class="col-sm-4">
			<form class="row form form-horizontal">
                <div class="form-group">
                    <label class="col-sm-4 control-label text-right" for="form-control-14">ประเภทสินค้า</label>
                    <div class="col-sm-8 has-feedback">
                        <select id="demo-select2-1" class="form-control">
						  <option value="0">ทั้งหมด</option>
                          <option value="1">ข่ายเอ็น</option>
														<option value="2">อวนปั๊ม</option>
														<option value="3">อวนปั๊ม</option>
														<option value="4">ด้ายโปลี</option>
														<option value="5">ข่ายรุม</option>
                        </select>
                    </div>
                </div>
			</form>
        </div>
		<div class="col-sm-3">
            <form class="row form form-horizontal">
                <div class="form-group">
                    <label class="col-sm-4 control-label text-right" for="form-control-14">ประเภทใย</label>
                    <div class="col-sm-8 has-feedback">
                        <select id="demo-select2-1" class="form-control">
						  <option value="0">ทั้งหมด</option>
                          	<option value="1">M : MONO</option>
														<option value="2">N : NYLON</option>
														<option value="3">P : POLY</option>
                        </select>
                    </div>
                </div>
            </form>
        </div>
		 <div class="text-right">
						<a class="btn btn-md btn-primary" href="menu_2_1.php" type="submit">ค้นหา</a>
                    </div>
										</form>
										</div>
									</div>
</div>
 <? include('inc.home_top_filter.php');?>
                        <!--  -->
                        <div class="table-responsive">
                            <table id="demo-datatables-3" class="table table-hover table-bordered table-nowrap dataTable text-center no-footer" cellspacing="0" width="100%">
                                <thead>
                        <tr class="at_bg_table_blue">
                            <th class="text-center">รหัสสินค้า</th>
                            <th class="text-center">รายละเอียด</th>
                            <th class="text-center">รหัสสาขา</th>
                            <th class="text-center">รายละเอียดปภ.สินค้า</th>
                            <th class="text-center">ความยาว(เมตร)</th>
                            <th class="text-center">หน่วยผลิต</th>
                            <th class="text-center">นน./ผืน(กรัม)</th>
                            <th class="text-center">วันที่เริ่มใช้งาน</th>
                            <th class="text-center">ผู้บันทึก</th>
                            <th class="text-center">ปรับแก้ไข</th>
                        </tr>
                                </thead>
                               <tbody>


<?php $column1 = array(
"F01003051",
"F01107053",
"H01400*11260250",
"H0A500H82010900",
"I0180///692/091",
"I0210///790/091",
"I1300///430/011",
"J00302031",
"J12P06030",
"K0281/9NSD04000",
"K03B0/+02506000",
"K1290/20/2ย4001",
"L03B2/*21403008",
"L07H0/900541000",
"L53D2/*10026000",
"M0111//017A081ง",
"M0120/N673F0751",
"M130PA/055ถ0504",
"M22F0A/03010500",
"MC251//030Y100P",
"N1290//17HI3000",
"N2230//10040051",
"N3F51//055D0502",
"P0560ฅ/09070501",
"P08A05*80000400",
"T1ZS0/*9001T900",
"T2A30//09600600",
"U1030/0021C0100",
"U3041/004150012",
"V163030D2",
"V3130A0H0",
"W20317001",
"W2060N051",
"X02009095",
"X0230R0A1",
"Y20209013",
"Y2090B048",
"1119P/013010400",
"1ฑ090/013000402",
"11190/013010400",
"19090/013000402",
"1ฌ094/013000502",
"210962013000400",
"210922013000400",
"5204D+030006002",
"5304D+030006002",
"5304D-030006002",
"5004D+030006002",
"718312AX",
"731312AX",
"736312AX",
"7403129C",
);
$arrlength = count($column1);

$column2 = array(
"2.5 mm  0.90 kg / spool  RED L026",
"3 mm  0.80 kg / spool  BLUE L340",
"0.14mm  1.1/2*25md*20yd  DK  YOKO  NW M206",
"110/2  3.70cm*90md*12m  SK  YOKO  BLACK N101",
"210/18P  5cm*200md*5kg  SK  TATE  NW N2061",
"210/21P  7.50cm*200md*5kg  SK  TATE  NW N2061",
"210/3P  26mmsq*100md*200m  SK  YOKO  YELLOW N277",
"210/9P  180 g / spool  NATURAL WHITE K2061",
"250/12P  240 g / spool  BLACK&TAR K126",
"210/8  12.40mmsq*400md*2080ml  SK  TATE  RED P170",
"210/8  25mm*600md*63m  SK  TATE  G.G.P513",
"210/9  20/20*400md*100m  SK  TATE  BROWN P167",
"380/15  2.1/4\"*300md*45m  SK  TELA  G.G.L519",
"30/21.  70mmsq*100md*50m  SK  YOKO  BLACK L804",
"380/24  1*600md*50yd  SK  TATE  BLACK L802",
"0.11mm  17mmsq*81md*500g  DK  TATE  GREEN M558",
"0.12mm  2.5*75md*200m  DK  TATE  LIGHT GREEN M558",
"0.30mm  55mmsq*50md*2000ml  DK  YOKO  LIGHT BLUE M330 (CON10)",
"0.25mm Q  30mmsq*50md*2000ml  DK  YOKO  GREEN M576(CON10)",
"0.25mm  30mmsq*100md*5kg  DK  YOKO  GRN M528",
"210D/9  17.80mmsq*300md*1000ml  DK  YOKO  NATURAL WHITE N206",
"210D/3  100mmsq*5md*500g  SK  YOKO  GREEN N528",
"210/12  55mmsq*50md*9.5kg  SK  TATE  NW N206",
"0.50mm Q  90mmsq*50md*55m  DK  YOKO  GREEN M503",
"0.80mm Q  8\"*40md*110m  DK  YOKO  GRAY M414",
"0.20*18  9\"*55md*400yd  DK  YOKO  NW T206",
"5*3  96mmsq*60md*55m  DK  YOKO  DARK GREEN T529",
"380/24  1\".*200md*50yd  SK  TATE  BLACK+UV L803",
"380/30  3 cm*300md*30 m  SK  YOKO  BLACK L802",
"020*15  500 g / spool  NATURAL WHITE T206",
"020*14  1 kg / spool  GRAY T419",
"380/3  900 g / 20 hanks  G.G.L5I9",
"380/6  450 g / spool  Apple green L537",
"MONO LINE 0.20mm  1000 m / spool  L.GREEN K579",
"MONO LINE 0.23mm  250 g / spool  GREEN M528",
"210/2  100 g / spool  GREY K419",
"210/9  1 lb / spool  SNOW WHITE",
"ข่ายเอ็น 0.09 SK 1.3*40*180A. สีง SK00 YOKO เรือใบเงิน",
"ข่ายเอ็น 0.09 SK 1.3*40*180 SK00 YOKO ป้ายเหลืองพิมพ์ 2*3 นิ้ว (ไม่ได้มาตรฐาน)",
"ข่ายเอ็น 0.09 SK 1.3*40*180 สีง SK00 YOKO เรือใบเงิน",
"ข่ายเอ็น 0.09 SK 1.3*40*180 SK00 YOKO ป้ายม้าน้ำเดี่ยว",
"ข่ายเอ็น 0.09 SK 1.3*50*90 สีหู SK00,SK00 YOKO ปลา SHARK BRAND",
"ข่ายรุม 0.09 SK 1.3*40*75-6 รุม กลมล่าง 2 เส้น สูตร 3 กทม.  SK00 YOKO เรือใบเงิน",
"ข่ายรุม 0.09 SK 1.3*40*EE รุม กลมล่าง 2 เส้น สูตร 3 กทม.  SK00 YOKO เรือใบเงิน",
"อวนปั๊ม 210/4 30/50*600*-- สีครีม P904 ไม่ชุบ มีกระดูก ป้ายเหลืองพิมพ์ 2*3 นิ้ว (ไม่ได้มาตรฐาน)",
"อวนปั๊ม 210/4 30/50*600*-- สีครีม P904 ไม่ชุบ มีกระดูก ป้ายเรือใบทอง",
"อวนปั๊ม 210/4 30/50*600*-- สีครีม P904 ชุบปกติ มีกระดูก ป้ายเรือใบทอง",
"อวนปั๊ม 210/4 30/50*600*-- สีครีม P904 ไม่ชุบ มีกระดูก ป้ายม้าน้ำเดี่ยว",
"ด้ายโปลี 380/12 รุ่น 20 ไจ/1 กก. สีเขียวขี้ม้า L540 ป้ายม้าน้ำเดี่ยว",
"ด้ายโปลี 380/12 รุ่น 0.95 กก./20 ไจ เขียว L502 ป้ายม้าน้ำเดี่ยว",
"ด้ายโปลี 380/12 รุ่น 0.95 กก./20 ไจ สีดำ L801 ป้ายม้าน้ำเดี่ยว",
"ด้ายโปลี 380/12 รุ่น 0.9 กก./หลอด สีแดงสดเข้มL005 ป้ายเรือใบ",);

$column3 = array(
"",
"",
"B&S1",
"BWC",
" ",
"",
"",
"",
"",
"KKF1",
"KKF1",
"KKF1",
"",
"FM1",
"FM1",
"B&S1",
"B&S1",
"NR",
"",
"",
"BWCN",
"KKF",
"BWCN",
"KKF4",
"KKF4",
"BWCT",
"BWCT",
"",
"KKF1",
"KKF2",
"KKF2",
"FM",
"FM",
"KKF2",
"KKF2",
"KKF2",
"KKF2",
"",
"",
"",
"",
"",
"",
"",
"",
"",
"",
"",
"",
"",
"",
"",
"",);

$column4 = array(

"PE ROPE",
"PE ROPE",
"COMPLETED NET ( H )",
"COMPLETED NET ( H )",
"POLYESTER NET",
"POLYESTER NET",
"POLYESTER NET",
"POLYESTER TWINE",
"POLYESTER TWINE",
"KNOTLESS NET",
"KNOTLESS NET",
"KNOTLESS NET",
"POLYETHYLENE NET",
"POLYETHYLENE NET",
"POLYETHYLENE NET",
"MONO NET",
"MONO NET",
"MONO NET",
"MONO NET",
"MONO NET",
"MULTI FILAMENT NET",
"MULTI FILAMENT NET",
"MULTI FILAMENT NET",
"TRAMMEL",
"TRAMMEL",
"MULTI-MONO",
"MULTI-MONO",
"PE KNOTLESS",
"PE KNOTLESS",
"MULTI-MONO TWINE",
"MULTI-MONO TWINE",
"POLYETHYLENE TWINE",
"POLYETHYLENE TWINE",
"MONO LINE",
"MONO LINE",
"NYLON TWINE",
"NYLON TWINE",
"ข่ายเอ็น",
"ข่ายเอ็น",
"ข่ายเอ็น",
"ข่ายเอ็น",
"ข่ายเอ็น",
"ข่ายรุม",
"ข่ายรุม",
"อวนปั๊ม",
"อวนปั๊ม",
"อวนปั๊ม",
"อวนปั๊ม",
"ด้ายโปลี",
"ด้ายโปลี",
"ด้ายโปลี",
"ด้ายโปลี",);

$column5 = array(

"0",
"0",
"18.29",
"12",
"0",
"0",
"200",
"0",
"0",
"51.58",
"63",
"100",
"45",
"50",
"45.72",
"0",
"200",
"220",
"120",
"0",
"35.6",
"0",
"0",
"55",
"110",
"365.76",
"55",
"45.72",
"30",
"0",
"0",
"0",
"0",
"100",
"0",
"",
"0",
"175",
"180",
"180",
"180",
"85",
"30",
"36",
"50",
"50",
"50",
"50",
"0",
"0",
"0",
"0",
);

$column6 = array(

"KG",
"KG",
"PC",
"PC",
"PC",
"PC",
"PC",
"KG",
"KG",
"PC",
"PC",
"PC",
"PC",
"PC",
"PC",
"PC",
"PC",
"PC",
"PC",
"PC",
"PC",
"PC",
"PC",
"PC",
"PC",
"PC",
"PC",
"PC",
"PC",
"KG",
"KG",
"KG",
"KG",
"KG",
"KG",
"KG",
"KG",
"",
"",
"",
"",
"",
"",
"",
"",
"",
"",
"",
"",
"",
"",
"",
);

$column7 = array(

"1000",
"1000",
"22",
"68.04",
"0",
"0",
"0",
"1000",
"1000",
"0",
"0",
"0",
"0",
"0",
"0",
"0",
"0",
"0",
"0",
"0",
"0",
"0",
"0",
"0",
"0",
"0",
"0",
"0",
"0",
"1000",
"1000",
"1000",
"1000",
"1000",
"1000",
"1000",
"1000",
"",
"",
"",
"",
"",
"",
"",
"",
"",
"",
"",
"",
"",
"",
"",
);

$column8 = array(

"24/08/2561",
"24/08/2561",
"24/08/2561",
"24/08/2561",
"24/08/2561",
"24/08/2561",
"24/08/2561",
"24/08/2561",
"24/08/2561",
"24/08/2561",
"24/08/2561",
"24/08/2561",
"24/08/2561",
"24/08/2561",
"24/08/2561",
"24/08/2561",
"24/08/2561",
"24/08/2561",
"24/08/2561",
"24/08/2561",
"24/08/2561",
"24/08/2561",
"24/08/2561",
"24/08/2561",
"24/08/2561",
"24/08/2561",
"24/08/2561",
"24/08/2561",
"24/08/2561",
"24/08/2561",
"24/08/2561",
"24/08/2561",
"24/08/2561",
"24/08/2561",
"24/08/2561",
"24/08/2561",
"24/08/2561",
"24/08/2561",
"24/08/2561",
"24/08/2561",
"24/08/2561",
"24/08/2561",
"24/08/2561",
"24/08/2561",
"24/08/2561",
"24/08/2561",
"24/08/2561",
"24/08/2561",
"24/08/2561",
"24/08/2561",
"24/08/2561",
"24/08/2561",
);

$column9 = array(

"USER_ID",
"USER_ID",
"USER_ID",
"USER_ID",
"USER_ID",
"USER_ID",
"USER_ID",
"USER_ID",
"USER_ID",
"USER_ID",
"USER_ID",
"USER_ID",
"USER_ID",
"USER_ID",
"USER_ID",
"USER_ID",
"USER_ID",
"USER_ID",
"USER_ID",
"USER_ID",
"USER_ID",
"USER_ID",
"USER_ID",
"USER_ID",
"USER_ID",
"USER_ID",
"USER_ID",
"USER_ID",
"USER_ID",
"USER_ID",
"USER_ID",
"USER_ID",
"USER_ID",
"USER_ID",
"USER_ID",
"USER_ID",
"USER_ID",
"USER_ID",
"USER_ID",
"USER_ID",
"USER_ID",
"USER_ID",
"USER_ID",
"USER_ID",
"USER_ID",
"USER_ID",
"USER_ID",
"USER_ID",
"USER_ID",
"USER_ID",
"USER_ID",
"USER_ID",

);
?>

<?php for($i=0;$i<$arrlength;$i++){?>
<tr>
    <td><?php echo $column1[$i];?></td> <!--รหัสสินค้า -->
    <td class="text-left"><?php echo $column2[$i];?></td><!--ชื่อสินค้า -->
    <td><?php echo $column3[$i];?></td><!--ประเภทสินค้า-->
    <td><?php echo $column4[$i];?></td><!--ประเภทใย -->
    <td><?php echo $column5[$i];?></td><!--เบอร์ใยผลิต -->
    <td><?php echo $column6[$i];?></td><!--อบ -->
    <td><?php echo $column7[$i];?></td>
    <td><?php echo $column8[$i];?></td>
    <td><?php echo $column9[$i];?></td>
    <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_2_1.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
<?}?>
                     </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
            </div>
        </div>










    </div>
</div>
<!--  -->
<? include('inc.footer.php');?>
<!--  -->
<? include('inc.footer.script.php');?>
<!--  -->
<div id="menu_2_1_edit" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">แก้ไข</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-6 m-b">
                            <label>รหัสสาขา</label>
                            <input class="form-control" type="text" placeholder="นางสาวตะข่าย">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>รหัสประเภทการขาย</label>
                            <input class="form-control" type="text" placeholder="กว้างขวาง">
                        </div>
						 <div class="col-xs-6 m-b">
                            <label>รหัสระดับคุณภาพ</label>
                            <input class="form-control" type="text" placeholder="การตลาด">
                        </div>
						 <div class="col-xs-6 m-b">
                            <label>ป</label>
                            <input class="form-control" type="text" placeholder="ผู้จัดการ">
                        </div>

						 <div class="col-xs-6 m-b">
                            <label>สิทธิการใช้งาน</label>
                           <select class="custom-select">
                        <option value="" selected="">Administrator</option>
                        <option value="c-plus-plus">Manager</option>
                        <option value="css">Customer</option>
                      </select>
                        </div>

						 <div class="col-xs-6 m-b">
                            <label>สถานะการใช้งาน</label>
							<div class="custom-controls-stacked m-t">
                      <label class="switch switch-primary">
                        <input class="switch-input" type="checkbox" checked="checked">
                        <span class="switch-track"></span>
                        <span class="switch-thumb"></span>
                      </label>
                      </div>
                        </div>

                    </div>
                    <div class="m-t text-center">
						<button onclick="myFunction()" class="btn btn-lg btn-primary" data-dismiss="modal" type="button">บันทึก</button>
                        <button class="btn btn-lg btn-default" data-dismiss="modal" type="button">ยกเลิก</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--  -->