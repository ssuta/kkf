<?php
    $select = 'open_menu_3';
    $select2 = 'menu_maintenance_2';
	
?>
<!--  -->
<? include('inc.header.php');?>
<!--  -->
<? include('inc.navbar.php');?>
<!--  -->
<? include('inc.menu.php');?>
<!-- <div class="layout-main"> -->
<!--  -->
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <h4 class="m-t-0">
                <!-- <span class="d-ib text-danger">--</span> <span class="icon icon-angle-double-right"></span> -->
                <!-- <span class="d-ib">ข้อมูลหลัก</span> -->
                <span class="d-ib text-primary">การซ่อมบำรุง</span> <span class="icon icon-angle-double-right"></span> บันทึกการจอดเครื่อง
                <!-- <span class="d-ib">
                    <a class="title-bar-shortcut" href="#" title="Add to shortcut list" data-container="body" data-toggle-text="Remove from shortcut list" data-trigger="hover" data-placement="right" data-toggle="tooltip">
                        <span class="sr-only">Add to shortcut list</span>
                    </a>
                </span> -->
            </h4>
            <!-- <p class="title-bar-description">
                <small>Latest update on 01/01/2017 - 10.57 am.</small>
            </p> -->
        </div>

        <div class="row gutter-xs">
            <div class="col-xs-12">
                <!--  -->
                <div class="card">
                    <!-- <div class="card-header">
                        <a href="javascript:window.history.back(-1);" class="btn btn-sm btn-outline-primary"><span class="icon icon-long-arrow-left"></span>&nbsp;&nbsp;Back</a>&nbsp;&nbsp;
                        <strong class="">Latest update on 01/01/2017 - 10.57 am.</strong>
                    </div> -->
                    <div class="card-body">
                        <!--  -->
                        <? include('inc.home_top_filter.php');?>
                        <!--  -->
                        <div class="table-responsive">
                            <table id="demo-datatables-2" class="table table-striped table-bordered table-nowrap dataTable text-center no-footer" cellspacing="0" width="100%">
                                <thead>
                                    <tr class="at_bg_table_blue">
                                        <th class="text-center">รหัสโรงทอ</th>
                                        <th class="text-center">เดือน</th>
                                        <th class="text-center">ปีค.ศ.</th>
                                        <th class="text-center">รหัสเครื่องทอ</th>
                                        <!-- <th class="text-center">รหัสประเภทงาน</th> -->
                                        <th class="text-center">ประเภทงาน</th>
                                        <!-- <th class="text-center">รหัสกิจกรรมงาน</th> -->
                                        <th class="text-center">กิจกรรมงาน</th>
                                        <!-- <th class="text-center">รหัสรายละเอียดงาน</th> -->
                                        <th class="text-center">รายละเอียดงาน</th>
                                        <th class="text-center">วันที่เริ่มจอดเครื่อง</th>
                                        <th class="text-center">วันที่สิ้นสุด</th>
                                        <th class="text-center">สถานะ</th>
                                        <th class="text-center">เริ่มใช้งาน</th>
                                        <th class="text-center">ปรับแก้ไข</th>
                                    </tr>
                                </thead>
                                <tbody>
								<tr>
    <td>A</td>
    <td>5</td>
    <td>2010</td>
    <td>A218</td>
    <!-- <td>A</td> -->
    <td>เปลี่ยนอะไหล่อุปกรณ์ตามรอบ</td>
    <!-- <td>A01</td> -->
    <td>การเปลี่ยนอุปกรณ์รอบ 1 เดือน</td>
    <!-- <td>001</td> -->
    <td>เปลี่ยนตะขอล่าง</td>
    <td>27/05/2553 0:00 น.</td>
    <td>2/06/2553 0:00 น.</td>
    <td>ยังไม่แล้วเสร็จ</td>
    <td>27/05/2553 14:20 น.</td>
    <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_6_1.php" type="button">
	 <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
<tr>
    <td>A</td>
    <td>5</td>
    <td>2010</td>
    <td>A209</td>
    <!-- <td>B</td> -->
    <td>PMตรวจสภาพเครื่อง</td>
    <!-- <td>A02</td> -->
    <td>การเปลี่ยนอุปกรณ์รอบ 3 เดือน</td>
    <!-- <td>002</td> -->
    <td>เปลี่ยนผ้ากรองกระสวย</td>
    <td>28/05/2553 0:00 น.</td>
    <td>28/05/2553 0:00 น.</td>
    <td>ยังไม่แล้วเสร็จ</td>
    <td>27/05/2553 15:05 น.</td>
   <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_6_1.php" type="button">
	 <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
<tr>
    <td>A</td>
    <td>5</td>
    <td>2010</td>
    <td>A006</td>
    <!-- <td>C</td> -->
    <td>บำรุงรักษาเครื่องจักร</td>
    <!-- <td>A03</td> -->
    <td>การเปลี่ยนอุปกรณ์รอบ 6 เดือน</td>
    <!-- <td>001</td> -->
    <td>เปลี่ยนตะขอล่าง</td>
    <td>27/05/2553 0:00 น.</td>
    <td>31/05/2553 0:00 น.</td>
    <td>ยังไม่แล้วเสร็จ</td>
    <td>27/05/2553 17:07 น.</td>
    <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_6_1.php" type="button">
	 <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
<tr>
    <td>A</td>
    <td>6</td>
    <td>2010</td>
    <td>A408</td>
    <!-- <td>D</td> -->
    <td>ปรับปรุงเครื่องจักร</td>
    <!-- <td>A04</td> -->
    <td>การเปลี่ยนอุปกรณ์รอบ 12 เดือน</td>
    <!-- <td>002</td> -->
    <td>เปลี่ยนผ้ากรองกระสวย</td>
    <td>16/06/2553 0:00 น.</td>
    <td>16/06/2553 0:00 น.</td>
    <td>ยังไม่แล้วเสร็จ</td>
    <td>12/06/2553 9:56 น.</td>
    <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_6_1.php" type="button">
	 <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
<tr>
    <td>A</td>
    <td>6</td>
    <td>2010</td>
    <td>A219</td>
    <!-- <td>E</td> -->
    <td>เปลี่ยนอุปกรณ์</td>
    <!-- <td>A05</td> -->
    <td>การเปลี่ยนอุปกรณ์รอบ 24 เดือน</td>
    <!-- <td>001</td> -->
    <td>เปลี่ยนตะขอล่าง</td>
    <td>16/06/2553 0:00 น.</td>
    <td>20/06/2553 0:00 น.</td>
    <td>ยังไม่แล้วเสร็จ</td>
    <td>16/06/2553 17:32 น.</td>
    <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_6_1.php" type="button">
	 <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
<tr>
    <td>A</td>
    <td>6</td>
    <td>2010</td>
    <td>A114</td>
    <!-- <td>A</td> -->
    <td>เปลี่ยนอะไหล่อุปกรณ์ตามรอบ</td>
    <!-- <td>B01</td> -->
    <td>การตรวจสอบสภาพเครื่องประจำสัปดาห์</td>
    <!-- <td>002</td> -->
    <td>เปลี่ยนผ้ากรองกระสวย</td>
    <td>16/06/2553 0:00 น.</td>
    <td>21/06/2553 0:00 น.</td>
    <td>ยังไม่แล้วเสร็จ</td>
    <td>16/06/2553 19:50 น.</td>
   <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_6_1.php" type="button">
	 <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
<tr>
    <td>A</td>
    <td>6</td>
    <td>2010</td>
    <td>A407</td>
    <!-- <td>B</td> -->
    <td>PMตรวจสภาพเครื่อง</td>
    <!-- <td>B02</td> -->
    <td>การตรวจสอบสภาพเครื่องประจำ 15 วัน</td>
    <!-- <td>001</td> -->
    <td>เปลี่ยนตะขอล่าง</td>
    <td>23/06/2553 0:00 น.</td>
    <td>30/06/2553 0:00 น.</td>
    <td>ยังไม่แล้วเสร็จ</td>
    <td>23/06/2553 11:06 น.</td>
    <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_6_1.php" type="button">
	 <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
<tr>
    <td>A</td>
    <td>6</td>
    <td>2010</td>
    <td>A618</td>
    <!-- <td>C</td> -->
    <td>บำรุงรักษาเครื่องจักร</td>
    <!-- <td>B03</td> -->
    <td>การตรวจสอบสภาพเครื่องประจำ 1 เดือน</td>
    <!-- <td>002</td> -->
    <td>เปลี่ยนผ้ากรองกระสวย</td>
    <td>24/06/2553 0:00 น.</td>
    <td>27/06/2553 0:00 น.</td>
    <td>ยังไม่แล้วเสร็จ</td>
    <td>24/06/2553 19:11 น.</td>
    <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_6_1.php" type="button">
	 <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
<tr>
    <td>A</td>
    <td>6</td>
    <td>2010</td>
    <td>A011</td>
    <!-- <td>D</td> -->
    <td>ปรับปรุงเครื่องจักร</td>
    <!-- <td>B04</td> -->
    <td>การตรวจสอบสภาพเครื่องประจำ 3 เดือน</td>
    <!-- <td>001</td> -->
    <td>เปลี่ยนตะขอล่าง</td>
    <td>26/06/2553 0:00 น.</td>
    <td>28/06/2553 0:00 น.</td>
    <td>ยังไม่แล้วเสร็จ</td>
    <td>26/06/2553 15:06 น.</td>
    <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_6_1.php" type="button">
	 <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
<tr>
    <td>A</td>
    <td>7</td>
    <td>2010</td>
    <td>A203</td>
    <!-- <td>E</td> -->
    <td>เปลี่ยนอุปกรณ์</td>
    <!-- <td>C01</td> -->
    <td>การหล่อลื่นเครื่องจักรประจำสัปดาห์</td>
    <!-- <td>002</td> -->
    <td>เปลี่ยนผ้ากรองกระสวย</td>
    <td>18/07/2553 0:00 น.</td>
    <td>18/07/2553 0:00 น.</td>
    <td>ยังไม่แล้วเสร็จ</td>
    <td>6/07/2553 11:43 น.</td>
    <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_6_1.php" type="button">
	 <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
<tr>
    <td>A</td>
    <td>3</td>
    <td>2010</td>
    <td>A118</td>
    <!-- <td>A</td> -->
    <td>เปลี่ยนอะไหล่อุปกรณ์ตามรอบ</td>
    <!-- <td>C02</td> -->
    <td>การหล่อลื่นเครื่องจักรประจำ 1 เดือน</td>
    <!-- <td>001</td> -->
    <td>เปลี่ยนตะขอล่าง</td>
    <td>10/03/2553 0:00 น.</td>
    <td>12/09/2553 0:00 น.</td>
    <td>ยังไม่แล้วเสร็จ</td>
    <td>10/09/2553 18:09 น.</td>
    <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_6_1.php" type="button">
	 <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
<tr>
    <td>A</td>
    <td>9</td>
    <td>2010</td>
    <td>A015</td>
    <!-- <td>B</td> -->
    <td>PMตรวจสภาพเครื่อง</td>
    <!-- <td>C03</td> -->
    <td>การหล่อลื่นเครื่องจักรประจำ 3 เดือน</td>
    <!-- <td>002</td> -->
    <td>เปลี่ยนผ้ากรองกระสวย</td>
    <td>11/09/2553 0:00 น.</td>
    <td>19/09/2553 0:00 น.</td>
    <td>สำเร็จ</td>
    <td>11/09/2553 8:49 น.</td>
    <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_6_1.php" type="button">
	 <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
<tr>
    <td>A</td>
    <td>9</td>
    <td>2010</td>
    <td>A009</td>
    <!-- <td>C</td> -->
    <td>บำรุงรักษาเครื่องจักร</td>
    <!-- <td>C04</td> -->
    <td>การทำความสะอาดเครื่องจักร</td>
    <!-- <td>001</td> -->
    <td>เปลี่ยนตะขอล่าง</td>
    <td>11/09/2553 0:00 น.</td>
    <td>13/09/2553 0:00 น.</td>
    <td>สำเร็จ</td>
    <td>11/09/2553 14:47 น.</td>
    <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_6_1.php" type="button">
	 <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
<tr>
    <td>A</td>
    <td>9</td>
    <td>2010</td>
    <td>A702</td>
    <!-- <td>D</td> -->
    <td>ปรับปรุงเครื่องจักร</td>
    <!-- <td>C05</td> -->
    <td>การทำความสะอาดล้างรางน้ำยา</td>
    <!-- <td>002</td> -->
    <td>เปลี่ยนผ้ากรองกระสวย</td>
    <td>21/09/2553 0:00 น.</td>
    <td>26/09/2553 0:00 น.</td>
    <td>สำเร็จ</td>
    <td>21/09/2553 14:22 น.</td>
    <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_6_1.php" type="button">
	 <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
<tr>
    <td>A</td>
    <td>10</td>
    <td>2010</td>
    <td>A705</td>
    <!-- <td>E</td> -->
    <td>เปลี่ยนอุปกรณ์</td>
    <!-- <td>D01</td> -->
    <td>การส่งตีปลอกแม่ตะขอ</td>
    <!-- <td>001</td> -->
    <td>เปลี่ยนตะขอล่าง</td>
    <td>1/10/2553 0:00 น.</td>
    <td>5/10/2553 0:00 น.</td>
    <td>สำเร็จ</td>
    <td>1/10/2553 13:41 น.</td>
   <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_6_1.php" type="button">
	 <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
<tr>
    <td>A</td>
    <td>5</td>
    <td>2010</td>
    <td>A417</td>
    <!-- <td>A</td> -->
    <td>เปลี่ยนอะไหล่อุปกรณ์ตามรอบ</td>
    <!-- <td>D02</td> -->
    <td>การส่งซ่อมลูกเบี้ยว</td>
    <!-- <td>002</td> -->
    <td>เปลี่ยนผ้ากรองกระสวย</td>
    <td>17/05/2553 0:00 น.</td>
    <td>31/05/2553 0:00 น.</td>
    <td>สำเร็จ</td>
    <td>19/11/2553 0:00 น.</td>
    <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_6_1.php" type="button">
	 <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
<tr>
    <td>A</td>
    <td>6</td>
    <td>2010</td>
    <td>A417</td>
    <!-- <td>B</td> -->
    <td>PMตรวจสภาพเครื่อง</td>
    <!-- <td>D03</td> -->
    <td>การจอดติดตั้งอุปกรณ์ต่างๆ ในส่วนงานโครงการ</td>
    <!-- <td>001</td> -->
    <td>เปลี่ยนตะขอล่าง</td>
    <td>1/06/2553 0:00 น.</td>
    <td>12/06/2553 0:00 น.</td>
    <td>สำเร็จ</td>
    <td>19/11/2553 0:00 น.</td>
    <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_6_1.php" type="button">
	 <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
<tr>
    <td>A</td>
    <td>9</td>
    <td>2010</td>
    <td>A417</td>
    <!-- <td>C</td> -->
    <td>บำรุงรักษาเครื่องจักร</td>
    <!-- <td>D04</td> -->
    <td>การเปลี่ยนอุปกรณ์ระยะพิทซ์</td>
    <!-- <td>002</td> -->
    <td>เปลี่ยนผ้ากรองกระสวย</td>
    <td>7/09/2553 0:00 น.</td>
    <td>15/09/2553 0:00 น.</td>
    <td>สำเร็จ</td>
    <td>19/11/2553 0:00 น.</td>
    <td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_6_1.php" type="button">
	 <span class="icon icon-lg icon-edit"></span>
        </a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
            </div>
        </div>










    </div>
</div>
<!--  -->
<? include('inc.footer.php');?>
<!--  -->
<? include('inc.footer.script.php');?>
<!--  -->
