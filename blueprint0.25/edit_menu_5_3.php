<?php
 $select = 'open_menu_1';
    $select2 = 'menu_5';
    $select3 = 'menu_5';
    $select4 = 'menu_5_3';
?>
<!--  -->
<? include('inc.header.php');?>
<!--  -->
<? include('inc.navbar.php');?>
<!--  -->
<? include('inc.menu.php');?>
<!-- <div class="layout-main"> -->
<!--  -->
<div class="layout-content">
    <div class="layout-content-body">
		<div class="title-bar">
            <h1 class="title-bar-title">
              <span class="d-ib">วงจรการผลิต</span>
            </h1>
            <div class="title-bar-description">
				 <span class="d-ib text-primary">การตั้งค่า</span> <span class="icon icon-angle-double-right"></span> กระบวนการผลิต <span class="icon icon-angle-double-right"></span> วงจรการผลิต <span class="icon icon-angle-double-right"></span> แก้ไข
			</div>
		</div>

		 <div class="row">
		
            <div class="col-md-12">
				 
                <!--  -->
                <div class="demo-form-wrapper">
                        <!--  -->
					<form class="form form-horizontal">
						<div class="panel m-b-lg">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#information" data-toggle="tab" aria-expanded="true">ข้อมูลทั่วไป</a></li>
								<li ><a href="#manufacture" data-toggle="tab" aria-expanded="true">ข้อมูลการผลิต</a></li>
                            </ul>
                            <div class="tab-content">
							 <!-- information -->
                                <div class="tab-pane fade active in" id="information">
							<div class="card" aria-expanded="true">
								<div class="card-header at_bg_table_light_gray">
								<div class="card-actions">
                                    <button type="button" class="card-action card-toggler" title="Collapse" aria-expanded="true"></button>
                                    <!-- <button type="button" class="card-action card-reload" title="Reload"></button>
                                    <button type="button" class="card-action card-remove" title="Remove"></button> -->
                                </div>
                                <strong>วงจรการผลิต</strong>
								</div>
								<div class="card-body" style="display: block;">
									<form class="form horizontal">
									<div class="form-group">
												<label class="col-sm-2 control-label" for="form-control-1">สาขา</label>
												<div class="col-sm-4">
													<select id="form-control-6" class="form-control">
														<option value="1">B&S : สารคาม</option>
														<option value="2">BWC : ศูนย์บวร</option>
														<option value="3">CY : เชียงยืน</option>
														<option value="3">EXT : ภายนอก</option>
														<option value="3">XFF : สาขาเมืองจีน</option>
													</select>
												</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="form-control-1">รหัสประเภทการขาย</label>
										<div class="col-sm-4">
											<input id="form-control-1" class="form-control" type="text" value="0">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="form-control-1">รหัสระดับคุณภาพ</label>
										<div class="col-sm-4">
											<input id="form-control-1" class="form-control" type="text" value="G">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="form-control-1">ปภ.กลุ่มวัตถุดิบ</label>
										<div class="col-sm-4">
											<input id="form-control-1" class="form-control" type="text" value="M">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="form-control-1">รหัสเงื่อน</label>
										<div class="col-sm-4">
											<input id="form-control-1" class="form-control" type="text" value="DK">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="form-control-1">รหัสประเภทการอบ</label>
										<div class="col-sm-4">
											<input id="form-control-1" class="form-control" type="text" value="Y">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="form-control-1">รหัสประเภทการอบ</label>
										<div class="col-sm-4">
											<input id="form-control-1" class="form-control" type="text" value="Y">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="form-control-1">รหัสประเภทหูทอ</label>
										<div class="col-sm-4">
											<input id="form-control-1" class="form-control" type="text" value="G">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="form-control-1">รหัสประเภทการฉีด</label>
										<div class="col-sm-4">
											<input id="form-control-1" class="form-control" type="text" value="1">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="form-control-1">ความยาวต่ำสุด</label>
										<div class="col-sm-4">
											<input id="form-control-1" class="form-control" type="text" value="1">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="form-control-1">ความยาวสูงสุด</label>
										<div class="col-sm-4">
											<input id="form-control-1" class="form-control" type="text" value="9999">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="form-control-1">จน. เส้นจัดเรียงต่ำสุด</label>
										<div class="col-sm-4">
											<input id="form-control-1" class="form-control" type="text" value="0">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="form-control-1">จน. เส้นจัดเรียงสูงสุด</label>
										<div class="col-sm-4">
											<input id="form-control-1" class="form-control" type="text" value="0">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="form-control-1">อักขระพิเศษต่ำสุด</label>
										<div class="col-sm-4">
											<input id="form-control-1" class="form-control" type="text" value="Q">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="form-control-1">เริ่มใช้งาน</label>
										<div class="col-sm-4">
											 <div class=" input-with-icon">
											  <input class="form-control" type="text" data-provide="datepicker" value="11/13/2009  5:09:08 PM">
												<span class="icon icon-calendar input-icon"></span>
											 </div>
										</div>
									</div>
									</form>
								</div>
							</div>

							</div>
							 <!--  -->
							 <div class="tab-pane fade" id="manufacture">
								<div class="card" aria-expanded="true">
								<div class="card-header at_bg_table_light_gray">
								<div class="card-actions">
                                    <button type="button" class="card-action card-toggler" title="Collapse" aria-expanded="true"></button>
                                    <!-- <button type="button" class="card-action card-reload" title="Reload"></button>
                                    <button type="button" class="card-action card-remove" title="Remove"></button> -->
                                </div>
                                <strong>ตา</strong>
								</div>
								<div class="card-body" style="display: block;">
									<form class="form horizontal">
									<div class="form-group">
										<label class="col-sm-2 control-label" for="form-control-1">ขนาดตาต่ำสุด</label>
										<div class="col-sm-4">
											<input id="form-control-1" class="form-control" type="text" value="1">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="form-control-1">ขนาดตาสูงสุด</label>
										<div class="col-sm-4">
											<input id="form-control-1" class="form-control" type="text" value="99">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="form-control-1">จน. ตา ต่ำสุด</label>
										<div class="col-sm-4">
											<input id="form-control-1" class="form-control" type="text" value="1">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="form-control-1">จน. ตา สูงสุด</label>
										<div class="col-sm-4">
											<input id="form-control-1" class="form-control" type="text" value="999">
										</div>
									</div>
									</form>
								</div>
							</div>

							<div class="card" aria-expanded="true">
								<div class="card-header at_bg_table_light_gray">
								<div class="card-actions">
                                    <button type="button" class="card-action card-toggler" title="Collapse" aria-expanded="true"></button>
                                    <!-- <button type="button" class="card-action card-reload" title="Reload"></button>
                                    <button type="button" class="card-action card-remove" title="Remove"></button> -->
                                </div>
                                <strong>วงจร</strong>
								</div>
								<div class="card-body" style="display: block;">
									<form class="form horizontal">
										<div class="form-group">
										<label class="col-sm-2 control-label" for="form-control-1">วงจร กอง 1 ดำ</label>
										<div class="col-sm-4">
											<input id="form-control-1" class="form-control" type="text" value="7">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="form-control-1">วงจร กอง 2 ฟอก</label>
										<div class="col-sm-4">
											<input id="form-control-1" class="form-control" type="text" value="2">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="form-control-1">วงจร กอง 2 อบ</label>
										<div class="col-sm-4">
											<input id="form-control-1" class="form-control" type="text" value="3">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="form-control-1">วงจร กอง 3 รับเข้า</label>
										<div class="col-sm-4">
											<input id="form-control-1" class="form-control" type="text" value="6">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="form-control-1">วงจร กอง 3 ส่งออก</label>
										<div class="col-sm-4">
											<input id="form-control-1" class="form-control" type="text" value="4">
										</div>
									</div>
									</form>
								</div>
							</div>

							<div class="card" aria-expanded="true">
								<div class="card-header at_bg_table_light_gray">
								<div class="card-actions">
                                    <button type="button" class="card-action card-toggler" title="Collapse" aria-expanded="true"></button>
                                    <!-- <button type="button" class="card-action card-reload" title="Reload"></button>
                                    <button type="button" class="card-action card-remove" title="Remove"></button> -->
                                </div>
                                <strong>ใย</strong>
								</div>
								<div class="card-body" style="display: block;">
									<form class="form horizontal">
									<div class="form-group">
										<label class="col-sm-2 control-label" for="form-control-1">เบอร์ใยต่ำสุด</label>
										<div class="col-sm-4">
											<input id="form-control-1" class="form-control" type="text" value="011 Q">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="form-control-1">เบอร์ใยสูงสุด</label>
										<div class="col-sm-4">
											<input id="form-control-1" class="form-control" type="text" value="001 Q">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="form-control-1">ขนาดใยจัดเรียงต่ำสุด</label>
										<div class="col-sm-4">
											<input id="form-control-1" class="form-control" type="text" value="0.11">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="form-control-1">ประเภทใยต่ำสุด</label>
										<div class="col-sm-4">
											<input id="form-control-1" class="form-control" type="text" value="1">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="form-control-1">ขนาดใยจัดเรียงสูงสุด</label>
										<div class="col-sm-4">
											<input id="form-control-1" class="form-control" type="text" value="0.11">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="form-control-1">ประเภทใยสูงสุด</label>
										<div class="col-sm-4">
											<input id="form-control-1" class="form-control" type="text" value="1">
										</div>
									</div>
									</form>
								</div>
							</div>
							 </div>
							  <!--  -->
							</div>
						</div>
						<div class="m-t text-center">
						<a class="btn btn-lg btn-primary" href="menu_5_3.php" type="submit">บันทึก</a>
                        <a class="btn btn-lg btn-default" href="menu_5_3.php" type="button>">ยกเลิก</a>
						</div>
					</form>
                    
                        <!--  -->
                    </div>
                </div>
                <!--  -->
            </div>
        </div>








    </div>
</div>
<!--  -->
<? include('inc.footer.php');?>
<!--  -->
<? include('inc.footer.script.php');?>
<!--  -->