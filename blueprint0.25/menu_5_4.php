<?php
   $select = 'open_menu_1';
    $select2 = 'menu_5';
    $select3 = 'menu_5';
    $select4 = 'menu_5_4';
?>
<!--  -->
<? include('inc.header.php');?>
<!--  -->
<? include('inc.navbar.php');?>
<!--  -->
<? include('inc.menu.php');?>
<!-- <div class="layout-main"> -->
<!--  -->
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <h4 class="m-t-0">
                <!-- <span class="d-ib text-danger">--</span> <span class="icon icon-angle-double-right"></span> -->
                <!-- <span class="d-ib">ข้อมูลหลัก</span> -->
                <span class="d-ib text-primary">ข้อมูลหลัก</span> <span class="icon icon-angle-double-right"></span> กระบวนการผลิต <span class="icon icon-angle-double-right"></span> วงจรการผลิตแบบวัน
                <!-- <span class="d-ib">
                    <a class="title-bar-shortcut" href="#" title="Add to shortcut list" data-container="body" data-toggle-text="Remove from shortcut list" data-trigger="hover" data-placement="right" data-toggle="tooltip">
                        <span class="sr-only">Add to shortcut list</span>
                    </a>
                </span> -->
            </h4>
            <!-- <p class="title-bar-description">
                <small>Latest update on 01/01/2017 - 10.57 am.</small>
            </p> -->
        </div>

        <div class="row gutter-xs">
            <div class="col-xs-12">
                <!--  -->
                <div class="card">
                    <!-- <div class="card-header">
                        <a href="javascript:window.history.back(-1);" class="btn btn-sm btn-outline-primary"><span class="icon icon-long-arrow-left"></span>&nbsp;&nbsp;Back</a>&nbsp;&nbsp;
                        <strong class="">Latest update on 01/01/2017 - 10.57 am.</strong>
                    </div> -->
                    <div class="card-body">
                        <!--  -->
                        <? include('inc.home_top_filter.php');?>
                        <!--  -->
                        <div class="table-responsive">
                              <table id="demo-datatables-2" class="table table-striped table-bordered table-nowrap dataTable text-center no-footer" cellspacing="0" width="100%">
                                <thead>
                                    <tr class="at_bg_table_blue">
                                        <th class="text-center">รหัสสาขา</th>
                                        <th class="text-center">จน. วันรวม</th>
                                        <th class="text-center">วงจร กอง 1 ดำ</th>
                                        <th class="text-center">เริ่วงจร กอง 2 ฟอก</th>
                                        <th class="text-center">ผวงจร กอง 2 อบ</th>
                                        <th class="text-center">ววงจร กอง 3 รับเข้า</th>
                                        <th class="text-center">วงจร กอง 3 ส่งออก</th>
                                        <th class="text-center">(CYCLE_TYPE)</th>
                                        <th class="text-center">เริ่มใช้งาน</th>
                                        <th class="text-center">ปรับแก้ไข</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <!--html.1 xsl.3-->
                                    <tr>
                                        <td>B&S</td>
                                        <td>9</td>
                                        <td>3</td>
                                        <td>1</td>
                                        <td>1</td>
                                        <td>2</td>
                                        <td>2</td>
                                        <td>DAY</td>
                                        <td>10/8/2013  10:15:22 น.</td>
                                        <td class="text-center" style="display: table-cell;">
                                           <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_5_4.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                                            <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                                                <span class="icon icon-lg icon-close"></span>
                                            </button>
                                        </td>
                                    </tr>
                                    <!--html.2 xsl.4-->
                                    <tr>
                                        <td>B&S</td>
                                        <td>10</td>
                                        <td>3</td>
                                        <td>1</td>
                                        <td>1</td>
                                        <td>3</td>
                                        <td>2</td>
                                        <td>DAY</td>
                                        <td>10/8/2013  10:15:11 น.</td>
                                        <td class="text-center" style="display: table-cell;">
                                           <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_5_4.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                                            <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                                                <span class="icon icon-lg icon-close"></span>
                                            </button>
                                        </td>
                                    </tr>
                                    <!--html.3 xsl.5-->
                                    <tr>
                                        <td>B&S</td>
                                        <td>12</td>
                                        <td>3</td>
                                        <td>2</td>
                                        <td>1</td>
                                        <td>4</td>
                                        <td>2</td>
                                        <td>DAY</td>
                                        <td>10/8/2013  10:14:46 น.</td>
                                        <td class="text-center" style="display: table-cell;">
                                           <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_5_4.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                                            <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                                                <span class="icon icon-lg icon-close"></span>
                                            </button>
                                        </td>
                                    </tr>
                                    <!--html.4 xsl.6-->
                                    <tr>
                                        <td>B&S</td>
                                        <td>14</td>
                                        <td>4</td>
                                        <td>1</td>
                                        <td>2</td>
                                        <td>4</td>
                                        <td>3</td>
                                        <td>DAY</td>
                                        <td>3/3/2016  14:48:34 น.</td>
                                        <td class="text-center" style="display: table-cell;">
                                           <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_5_4.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                                            <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                                                <span class="icon icon-lg icon-close"></span>
                                            </button>
                                        </td>
                                    </tr>
                                    <!--html.5 xsl.7-->
                                    <tr>
                                        <td>B&S</td>
                                        <td>15</td>
                                        <td>5</td>
                                        <td>2</td>
                                        <td>2</td>
                                        <td>4</td>
                                        <td>2</td>
                                        <td>DAY</td>
                                        <td>10/8/2013  10:14:25 น.</td>
                                        <td class="text-center" style="display: table-cell;">
                                           <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_5_4.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                                            <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                                                <span class="icon icon-lg icon-close"></span>
                                            </button>
                                        </td>
                                    </tr>
                                    <!--html.6 xsl.8-->
                                    <tr>
                                        <td>B&S</td>
                                        <td>16</td>
                                        <td>4</td>
                                        <td>2</td>
                                        <td>2</td>
                                        <td>4</td>
                                        <td>4</td>
                                        <td>DAY</td>
                                        <td>3/3/2016  14:47:58 น.</td>
                                        <td class="text-center" style="display: table-cell;">
                                           <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_5_4.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                                            <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                                                <span class="icon icon-lg icon-close"></span>
                                            </button>
                                        </td>
                                    </tr>
                                    <!--html.7 xsl.9-->
                                    <tr>
                                        <td>BWC</td>
                                        <td>12</td>
                                        <td>4</td>
                                        <td>2</td>
                                        <td>1</td>
                                        <td>0</td>
                                        <td>5</td>
                                        <td>DAY</td>
                                        <td>5/7/2010  8:52:06 น.</td>
                                        <td class="text-center" style="display: table-cell;">
                                           <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_5_4.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                                            <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                                                <span class="icon icon-lg icon-close"></span>
                                            </button>
                                        </td>
                                    </tr>
                                    <!--html.8 xsl.10-->
                                    <tr>
                                        <td>CY</td>
                                        <td>9</td>
                                        <td>3</td>
                                        <td>1</td>
                                        <td>1</td>
                                        <td>2</td>
                                        <td>2</td>
                                        <td>DAY</td>
                                        <td>23/8/2013  9:27:15 น.</td>
                                        <td class="text-center" style="display: table-cell;">
                                           <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_5_4.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                                            <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                                                <span class="icon icon-lg icon-close"></span>
                                            </button>
                                        </td>
                                    </tr>
                                    <!--html.9 xsl.11-->
                                    <tr>
                                        <td>CY</td>
                                        <td>10</td>
                                        <td>3</td>
                                        <td>1</td>
                                        <td>1</td>
                                        <td>3</td>
                                        <td>2</td>
                                        <td>DAY</td>
                                        <td>10/8/2013  10:16:54 น.</td>
                                        <td class="text-center" style="display: table-cell;">
                                           <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_5_4.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                                            <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                                                <span class="icon icon-lg icon-close"></span>
                                            </button>
                                        </td>
                                    </tr>
                                    <!--html.10 xsl.12-->
                                    <tr>
                                        <td>CY</td>
                                        <td>12</td>
                                        <td>3</td>
                                        <td>2</td>
                                        <td>1</td>
                                        <td>4</td>
                                        <td>2</td>
                                        <td>DAY</td>
                                        <td>23/8/2013  9:27:18 น.</td>
                                        <td class="text-center" style="display: table-cell;">
                                           <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_5_4.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                                            <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                                                <span class="icon icon-lg icon-close"></span>
                                            </button>
                                        </td>
                                    </tr>
                                    <!--html.11 xsl.13-->
                                    <tr>
                                        <td>CY</td>
                                        <td>15</td>
                                        <td>5</td>
                                        <td>2</td>
                                        <td>2</td>
                                        <td>4</td>
                                        <td>2</td>
                                        <td>DAY</td>
                                        <td>10/8/2013  10:16:39 น.</td>
                                        <td class="text-center" style="display: table-cell;">
                                           <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_5_4.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                                            <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                                                <span class="icon icon-lg icon-close"></span>
                                            </button>
                                        </td>
                                    </tr>
                                    <!--html.12 xsl.14-->
                                    <tr>
                                        <td>KKF</td>
                                        <td>9</td>
                                        <td>6</td>
                                        <td>1</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>2</td>
                                        <td>DAY</td>
                                        <td>10/7/2018  14:18:59 น.</td>
                                        <td class="text-center" style="display: table-cell;">
                                           <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_5_4.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                                            <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                                                <span class="icon icon-lg icon-close"></span>
                                            </button>
                                        </td>
                                    </tr>
                                    <!--html.13 xsl.15-->
                                    <tr>
                                        <td>KKF</td>
                                        <td>21</td>
                                        <td>9</td>
                                        <td>3</td>
                                        <td>3</td>
                                        <td>3</td>
                                        <td>3</td>
                                        <td>DAY</td>
                                        <td>13/7/2009  10:24:25 น.</td>
                                        <td class="text-center" style="display: table-cell;">
                                           <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_5_4.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                                            <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                                                <span class="icon icon-lg icon-close"></span>
                                            </button>
                                        </td>
                                    </tr>
                                    <!--html.14 xsl.16-->
                                    <tr>
                                        <td>KKF</td>
                                        <td>22</td>
                                        <td>9</td>
                                        <td>3</td>
                                        <td>3</td>
                                        <td>3</td>
                                        <td>4</td>
                                        <td>DAY</td>
                                        <td>13/7/2009  10:29:42 น.</td>
                                        <td class="text-center" style="display: table-cell;">
                                           <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_5_4.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                                            <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                                                <span class="icon icon-lg icon-close"></span>
                                            </button>
                                        </td>
                                    </tr>
                                    <!--html.15 xsl.17-->
                                    <tr>
                                        <td>NR</td>
                                        <td>7</td>
                                        <td>2</td>
                                        <td>1</td>
                                        <td>1</td>
                                        <td>1</td>
                                        <td>2</td>
                                        <td>DAY</td>
                                        <td>22/3/2010  10:12:59 น.</td>
                                        <td class="text-center" style="display: table-cell;">
                                           <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_5_4.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                                            <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                                                <span class="icon icon-lg icon-close"></span>
                                            </button>
                                        </td>
                                    </tr>
                                    <!--html.16 xsl.18-->
                                    <tr>
                                        <td>NR</td>
                                        <td>8</td>
                                        <td>2</td>
                                        <td>1</td>
                                        <td>2</td>
                                        <td>1</td>
                                        <td>2</td>
                                        <td>DAY</td>
                                        <td>22/3/2010  10:13:12 น.</td>
                                        <td class="text-center" style="display: table-cell;">
                                           <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_5_4.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                                            <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                                                <span class="icon icon-lg icon-close"></span>
                                            </button>
                                        </td>
                                    </tr>
                                    <!--html.17 xsl.19-->
                                    <tr>
                                        <td>NR</td>
                                        <td>9</td>
                                        <td>2</td>
                                        <td>2</td>
                                        <td>2</td>
                                        <td>1</td>
                                        <td>2</td>
                                        <td>DAY</td>
                                        <td>22/3/2010  10:12:28 น.</td>
                                        <td class="text-center" style="display: table-cell;">
                                           <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_5_4.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                                            <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                                                <span class="icon icon-lg icon-close"></span>
                                            </button>
                                        </td>
                                    </tr>
                                    <!--html.18 xsl.20-->
                                    <tr>
                                        <td>NR</td>
                                        <td>10</td>
                                        <td>2</td>
                                        <td>2</td>
                                        <td>2</td>
                                        <td>2</td>
                                        <td>2</td>
                                        <td>DAY</td>
                                        <td>22/3/2010  10:11:59 น.</td>
                                        <td class="text-center" style="display: table-cell;">
                                           <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_5_4.php" type="button">
        <span class="icon icon-lg icon-edit"></span>
        </a>
                                            <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
                                                <span class="icon icon-lg icon-close"></span>
                                            </button>
                                        </td>
                                    </tr>



                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
            </div>
        </div>










    </div>
</div>
<!--  -->
<? include('inc.footer.php');?>
<!--  -->
<? include('inc.footer.script.php');?>
<!--  -->
