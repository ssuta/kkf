<?php
    $select = 'open_menu_1';
    $select2 = 'menu_6';
    $select3 = 'menu_6';
    $select4 = 'menu_6_4';
	
?>
<!--  -->
<? include('inc.header.php');?>
<!--  -->
<? include('inc.navbar.php');?>
<!--  -->
<? include('inc.menu.php');?>
<!-- <div class="layout-main"> -->
<!--  -->
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <h4 class="m-t-0">
                <!-- <span class="d-ib text-danger">--</span> <span class="icon icon-angle-double-right"></span> -->
                <!-- <span class="d-ib">ข้อมูลหลัก</span> -->
                <span class="d-ib text-primary">ข้อมูลหลัก</span> <span class="icon icon-angle-double-right"></span> ซ่อมบำรุง <span class="icon icon-angle-double-right"></span> ประเภทงานซ่อมบำรุง
                <!-- <span class="d-ib">
                    <a class="title-bar-shortcut" href="#" title="Add to shortcut list" data-container="body" data-toggle-text="Remove from shortcut list" data-trigger="hover" data-placement="right" data-toggle="tooltip">
                        <span class="sr-only">Add to shortcut list</span>
                    </a>
                </span> -->
            </h4>
            <!-- <p class="title-bar-description">
                <small>Latest update on 01/01/2017 - 10.57 am.</small>
            </p> -->
        </div>

        <div class="row gutter-xs">
            <div class="col-xs-12">
                <!--  -->
                <div class="card">
                    <!-- <div class="card-header">
                        <a href="javascript:window.history.back(-1);" class="btn btn-sm btn-outline-primary"><span class="icon icon-long-arrow-left"></span>&nbsp;&nbsp;Back</a>&nbsp;&nbsp;
                        <strong class="">Latest update on 01/01/2017 - 10.57 am.</strong>
                    </div> -->
                    <div class="card-body">
                        <!--  -->
                        <? include('inc.home_top_filter.php');?>
                        <!--  -->
                        <div class="table-responsive">
                            <table id="datatables-base-no-sort" class="table table-hover table-bordered table-nowrap dataTable text-center no-footer" cellspacing="0" width="100%">
                                <thead>
                                    <tr class="at_bg_table_blue">
                                        <th height=19 class="add-padding-left text-center" width=102 style='height:14.4pt;width:76pt'>&#3619;&#3627;&#3633;&#3626;&#3611;&#3619;&#3632;&#3648;&#3616;&#3607;&#3591;&#3634;&#3609;</th>
  <th class="add-padding-left text-center" width=184 style='width:138pt'>&#3611;&#3619;&#3632;&#3648;&#3616;&#3607;&#3591;&#3634;&#3609;</th>
  <th class="add-padding-left text-center" width=124 style='width:93pt'>&#3619;&#3627;&#3633;&#3626;&#3619;&#3634;&#3618;&#3621;&#3632;&#3648;&#3629;&#3637;&#3618;&#3604;&#3591;&#3634;&#3609;</th>
  <th class="add-padding-left text-center" width=141 style='width:106pt'>&#3619;&#3634;&#3618;&#3621;&#3632;&#3648;&#3629;&#3637;&#3618;&#3604;&#3591;&#3634;&#3609;</th>
  <th class="add-padding-left text-center" width=130 style='width:97pt'>&#3648;&#3619;&#3636;&#3656;&#3617;&#3651;&#3594;&#3657;&#3591;&#3634;&#3609;</th>
  <th class="add-padding-left text-center" width=64 style='width:48pt'>&#3611;&#3619;&#3633;&#3610;&#3649;&#3585;&#3657;&#3652;&#3586;</th>
                                    </tr>
                                </thead>
                               <tbody>
 <tr height=19 style='height:14.4pt'>
  <td height=19 class=xl65 style='height:14.4pt'>A</td>
  <td class=xl66>&#3648;&#3611;&#3621;&#3637;&#3656;&#3618;&#3609;&#3629;&#3632;&#3652;&#3627;&#3621;&#3656;&#3629;&#3640;&#3611;&#3585;&#3619;&#3603;&#3660;&#3605;&#3634;&#3617;&#3619;&#3629;&#3610;</td>
  <td class=xl65>001</td>
  <td class=xl66>&#3648;&#3611;&#3621;&#3637;&#3656;&#3618;&#3609;&#3605;&#3632;&#3586;&#3629;&#3621;&#3656;&#3634;&#3591;</td>
  <td class=xl67>27/05/2553 14:20 &#3609;.</td>
  <td class=xl65><a class="btn btn-lg btn-outline-primary btn-pill
  btn-xs" href="edit_menu_6_4.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 class=xl65 style='height:14.4pt'></td>
  <td class=xl66></td>
  <td class=xl65>002</td>
  <td class=xl66>&#3648;&#3611;&#3621;&#3637;&#3656;&#3618;&#3609;&#3612;&#3657;&#3634;&#3585;&#3619;&#3629;&#3591;&#3585;&#3619;&#3632;&#3626;&#3623;&#3618;</td>
  <td class=xl67>27/05/2553 14:20 &#3609;.</td>
  <td class=xl65><a class="btn btn-lg btn-outline-primary btn-pill
  btn-xs" href="edit_menu_6_4.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 class=xl65 style='height:14.4pt'></td>
  <td class=xl66></td>
  <td class=xl65>003</td>
  <td class=xl66>&#3648;&#3611;&#3621;&#3637;&#3656;&#3618;&#3609; 0001</td>
  <td class=xl67>27/05/2553 14:20 &#3609;.</td>
  <td class=xl65><a class="btn btn-lg btn-outline-primary btn-pill
  btn-xs" href="edit_menu_6_4.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 class=xl65 style='height:14.4pt'></td>
  <td class=xl66></td>
  <td class=xl65>004</td>
  <td class=xl66>&#3648;&#3611;&#3621;&#3637;&#3656;&#3618;&#3609; 0002</td>
  <td class=xl67>27/05/2553 14:20 &#3609;.</td>
  <td class=xl65><a class="btn btn-lg btn-outline-primary btn-pill
  btn-xs" href="edit_menu_6_4.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 class=xl65 style='height:14.4pt'></td>
  <td class=xl66></td>
  <td class=xl65>005</td>
  <td class=xl66>&#3648;&#3611;&#3621;&#3637;&#3656;&#3618;&#3609; 0003</td>
  <td class=xl67>27/05/2553 14:20 &#3609;.</td>
  <td class=xl65><a class="btn btn-lg btn-outline-primary btn-pill
  btn-xs" href="edit_menu_6_4.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 class=xl65 style='height:14.4pt'></td>
  <td class=xl66></td>
  <td class=xl65>006</td>
  <td class=xl66>&#3648;&#3611;&#3621;&#3637;&#3656;&#3618;&#3609; 0004</td>
  <td class=xl67>27/05/2553 14:20 &#3609;.</td>
  <td class=xl65><a class="btn btn-lg btn-outline-primary btn-pill
  btn-xs" href="edit_menu_6_4.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 class=xl65 style='height:14.4pt'>B</td>
  <td class=xl66>PM<span style='mso-spacerun:yes'> 
  </span>&#3605;&#3619;&#3623;&#3592;&#3626;&#3616;&#3634;&#3614;&#3648;&#3588;&#3619;&#3639;&#3656;&#3629;&#3591;</td>
  <td class=xl65>007</td>
  <td class=xl66>&#3605;&#3619;&#3623;&#3592; 0001</td>
  <td class=xl67>27/05/2553 14:20 &#3609;.</td>
  <td class=xl65><a class="btn btn-lg btn-outline-primary btn-pill
  btn-xs" href="edit_menu_6_4.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 class=xl65 style='height:14.4pt'></td>
  <td class=xl66></td>
  <td class=xl65>008</td>
  <td class=xl66>&#3605;&#3619;&#3623;&#3592; 0002</td>
  <td class=xl67>27/05/2553 14:20 &#3609;.</td>
  <td class=xl65><a class="btn btn-lg btn-outline-primary btn-pill
  btn-xs" href="edit_menu_6_4.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 class=xl65 style='height:14.4pt'></td>
  <td class=xl66></td>
  <td class=xl65>009</td>
  <td class=xl66>&#3605;&#3619;&#3623;&#3592; 0003</td>
  <td class=xl67>27/05/2553 14:20 &#3609;.</td>
  <td class=xl65><a class="btn btn-lg btn-outline-primary btn-pill
  btn-xs" href="edit_menu_6_4.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 class=xl65 style='height:14.4pt'></td>
  <td class=xl66></td>
  <td class=xl65>010</td>
  <td class=xl66>&#3605;&#3619;&#3623;&#3592; 0004</td>
  <td class=xl67>27/05/2553 14:20 &#3609;.</td>
  <td class=xl65><a class="btn btn-lg btn-outline-primary btn-pill
  btn-xs" href="edit_menu_6_4.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 class=xl65 style='height:14.4pt'></td>
  <td class=xl66></td>
  <td class=xl65>011</td>
  <td class=xl66>&#3605;&#3619;&#3623;&#3592; 0005</td>
  <td class=xl67>27/05/2553 14:20 &#3609;.</td>
  <td class=xl65><a class="btn btn-lg btn-outline-primary btn-pill
  btn-xs" href="edit_menu_6_4.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 class=xl65 style='height:14.4pt'>C</td>
  <td class=xl66>&#3610;&#3635;&#3619;&#3640;&#3591;&#3619;&#3633;&#3585;&#3625;&#3634;&#3648;&#3588;&#3619;&#3639;&#3656;&#3629;&#3591;&#3592;&#3633;&#3585;&#3619;</td>
  <td class=xl65>012</td>
  <td class=xl66>&#3621;&#3657;&#3634;&#3591; 001</td>
  <td class=xl67>27/05/2553 14:20 &#3609;.</td>
  <td class=xl65><a class="btn btn-lg btn-outline-primary btn-pill
  btn-xs" href="edit_menu_6_4.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 class=xl65 style='height:14.4pt'></td>
  <td class=xl66></td>
  <td class=xl65>013</td>
  <td class=xl66>&#3621;&#3657;&#3634;&#3591; 002</td>
  <td class=xl67>27/05/2553 14:20 &#3609;.</td>
  <td class=xl65><a class="btn btn-lg btn-outline-primary btn-pill
  btn-xs" href="edit_menu_6_4.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 class=xl65 style='height:14.4pt'></td>
  <td class=xl66></td>
  <td class=xl65>014</td>
  <td class=xl66>&#3621;&#3657;&#3634;&#3591; 003</td>
  <td class=xl67>27/05/2553 14:20 &#3609;.</td>
  <td class=xl65><a class="btn btn-lg btn-outline-primary btn-pill
  btn-xs" href="edit_menu_6_4.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 class=xl65 style='height:14.4pt'></td>
  <td class=xl66></td>
  <td class=xl65>015</td>
  <td class=xl66>&#3621;&#3657;&#3634;&#3591; 004</td>
  <td class=xl67>27/05/2553 14:20 &#3609;.</td>
  <td class=xl65><a class="btn btn-lg btn-outline-primary btn-pill
  btn-xs" href="edit_menu_6_4.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 class=xl65 style='height:14.4pt'></td>
  <td class=xl66></td>
  <td class=xl65>016</td>
  <td class=xl66>&#3621;&#3657;&#3634;&#3591; 005</td>
  <td class=xl67>27/05/2553 14:20 &#3609;.</td>
  <td class=xl65><a class="btn btn-lg btn-outline-primary btn-pill
  btn-xs" href="edit_menu_6_4.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 class=xl65 style='height:14.4pt'></td>
  <td class=xl66></td>
  <td class=xl65>017</td>
  <td class=xl66>&#3606;&#3656;&#3634;&#3618;&#3609;&#3657;&#3635;&#3617;&#3633;&#3609;
  001</td>
  <td class=xl67>27/05/2553 14:20 &#3609;.</td>
  <td class=xl65><a class="btn btn-lg btn-outline-primary btn-pill
  btn-xs" href="edit_menu_6_4.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 class=xl65 style='height:14.4pt'></td>
  <td class=xl66></td>
  <td class=xl65>018</td>
  <td class=xl66>&#3606;&#3656;&#3634;&#3618;&#3609;&#3657;&#3635;&#3617;&#3633;&#3609;
  002</td>
  <td class=xl67>27/05/2553 14:20 &#3609;.</td>
  <td class=xl65><a class="btn btn-lg btn-outline-primary btn-pill
  btn-xs" href="edit_menu_6_4.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 class=xl65 style='height:14.4pt'></td>
  <td class=xl66></td>
  <td class=xl65>019</td>
  <td class=xl66>&#3606;&#3656;&#3634;&#3618;&#3609;&#3657;&#3635;&#3617;&#3633;&#3609;
  003</td>
  <td class=xl67>27/05/2553 14:20 &#3609;.</td>
  <td class=xl65><a class="btn btn-lg btn-outline-primary btn-pill
  btn-xs" href="edit_menu_6_4.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 class=xl65 style='height:14.4pt'></td>
  <td class=xl66></td>
  <td class=xl65>020</td>
  <td class=xl66>&#3606;&#3656;&#3634;&#3618;&#3609;&#3657;&#3635;&#3617;&#3633;&#3609;
  004</td>
  <td class=xl67>27/05/2553 14:20 &#3609;.</td>
  <td class=xl65><a class="btn btn-lg btn-outline-primary btn-pill
  btn-xs" href="edit_menu_6_4.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 class=xl65 style='height:14.4pt'></td>
  <td class=xl66></td>
  <td class=xl65>021</td>
  <td class=xl66>&#3606;&#3656;&#3634;&#3618;&#3609;&#3657;&#3635;&#3617;&#3633;&#3609;
  005</td>
  <td class=xl67>27/05/2553 14:20 &#3609;.</td>
  <td class=xl65><a class="btn btn-lg btn-outline-primary btn-pill
  btn-xs" href="edit_menu_6_4.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 class=xl65 style='height:14.4pt'>D</td>
  <td class=xl66>&#3611;&#3619;&#3633;&#3610;&#3611;&#3619;&#3640;&#3591;&#3648;&#3588;&#3619;&#3639;&#3656;&#3629;&#3591;&#3592;&#3633;&#3585;&#3619;</td>
  <td class=xl65>022</td>
  <td class=xl66>&#3595;&#3656;&#3629;&#3617; 001</td>
  <td class=xl67>27/05/2553 14:20 &#3609;.</td>
  <td class=xl65><a class="btn btn-lg btn-outline-primary btn-pill
  btn-xs" href="edit_menu_6_4.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 class=xl65 style='height:14.4pt'></td>
  <td class=xl66></td>
  <td class=xl65>023</td>
  <td class=xl66>&#3595;&#3656;&#3629;&#3617; 002</td>
  <td class=xl67>27/05/2553 14:20 &#3609;.</td>
  <td class=xl65><a class="btn btn-lg btn-outline-primary btn-pill
  btn-xs" href="edit_menu_6_4.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 class=xl65 style='height:14.4pt'></td>
  <td class=xl66></td>
  <td class=xl65>024</td>
  <td class=xl66>&#3595;&#3656;&#3629;&#3617; 003</td>
  <td class=xl67>27/05/2553 14:20 &#3609;.</td>
  <td class=xl65><a class="btn btn-lg btn-outline-primary btn-pill
  btn-xs" href="edit_menu_6_4.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 class=xl65 style='height:14.4pt'></td>
  <td class=xl66></td>
  <td class=xl65>025</td>
  <td class=xl66>&#3595;&#3656;&#3629;&#3617; 004</td>
  <td class=xl67>27/05/2553 14:20 &#3609;.</td>
  <td class=xl65><a class="btn btn-lg btn-outline-primary btn-pill
  btn-xs" href="edit_menu_6_4.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 class=xl65 style='height:14.4pt'></td>
  <td class=xl66></td>
  <td class=xl65>026</td>
  <td class=xl66>&#3595;&#3656;&#3629;&#3617; 005</td>
  <td class=xl67>27/05/2553 14:20 &#3609;.</td>
  <td class=xl65><a class="btn btn-lg btn-outline-primary btn-pill
  btn-xs" href="edit_menu_6_4.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 class=xl65 style='height:14.4pt'></td>
  <td class=xl66></td>
  <td class=xl65>027</td>
  <td class=xl66>&#3595;&#3656;&#3629;&#3617; 006</td>
  <td class=xl67>27/05/2553 14:20 &#3609;.</td>
  <td class=xl65><a class="btn btn-lg btn-outline-primary btn-pill
  btn-xs" href="edit_menu_6_4.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 class=xl65 style='height:14.4pt'>E</td>
  <td class=xl66>&#3648;&#3611;&#3621;&#3637;&#3656;&#3618;&#3609;&#3629;&#3640;&#3611;&#3585;&#3619;&#3603;&#3660;</td>
  <td class=xl65>028</td>
  <td class=xl66>&#3648;&#3611;&#3621;&#3637;&#3656;&#3618;&#3609; 0001</td>
  <td class=xl67>27/05/2553 14:20 &#3609;.</td>
  <td class=xl65><a class="btn btn-lg btn-outline-primary btn-pill
  btn-xs" href="edit_menu_6_4.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 class=xl65 style='height:14.4pt'></td>
  <td class=xl66></td>
  <td class=xl65>029</td>
  <td class=xl66>&#3648;&#3611;&#3621;&#3637;&#3656;&#3618;&#3609; 0002</td>
  <td class=xl67>27/05/2553 14:20 &#3609;.</td>
  <td class=xl65><a class="btn btn-lg btn-outline-primary btn-pill
  btn-xs" href="edit_menu_6_4.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 class=xl65 style='height:14.4pt'></td>
  <td class=xl66></td>
  <td class=xl65>030</td>
  <td class=xl66>&#3648;&#3611;&#3621;&#3637;&#3656;&#3618;&#3609; 0003</td>
  <td class=xl67>27/05/2553 14:20 &#3609;.</td>
  <td class=xl65><a class="btn btn-lg btn-outline-primary btn-pill
  btn-xs" href="edit_menu_6_4.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
 <tr height=19 style='height:14.4pt'>
  <td height=19 class=xl65 style='height:14.4pt'></td>
  <td class=xl66></td>
  <td class=xl65>031</td>
  <td class=xl66>&#3648;&#3611;&#3621;&#3637;&#3656;&#3618;&#3609; 0004</td>
  <td class=xl67>27/05/2553 14:20 &#3609;.</td>
  <td class=xl65><a class="btn btn-lg btn-outline-primary btn-pill
  btn-xs" href="edit_menu_6_4.php"
  type="button"><span class="icon icon-lg
  icon-edit"></span></a><button class="btn
  btn-outline-danger btn-pill btn-xs" data-toggle="modal"
  data-target="#Button_Deleted_New"
  type="button"><span class="icon icon-lg
  icon-close"></span></button></td>
 </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
            </div>
        </div>










    </div>
</div>
<!--  -->
<? include('inc.footer.php');?>
<!--  -->
<? include('inc.footer.script.php');?>
<!--  -->
