<?php
    $select = 'open_menu_1';
    $select2 = 'menu_1';
    $select3 = 'menu_1';
    $select4 = 'menu_1_1';
    $select5 = '';
?>
<!--  -->
<? include('inc.header.php');?>
<!--  -->
<? include('inc.navbar.php');?>
<!--  -->
<? include('inc.menu.php');?>
<!-- <div class="layout-main"> -->
<!--  -->
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <h4 class="m-t-0">
                <!-- <span class="d-ib text-danger">--</span> <span class="icon icon-angle-double-right"></span> -->
                <!-- <span class="d-ib">ข้อมูลหลัก</span> -->
                <span class="d-ib text-primary">ข้อมูลหลัก</span> <span class="icon icon-angle-double-right"></span> โรงงาน <span class="icon icon-angle-double-right"></span> สาขา
                <!-- <span class="d-ib">
                    <a class="title-bar-shortcut" href="#" title="Add to shortcut list" data-container="body" data-toggle-text="Remove from shortcut list" data-trigger="hover" data-placement="right" data-toggle="tooltip">
                        <span class="sr-only">Add to shortcut list</span>
                    </a>
                </span> -->
            </h4>
            <!-- <p class="title-bar-description">
                <small>Latest update on 01/01/2017 - 10.57 am.</small>
            </p> -->
        </div>

        <div class="row gutter-xs">
            <div class="col-xs-12">
                <!--  -->
                <div class="card">
                    <!-- <div class="card-header">
                        <a href="javascript:window.history.back(-1);" class="btn btn-sm btn-outline-primary"><span class="icon icon-long-arrow-left"></span>&nbsp;&nbsp;Back</a>&nbsp;&nbsp;
                        <strong class="">Latest update on 01/01/2017 - 10.57 am.</strong>
                    </div> -->
                    <div class="card-body">
                        <!--  -->
                        <div class="at_add_box">
                            <div class="row">
                                <div class="col-sm-12 text-right">
                                    <button class="btn btn-success" data-toggle="modal" data-target="#menu_1_1_add" type="button"><span class="icon icon-lg icon-plus"></span>&nbsp;&nbsp;เพิ่มใหม่</button>
                                    <? if ($select5 == 'menu_all'){?>
                                        <!-- <a href="menu_1_1_all.php" class="btn btn-info"><span class="icon icon-lg icon-table"></span>&nbsp;&nbsp;แสดงทั้งหมด</a> -->
                                    <? }else{?>
                                        <!-- <a href="menu_1_1.php" class="btn btn-info"><span class="icon icon-lg icon-table"></span>&nbsp;&nbsp;มุมมองปกติ</a> -->
                                    <? } ?>
                                    <a href="menu_1_1_history.php" class="btn btn-info"><span class="icon icon-lg icon-history"></span>&nbsp;&nbsp;ประวัติการแก้ไข</a>
                                    <button onClick="window.print()" class="btn btn-default" type="button"><span class="icon icon-lg icon-print"></span>&nbsp;&nbsp;พิมพ์</button>
                                    <a href="images/SampleXLSFile_212kb.xls" class="btn btn-warning" type="button"><span class="icon icon-lg icon-file-text-o"></span>&nbsp;&nbsp;ส่งออกไฟล์</a>
                                    <!-- <a href="javascript:window.history.back(-1);" class="btn btn-info"><span class="icon icon-long-arrow-left"></span>&nbsp;&nbsp;ย้อนกลับ</a> -->
                                </div>
                            </div>
                        </div>
                        <!--  -->
                        <div class="table-responsive">
                            <table id="demo-datatables-2" class="table table-striped table-bordered table-nowrap dataTable text-center no-footer" cellspacing="0" width="100%">
                                <thead>
                                    <tr class="at_bg_table_blue">
                                        <th class="text-center">รหัสสาขา</th>
                                        <th class="text-center">ชื่อสาขา</th>
                                        <th class="text-center">รหัสสาขา  PROFIT</th>
                                        <th class="text-center">ชื่อ สาขา PROFIT</th>
                                        <th class="text-center">Flag สาขาภายนอก</th>
                                        <th class="text-center">สาขารูดบัตร</th>
                                        <th class="text-center">(prd_bill)</th>
                                        <th class="text-center">(BRN_PRD)</th>
                                        <th class="text-center">(BRN_PHYSICAL)</th>
                                        <th class="text-center">(BRN_PERCENADJ)</th>
                                        <th class="text-center">(prd_billStock)</th>
                                        <th class="text-center">(LOCATDOC)</th>
                                        <th class="text-center">Database  Dos</th>
                                        <th class="text-center">เริ่มใช้งาน</th>
                                        <th class="text-center">ผู้บันทึก</th>
                                        <th class="text-center">วันที่บันทึก</th>
                                        <th class="text-center">ปรับแก้ไข</th>
                                    </tr>
                                </thead>
                                <tbody>
<tr>
    <td>B&amp;S</td>
    <td>สารคาม</td>
    <td>CY</td>
    <td>เชียงยืน</td>
    <td>N</td>
    <td class="at_text_red"><u>M</u></td>
    <td></td>
    <td></td>
    <td></td>
    <td>M</td>
    <td></td>
    <td>S</td>
    <td>BS_DB</td>
    <td>10/08/2561 15:30 น.</td>
    <td>USER0001</td>
    <td>10/08/2561 15:30 น.</td>
    <td class="text-center" style="display: table-cell;">
        <button class="btn btn-lg btn-outline-primary btn-pill btn-xs" data-toggle="modal" data-target="#menu_1_1_edit" type="button">
            <span class="icon icon-lg icon-edit"></span>
        </button>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
<tr>
    <td>BWC</td>
    <td>ศูนย์บวร</td>
    <td>BWC</td>
    <td>ศูนย์บวร</td>
    <td>N</td>
    <td>B</td>
    <td>0</td>
    <td></td>
    <td></td>
    <td>M</td>
    <td>HA</td>
    <td>B</td>
    <td>BWC_DB</td>
    <td>3/07/2561 0:00 น.</td>
    <td>USER0001</td>
    <td>3/07/2561 0:00 น.</td>
    <td class="text-center" style="display: table-cell;">
        <button class="btn btn-lg btn-outline-primary btn-pill btn-xs" data-toggle="modal" data-target="#menu_1_1_edit" type="button">
            <span class="icon icon-lg icon-edit"></span>
        </button>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
<tr>
    <td>CY</td>
    <td>เชียงยืน</td>
    <td>CY</td>
    <td>เชียงยืน</td>
    <td>N</td>
    <td>C</td>
    <td>V</td>
    <td></td>
    <td></td>
    <td>M</td>
    <td>TW</td>
    <td>C</td>
    <td>CY_DB</td>
    <td>4/07/2561 0:00 น.</td>
    <td>USER0001</td>
    <td>4/07/2561 0:00 น.</td>
    <td class="text-center" style="display: table-cell;">
        <button class="btn btn-lg btn-outline-primary btn-pill btn-xs" data-toggle="modal" data-target="#menu_1_1_edit" type="button">
            <span class="icon icon-lg icon-edit"></span>
        </button>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
<tr>
    <td>EXT</td>
    <td>ภายนอก</td>
    <td>EXT</td>
    <td>ภายนอก</td>
    <td>Y</td>
    <td>E</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>E</td>
    <td>&nbsp;</td>
    <td>25/08/2554 0:00 น.</td>
    <td>USER0001</td>
    <td>25/08/2554 0:00 น.</td>
    <td class="text-center" style="display: table-cell;">
        <button class="btn btn-lg btn-outline-primary btn-pill btn-xs" data-toggle="modal" data-target="#menu_1_1_edit" type="button">
            <span class="icon icon-lg icon-edit"></span>
        </button>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
<tr>
    <td>FM</td>
    <td>FM ด้ายโพลี</td>
    <td>FM</td>
    <td>FM โพลี</td>
    <td>N</td>
    <td>F</td>
    <td>L</td>
    <td></td>
    <td></td>
    <td>P</td>
    <td>NF</td>
    <td>F</td>
    <td>&nbsp;</td>
    <td>20/07/2555 10:41 น.</td>
    <td>USER0001</td>
    <td>20/07/2555 10:41 น.</td>
    <td class="text-center" style="display: table-cell;">
        <button class="btn btn-lg btn-outline-primary btn-pill btn-xs" data-toggle="modal" data-target="#menu_1_1_edit" type="button">
            <span class="icon icon-lg icon-edit"></span>
        </button>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
<tr>
    <td>FM1</td>
    <td>FM อวนโพลี</td>
    <td>FM</td>
    <td>FM โพลี</td>
    <td>N</td>
    <td>F</td>
    <td>L</td>
    <td>L</td>
    <td>L</td>
    <td>P</td>
    <td>HF</td>
    <td>F</td>
    <td>FM_DB</td>
    <td>8/02/2553 11:15 น.</td>
    <td>USER0001</td>
    <td>8/02/2553 11:15 น.</td>
    <td class="text-center" style="display: table-cell;">
        <button class="btn btn-lg btn-outline-primary btn-pill btn-xs" data-toggle="modal" data-target="#menu_1_1_edit" type="button">
            <span class="icon icon-lg icon-edit"></span>
        </button>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
<tr>
    <td>KFI</td>
    <td>KFI</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>N</td>
    <td>N</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>&nbsp;</td>
    <td>27/03/2556 13:32 น.</td>
    <td>USER0001</td>
    <td>27/03/2556 13:32 น.</td>
    <td class="text-center" style="display: table-cell;">
        <button class="btn btn-lg btn-outline-primary btn-pill btn-xs" data-toggle="modal" data-target="#menu_1_1_edit" type="button">
            <span class="icon icon-lg icon-edit"></span>
        </button>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
<tr>
    <td>KKF</td>
    <td>เค เค เอฟ</td>
    <td>BWC</td>
    <td>ศูนย์บวร</td>
    <td>N</td>
    <td>K</td>
    <td></td>
    <td></td>
    <td></td>
    <td>M</td>
    <td></td>
    <td>K</td>
    <td>KKF_DB</td>
    <td>2/07/2561 0:00 น.</td>
    <td>USER0001</td>
    <td>2/07/2561 0:00 น.</td>
    <td class="text-center" style="display: table-cell;">
        <button class="btn btn-lg btn-outline-primary btn-pill btn-xs" data-toggle="modal" data-target="#menu_1_1_edit" type="button">
            <span class="icon icon-lg icon-edit"></span>
        </button>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
<tr>
    <td>KKF1</td>
    <td>อวนปั๊ม</td>
    <td>NR</td>
    <td>หนองเรือ</td>
    <td>N</td>
    <td>N</td>
    <td></td>
    <td></td>
    <td></td>
    <td>M</td>
    <td></td>
    <td>R</td>
    <td>NR_DB</td>
    <td>2/07/2553 10:02 น.</td>
    <td>USER0001</td>
    <td>2/07/2553 10:02 น.</td>
    <td class="text-center" style="display: table-cell;">
        <button class="btn btn-lg btn-outline-primary btn-pill btn-xs" data-toggle="modal" data-target="#menu_1_1_edit" type="button">
            <span class="icon icon-lg icon-edit"></span>
        </button>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
<tr>
    <td>KKF2</td>
    <td>สายเอ็น/ด้ายไนล่อน</td>
    <td>KKF2</td>
    <td>สายเอ็น/ด้ายไนล่อน</td>
    <td>N</td>
    <td></td>
    <td>0</td>
    <td></td>
    <td></td>
    <td>T</td>
    <td>N0</td>
    <td></td>
    <td>KKF_DB</td>
    <td>5/01/2561 16:15 น.</td>
    <td>USER0001</td>
    <td>5/01/2561 16:15 น.</td>
    <td class="text-center" style="display: table-cell;">
        <button class="btn btn-lg btn-outline-primary btn-pill btn-xs" data-toggle="modal" data-target="#menu_1_1_edit" type="button">
            <span class="icon icon-lg icon-edit"></span>
        </button>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
<tr>
    <td>KKF4</td>
    <td>อวนรุม/อวนแปรรูป</td>
    <td>KKF4</td>
    <td>อวนรุม/อวนแปรรูป</td>
    <td>N</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>R</td>
    <td></td>
    <td></td>
    <td>&nbsp;</td>
    <td>2/12/2552 21:11 น.</td>
    <td>USER0001</td>
    <td>2/12/2552 21:11 น.</td>
    <td class="text-center" style="display: table-cell;">
        <button class="btn btn-lg btn-outline-primary btn-pill btn-xs" data-toggle="modal" data-target="#menu_1_1_edit" type="button">
            <span class="icon icon-lg icon-edit"></span>
        </button>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
<tr>
    <td>KKF5</td>
    <td>ผลิตอุปกรณ์ฉีด-ปั๊ม</td>
    <td>KKF5</td>
    <td>ผลิตอุปกรณ์ฉีด-ปั๊ม</td>
    <td>N</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>&nbsp;</td>
    <td>14/05/2555 0:00 น.</td>
    <td>USER0001</td>
    <td>14/05/2555 0:00 น.</td>
    <td class="text-center" style="display: table-cell;">
        <button class="btn btn-lg btn-outline-primary btn-pill btn-xs" data-toggle="modal" data-target="#menu_1_1_edit" type="button">
            <span class="icon icon-lg icon-edit"></span>
        </button>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
<tr>
    <td>KR</td>
    <td>เคอาร์</td>
    <td>KR</td>
    <td>เคอาร์</td>
    <td>N</td>
    <td>R</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>&nbsp;</td>
    <td>14/07/2553 13:45 น.</td>
    <td>USER0001</td>
    <td>14/07/2553 13:45 น.</td>
    <td class="text-center" style="display: table-cell;">
        <button class="btn btn-lg btn-outline-primary btn-pill btn-xs" data-toggle="modal" data-target="#menu_1_1_edit" type="button">
            <span class="icon icon-lg icon-edit"></span>
        </button>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
<tr>
    <td>MK</td>
    <td>ตลาด</td>
    <td>MK</td>
    <td>ตลาด</td>
    <td>N</td>
    <td>K</td>
    <td>M</td>
    <td>M</td>
    <td>K</td>
    <td></td>
    <td></td>
    <td>M</td>
    <td>SALEDM</td>
    <td>25/08/2554 0:00 น.</td>
    <td>USER0001</td>
    <td>25/08/2554 0:00 น.</td>
    <td class="text-center" style="display: table-cell;">
        <button class="btn btn-lg btn-outline-primary btn-pill btn-xs" data-toggle="modal" data-target="#menu_1_1_edit" type="button">
            <span class="icon icon-lg icon-edit"></span>
        </button>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
<tr>
    <td>NR</td>
    <td>หนองเรือ</td>
    <td>NR</td>
    <td>หนองเรือ</td>
    <td>N</td>
    <td>N</td>
    <td>G</td>
    <td></td>
    <td></td>
    <td>M</td>
    <td>HH</td>
    <td>R</td>
    <td>NR_DB</td>
    <td>3/07/2561 0:00 น.</td>
    <td>USER0001</td>
    <td>3/07/2561 0:00 น.</td>
    <td class="text-center" style="display: table-cell;">
        <button class="btn btn-lg btn-outline-primary btn-pill btn-xs" data-toggle="modal" data-target="#menu_1_1_edit" type="button">
            <span class="icon icon-lg icon-edit"></span>
        </button>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
<tr>
    <td>XFF</td>
    <td>สาขาเมืองจีน</td>
    <td>XFF</td>
    <td>สาขาเมืองจีน</td>
    <td>Y</td>
    <td>X</td>
    <td>X</td>
    <td>X</td>
    <td>X</td>
    <td></td>
    <td></td>
    <td>X</td>
    <td>&nbsp;</td>
    <td>20/07/2555 10:41 น.</td>
    <td>USER0001</td>
    <td>20/07/2555 10:41 น.</td>
    <td class="text-center" style="display: table-cell;">
        <button class="btn btn-lg btn-outline-primary btn-pill btn-xs" data-toggle="modal" data-target="#menu_1_1_edit" type="button">
            <span class="icon icon-lg icon-edit"></span>
        </button>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
            </div>
        </div>










    </div>
</div>
<!--  -->
<? include('inc.footer.php');?>
<!--  -->
<? include('inc.footer.script.php');?>
<!--  -->
