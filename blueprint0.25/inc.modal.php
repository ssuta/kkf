﻿<div id="modal-edit" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">แก้ไข</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-6 m-b">
                            <label>รหัสสาขา</label>
                            <input class="form-control" type="text" placeholder="B&S">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>ชื่อสาขา</label>
                            <input class="form-control" type="text" placeholder="สารคาม">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>รหัสสาขา PROFIT</label>
                            <input class="form-control" type="text" placeholder="CY">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>ชื่อ สาขา PROFIT</label>
                            <input class="form-control" type="text" placeholder="เชียงยืน">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Flag สาขาภายนอก</label>
                            <input class="form-control" type="text" placeholder="N">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>สาขารูดบัตร</label>
                            <input class="form-control" type="text" placeholder="M">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>(prd_bill)</label>
                            <input class="form-control" type="text" placeholder="-">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>(BRN_PRD)</label>
                            <input class="form-control" type="text" placeholder="-">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>(BRN_PHYSICAL)</label>
                            <input class="form-control" type="text" placeholder="-">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>(BRN_PERCENADJ)</label>
                            <input class="form-control" type="text" placeholder="M">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>(prd_billStock)</label>
                            <input class="form-control" type="text" placeholder="-">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>(LOCATDOC)</label>
                            <input class="form-control" type="text" placeholder="S">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Database  Dos</label>
                            <input class="form-control" type="text" placeholder="BS_DB">
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button onclick="myFunction()" class="btn btn-lg btn-primary" data-dismiss="modal" type="button">บันทึก</button>
                        <button class="btn btn-lg btn-default" data-dismiss="modal" type="button">ยกเลิก</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="order-confirmation" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">ยืนยันสั่งซื้อ</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-4 m-b">
                            <label>เลขที่ Enquiry</label>
                            <input class="form-control" type="text" placeholder="" readonly>
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>เลขที่ Qoutation</label>
                            <input class="form-control" type="text" placeholder="" readonly>
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>สาขา</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>รหัสลูกค้า</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>ชื่อลูกค้า</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>ชื่อประเทศ</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>ประเภทการขาย</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>ประเภทสินค้า</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>รหัสสินค้า</label>
                            <input class="form-control" type="text" placeholder="" value="1119P/013010400">
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>ชื่อสินค้า</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>สีอวน</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>คุณภาพ</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>กระบวนการ</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>ชุบนุ่ม</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>ลักษณะหู</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>ตะเข็บ</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>จำนวน (หน่วย)</label>
							<div class="row">
								<div class="col-xs-6 n-p-r">
									<input class="form-control" type="text" placeholder="">
								</div>
								<div class="col-xs-6">
									<select class="form-control" type="text" placeholder="">
										<option>PC</option>
									</select>
								</div>
							</div>
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>ทอ</label>
							<div class="input-group input-daterange" data-date-autoclose="true" data-date-format="dd/mm/yyyy">
								<input class="form-control" type="text" value="20/06/2555" readonly>
								<span class="input-group-addon">-</span>
								<input class="form-control" type="text" value="20/06/2555" readonly>
							</div>
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>วงจรส่วนหลัง</label>
                            <div class="input-group input-daterange" data-date-autoclose="true" data-date-format="dd/mm/yyyy">
								<input class="form-control" type="text" value="20/06/2555" readonly>
								<span class="input-group-addon">-</span>
								<input class="form-control" type="text" value="20/06/2555" readonly>
							</div>
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>จำนวนชุดทอ</label>
                            <input class="form-control" type="text" placeholder="" value="6" readonly>
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>จำนวนผืนทอ</label>
                            <input class="form-control" type="text" placeholder="" value="4" readonly>
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>ส่งฝ่ายการตลาด</label>
                            <div class="input-group input-daterange" data-date-autoclose="true" data-date-format="dd/mm/yyyy">
								<input class="form-control" type="text" value="20/06/2555" readonly>
								<span class="input-group-addon">-</span>
								<input class="form-control" type="text" value="20/06/2555" readonly>
							</div>
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>วันที่จัดส่งสินค้า</label>
                            <input class="form-control" type="text" placeholder="" value="20/06/2555" readonly>
                        </div>
                    </div>
                    <div class="m-t text-center">
						<div class="text-warning">*หากมีการแก้ไขข้อมูล ระบบจะทำการบันทึกการเปลี่ยนแปลงและยืนยันคำสั่งซื้อ</div>
                        <button onclick="myFunction()" class="btn btn-lg btn-primary" data-dismiss="modal" type="button">ยืนยัน</button>
                        <button class="btn btn-lg btn-default" data-dismiss="modal" type="button">ยกเลิก</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="approve-plan" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">การอนุมัติแผน</h4>
            </div>
            <div class="modal-body">
				<div class="text-detail-modal"><u>หมายเหตุ</u> : ร่างแผนการผลิต ไม่สามารถวางตามความต้องการได้</div>
				<div class="text-detail-modal"><u>เหตุผล</u> : จำนวนเครื่องทอในช่วงดังกล่าวไม่เพียงพอขอยืดระยะเวลาส่งมอบสินค้า</div>
				<br>
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-4 m-b">
                            <label>เลขที่ Enquiry</label>
                            <input class="form-control" type="text" placeholder="" readonly>
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>เลขที่ Qoutation</label>
                            <input class="form-control" type="text" placeholder="" readonly>
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>สาขา</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>รหัสลูกค้า</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>ชื่อลูกค้า</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>ชื่อประเทศ</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>ประเภทการขาย</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>ประเภทสินค้า</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>รหัสสินค้า</label>
                            <input class="form-control" type="text" placeholder="" value="1119P/013010400">
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>ชื่อสินค้า</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>สีอวน</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>คุณภาพ</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>กระบวนการ</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>ชุบนุ่ม</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>ลักษณะหู</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>ตะเข็บ</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>จำนวน (หน่วย)</label>
							<div class="row">
								<div class="col-xs-6 n-p-r">
									<input class="form-control" type="text" placeholder="">
								</div>
								<div class="col-xs-6">
									<select class="form-control" type="text" placeholder="">
										<option>PC</option>
									</select>
								</div>
							</div>
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>ทอ</label>
							<div class="input-group input-daterange" data-date-autoclose="true" data-date-format="dd/mm/yyyy">
								<input class="form-control" type="text" value="20/06/2555" readonly>
								<span class="input-group-addon">-</span>
								<input class="form-control" type="text" value="20/06/2555" readonly>
							</div>
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>วงจรส่วนหลัง</label>
                            <div class="input-group input-daterange" data-date-autoclose="true" data-date-format="dd/mm/yyyy">
								<input class="form-control" type="text" value="20/06/2555" readonly>
								<span class="input-group-addon">-</span>
								<input class="form-control" type="text" value="20/06/2555" readonly>
							</div>
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>จำนวนชุดทอ</label>
                            <input class="form-control" type="text" placeholder="" value="6" readonly>
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>จำนวนผืนทอ</label>
                            <input class="form-control" type="text" placeholder="" value="4" readonly>
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>ส่งฝ่ายการตลาด</label>
                            <div class="input-group input-daterange" data-date-autoclose="true" data-date-format="dd/mm/yyyy">
								<input class="form-control" type="text" value="20/06/2555" readonly>
								<span class="input-group-addon">-</span>
								<input class="form-control" type="text" value="20/06/2555" readonly>
							</div>
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>วันที่จัดส่งสินค้าที่ต้องการ</label>
                            <input class="form-control" type="text" placeholder="" value="20/06/2555" readonly>
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>วันที่จัดส่งสินค้าได้จริง</label>
                            <input class="form-control" type="text" placeholder="" value="20/06/2555" readonly>
                        </div>

						<div class="col-xs-12">
							<label>ตรวจสอบร่างแผนการผลิต : <a href="menu_mk_3.php" target="_blank">http://ctoo.work/blueprint0.50/menu_mk_3.php</a></label>
                        </div>
                    </div>
                    <div class="m-t text-center">
						<div>*หากมีการแก้ไขข้อมูล ระบบจะทำการบันทึกการเปลี่ยนแปลงและทำการอนุมัติแผน</div>
                        <button class="btn btn-lg btn-primary" data-dismiss="modal" type="button">อนุมัติแผน</button>
                        <button class="btn btn-lg btn-warning" data-dismiss="modal" data-toggle="modal" data-target="#modal-reject-plan" type="button">ไม่อนุมัติแผน</button>
                        <button class="btn btn-lg btn-default" data-dismiss="modal" type="button">ยกเลิก</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="modalConfirmInquiry" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">ยืนยันสั่งซื้อ</h4>
            </div>
            <div class="modal-body">
				<div class="text-detail-modal"><u>หมายเหตุ</u> : ร่างแผนการผลิต ไม่สามารถวางตามความต้องการได้</div>
				<div class="text-detail-modal"><u>เหตุผล</u> : จำนวนเครื่องทอในช่วงดังกล่าวไม่เพียงพอขอยืดระยะเวลาส่งมอบสินค้า</div>
				<br>
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-4 m-b">
                            <label>เลขที่ Enquiry</label>
                            <input class="form-control" type="text" placeholder="" readonly>
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>เลขที่ Qoutation</label>
                            <input class="form-control" type="text" placeholder="" readonly>
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>สาขา</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>รหัสลูกค้า</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>ชื่อลูกค้า</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>ชื่อประเทศ</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>ประเภทการขาย</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>ประเภทสินค้า</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>รหัสสินค้า</label>
                            <input class="form-control" type="text" placeholder="" value="1119P/013010400">
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>ชื่อสินค้า</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>สีอวน</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>คุณภาพ</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>กระบวนการ</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>ชุบนุ่ม</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>ลักษณะหู</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>ตะเข็บ</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>จำนวน (หน่วย)</label>
							<div class="row">
								<div class="col-xs-6 n-p-r">
									<input class="form-control" type="text" placeholder="">
								</div>
								<div class="col-xs-6">
									<select class="form-control" type="text" placeholder="">
										<option>PC</option>
									</select>
								</div>
							</div>
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>ทอ</label>
							<div class="input-group input-daterange" data-date-autoclose="true" data-date-format="dd/mm/yyyy">
								<input class="form-control" type="text" value="20/06/2555" readonly>
								<span class="input-group-addon">-</span>
								<input class="form-control" type="text" value="20/06/2555" readonly>
							</div>
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>วงจรส่วนหลัง</label>
                            <div class="input-group input-daterange" data-date-autoclose="true" data-date-format="dd/mm/yyyy">
								<input class="form-control" type="text" value="20/06/2555" readonly>
								<span class="input-group-addon">-</span>
								<input class="form-control" type="text" value="20/06/2555" readonly>
							</div>
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>จำนวนชุดทอ</label>
                            <input class="form-control" type="text" placeholder="" value="6" readonly>
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>จำนวนผืนทอ</label>
                            <input class="form-control" type="text" placeholder="" value="4" readonly>
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>ส่งฝ่ายการตลาด</label>
                            <div class="input-group input-daterange" data-date-autoclose="true" data-date-format="dd/mm/yyyy">
								<input class="form-control" type="text" value="20/06/2555" readonly>
								<span class="input-group-addon">-</span>
								<input class="form-control" type="text" value="20/06/2555" readonly>
							</div>
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>วันที่จัดส่งสินค้าที่ต้องการ</label>
                            <input class="form-control" type="text" placeholder="" value="20/06/2555" readonly>
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>วันที่จัดส่งสินค้าได้จริง</label>
                            <input class="form-control" type="text" placeholder="" value="20/06/2555" readonly>
                        </div>

						<div class="col-xs-12">
							<label>ตรวจสอบร่างแผนการผลิต : <a href="menu_mk_3.php" target="_blank">http://ctoo.work/blueprint0.50/menu_mk_3.php</a></label>
                        </div>
                    </div>
                    <div class="m-t text-center">
						<div>*หากมีการแก้ไขข้อมูล ระบบจะทำการบันทึกการเปลี่ยนแปลงและทำการอนุมัติแผน</div>
                        <button class="btn btn-lg btn-primary" data-dismiss="modal" type="button" onclick="window.location.href='menu_mk_1.php'">ยืนยันสั่งซื้อ</button>
                        <button class="btn btn-lg btn-default" data-dismiss="modal" type="button">ยกเลิก</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="modal-reject-plan" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">ขอให้ทำการแก้ไขร่างแผนใหม่</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12 m-b">
                            <label>ความเห็น / เหตุผล</label>
                            <textarea class="form-control" rows="10" placeholder="กรอกรายละเอียด comment ให้ฝ่ายวางแผน..."></textarea>
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button onclick="myFunction()" class="btn btn-lg btn-primary" data-dismiss="modal" type="button">ยืนยัน</button>
                        <button class="btn btn-lg btn-default" data-dismiss="modal" type="button">ยกเลิก</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="modal-edit-manage-product" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-lg" style="width:1200px;">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">การจัดการ</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-6 m-b">
							<div class="table-responsive">
								<table id="" class="table table-hover table-bordered text-center" cellspacing="0" width="100%">
									<thead>
										<tr class="at_bg_table_blue">
											<th class="text-center middle" rowspan="3">เลขที่ออเดอร์</th>
											<th class="text-center middle" colspan="3">สั่งซื้อ</th>
											<th class="text-center middle" rowspan="3">วันที่สั่งซื้อ</th>
										</tr>
										<tr class="at_bg_table_blue">
											<th class="text-center" colspan="2">จำนวน</th>
											<th class="text-center" rowspan="2">หน่วย</th>
										</tr>
                                        <tr class="at_bg_table_blue">
                                            <th class="text-center">สั่งซื้อ</th>
                                            <th class="text-center">ยังไม่ได้<br>จัดการ</th>
                                        </tr>
									</thead>
									<tbody>
										<tr>
											<td>ORDER002</td>
											<td>200</td>
											<td>0</td>
											<td>PC</td>
											<td>21/07/2555</td>
										</tr>
										<tr>
											<td>ORDER003</td>
											<td>300</td>
											<td>0</td>
											<td>PC</td>
											<td>22/07/2555</td>
										</tr>
										<tr>
											<td>ORDER004</td>
											<td>600</td>
											<td>0</td>
											<td>PC</td>
											<td>25/07/2555</td>
										</tr>
										<tr>
											<td>ORDER005</td>
											<td>700</td>
											<td>0</td>
											<td>PC</td>
											<td>26/07/2555</td>
										</tr>
									</tbody>
								</table>
							</div>
                        </div>
                        <div class="col-xs-6 m-b">
                            <div class="table-responsive">
								<table id="" class="table table-hover table-bordered text-center" cellspacing="0" width="100%">
									<thead>
										<tr class="at_bg_table_blue">
											<th class="text-center">การจัดการสินค้า</th>
											<th class="text-center">เลขที่ออเดอร์</th>
											<th class="text-center">จำนวน</th>
											<th class="text-center">หน่วย</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>ลำดับ 1</td>
											<td>ORDER003</td>
											<td>
												<input type="text" class="form-control text-table t-r" value="100">
											</td>
											<td>PC</td>
										</tr>
										<tr>
											<td>ลำดับ 2</td>
											<td>ORDER002</td>
											<td>
												<input type="text" class="form-control text-table t-r" value="100">
											</td>
											<td>PC</td>
										</tr>
										<tr>
											<td>ลำดับ 3</td>
											<td>ORDER003</td>
											<td>
												<input type="text" class="form-control text-table t-r" value="200">
											</td>
											<td>PC</td>
										</tr>
										<tr>
											<td>ลำดับ 4</td>
											<td>ORDER004</td>
											<td>
												<input type="text" class="form-control text-table t-r" value="600">
											</td>
											<td>PC</td>
										</tr>
										<tr>
											<td>ลำดับ 5</td>
											<td>ORDER005</td>
											<td>
												<input type="text" class="form-control text-table t-r" value="700">
											</td>
											<td>PC</td>
										</tr>
									</tbody>
								</table>
							</div>
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-lg btn-primary" data-dismiss="modal" type="button">บันทึก</button>
                        <button class="btn btn-lg btn-default" data-dismiss="modal" type="button">ยกเลิก</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="modal-graph-detail" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-lg" style="width:1000px;">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">รายละเอียด</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12 m-b">
                            <div class="table-responsive">
								<table id="" class="table table-hover table-bordered text-center table-graph" cellspacing="0" width="100%">
									<tbody>
										<tr>
											<td colspan="2" class="at_bg_table_blue">รหัสเครื่องทอ</td>
											<td>
												<input type="text" class="form-control" value="100" readonly>
                                            </td>
                                            <td colspan="2" class="at_bg_table_blue">สถานะวางแผน</td>
											<td>
												<select class="form-control" value="1">
                                                    <option>ยังไม่ได้ปรับแผน</option>
                                                </select>
                                            </td>
										</tr>
										<tr>
											<td colspan="2" class="at_bg_table_blue">ประเภทงาน</td>
											<td>
												<select class="form-control" value="1">
                                                    <option>เปลี่ยนอะไหล่อุปกรณ์ตามรอบ</option>
                                                </select>
                                            </td>
                                            <td colspan="2" class="at_bg_table_blue">สถานะ</td>
											<td>
												<select class="form-control" value="1">
                                                    <option>ยังไม่แล้วเสร็จ</option>
                                                </select>
                                            </td>
										</tr>
										<tr>
											<td colspan="2" class="at_bg_table_blue">รายละเอียดงาน</td>
											<td>
												<textarea class="form-control">เปลี่ยนตะขอล่าง</textarea>
                                            </td>
                                            <td colspan="2" class="at_bg_table_blue">ผู้บันทึก</td>
											<td>
												<input type="text" class="form-control" value="USER001" readonly>
											</td>
										</tr>
										<tr>
											<td colspan="2" class="at_bg_table_blue">จอดเครื่อง</td>
											<td>
												<input type="text" class="form-control" value="จอด">
                                            </td>
                                            <td colspan="2" class="at_bg_table_blue">วันที่บันทึก</td>
											<td>
												<input type="text" class="form-control" value="24/08/2553" readonly>
											</td>
										</tr>
										<tr>
											<td class="at_bg_table_blue">แผนซ่อมบำรุง</td>
											<td class="at_bg_table_blue">วันที่<br>เวลาเริ่ม<br>เวลาเสร็จ<br>กำลังคน</td>
											<td>
												<input type="text" class="form-control t-r extra" value="25/08/2561">
												<input type="text" class="form-control extra" value="8:00">
												<input type="text" class="form-control extra" value="15:00">
												<input type="text" class="form-control" value="1">
                                            </td>
                                            <td class="at_bg_table_blue">ซ่อมบำรุงจริง</td>
											<td class="at_bg_table_blue">วันที่<br>เวลาเริ่ม<br>เวลาเสร็จ<br>กำลังคน</td>
											<td>
												<input type="text" class="form-control t-r extra" value="">
												<input type="text" class="form-control extra" value="">
												<input type="text" class="form-control extra" value="">
												<input type="text" class="form-control" value="">
                                            </td>
                                        </tr>

                                        
										<tr>
											<td></td>
											<td></td>
                                            <td></td>
                                            <td colspan="2" class="at_bg_table_blue">สาเหตุ</td>
											<td>
												<input type="text" class="form-control" value="">
											</td>
                                        </tr>

                                        <tr>
											<td></td>
											<td></td>
                                            <td></td>
                                            <td colspan="2" class="at_bg_table_blue">ผู้บันทึก</td>
											<td>
												<input type="text" class="form-control" value="">
											</td>
                                        </tr>


                                        <tr>
											<td></td>
											<td></td>
                                            <td></td>
                                            <td colspan="2" class="at_bg_table_blue">วันที่บันทึก</td>
											<td>
												<input type="text" class="form-control" value="">
											</td>
                                        </tr>


                                        
									</tbody>
								</table>
							</div>
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-lg btn-primary" data-dismiss="modal" type="button">บันทึก</button>
                        <button class="btn btn-lg btn-default" data-dismiss="modal" type="button">ยกเลิก</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="modalPlanningCustom" tabindex="-1" role="dialog" class="modal fade">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header bg-primary">
            <button type="button" class="close" data-dismiss="modal">
              <span aria-hidden="true">×</span>
              <span class="sr-only">Close</span>
            </button>
            <h4 class="modal-title">ตัวเลือกวางแผน</h4>
          </div>
          <div class="modal-body">
            <div class="at_add_box">
                <div class="card" aria-expanded="true">
                    <!-- <div class="card-header at_bg_table_light_gray">
                        <div class="card-actions">
                                <button type="button" class="card-action card-toggler" title="Collapse" aria-expanded="true"></button>
                        </div>
                        <strong>เลือกรายการ</strong>
                    </div> -->
                    <div class="card-body" style="display: block;">
                        <form class="form form-horizontal form-detail">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">รหัสรายการ :</label>
                                        <div class="col-sm-8">
                                            <p class="form-control-static">ORDER001</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">สาขา :</label>
                                        <div class="col-sm-8">
                                            <p class="form-control-static">A</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">รหัสสินค้า :</label>
                                        <div class="col-sm-8">
                                            <p class="form-control-static">P001</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">ชื่อสินค้า :</label>
                                        <div class="col-sm-8">
                                            <p class="form-control-static">สินค้า A</p>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">จำนวน :</label>
                                        <div class="col-sm-8">
                                            <p class="form-control-static"><span>850</span> ผืน</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">วันที่ส่งมอบ :</label>
                                        <div class="col-sm-8">
                                            <p class="form-control-static">29/08/2556</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>


             <div class="at_add_box n-m-b">
                <div class="card n-m-b" aria-expanded="true">
                    <!-- <div class="card-header at_bg_table_light_gray">
                        <div class="card-actions">
                                <button type="button" class="card-action card-toggler" title="Collapse" aria-expanded="true"></button>
                        </div>
                        <strong>เลือกรายการ</strong>
                    </div> -->
                    <div class="card-body" style="display: block;">
                        <form class="form form-horizontal">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">วันที่ผลิตเสร็จ :</label>
                                        <div class="col-sm-8">
                                            <div class="input-with-icon">
                                                <input class="form-control" type="text" data-provide="datepicker" data-date-today-btn="linked" value="16/08/2556">
                                                <span class="icon icon-calendar input-icon"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <!-- <label class="col-sm-4 control-label">สาขา :</label>
                                        <div class="col-sm-8">
                                            <p class="form-control-static">A</p>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
          </div>
          <div class="modal-footer text-center-ipt n-p-t">
            <button class="btn btn-primary width-fit" data-dismiss="modal" type="button">ตกลง</button>
            <button class="btn btn-default width-fit" data-dismiss="modal" type="button">ยกเลิก</button>
          </div>
        </div>
      </div>
    </div>

    <div id="modalSavePlanning" tabindex="-1" role="dialog" class="modal fade in">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">
              <span aria-hidden="true">×</span>
              <span class="sr-only">Close</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="text-center">
              <span class="text-success icon icon-check icon-5x"></span>
              <h3 class="text-success">บันทึกแผนสำเร็จ</h3>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                <br>Animi ducimus id itaque totam saepe reiciendis corporis consectetur.</p>
              <div class="m-t-lg">
                <button class="btn btn-success" data-dismiss="modal" type="button" onclick="window.location.href='menu_pp_list.php'">ดำเนินการต่อ</button>
                <button class="btn btn-default" data-dismiss="modal" type="button">ยกเลิก</button>
              </div>
            </div>
          </div>
          <div class="modal-footer"></div>
        </div>
      </div>
    </div>