<div class="layout-footer">
        <div class="layout-footer-body">
            <small class="version">Version 0.5</small>
            <!--<small class="copyright">2017 &copy; Smart Factory By <a href="#" target="_blank">siamdatatronic.com</a></small>-->
        </div>
    </div>
</div>
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<div id="Button_Deleted_New" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">ปิด</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <span class="text-danger icon icon-times-circle icon-5x"></span>
                    <h3 class="text-danger">ยืนยันการลบ</h3>
                    <p>โปรดทราบ เมื่อทำการลบแล้วจะไม่สามารถกลับมาใช้ข้อมูลที่ลบแล้วได้อีก</p>
                    <div class="m-t-lg">
                        <button class="btn btn-outline-danger" data-dismiss="modal" type="button">ตกลง</button>
                        <button class="btn btn-danger" data-dismiss="modal" type="button">ยกเลิก</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>
<!--  -->
<div id="menu_1_1_add" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">ปิด</span>
                </button>
                <h4 class="modal-title">เพิ่ม</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-6 m-b">
                            <label>รหัสสาขา</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>ชื่อสาขา</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>รหัสสาขา PROFIT</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>ชื่อ สาขา PROFIT</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Flag สาขาภายนอก</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>สาขารูดบัตร</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>(prd_bill)</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>(BRN_PRD)</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>(BRN_PHYSICAL)</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>(BRN_PERCENADJ)</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>(prd_billStock)</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>(LOCATDOC)</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Database  Dos</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button onclick="myFunction()" class="btn btn-lg btn-primary" data-dismiss="modal" type="button">บันทึก</button>
                        <button class="btn btn-lg btn-default" data-dismiss="modal" type="button">ยกเลิก</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--  -->
<div id="menu_1_1_edit" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">แก้ไข</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-6 m-b">
                            <label>รหัสสาขา</label>
                            <input class="form-control" type="text" placeholder="B&S">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>ชื่อสาขา</label>
                            <input class="form-control" type="text" placeholder="สารคาม">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>รหัสสาขา PROFIT</label>
                            <input class="form-control" type="text" placeholder="CY">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>ชื่อ สาขา PROFIT</label>
                            <input class="form-control" type="text" placeholder="เชียงยืน">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Flag สาขาภายนอก</label>
							<div class="custom-controls-stacked m-t">
                        <label class="custom-control custom-control-primary custom-checkbox">
                          <input class="custom-control-input" type="checkbox" name="mode" checked="checked">
                          <span class="custom-control-indicator"></span>
                          <span class="custom-control-label">สาขาภายนอก</span>
                        </label>
                      </div>
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>สาขารูดบัตร</label>
                            <input class="form-control" type="text" placeholder="M">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>(prd_bill)</label>
                            <input class="form-control" type="text" placeholder="-">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>(BRN_PRD)</label>
                            <input class="form-control" type="text" placeholder="-">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>(BRN_PHYSICAL)</label>
                            <input class="form-control" type="text" placeholder="-">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>(BRN_PERCENADJ)</label>
                            <input class="form-control" type="text" placeholder="M">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>(prd_billStock)</label>
                            <input class="form-control" type="text" placeholder="-">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>(LOCATDOC)</label>
                            <input class="form-control" type="text" placeholder="S">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Database  Dos</label>
                            <input class="form-control" type="text" placeholder="BS_DB">
                        </div>
                    </div>
                    <div class="m-t text-center">
						<button onclick="myFunction()" class="btn btn-lg btn-primary" data-dismiss="modal" type="button">บันทึก</button>
                        <button class="btn btn-lg btn-default" data-dismiss="modal" type="button">ยกเลิก</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<div id="Button_Addnew_Group_of_product" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Add</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12 m-b">
                            <label>Name group</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-primary" data-dismiss="modal" type="button">Add group</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Edit_Group_of_product" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Edit</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12 m-b">
                            <label>Name group</label>
                            <input class="form-control" type="text" placeholder="Name group..">
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-primary" data-dismiss="modal" type="button">Save</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Addnew_Color_of_product" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Add</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12 m-b">
                            <label>Name color</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-primary" data-dismiss="modal" type="button">Add color</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Edit_Color_of_product" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Edit</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12 m-b">
                            <label>Name color</label>
                            <input class="form-control" type="text" placeholder="Name color..">
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-primary" data-dismiss="modal" type="button">Save</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Addnew_Unit_of_product" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Add</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12 m-b">
                            <label>Name unit</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-primary" data-dismiss="modal" type="button">Add unit</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Edit_Unit_of_product" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Edit</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12 m-b">
                            <label>Name unit</label>
                            <input class="form-control" type="text" placeholder="Name unit..">
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-primary" data-dismiss="modal" type="button">Save</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Addnew_Package_of_product" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Add</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12 m-b">
                            <label>Name package</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-primary" data-dismiss="modal" type="button">Add package</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Edit_Package_of_product" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Edit</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12 m-b">
                            <label>Name package</label>
                            <input class="form-control" type="text" placeholder="Name package..">
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-primary" data-dismiss="modal" type="button">Save</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Addnew_Material_Group_of_material" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-warning">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Add</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12 m-b">
                            <label>Name group</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-warning" data-dismiss="modal" type="button">Add group</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Edit_Material_Group_of_material" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-warning">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Edit</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12 m-b">
                            <label>Name group</label>
                            <input class="form-control" type="text" placeholder="Name group..">
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-warning" data-dismiss="modal" type="button">Save</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Addnew_Material_Status_of_material" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-warning">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Add</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12 m-b">
                            <label>Name status</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-warning" data-dismiss="modal" type="button">Add status</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Edit_Material_Status_of_material" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-warning">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Edit</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12 m-b">
                            <label>Name status</label>
                            <input class="form-control" type="text" placeholder="Name status..">
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-warning" data-dismiss="modal" type="button">Save</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Addnew_Material_Unit_of_material" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-warning">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Add</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12 m-b">
                            <label>Name unit</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-warning" data-dismiss="modal" type="button">Add unit</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Edit_Material_Unit_of_material" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-warning">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Edit</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12 m-b">
                            <label>Name unit</label>
                            <input class="form-control" type="text" placeholder="Name unit..">
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-warning" data-dismiss="modal" type="button">Save</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Addnew_Process_Production_line" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Add</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12 m-b">
                            <label>Name Production line</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-danger" data-dismiss="modal" type="button">Add Production line</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Edit_Process_Production_line" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Edit</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12 m-b">
                            <label>Name production line</label>
                            <input class="form-control" type="text" placeholder="Name production line..">
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-danger" data-dismiss="modal" type="button">Save</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Addnew_Process_Group_of_work" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Add</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12 m-b">
                            <label>Name group of work</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-danger" data-dismiss="modal" type="button">Add Group of work</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Edit_Process_Group_of_work" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Edit</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12 m-b">
                            <label>Name group of work</label>
                            <input class="form-control" type="text" placeholder="Name group of work..">
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-danger" data-dismiss="modal" type="button">Save</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Addnew_Process_work" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Add</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12 m-b">
                            <label>Select group of work</label>
                            <select id="" class="form-control">
                                <option value="">Prepare</option>
                                <option value="">Activity</option>
                                <option value="">Machine</option>
                            </select>
                        </div>
                        <div class="col-xs-12 m-b">
                            <label>Name work</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-danger" data-dismiss="modal" type="button">Add Work</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Edit_Process_work" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Edit</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12 m-b">
                            <label>Select work</label>
                            <select id="" class="form-control">
                                <option value="">Prepare</option>
                                <option value="">Activity</option>
                                <option value="">Machine</option>
                            </select>
                        </div>
                        <div class="col-xs-12 m-b">
                            <label>Name group of work</label>
                            <input class="form-control" type="text" placeholder="Name work..">
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-danger" data-dismiss="modal" type="button">Save</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Addnew_Process_unit" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Add</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12 m-b">
                            <label>Name unit</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-danger" data-dismiss="modal" type="button">Add unit</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Edit_Process_unit" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Edit</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12 m-b">
                            <label>Name unit</label>
                            <input class="form-control" type="text" placeholder="Name unit..">
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-danger" data-dismiss="modal" type="button">Save</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Addnew_Machine_Group_of_machine" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-success">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Add</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12 m-b">
                            <label>Name group</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-success" data-dismiss="modal" type="button">Add group</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Edit_Machine_Group_of_machine" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-success">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Edit</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12 m-b">
                            <label>Name group</label>
                            <input class="form-control" type="text" placeholder="Name group..">
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-success" data-dismiss="modal" type="button">Save</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Addnew_Machine_Status_of_machine" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-success">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Add</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12 m-b">
                            <label>Name status</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-success" data-dismiss="modal" type="button">Add status</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Edit_Machine_Status_of_machine" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-success">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Edit</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12 m-b">
                            <label>Name status</label>
                            <input class="form-control" type="text" placeholder="Name status..">
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-success" data-dismiss="modal" type="button">Save</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Addnew_Program_user_role" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header bg-default">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Add</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12 m-b">
                            <label>Name role management</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <!-- <div class="col-xs-6 m-b">
                            <label>Active</label>
                            <div class="clearfix"></div>
                            <label class="radio-inline">
                                <input type="radio" name="" id="inlineRadio1" value="" checked=""> Enable
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="" id="inlineRadio1" value=""> Disable
                            </label>
                        </div> -->
                        <div class="col-xs-12">
                            <div class="panel">
                                <ul class="nav nav-tabs nav-justified">
                                    <li class="active"><a href="#role_tab_1" data-toggle="tab">Config Data</a></li>
                                    <li><a href="#role_tab_2" data-toggle="tab">Master Data</a></li>
                                    <!-- <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="#dropdown-11" tabindex="-1" data-toggle="tab">@fat</a></li>
                                            <li><a href="#dropdown-12" tabindex="-1" data-toggle="tab">@mdo</a></li>
                                        </ul>
                                    </li> -->
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane fade active in" id="role_tab_1">
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th width=""></th>
                                                    <th class="text-center">View</th>
                                                    <th class="text-center">Add</th>
                                                    <th class="text-center">Edit</th>
                                                    <th class="text-center">Delete</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="success">
                                                    <td width=""><strong>Product</strong></td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="p-l-lg"><span class="icon icon-long-arrow-right"></span> Group of product</td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="p-l-lg"><span class="icon icon-long-arrow-right"></span> Color of product</td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="p-l-lg"><span class="icon icon-long-arrow-right"></span> Unit of product</td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="p-l-lg"><span class="icon icon-long-arrow-right"></span> Package of product</td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                </tr>
                                                <tr class="success">
                                                    <td width=""><strong>Material</strong></td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="p-l-lg"><span class="icon icon-long-arrow-right"></span> Group of material</td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="p-l-lg"><span class="icon icon-long-arrow-right"></span> Status of material</td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="p-l-lg"><span class="icon icon-long-arrow-right"></span> Unit of material</td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                </tr>
                                                <tr class="success">
                                                    <td width=""><strong>Process</strong></td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="p-l-lg"><span class="icon icon-long-arrow-right"></span> Group of work</td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="p-l-lg"><span class="icon icon-long-arrow-right"></span> Work</td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="p-l-lg"><span class="icon icon-long-arrow-right"></span> Unit of work time</td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="p-l-lg"><span class="icon icon-long-arrow-right"></span> Type of work time per unit</td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                </tr>
                                                <tr class="success">
                                                    <td width=""><strong>Machine</strong></td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="p-l-lg"><span class="icon icon-long-arrow-right"></span> Group of machine</td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="p-l-lg"><span class="icon icon-long-arrow-right"></span> Status of machine</td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                </tr>
                                                <tr class="success">
                                                    <td width=""><strong>Program user</strong></td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="p-l-lg"><span class="icon icon-long-arrow-right"></span> Role Management</td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="p-l-lg"><span class="icon icon-long-arrow-right"></span> User Management</td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="tab-pane fade" id="role_tab_2">
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th width=""></th>
                                                    <th class="text-center">View</th>
                                                    <th class="text-center">Add</th>
                                                    <th class="text-center">Edit</th>
                                                    <th class="text-center">Delete</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="success">
                                                    <td width=""><strong>xxxxxxxxxx</strong></td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="p-l-lg"><span class="icon icon-long-arrow-right"></span> xxxxxxxxxx</td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="p-l-lg"><span class="icon icon-long-arrow-right"></span> xxxxxxxxxx</td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-primary" data-dismiss="modal" type="button">Add role</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Edit_Program_user_role" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header bg-default">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Edit</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12 m-b">
                            <label>Name role management</label>
                            <input class="form-control" type="text" placeholder="Name role management..">
                        </div>
                        <div class="col-xs-12">
                            <div class="panel">
                                <ul class="nav nav-tabs nav-justified">
                                    <li class="active"><a href="#role_tab_1" data-toggle="tab">Config Data</a></li>
                                    <li><a href="#role_tab_2" data-toggle="tab">Master Data</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane fade active in" id="role_tab_1">
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th width=""></th>
                                                    <th class="text-center">View</th>
                                                    <th class="text-center">Add</th>
                                                    <th class="text-center">Edit</th>
                                                    <th class="text-center">Delete</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="success">
                                                    <td width=""><strong>Product</strong></td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="" checked="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="" checked="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="" checked="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="" checked="">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="p-l-lg"><span class="icon icon-long-arrow-right"></span> xxxxxxxxxxx</td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="" checked="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="" checked="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="" checked="">
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="" id="" value="" checked="">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-primary" data-dismiss="modal" type="button">Save</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Addnew_Program_user_user" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header bg-default">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Add</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12 m-b">
                            <label>ID</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-12 m-b">
                            <label>Name / Last names</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-12 m-b">
                            <label>Department</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-12 m-b">
                            <label>Position</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-12 m-b">
                            <label>Role</label>
                            <select id="" class="form-control">
                                <option value="">ADMINISTRATOR1</option>
                                <option value="">ADMINISTRATOR2</option>
                                <option value="">SALES & MARKETING MANAGER</option>
                                <option value="">SALES & MARKETING USER1</option>
                                <option value="">SALES & MARKETING USER2</option>
                                <option value="">PRODUCTION MANAGER</option>
                                <option value="">PRODUCTION USER1</option>
                                <option value="">PRODUCTION USER2</option>
                                <option value="">ACCOUNTING MANAGER</option>
                                <option value="">ACCOUNTING USER1</option>
                                <option value="">ENGINEER MANAGER</option>
                                <option value="">ENGINEER USER1</option>
                                <option value="">ENGINEER USER2</option>
                                <option value="">ENGINEER USER3</option>
                                <option value="">ENGINEER USER4</option>
                                <option value="">WAREHOUSE MANAGER</option>
                                <option value="">WAREHOUSE USER1</option>
                                <option value="">WAREHOUSE USER2</option>
                                <option value="">SHIPPING MANAGER</option>
                                <option value="">SHIPPING USER1</option>
                                <option value="">PURCHASING MANAGER</option>
                                <option value="">PURCHASING USER</option>
                                <option value="">MAINTENANCE MANAGER</option>
                                <option value="">MAINTENANCE USER1</option>
                                <option value="">MAINTENANCE USER2</option>
                                <option value="">CEOs</option>
                            </select>
                        </div>
                        <!-- <div class="col-xs-6 m-b">
                            <label>Active</label>
                            <div class="clearfix"></div>
                            <label class="radio-inline">
                                <input type="radio" name="" id="inlineRadio1" value="" checked=""> Enable
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="" id="inlineRadio1" value=""> Disable
                            </label>
                        </div> -->
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-primary" data-dismiss="modal" type="button">Add user</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Edit_Program_user_user" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header bg-default">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Edit</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12 m-b">
                            <label>ID</label>
                            <input class="form-control" type="text" placeholder="Number..">
                        </div>
                        <div class="col-xs-12 m-b">
                            <label>Name / Last names</label>
                            <input class="form-control" type="text" placeholder="Name / Last names..">
                        </div>
                        <div class="col-xs-12 m-b">
                            <label>Department</label>
                            <input class="form-control" type="text" placeholder="Department..">
                        </div>
                        <div class="col-xs-12 m-b">
                            <label>Position</label>
                            <input class="form-control" type="text" placeholder="Position..">
                        </div>
                        <div class="col-xs-12 m-b">
                            <label>Role</label>
                            <select id="" class="form-control">
                                <option value="">ADMINISTRATOR1</option>
                                <option value="">ADMINISTRATOR2</option>
                                <option value="">SALES & MARKETING MANAGER</option>
                                <option value="">SALES & MARKETING USER1</option>
                                <option value="">SALES & MARKETING USER2</option>
                                <option value="">PRODUCTION MANAGER</option>
                                <option value="">PRODUCTION USER1</option>
                                <option value="">PRODUCTION USER2</option>
                                <option value="">ACCOUNTING MANAGER</option>
                                <option value="">ACCOUNTING USER1</option>
                                <option value="">ENGINEER MANAGER</option>
                                <option value="">ENGINEER USER1</option>
                                <option value="">ENGINEER USER2</option>
                                <option value="">ENGINEER USER3</option>
                                <option value="">ENGINEER USER4</option>
                                <option value="">WAREHOUSE MANAGER</option>
                                <option value="">WAREHOUSE USER1</option>
                                <option value="">WAREHOUSE USER2</option>
                                <option value="">SHIPPING MANAGER</option>
                                <option value="">SHIPPING USER1</option>
                                <option value="">PURCHASING MANAGER</option>
                                <option value="">PURCHASING USER</option>
                                <option value="">MAINTENANCE MANAGER</option>
                                <option value="">MAINTENANCE USER1</option>
                                <option value="">MAINTENANCE USER2</option>
                                <option value="">CEOs</option>
                            </select>
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-primary" data-dismiss="modal" type="button">Save</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Addnew_Master_Product_list" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Add</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-6 m-b">
                            <label>Code</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Group</label>
                            <select id="" class="form-control">
                                <option value="">Gill net</option>
                                <option value="">Purse Seine net</option>
                                <option value="">Trawi net</option>
                                <option value="">MONO</option>
                                <option value="">PE Net</option>
                                <option value="">Long line and branch line</option>
                                <option value="">Monofilament twine</option>
                                <option value="">Multifilament twine</option>
                                <option value="">Polyethylene twine</option>
                                <option value="">Raschel net</option>
                                <option value="">Other</option>
                            </select>
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Model product</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Color</label>
                            <select id="" class="form-control">
                                <option value="">Gray</option>
                                <option value="">Green</option>
                                <option value="">Orange</option>
                                <option value="">Light Blue</option>
                                <option value="">Blue</option>
                                <option value="">Red</option>
                                <option value="">Dark green</option>
                                <option value="">Yellow</option>
                                <option value="">White</option>
                            </select>
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Unit</label>
                            <select id="" class="form-control">
                                <option value="">Meters</option>
                                <option value="">Rolls</option>
                                <option value="">Kgs.</option>
                                <option value="">Tons</option>
                                <option value="">Dozens</option>
                                <option value="">Sheets</option>
                            </select>
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Package</label>
                            <select id="" class="form-control">
                                <option value="">Pack type 1</option>
                                <option value="">Pack type 2</option>
                                <option value="">Pack type 3</option>
                                <option value="">Pack type 4</option>
                                <option value="">Pack type 5</option>
                                <option value="">Pack type 6</option>
                            </select>
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-primary" data-dismiss="modal" type="button">Add List</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Addnew_Master_Product_detail_use" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Add</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-6 m-b">
                            <label>Group</label>
                            <select id="" class="form-control">
                                <option value="">Raw nylon</option>
                                <option value="">Raw color</option>
                            </select>
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Name</label>
                            <select id="" class="form-control">
                                <option value="">Polymide black</option>
                                <option value="">Color Dark green</option>
                            </select>
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Unit</label>
                            <select id="" class="form-control">
                                <option value="">Kgs.</option>
                                <option value="">Tanks</option>
                            </select>
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Use</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-primary" data-dismiss="modal" type="button">Add Material</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Edit_Master_Product_detail_use" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Edit</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-6 m-b">
                            <label>Group</label>
                            <select id="" class="form-control">
                                <option value="">Raw nylon</option>
                                <option value="">Raw color</option>
                            </select>
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Name</label>
                            <select id="" class="form-control">
                                <option value="">Polymide black</option>
                                <option value="">Color Dark green</option>
                            </select>
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Unit</label>
                            <select id="" class="form-control">
                                <option value="">Kgs.</option>
                                <option value="">Tanks</option>
                            </select>
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Use</label>
                            <input class="form-control" type="text" placeholder="0.1254">
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-primary" data-dismiss="modal" type="button">Save</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Edit_Master_Product_detail_information" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Edit</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12 m-b">
                            <label>Convert to Std Unit</label>
                            <input class="form-control" type="text" placeholder="25.375">
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-primary" data-dismiss="modal" type="button">Save</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Addnew_Master_Product_detail_flow" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Add</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12 m-b">
                            <label>Production line</label>
                            <select id="" class="form-control">
                                <option value="">Line 3 : Sport Net</option>
                                <option value="">Line 4 : Twine</option>
                                <option value="">Line 5 : Aquaculture Net</option>
                            </select>
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-primary" data-dismiss="modal" type="button">Add Production line</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Edit_Master_Product_detail_flow" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-lg" style="width: 80%;">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Edit</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group m-b-0">
                        <div class="col-xs-12">
                            <div class="">
                                <table id="" class="table table-hover table-bordered text-center m-b-sm" cellspacing="0" width="100%" style="background-color: #FFF; white-space: normal;">
                                    <thead>
                                        <tr class="warning">
                                            <th class="text-center" width="">Line</th>
                                            <th class="text-center" width="">Active</th>
                                            <th class="text-center" width="">No.</th>
                                            <th class="text-center" width="">Group of work</th>
                                            <th class="text-center" width="">Work</th>
                                            <th class="text-center" width="">Number</th>
                                            <th class="text-center" width="">Work time per number</th>
                                            <th class="text-center" width="">Unit</th>
                                            <th class="text-center" width="">Time per unit</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Line 1 : Fishing Net</td>
                                            <td>
                                                <label class="switch switch-primary">
                                                    <input class="switch-input" type="checkbox" checked="checked">
                                                    <span class="switch-track"></span>
                                                    <span class="switch-thumb"></span>
                                                </label>
                                            </td>
                                            <td>1</td>
                                            <td>Prepare</td>
                                            <td class="text-left">Prepare raw material input</td>
                                            <td>2</td>
                                            <td>
                                                <input class="form-control at-custom-select-sm text-center" type="text" placeholder="">
                                            </td>
                                            <td>mins.</td>
                                            <td>per job</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <label class="switch switch-primary">
                                                    <input class="switch-input" type="checkbox" checked="checked">
                                                    <span class="switch-track"></span>
                                                    <span class="switch-thumb"></span>
                                                </label>
                                            </td>
                                            <td>2</td>
                                            <td>Activity</td>
                                            <td class="text-left">Load material</td>
                                            <td>3</td>
                                            <td>
                                                <input class="form-control at-custom-select-sm text-center" type="text" placeholder="">
                                            </td>
                                            <td>mins.</td>
                                            <td>per product 1 Unit</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <label class="switch switch-primary">
                                                    <input class="switch-input" type="checkbox" checked="checked">
                                                    <span class="switch-track"></span>
                                                    <span class="switch-thumb"></span>
                                                </label>
                                            </td>
                                            <td>3</td>
                                            <td>Prepare</td>
                                            <td class="text-left">Prepare/setup machine "Extruding"</td>
                                            <td>2</td>
                                            <td>
                                                <input class="form-control at-custom-select-sm text-center" type="text" placeholder="">
                                            </td>
                                            <td>hrs.</td>
                                            <td>per job</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <label class="switch switch-primary">
                                                    <input class="switch-input" type="checkbox">
                                                    <span class="switch-track"></span>
                                                    <span class="switch-thumb"></span>
                                                </label>
                                            </td>
                                            <td>4</td>
                                            <td>Machine</td>
                                            <td class="text-left">Extruding</td>
                                            <td>5</td>
                                            <td>
                                                <input class="form-control at-custom-select-sm text-center" type="text" placeholder="">
                                            </td>
                                            <td>hrs.</td>
                                            <td>per product 1 Unit</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <label class="switch switch-primary">
                                                    <input class="switch-input" type="checkbox" checked="checked">
                                                    <span class="switch-track"></span>
                                                    <span class="switch-thumb"></span>
                                                </label>
                                            </td>
                                            <td>5</td>
                                            <td>Prepare</td>
                                            <td class="text-left">Prepare/setup machine "Twisting"</td>
                                            <td>10</td>
                                            <td>
                                                <input class="form-control at-custom-select-sm text-center" type="text" placeholder="">
                                            </td>
                                            <td>hrs.</td>
                                            <td>per job</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <label class="switch switch-primary">
                                                    <input class="switch-input" type="checkbox" checked="checked">
                                                    <span class="switch-track"></span>
                                                    <span class="switch-thumb"></span>
                                                </label>
                                            </td>
                                            <td>6</td>
                                            <td>Machine</td>
                                            <td class="text-left">Twisting</td>
                                            <td>15</td>
                                            <td>
                                                <input class="form-control at-custom-select-sm text-center" type="text" placeholder="">
                                            </td>
                                            <td>hrs.</td>
                                            <td>per product 1 Unit</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <label class="switch switch-primary">
                                                    <input class="switch-input" type="checkbox">
                                                    <span class="switch-track"></span>
                                                    <span class="switch-thumb"></span>
                                                </label>
                                            </td>
                                            <td>7</td>
                                            <td>Prepare</td>
                                            <td class="text-left">Prepare/setup machine "Weaving"</td>
                                            <td>2</td>
                                            <td>
                                                <input class="form-control at-custom-select-sm text-center" type="text" placeholder="">
                                            </td>
                                            <td>hrs.</td>
                                            <td>per job</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <label class="switch switch-primary">
                                                    <input class="switch-input" type="checkbox">
                                                    <span class="switch-track"></span>
                                                    <span class="switch-thumb"></span>
                                                </label>
                                            </td>
                                            <td>8</td>
                                            <td>Machine</td>
                                            <td class="text-left">Weaving</td>
                                            <td>20</td>
                                            <td>
                                                <input class="form-control at-custom-select-sm text-center" type="text" placeholder="">
                                            </td>
                                            <td>hrs.</td>
                                            <td>per product 1 Unit</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <label class="switch switch-primary">
                                                    <input class="switch-input" type="checkbox">
                                                    <span class="switch-track"></span>
                                                    <span class="switch-thumb"></span>
                                                </label>
                                            </td>
                                            <td>9</td>
                                            <td>Prepare</td>
                                            <td class="text-left">Prepare/setup machine "Dyeing"</td>
                                            <td>2</td>
                                            <td>
                                                <input class="form-control at-custom-select-sm text-center" type="text" placeholder="">
                                            </td>
                                            <td>hrs.</td>
                                            <td>per job</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <label class="switch switch-primary">
                                                    <input class="switch-input" type="checkbox" checked="checked">
                                                    <span class="switch-track"></span>
                                                    <span class="switch-thumb"></span>
                                                </label>
                                            </td>
                                            <td>10</td>
                                            <td>Machine</td>
                                            <td class="text-left">Dyeing</td>
                                            <td>5</td>
                                            <td>
                                                <input class="form-control at-custom-select-sm text-center" type="text" placeholder="">
                                            </td>
                                            <td>hrs.</td>
                                            <td>per product 1 Unit</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <label class="switch switch-primary">
                                                    <input class="switch-input" type="checkbox" checked="checked">
                                                    <span class="switch-track"></span>
                                                    <span class="switch-thumb"></span>
                                                </label>
                                            </td>
                                            <td>11</td>
                                            <td>Prepare</td>
                                            <td class="text-left">Prepare/setup machine "Stretching"</td>
                                            <td>2</td>
                                            <td>
                                                <input class="form-control at-custom-select-sm text-center" type="text" placeholder="">
                                            </td>
                                            <td>hrs.</td>
                                            <td>per job</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <label class="switch switch-primary">
                                                    <input class="switch-input" type="checkbox">
                                                    <span class="switch-track"></span>
                                                    <span class="switch-thumb"></span>
                                                </label>
                                            </td>
                                            <td>12</td>
                                            <td>Machine</td>
                                            <td class="text-left">Stretching</td>
                                            <td>5</td>
                                            <td>
                                                <input class="form-control at-custom-select-sm text-center" type="text" placeholder="">
                                            </td>
                                            <td>hrs.</td>
                                            <td>per product 1 Unit</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <label class="switch switch-primary">
                                                    <input class="switch-input" type="checkbox">
                                                    <span class="switch-track"></span>
                                                    <span class="switch-thumb"></span>
                                                </label>
                                            </td>
                                            <td>13</td>
                                            <td>Activity</td>
                                            <td class="text-left">Unload material</td>
                                            <td>2</td>
                                            <td>
                                                <input class="form-control at-custom-select-sm text-center" type="text" placeholder="">
                                            </td>
                                            <td>mins.</td>
                                            <td>per product 1 Unit</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <label class="switch switch-primary">
                                                    <input class="switch-input" type="checkbox" checked="checked">
                                                    <span class="switch-track"></span>
                                                    <span class="switch-thumb"></span>
                                                </label>
                                            </td>
                                            <td>14</td>
                                            <td>Activity</td>
                                            <td class="text-left">Chaeck quality</td>
                                            <td>5</td>
                                            <td>
                                                <input class="form-control at-custom-select-sm text-center" type="text" placeholder="">
                                            </td>
                                            <td>mins.</td>
                                            <td>per product 1 Unit</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <label class="switch switch-primary">
                                                    <input class="switch-input" type="checkbox" checked="checked">
                                                    <span class="switch-track"></span>
                                                    <span class="switch-thumb"></span>
                                                </label>
                                            </td>
                                            <td>15</td>
                                            <td>Prepare</td>
                                            <td class="text-left">Prepare/setup machine "Packaging"</td>
                                            <td>2</td>
                                            <td>
                                                <input class="form-control at-custom-select-sm text-center" type="text" placeholder="">
                                            </td>
                                            <td>mins.</td>
                                            <td>per product 1 Unit</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <label class="switch switch-primary">
                                                    <input class="switch-input" type="checkbox" checked="checked">
                                                    <span class="switch-track"></span>
                                                    <span class="switch-thumb"></span>
                                                </label>
                                            </td>
                                            <td>16</td>
                                            <td>Machine</td>
                                            <td class="text-left">Packaging</td>
                                            <td>10</td>
                                            <td>
                                                <input class="form-control at-custom-select-sm text-center" type="text" placeholder="">
                                            </td>
                                            <td>mins.</td>
                                            <td>per product 1 Unit</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-primary" data-dismiss="modal" type="button">Save</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Edit_Master_Product_detail_flow2" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-lg" style="width: 90%;">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Edit</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group m-b-0">
                        <div class="col-xs-12">
                            <div class="">
                                <table id="" class="table table-hover table-bordered text-center m-b-sm" cellspacing="0" width="100%" style="background-color: #FFF; white-space: normal;">
                                    <thead>
                                        <tr class="warning">
                                            <th class="text-center" width="">Line</th>
                                            <th class="text-center" width="">Active</th>
                                            <th class="text-center" width="">No.</th>
                                            <th class="text-center" width="">Group of work</th>
                                            <th class="text-center" width="">Work</th>
                                            <th class="text-center" width="">Number</th>
                                            <th class="text-center" width="">Work time per number</th>
                                            <th class="text-center" width="">Unit</th>
                                            <th class="text-center" width="">Time per unit</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Line 2 : Agriculture</td>
                                            <td>
                                                <label class="switch switch-primary">
                                                    <input class="switch-input" type="checkbox" checked="checked">
                                                    <span class="switch-track"></span>
                                                    <span class="switch-thumb"></span>
                                                </label>
                                            </td>
                                            <td>1</td>
                                            <td>Prepare</td>
                                            <td class="text-left">Prepare raw material input</td>
                                            <td>2</td>
                                            <td>
                                                <input class="form-control at-custom-select-sm text-center" type="text" placeholder="">
                                            </td>
                                            <td>mins.</td>
                                            <td>per job</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <label class="switch switch-primary">
                                                    <input class="switch-input" type="checkbox">
                                                    <span class="switch-track"></span>
                                                    <span class="switch-thumb"></span>
                                                </label>
                                            </td>
                                            <td>2</td>
                                            <td>Activity</td>
                                            <td class="text-left">Load material</td>
                                            <td>3</td>
                                            <td>
                                                <input class="form-control at-custom-select-sm text-center" type="text" placeholder="">
                                            </td>
                                            <td>mins.</td>
                                            <td>per product 1 Unit</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <label class="switch switch-primary">
                                                    <input class="switch-input" type="checkbox" checked="checked">
                                                    <span class="switch-track"></span>
                                                    <span class="switch-thumb"></span>
                                                </label>
                                            </td>
                                            <td>3</td>
                                            <td>Prepare</td>
                                            <td class="text-left">Prepare/setup machine "Extruding"</td>
                                            <td>2</td>
                                            <td>
                                                <input class="form-control at-custom-select-sm text-center" type="text" placeholder="">
                                            </td>
                                            <td>hrs.</td>
                                            <td>per job</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <label class="switch switch-primary">
                                                    <input class="switch-input" type="checkbox">
                                                    <span class="switch-track"></span>
                                                    <span class="switch-thumb"></span>
                                                </label>
                                            </td>
                                            <td>4</td>
                                            <td>Machine</td>
                                            <td class="text-left">Extruding</td>
                                            <td>5</td>
                                            <td>
                                                <input class="form-control at-custom-select-sm text-center" type="text" placeholder="">
                                            </td>
                                            <td>hrs.</td>
                                            <td>per product 1 Unit</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <label class="switch switch-primary">
                                                    <input class="switch-input" type="checkbox">
                                                    <span class="switch-track"></span>
                                                    <span class="switch-thumb"></span>
                                                </label>
                                            </td>
                                            <td>5</td>
                                            <td>Prepare</td>
                                            <td class="text-left">Prepare/setup machine "Twisting"</td>
                                            <td>10</td>
                                            <td>
                                                <input class="form-control at-custom-select-sm text-center" type="text" placeholder="">
                                            </td>
                                            <td>hrs.</td>
                                            <td>per job</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <label class="switch switch-primary">
                                                    <input class="switch-input" type="checkbox">
                                                    <span class="switch-track"></span>
                                                    <span class="switch-thumb"></span>
                                                </label>
                                            </td>
                                            <td>6</td>
                                            <td>Machine</td>
                                            <td class="text-left">Twisting</td>
                                            <td>15</td>
                                            <td>
                                                <input class="form-control at-custom-select-sm text-center" type="text" placeholder="">
                                            </td>
                                            <td>hrs.</td>
                                            <td>per product 1 Unit</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <label class="switch switch-primary">
                                                    <input class="switch-input" type="checkbox" checked="checked">
                                                    <span class="switch-track"></span>
                                                    <span class="switch-thumb"></span>
                                                </label>
                                            </td>
                                            <td>7</td>
                                            <td>Prepare</td>
                                            <td class="text-left">Prepare/setup machine "Weaving"</td>
                                            <td>2</td>
                                            <td>
                                                <input class="form-control at-custom-select-sm text-center" type="text" placeholder="">
                                            </td>
                                            <td>hrs.</td>
                                            <td>per job</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-primary" data-dismiss="modal" type="button">Save</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Addnew_Master_material_list" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header bg-warning">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Add</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-6 m-b">
                            <label>Code</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Group</label>
                            <select id="" class="form-control">
                                <option value="">Raw nylon</option>
                                <option value="">Raw color</option>
                                <option value="">Raw plastic</option>
                            </select>
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Name</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Level to alarm</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Unit</label>
                            <select id="" class="form-control">
                                <option value="">Bags</option>
                                <option value="">fts.</option>
                                <option value="">Dozens</option>
                                <option value="">Tons</option>
                                <option value="">Sheets</option>
                                <option value="">Tanks</option>
                                <option value="">Kgs.</option>
                                <option value="">Pieces</option>
                                <option value="">Meters</option>
                                <option value="">Rolls</option>
                                <option value="">Packs</option>
                                <option value="">Pound</option>
                            </select>
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Supplier</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Convert (Kgs.)</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Cost per unit</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-warning" data-dismiss="modal" type="button">Add Material</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Edit_Master_material_list" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Edit</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group m-b-0">
                        <div class="col-xs-6 m-b">
                            <label>Code</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Group</label>
                            <select id="" class="form-control">
                                <option value="">Raw nylon</option>
                                <option value="">Raw color</option>
                                <option value="">Raw plastic</option>
                            </select>
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Name</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-primary" data-dismiss="modal" type="button">Save</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Addnew_Master_material_Purchase" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header bg-warning">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Add</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-6 m-b">
                            <label>PO No.</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Code</label>
                            <select id="" class="form-control">
                                <option value="">PM00001</option>
                                <option value="">CL00008</option>
                                <option value="">CL00012</option>
                                <option value="">CL00013</option>
                                <option value="">CL00014</option>
                                <option value="">PE00030</option>
                                <option value="">PP00033</option>
                            </select>
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Group</label>
                            <select id="" class="form-control">
                                <option value="">Raw nylon</option>
                                <option value="">Raw color</option>
                                <option value="">Raw plastic</option>
                            </select>
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Name</label>
                            <select id="" class="form-control">
                                <option value="">Polymide type1</option>
                                <option value="">Color Brown</option>
                                <option value="">Color Gray</option>
                                <option value="">Color Green</option>
                                <option value="">Polyethylene</option>
                                <option value="">Polypropylene</option>
                            </select>
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Supplier</label>
                            <select id="" class="form-control">
                                <option value="">Japan</option>
                                <option value="">German</option>
                                <option value="">China</option>
                            </select>
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Unit</label>
                            <select id="" class="form-control">
                                <option value="">Packs</option>
                                <option value="">Bags</option>
                                <option value="">Kgs.</option>
                                <option value="">Tank</option>
                            </select>
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Quantity</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label for="demo-select2-2" class="form-label">Order date / Request date</label>
                            <div class="input-group input-daterange" data-provide="datepicker" data-date-autoclose="true" data-date-format="dd-M-yyyy">
                                <input class="form-control" type="text" value="Order date">
                                <span class="input-group-addon">to</span>
                                <input class="form-control" type="text" value="Request date">
                            </div>
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-warning" data-dismiss="modal" type="button">Add Purchase</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Edit_Master_material_Purchase" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header bg-warning">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Edit</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-6 m-b">
                            <label>PO No.</label>
                            <input class="form-control" type="text" placeholder="PO201608001">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Code</label>
                            <select id="" class="form-control">
                                <option value="">PM00001</option>
                                <option value="">CL00008</option>
                                <option value="">CL00012</option>
                                <option value="">CL00013</option>
                                <option value="">CL00014</option>
                                <option value="">PE00030</option>
                                <option value="">PP00033</option>
                            </select>
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Group</label>
                            <select id="" class="form-control">
                                <option value="">Raw nylon</option>
                                <option value="">Raw color</option>
                                <option value="">Raw plastic</option>
                            </select>
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Name</label>
                            <select id="" class="form-control">
                                <option value="">Polymide type1</option>
                                <option value="">Color Brown</option>
                                <option value="">Color Gray</option>
                                <option value="">Color Green</option>
                                <option value="">Polyethylene</option>
                                <option value="">Polypropylene</option>
                            </select>
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Supplier</label>
                            <select id="" class="form-control">
                                <option value="">Japan</option>
                                <option value="">German</option>
                                <option value="">China</option>
                            </select>
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Unit</label>
                            <select id="" class="form-control">
                                <option value="">Packs</option>
                                <option value="">Bags</option>
                                <option value="">Kgs.</option>
                                <option value="">Tank</option>
                            </select>
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Quantity</label>
                            <input class="form-control" type="text" placeholder="100">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label for="demo-select2-2" class="form-label">Order date / Request date</label>
                            <div class="input-group input-daterange" data-provide="datepicker" data-date-autoclose="true" data-date-format="dd-M-yyyy">
                                <input class="form-control" type="text" value="Order date">
                                <span class="input-group-addon">to</span>
                                <input class="form-control" type="text" value="Request date">
                            </div>
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-warning" data-dismiss="modal" type="button">Save</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Addnew_Master_material_detail_Supplier" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header bg-warning">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Add</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-6 m-b">
                            <label>Supplier</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Level to alarm</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Unit</label>
                            <select id="" class="form-control">
                                <option value="">Bags</option>
                                <option value="">fts.</option>
                                <option value="">Dozens</option>
                                <option value="">Tons</option>
                                <option value="">Sheets</option>
                                <option value="">Tanks</option>
                                <option value="">Kgs.</option>
                                <option value="">Pieces</option>
                                <option value="">Meters</option>
                                <option value="">Rolls</option>
                                <option value="">Packs</option>
                                <option value="">Pound</option>
                            </select>
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Convert to Standard Unit (Kgs.)</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-warning" data-dismiss="modal" type="button">Add Supplier</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Edit_Master_material_detail_Supplier" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Edit</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-6 m-b">
                            <label>Supplier</label>
                            <input class="form-control" type="text" placeholder="Japan">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Level to alarm</label>
                            <input class="form-control" type="text" placeholder="100">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Unit</label>
                            <select id="" class="form-control">
                                <option value="">Bags</option>
                                <option value="">fts.</option>
                                <option value="">Dozens</option>
                                <option value="">Tons</option>
                                <option value="">Sheets</option>
                                <option value="">Tanks</option>
                                <option value="">Kgs.</option>
                                <option value="">Pieces</option>
                                <option value="">Meters</option>
                                <option value="">Rolls</option>
                                <option value="">Packs</option>
                                <option value="">Pound</option>
                            </select>
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Convert to Standard Unit (Kgs.)</label>
                            <input class="form-control" type="text" placeholder="1.000">
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-primary" data-dismiss="modal" type="button">Save</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Addnew_Process_Product_list" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-lg" style="width: 80%;">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Add</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group m-b-0">
                        <div class="col-xs-12">
                            <div class="">
                                <table id="" class="table table-hover table-bordered text-center m-b-sm" cellspacing="0" width="100%" style="background-color: #FFF; white-space: normal;">
                                    <thead>
                                        <tr class="at_bg_table_red">
                                            <th class="text-center">Line</th>
                                            <th class="text-center">No.</th>
                                            <th class="text-center">Group of work</th>
                                            <th class="text-center">Work</th>
                                            <th class="text-center" width="150">Number for calculate</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="">
                                            </td>
                                            <td>1</td>
                                            <td>
                                                <select id="" class="custom-select custom-select-sm">
                                                    <option value="">Prepare</option>
                                                    <option value="">Activity</option>
                                                    <option value="">Machine</option>
                                                </select>
                                            </td>
                                            <td>
                                                <select id="" class="custom-select custom-select-sm">
                                                    <option value="">Prepare raw material input</option>
                                                    <option value="">Prepare/setup machine "Extruding"</option>
                                                    <option value="">Prepare/setup machine "Twisting"</option>
                                                    <option value="">Prepare/setup machine "Weaving"</option>
                                                    <option value="">Prepare/setup machine "Dyeing"</option>
                                                    <option value="">Prepare/setup machine "Stretching"</option>
                                                    <option value="">Prepare/setup machine "Packaging"</option>
                                                    <option value="">Load material</option>
                                                    <option value="">Unload material</option>
                                                    <option value="">Chaeck quality</option>
                                                    <option value="">Extruding</option>
                                                    <option value="">Twisting</option>
                                                    <option value="">Weaving</option>
                                                    <option value="">Dyeing</option>
                                                    <option value="">Stretching</option>
                                                    <option value="">Packaging</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>2</td>
                                            <td>
                                                <select id="" class="custom-select custom-select-sm">
                                                    <option value="">Prepare</option>
                                                    <option value="">Activity</option>
                                                    <option value="">Machine</option>
                                                </select>
                                            </td>
                                            <td>
                                                <select id="" class="custom-select custom-select-sm">
                                                    <option value="">Prepare raw material input</option>
                                                    <option value="">Prepare/setup machine "Extruding"</option>
                                                    <option value="">Prepare/setup machine "Twisting"</option>
                                                    <option value="">Prepare/setup machine "Weaving"</option>
                                                    <option value="">Prepare/setup machine "Dyeing"</option>
                                                    <option value="">Prepare/setup machine "Stretching"</option>
                                                    <option value="">Prepare/setup machine "Packaging"</option>
                                                    <option value="">Load material</option>
                                                    <option value="">Unload material</option>
                                                    <option value="">Chaeck quality</option>
                                                    <option value="">Extruding</option>
                                                    <option value="">Twisting</option>
                                                    <option value="">Weaving</option>
                                                    <option value="">Dyeing</option>
                                                    <option value="">Stretching</option>
                                                    <option value="">Packaging</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>3</td>
                                            <td>
                                                <select id="" class="custom-select custom-select-sm">
                                                    <option value="">Prepare</option>
                                                    <option value="">Activity</option>
                                                    <option value="">Machine</option>
                                                </select>
                                            </td>
                                            <td>
                                                <select id="" class="custom-select custom-select-sm">
                                                    <option value="">Prepare raw material input</option>
                                                    <option value="">Prepare/setup machine "Extruding"</option>
                                                    <option value="">Prepare/setup machine "Twisting"</option>
                                                    <option value="">Prepare/setup machine "Weaving"</option>
                                                    <option value="">Prepare/setup machine "Dyeing"</option>
                                                    <option value="">Prepare/setup machine "Stretching"</option>
                                                    <option value="">Prepare/setup machine "Packaging"</option>
                                                    <option value="">Load material</option>
                                                    <option value="">Unload material</option>
                                                    <option value="">Chaeck quality</option>
                                                    <option value="">Extruding</option>
                                                    <option value="">Twisting</option>
                                                    <option value="">Weaving</option>
                                                    <option value="">Dyeing</option>
                                                    <option value="">Stretching</option>
                                                    <option value="">Packaging</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>4</td>
                                            <td>
                                                <select id="" class="custom-select custom-select-sm">
                                                    <option value="">Prepare</option>
                                                    <option value="">Activity</option>
                                                    <option value="">Machine</option>
                                                </select>
                                            </td>
                                            <td>
                                                <select id="" class="custom-select custom-select-sm">
                                                    <option value="">Prepare raw material input</option>
                                                    <option value="">Prepare/setup machine "Extruding"</option>
                                                    <option value="">Prepare/setup machine "Twisting"</option>
                                                    <option value="">Prepare/setup machine "Weaving"</option>
                                                    <option value="">Prepare/setup machine "Dyeing"</option>
                                                    <option value="">Prepare/setup machine "Stretching"</option>
                                                    <option value="">Prepare/setup machine "Packaging"</option>
                                                    <option value="">Load material</option>
                                                    <option value="">Unload material</option>
                                                    <option value="">Chaeck quality</option>
                                                    <option value="">Extruding</option>
                                                    <option value="">Twisting</option>
                                                    <option value="">Weaving</option>
                                                    <option value="">Dyeing</option>
                                                    <option value="">Stretching</option>
                                                    <option value="">Packaging</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>5</td>
                                            <td>
                                                <select id="" class="custom-select custom-select-sm">
                                                    <option value="">Prepare</option>
                                                    <option value="">Activity</option>
                                                    <option value="">Machine</option>
                                                </select>
                                            </td>
                                            <td>
                                                <select id="" class="custom-select custom-select-sm">
                                                    <option value="">Prepare raw material input</option>
                                                    <option value="">Prepare/setup machine "Extruding"</option>
                                                    <option value="">Prepare/setup machine "Twisting"</option>
                                                    <option value="">Prepare/setup machine "Weaving"</option>
                                                    <option value="">Prepare/setup machine "Dyeing"</option>
                                                    <option value="">Prepare/setup machine "Stretching"</option>
                                                    <option value="">Prepare/setup machine "Packaging"</option>
                                                    <option value="">Load material</option>
                                                    <option value="">Unload material</option>
                                                    <option value="">Chaeck quality</option>
                                                    <option value="">Extruding</option>
                                                    <option value="">Twisting</option>
                                                    <option value="">Weaving</option>
                                                    <option value="">Dyeing</option>
                                                    <option value="">Stretching</option>
                                                    <option value="">Packaging</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>6</td>
                                            <td>
                                                <select id="" class="custom-select custom-select-sm">
                                                    <option value="">Prepare</option>
                                                    <option value="">Activity</option>
                                                    <option value="">Machine</option>
                                                </select>
                                            </td>
                                            <td>
                                                <select id="" class="custom-select custom-select-sm">
                                                    <option value="">Prepare raw material input</option>
                                                    <option value="">Prepare/setup machine "Extruding"</option>
                                                    <option value="">Prepare/setup machine "Twisting"</option>
                                                    <option value="">Prepare/setup machine "Weaving"</option>
                                                    <option value="">Prepare/setup machine "Dyeing"</option>
                                                    <option value="">Prepare/setup machine "Stretching"</option>
                                                    <option value="">Prepare/setup machine "Packaging"</option>
                                                    <option value="">Load material</option>
                                                    <option value="">Unload material</option>
                                                    <option value="">Chaeck quality</option>
                                                    <option value="">Extruding</option>
                                                    <option value="">Twisting</option>
                                                    <option value="">Weaving</option>
                                                    <option value="">Dyeing</option>
                                                    <option value="">Stretching</option>
                                                    <option value="">Packaging</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>7</td>
                                            <td>
                                                <select id="" class="custom-select custom-select-sm">
                                                    <option value="">Prepare</option>
                                                    <option value="">Activity</option>
                                                    <option value="">Machine</option>
                                                </select>
                                            </td>
                                            <td>
                                                <select id="" class="custom-select custom-select-sm">
                                                    <option value="">Prepare raw material input</option>
                                                    <option value="">Prepare/setup machine "Extruding"</option>
                                                    <option value="">Prepare/setup machine "Twisting"</option>
                                                    <option value="">Prepare/setup machine "Weaving"</option>
                                                    <option value="">Prepare/setup machine "Dyeing"</option>
                                                    <option value="">Prepare/setup machine "Stretching"</option>
                                                    <option value="">Prepare/setup machine "Packaging"</option>
                                                    <option value="">Load material</option>
                                                    <option value="">Unload material</option>
                                                    <option value="">Chaeck quality</option>
                                                    <option value="">Extruding</option>
                                                    <option value="">Twisting</option>
                                                    <option value="">Weaving</option>
                                                    <option value="">Dyeing</option>
                                                    <option value="">Stretching</option>
                                                    <option value="">Packaging</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>8</td>
                                            <td>
                                                <select id="" class="custom-select custom-select-sm">
                                                    <option value="">Prepare</option>
                                                    <option value="">Activity</option>
                                                    <option value="">Machine</option>
                                                </select>
                                            </td>
                                            <td>
                                                <select id="" class="custom-select custom-select-sm">
                                                    <option value="">Prepare raw material input</option>
                                                    <option value="">Prepare/setup machine "Extruding"</option>
                                                    <option value="">Prepare/setup machine "Twisting"</option>
                                                    <option value="">Prepare/setup machine "Weaving"</option>
                                                    <option value="">Prepare/setup machine "Dyeing"</option>
                                                    <option value="">Prepare/setup machine "Stretching"</option>
                                                    <option value="">Prepare/setup machine "Packaging"</option>
                                                    <option value="">Load material</option>
                                                    <option value="">Unload material</option>
                                                    <option value="">Chaeck quality</option>
                                                    <option value="">Extruding</option>
                                                    <option value="">Twisting</option>
                                                    <option value="">Weaving</option>
                                                    <option value="">Dyeing</option>
                                                    <option value="">Stretching</option>
                                                    <option value="">Packaging</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>9</td>
                                            <td>
                                                <select id="" class="custom-select custom-select-sm">
                                                    <option value="">Prepare</option>
                                                    <option value="">Activity</option>
                                                    <option value="">Machine</option>
                                                </select>
                                            </td>
                                            <td>
                                                <select id="" class="custom-select custom-select-sm">
                                                    <option value="">Prepare raw material input</option>
                                                    <option value="">Prepare/setup machine "Extruding"</option>
                                                    <option value="">Prepare/setup machine "Twisting"</option>
                                                    <option value="">Prepare/setup machine "Weaving"</option>
                                                    <option value="">Prepare/setup machine "Dyeing"</option>
                                                    <option value="">Prepare/setup machine "Stretching"</option>
                                                    <option value="">Prepare/setup machine "Packaging"</option>
                                                    <option value="">Load material</option>
                                                    <option value="">Unload material</option>
                                                    <option value="">Chaeck quality</option>
                                                    <option value="">Extruding</option>
                                                    <option value="">Twisting</option>
                                                    <option value="">Weaving</option>
                                                    <option value="">Dyeing</option>
                                                    <option value="">Stretching</option>
                                                    <option value="">Packaging</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>10</td>
                                            <td>
                                                <select id="" class="custom-select custom-select-sm">
                                                    <option value="">Prepare</option>
                                                    <option value="">Activity</option>
                                                    <option value="">Machine</option>
                                                </select>
                                            </td>
                                            <td>
                                                <select id="" class="custom-select custom-select-sm">
                                                    <option value="">Prepare raw material input</option>
                                                    <option value="">Prepare/setup machine "Extruding"</option>
                                                    <option value="">Prepare/setup machine "Twisting"</option>
                                                    <option value="">Prepare/setup machine "Weaving"</option>
                                                    <option value="">Prepare/setup machine "Dyeing"</option>
                                                    <option value="">Prepare/setup machine "Stretching"</option>
                                                    <option value="">Prepare/setup machine "Packaging"</option>
                                                    <option value="">Load material</option>
                                                    <option value="">Unload material</option>
                                                    <option value="">Chaeck quality</option>
                                                    <option value="">Extruding</option>
                                                    <option value="">Twisting</option>
                                                    <option value="">Weaving</option>
                                                    <option value="">Dyeing</option>
                                                    <option value="">Stretching</option>
                                                    <option value="">Packaging</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>11</td>
                                            <td>
                                                <select id="" class="custom-select custom-select-sm">
                                                    <option value="">Prepare</option>
                                                    <option value="">Activity</option>
                                                    <option value="">Machine</option>
                                                </select>
                                            </td>
                                            <td>
                                                <select id="" class="custom-select custom-select-sm">
                                                    <option value="">Prepare raw material input</option>
                                                    <option value="">Prepare/setup machine "Extruding"</option>
                                                    <option value="">Prepare/setup machine "Twisting"</option>
                                                    <option value="">Prepare/setup machine "Weaving"</option>
                                                    <option value="">Prepare/setup machine "Dyeing"</option>
                                                    <option value="">Prepare/setup machine "Stretching"</option>
                                                    <option value="">Prepare/setup machine "Packaging"</option>
                                                    <option value="">Load material</option>
                                                    <option value="">Unload material</option>
                                                    <option value="">Chaeck quality</option>
                                                    <option value="">Extruding</option>
                                                    <option value="">Twisting</option>
                                                    <option value="">Weaving</option>
                                                    <option value="">Dyeing</option>
                                                    <option value="">Stretching</option>
                                                    <option value="">Packaging</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>12</td>
                                            <td>
                                                <select id="" class="custom-select custom-select-sm">
                                                    <option value="">Prepare</option>
                                                    <option value="">Activity</option>
                                                    <option value="">Machine</option>
                                                </select>
                                            </td>
                                            <td>
                                                <select id="" class="custom-select custom-select-sm">
                                                    <option value="">Prepare raw material input</option>
                                                    <option value="">Prepare/setup machine "Extruding"</option>
                                                    <option value="">Prepare/setup machine "Twisting"</option>
                                                    <option value="">Prepare/setup machine "Weaving"</option>
                                                    <option value="">Prepare/setup machine "Dyeing"</option>
                                                    <option value="">Prepare/setup machine "Stretching"</option>
                                                    <option value="">Prepare/setup machine "Packaging"</option>
                                                    <option value="">Load material</option>
                                                    <option value="">Unload material</option>
                                                    <option value="">Chaeck quality</option>
                                                    <option value="">Extruding</option>
                                                    <option value="">Twisting</option>
                                                    <option value="">Weaving</option>
                                                    <option value="">Dyeing</option>
                                                    <option value="">Stretching</option>
                                                    <option value="">Packaging</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>13</td>
                                            <td>
                                                <select id="" class="custom-select custom-select-sm">
                                                    <option value="">Prepare</option>
                                                    <option value="">Activity</option>
                                                    <option value="">Machine</option>
                                                </select>
                                            </td>
                                            <td>
                                                <select id="" class="custom-select custom-select-sm">
                                                    <option value="">Prepare raw material input</option>
                                                    <option value="">Prepare/setup machine "Extruding"</option>
                                                    <option value="">Prepare/setup machine "Twisting"</option>
                                                    <option value="">Prepare/setup machine "Weaving"</option>
                                                    <option value="">Prepare/setup machine "Dyeing"</option>
                                                    <option value="">Prepare/setup machine "Stretching"</option>
                                                    <option value="">Prepare/setup machine "Packaging"</option>
                                                    <option value="">Load material</option>
                                                    <option value="">Unload material</option>
                                                    <option value="">Chaeck quality</option>
                                                    <option value="">Extruding</option>
                                                    <option value="">Twisting</option>
                                                    <option value="">Weaving</option>
                                                    <option value="">Dyeing</option>
                                                    <option value="">Stretching</option>
                                                    <option value="">Packaging</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>14</td>
                                            <td>
                                                <select id="" class="custom-select custom-select-sm">
                                                    <option value="">Prepare</option>
                                                    <option value="">Activity</option>
                                                    <option value="">Machine</option>
                                                </select>
                                            </td>
                                            <td>
                                                <select id="" class="custom-select custom-select-sm">
                                                    <option value="">Prepare raw material input</option>
                                                    <option value="">Prepare/setup machine "Extruding"</option>
                                                    <option value="">Prepare/setup machine "Twisting"</option>
                                                    <option value="">Prepare/setup machine "Weaving"</option>
                                                    <option value="">Prepare/setup machine "Dyeing"</option>
                                                    <option value="">Prepare/setup machine "Stretching"</option>
                                                    <option value="">Prepare/setup machine "Packaging"</option>
                                                    <option value="">Load material</option>
                                                    <option value="">Unload material</option>
                                                    <option value="">Chaeck quality</option>
                                                    <option value="">Extruding</option>
                                                    <option value="">Twisting</option>
                                                    <option value="">Weaving</option>
                                                    <option value="">Dyeing</option>
                                                    <option value="">Stretching</option>
                                                    <option value="">Packaging</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>15</td>
                                            <td>
                                                <select id="" class="custom-select custom-select-sm">
                                                    <option value="">Prepare</option>
                                                    <option value="">Activity</option>
                                                    <option value="">Machine</option>
                                                </select>
                                            </td>
                                            <td>
                                                <select id="" class="custom-select custom-select-sm">
                                                    <option value="">Prepare raw material input</option>
                                                    <option value="">Prepare/setup machine "Extruding"</option>
                                                    <option value="">Prepare/setup machine "Twisting"</option>
                                                    <option value="">Prepare/setup machine "Weaving"</option>
                                                    <option value="">Prepare/setup machine "Dyeing"</option>
                                                    <option value="">Prepare/setup machine "Stretching"</option>
                                                    <option value="">Prepare/setup machine "Packaging"</option>
                                                    <option value="">Load material</option>
                                                    <option value="">Unload material</option>
                                                    <option value="">Chaeck quality</option>
                                                    <option value="">Extruding</option>
                                                    <option value="">Twisting</option>
                                                    <option value="">Weaving</option>
                                                    <option value="">Dyeing</option>
                                                    <option value="">Stretching</option>
                                                    <option value="">Packaging</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="">
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-success" data-toggle="modal" data-target="#Button_Plus_Process_Product_list" type="button">
                            <span class="icon icon-lg icon-plus"></span> Add Row
                        </button>
                        <button class="btn btn-danger" data-dismiss="modal" type="button">Add New Process line</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Edit_Process_Product_list" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-lg" style="width: 80%;">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Add</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group m-b-0">
                        <div class="col-xs-12">
                            <div class="">
                                <table id="" class="table table-hover table-bordered text-center m-b-sm" cellspacing="0" width="100%" style="background-color: #FFF; white-space: normal;">
                                    <thead>
                                        <tr class="at_bg_table_blue">
                                            <th class="text-center">Line</th>
                                            <th class="text-center">No.</th>
                                            <th class="text-center">Group of work</th>
                                            <th class="text-center">Work</th>
                                            <th class="text-center" width="150">Number for calculate</th>
                                            <th class="text-center">Modify</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="Line 1 : Fishing Net">
                                            </td>
                                            <td>1</td>
                                            <td>
                                                <select id="" class="custom-select custom-select-sm">
                                                    <option value="">Prepare</option>
                                                    <option value="">Activity</option>
                                                    <option value="">Machine</option>
                                                </select>
                                            </td>
                                            <td>
                                                <select id="" class="custom-select custom-select-sm">
                                                    <option value="">Prepare raw material input</option>
                                                    <option value="">Prepare/setup machine "Extruding"</option>
                                                    <option value="">Prepare/setup machine "Twisting"</option>
                                                    <option value="">Prepare/setup machine "Weaving"</option>
                                                    <option value="">Prepare/setup machine "Dyeing"</option>
                                                    <option value="">Prepare/setup machine "Stretching"</option>
                                                    <option value="">Prepare/setup machine "Packaging"</option>
                                                    <option value="">Load material</option>
                                                    <option value="">Unload material</option>
                                                    <option value="">Chaeck quality</option>
                                                    <option value="">Extruding</option>
                                                    <option value="">Twisting</option>
                                                    <option value="">Weaving</option>
                                                    <option value="">Dyeing</option>
                                                    <option value="">Stretching</option>
                                                    <option value="">Packaging</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="2">
                                            </td>
                                            <td>
                                                <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted" type="button">
                                                    <span class="icon icon-lg icon-close"></span>
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>2</td>
                                            <td>
                                                <select id="" class="custom-select custom-select-sm">
                                                    <option value="">Prepare</option>
                                                    <option value="">Activity</option>
                                                    <option value="">Machine</option>
                                                </select>
                                            </td>
                                            <td>
                                                <select id="" class="custom-select custom-select-sm">
                                                    <option value="">Prepare raw material input</option>
                                                    <option value="">Prepare/setup machine "Extruding"</option>
                                                    <option value="">Prepare/setup machine "Twisting"</option>
                                                    <option value="">Prepare/setup machine "Weaving"</option>
                                                    <option value="">Prepare/setup machine "Dyeing"</option>
                                                    <option value="">Prepare/setup machine "Stretching"</option>
                                                    <option value="">Prepare/setup machine "Packaging"</option>
                                                    <option value="">Load material</option>
                                                    <option value="">Unload material</option>
                                                    <option value="">Chaeck quality</option>
                                                    <option value="">Extruding</option>
                                                    <option value="">Twisting</option>
                                                    <option value="">Weaving</option>
                                                    <option value="">Dyeing</option>
                                                    <option value="">Stretching</option>
                                                    <option value="">Packaging</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="3">
                                            </td>
                                            <td>
                                                <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted" type="button">
                                                    <span class="icon icon-lg icon-close"></span>
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>3</td>
                                            <td>
                                                <select id="" class="custom-select custom-select-sm">
                                                    <option value="">Prepare</option>
                                                    <option value="">Activity</option>
                                                    <option value="">Machine</option>
                                                </select>
                                            </td>
                                            <td>
                                                <select id="" class="custom-select custom-select-sm">
                                                    <option value="">Prepare raw material input</option>
                                                    <option value="">Prepare/setup machine "Extruding"</option>
                                                    <option value="">Prepare/setup machine "Twisting"</option>
                                                    <option value="">Prepare/setup machine "Weaving"</option>
                                                    <option value="">Prepare/setup machine "Dyeing"</option>
                                                    <option value="">Prepare/setup machine "Stretching"</option>
                                                    <option value="">Prepare/setup machine "Packaging"</option>
                                                    <option value="">Load material</option>
                                                    <option value="">Unload material</option>
                                                    <option value="">Chaeck quality</option>
                                                    <option value="">Extruding</option>
                                                    <option value="">Twisting</option>
                                                    <option value="">Weaving</option>
                                                    <option value="">Dyeing</option>
                                                    <option value="">Stretching</option>
                                                    <option value="">Packaging</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="2">
                                            </td>
                                            <td>
                                                <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted" type="button">
                                                    <span class="icon icon-lg icon-close"></span>
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>4</td>
                                            <td>
                                                <select id="" class="custom-select custom-select-sm">
                                                    <option value="">Prepare</option>
                                                    <option value="">Activity</option>
                                                    <option value="">Machine</option>
                                                </select>
                                            </td>
                                            <td>
                                                <select id="" class="custom-select custom-select-sm">
                                                    <option value="">Prepare raw material input</option>
                                                    <option value="">Prepare/setup machine "Extruding"</option>
                                                    <option value="">Prepare/setup machine "Twisting"</option>
                                                    <option value="">Prepare/setup machine "Weaving"</option>
                                                    <option value="">Prepare/setup machine "Dyeing"</option>
                                                    <option value="">Prepare/setup machine "Stretching"</option>
                                                    <option value="">Prepare/setup machine "Packaging"</option>
                                                    <option value="">Load material</option>
                                                    <option value="">Unload material</option>
                                                    <option value="">Chaeck quality</option>
                                                    <option value="">Extruding</option>
                                                    <option value="">Twisting</option>
                                                    <option value="">Weaving</option>
                                                    <option value="">Dyeing</option>
                                                    <option value="">Stretching</option>
                                                    <option value="">Packaging</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="5">
                                            </td>
                                            <td>
                                                <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted" type="button">
                                                    <span class="icon icon-lg icon-close"></span>
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>5</td>
                                            <td>
                                                <select id="" class="custom-select custom-select-sm">
                                                    <option value="">Prepare</option>
                                                    <option value="">Activity</option>
                                                    <option value="">Machine</option>
                                                </select>
                                            </td>
                                            <td>
                                                <select id="" class="custom-select custom-select-sm">
                                                    <option value="">Prepare raw material input</option>
                                                    <option value="">Prepare/setup machine "Extruding"</option>
                                                    <option value="">Prepare/setup machine "Twisting"</option>
                                                    <option value="">Prepare/setup machine "Weaving"</option>
                                                    <option value="">Prepare/setup machine "Dyeing"</option>
                                                    <option value="">Prepare/setup machine "Stretching"</option>
                                                    <option value="">Prepare/setup machine "Packaging"</option>
                                                    <option value="">Load material</option>
                                                    <option value="">Unload material</option>
                                                    <option value="">Chaeck quality</option>
                                                    <option value="">Extruding</option>
                                                    <option value="">Twisting</option>
                                                    <option value="">Weaving</option>
                                                    <option value="">Dyeing</option>
                                                    <option value="">Stretching</option>
                                                    <option value="">Packaging</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="10">
                                            </td>
                                            <td>
                                                <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted" type="button">
                                                    <span class="icon icon-lg icon-close"></span>
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>6</td>
                                            <td>
                                                <select id="" class="custom-select custom-select-sm">
                                                    <option value="">Prepare</option>
                                                    <option value="">Activity</option>
                                                    <option value="">Machine</option>
                                                </select>
                                            </td>
                                            <td>
                                                <select id="" class="custom-select custom-select-sm">
                                                    <option value="">Prepare raw material input</option>
                                                    <option value="">Prepare/setup machine "Extruding"</option>
                                                    <option value="">Prepare/setup machine "Twisting"</option>
                                                    <option value="">Prepare/setup machine "Weaving"</option>
                                                    <option value="">Prepare/setup machine "Dyeing"</option>
                                                    <option value="">Prepare/setup machine "Stretching"</option>
                                                    <option value="">Prepare/setup machine "Packaging"</option>
                                                    <option value="">Load material</option>
                                                    <option value="">Unload material</option>
                                                    <option value="">Chaeck quality</option>
                                                    <option value="">Extruding</option>
                                                    <option value="">Twisting</option>
                                                    <option value="">Weaving</option>
                                                    <option value="">Dyeing</option>
                                                    <option value="">Stretching</option>
                                                    <option value="">Packaging</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="15">
                                            </td>
                                            <td>
                                                <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted" type="button">
                                                    <span class="icon icon-lg icon-close"></span>
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>7</td>
                                            <td>
                                                <select id="" class="custom-select custom-select-sm">
                                                    <option value="">Prepare</option>
                                                    <option value="">Activity</option>
                                                    <option value="">Machine</option>
                                                </select>
                                            </td>
                                            <td>
                                                <select id="" class="custom-select custom-select-sm">
                                                    <option value="">Prepare raw material input</option>
                                                    <option value="">Prepare/setup machine "Extruding"</option>
                                                    <option value="">Prepare/setup machine "Twisting"</option>
                                                    <option value="">Prepare/setup machine "Weaving"</option>
                                                    <option value="">Prepare/setup machine "Dyeing"</option>
                                                    <option value="">Prepare/setup machine "Stretching"</option>
                                                    <option value="">Prepare/setup machine "Packaging"</option>
                                                    <option value="">Load material</option>
                                                    <option value="">Unload material</option>
                                                    <option value="">Chaeck quality</option>
                                                    <option value="">Extruding</option>
                                                    <option value="">Twisting</option>
                                                    <option value="">Weaving</option>
                                                    <option value="">Dyeing</option>
                                                    <option value="">Stretching</option>
                                                    <option value="">Packaging</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="2">
                                            </td>
                                            <td>
                                                <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted" type="button">
                                                    <span class="icon icon-lg icon-close"></span>
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>8</td>
                                            <td>
                                                <select id="" class="custom-select custom-select-sm">
                                                    <option value="">Prepare</option>
                                                    <option value="">Activity</option>
                                                    <option value="">Machine</option>
                                                </select>
                                            </td>
                                            <td>
                                                <select id="" class="custom-select custom-select-sm">
                                                    <option value="">Prepare raw material input</option>
                                                    <option value="">Prepare/setup machine "Extruding"</option>
                                                    <option value="">Prepare/setup machine "Twisting"</option>
                                                    <option value="">Prepare/setup machine "Weaving"</option>
                                                    <option value="">Prepare/setup machine "Dyeing"</option>
                                                    <option value="">Prepare/setup machine "Stretching"</option>
                                                    <option value="">Prepare/setup machine "Packaging"</option>
                                                    <option value="">Load material</option>
                                                    <option value="">Unload material</option>
                                                    <option value="">Chaeck quality</option>
                                                    <option value="">Extruding</option>
                                                    <option value="">Twisting</option>
                                                    <option value="">Weaving</option>
                                                    <option value="">Dyeing</option>
                                                    <option value="">Stretching</option>
                                                    <option value="">Packaging</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="20">
                                            </td>
                                            <td>
                                                <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted" type="button">
                                                    <span class="icon icon-lg icon-close"></span>
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>9</td>
                                            <td>
                                                <select id="" class="custom-select custom-select-sm">
                                                    <option value="">Prepare</option>
                                                    <option value="">Activity</option>
                                                    <option value="">Machine</option>
                                                </select>
                                            </td>
                                            <td>
                                                <select id="" class="custom-select custom-select-sm">
                                                    <option value="">Prepare raw material input</option>
                                                    <option value="">Prepare/setup machine "Extruding"</option>
                                                    <option value="">Prepare/setup machine "Twisting"</option>
                                                    <option value="">Prepare/setup machine "Weaving"</option>
                                                    <option value="">Prepare/setup machine "Dyeing"</option>
                                                    <option value="">Prepare/setup machine "Stretching"</option>
                                                    <option value="">Prepare/setup machine "Packaging"</option>
                                                    <option value="">Load material</option>
                                                    <option value="">Unload material</option>
                                                    <option value="">Chaeck quality</option>
                                                    <option value="">Extruding</option>
                                                    <option value="">Twisting</option>
                                                    <option value="">Weaving</option>
                                                    <option value="">Dyeing</option>
                                                    <option value="">Stretching</option>
                                                    <option value="">Packaging</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="2">
                                            </td>
                                            <td>
                                                <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted" type="button">
                                                    <span class="icon icon-lg icon-close"></span>
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>10</td>
                                            <td>
                                                <select id="" class="custom-select custom-select-sm">
                                                    <option value="">Prepare</option>
                                                    <option value="">Activity</option>
                                                    <option value="">Machine</option>
                                                </select>
                                            </td>
                                            <td>
                                                <select id="" class="custom-select custom-select-sm">
                                                    <option value="">Prepare raw material input</option>
                                                    <option value="">Prepare/setup machine "Extruding"</option>
                                                    <option value="">Prepare/setup machine "Twisting"</option>
                                                    <option value="">Prepare/setup machine "Weaving"</option>
                                                    <option value="">Prepare/setup machine "Dyeing"</option>
                                                    <option value="">Prepare/setup machine "Stretching"</option>
                                                    <option value="">Prepare/setup machine "Packaging"</option>
                                                    <option value="">Load material</option>
                                                    <option value="">Unload material</option>
                                                    <option value="">Chaeck quality</option>
                                                    <option value="">Extruding</option>
                                                    <option value="">Twisting</option>
                                                    <option value="">Weaving</option>
                                                    <option value="">Dyeing</option>
                                                    <option value="">Stretching</option>
                                                    <option value="">Packaging</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="5">
                                            </td>
                                            <td>
                                                <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted" type="button">
                                                    <span class="icon icon-lg icon-close"></span>
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>11</td>
                                            <td>
                                                <select id="" class="custom-select custom-select-sm">
                                                    <option value="">Prepare</option>
                                                    <option value="">Activity</option>
                                                    <option value="">Machine</option>
                                                </select>
                                            </td>
                                            <td>
                                                <select id="" class="custom-select custom-select-sm">
                                                    <option value="">Prepare raw material input</option>
                                                    <option value="">Prepare/setup machine "Extruding"</option>
                                                    <option value="">Prepare/setup machine "Twisting"</option>
                                                    <option value="">Prepare/setup machine "Weaving"</option>
                                                    <option value="">Prepare/setup machine "Dyeing"</option>
                                                    <option value="">Prepare/setup machine "Stretching"</option>
                                                    <option value="">Prepare/setup machine "Packaging"</option>
                                                    <option value="">Load material</option>
                                                    <option value="">Unload material</option>
                                                    <option value="">Chaeck quality</option>
                                                    <option value="">Extruding</option>
                                                    <option value="">Twisting</option>
                                                    <option value="">Weaving</option>
                                                    <option value="">Dyeing</option>
                                                    <option value="">Stretching</option>
                                                    <option value="">Packaging</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="2">
                                            </td>
                                            <td>
                                                <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted" type="button">
                                                    <span class="icon icon-lg icon-close"></span>
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>12</td>
                                            <td>
                                                <select id="" class="custom-select custom-select-sm">
                                                    <option value="">Prepare</option>
                                                    <option value="">Activity</option>
                                                    <option value="">Machine</option>
                                                </select>
                                            </td>
                                            <td>
                                                <select id="" class="custom-select custom-select-sm">
                                                    <option value="">Prepare raw material input</option>
                                                    <option value="">Prepare/setup machine "Extruding"</option>
                                                    <option value="">Prepare/setup machine "Twisting"</option>
                                                    <option value="">Prepare/setup machine "Weaving"</option>
                                                    <option value="">Prepare/setup machine "Dyeing"</option>
                                                    <option value="">Prepare/setup machine "Stretching"</option>
                                                    <option value="">Prepare/setup machine "Packaging"</option>
                                                    <option value="">Load material</option>
                                                    <option value="">Unload material</option>
                                                    <option value="">Chaeck quality</option>
                                                    <option value="">Extruding</option>
                                                    <option value="">Twisting</option>
                                                    <option value="">Weaving</option>
                                                    <option value="">Dyeing</option>
                                                    <option value="">Stretching</option>
                                                    <option value="">Packaging</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="5">
                                            </td>
                                            <td>
                                                <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted" type="button">
                                                    <span class="icon icon-lg icon-close"></span>
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>13</td>
                                            <td>
                                                <select id="" class="custom-select custom-select-sm">
                                                    <option value="">Prepare</option>
                                                    <option value="">Activity</option>
                                                    <option value="">Machine</option>
                                                </select>
                                            </td>
                                            <td>
                                                <select id="" class="custom-select custom-select-sm">
                                                    <option value="">Prepare raw material input</option>
                                                    <option value="">Prepare/setup machine "Extruding"</option>
                                                    <option value="">Prepare/setup machine "Twisting"</option>
                                                    <option value="">Prepare/setup machine "Weaving"</option>
                                                    <option value="">Prepare/setup machine "Dyeing"</option>
                                                    <option value="">Prepare/setup machine "Stretching"</option>
                                                    <option value="">Prepare/setup machine "Packaging"</option>
                                                    <option value="">Load material</option>
                                                    <option value="">Unload material</option>
                                                    <option value="">Chaeck quality</option>
                                                    <option value="">Extruding</option>
                                                    <option value="">Twisting</option>
                                                    <option value="">Weaving</option>
                                                    <option value="">Dyeing</option>
                                                    <option value="">Stretching</option>
                                                    <option value="">Packaging</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="2">
                                            </td>
                                            <td>
                                                <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted" type="button">
                                                    <span class="icon icon-lg icon-close"></span>
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>14</td>
                                            <td>
                                                <select id="" class="custom-select custom-select-sm">
                                                    <option value="">Prepare</option>
                                                    <option value="">Activity</option>
                                                    <option value="">Machine</option>
                                                </select>
                                            </td>
                                            <td>
                                                <select id="" class="custom-select custom-select-sm">
                                                    <option value="">Prepare raw material input</option>
                                                    <option value="">Prepare/setup machine "Extruding"</option>
                                                    <option value="">Prepare/setup machine "Twisting"</option>
                                                    <option value="">Prepare/setup machine "Weaving"</option>
                                                    <option value="">Prepare/setup machine "Dyeing"</option>
                                                    <option value="">Prepare/setup machine "Stretching"</option>
                                                    <option value="">Prepare/setup machine "Packaging"</option>
                                                    <option value="">Load material</option>
                                                    <option value="">Unload material</option>
                                                    <option value="">Chaeck quality</option>
                                                    <option value="">Extruding</option>
                                                    <option value="">Twisting</option>
                                                    <option value="">Weaving</option>
                                                    <option value="">Dyeing</option>
                                                    <option value="">Stretching</option>
                                                    <option value="">Packaging</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="5">
                                            </td>
                                            <td>
                                                <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted" type="button">
                                                    <span class="icon icon-lg icon-close"></span>
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>15</td>
                                            <td>
                                                <select id="" class="custom-select custom-select-sm">
                                                    <option value="">Prepare</option>
                                                    <option value="">Activity</option>
                                                    <option value="">Machine</option>
                                                </select>
                                            </td>
                                            <td>
                                                <select id="" class="custom-select custom-select-sm">
                                                    <option value="">Prepare raw material input</option>
                                                    <option value="">Prepare/setup machine "Extruding"</option>
                                                    <option value="">Prepare/setup machine "Twisting"</option>
                                                    <option value="">Prepare/setup machine "Weaving"</option>
                                                    <option value="">Prepare/setup machine "Dyeing"</option>
                                                    <option value="">Prepare/setup machine "Stretching"</option>
                                                    <option value="">Prepare/setup machine "Packaging"</option>
                                                    <option value="">Load material</option>
                                                    <option value="">Unload material</option>
                                                    <option value="">Chaeck quality</option>
                                                    <option value="">Extruding</option>
                                                    <option value="">Twisting</option>
                                                    <option value="">Weaving</option>
                                                    <option value="">Dyeing</option>
                                                    <option value="">Stretching</option>
                                                    <option value="">Packaging</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="2">
                                            </td>
                                            <td>
                                                <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted" type="button">
                                                    <span class="icon icon-lg icon-close"></span>
                                                </button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-primary" data-toggle="modal" data-target="#Button_Plus_Process_Product_list" type="button">
                            <span class="icon icon-lg icon-plus"></span> Add Row
                        </button>
                        <button class="btn btn-success" data-dismiss="modal" type="button">Save</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Plus_Process_Product_list" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-success">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Add</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group m-b-0">
                        <!-- <div class="col-xs-12 m-b">
                            <label>Line : <span class="text-danger">Line 1 : Fishing Net</span></label>
                        </div> -->
                        <div class="col-xs-12 m-b">
                            <label>Group of work</label>
                            <select id="" class="custom-select">
                                <option value="">Prepare</option>
                                <option value="">Activity</option>
                                <option value="">Machine</option>
                            </select>
                        </div>
                        <div class="col-xs-12 m-b">
                            <label>Work</label>
                            <select id="" class="custom-select">
                                <option value="">Prepare raw material input</option>
                                <option value="">Prepare/setup machine "Extruding"</option>
                                <option value="">Prepare/setup machine "Twisting"</option>
                                <option value="">Prepare/setup machine "Weaving"</option>
                                <option value="">Prepare/setup machine "Dyeing"</option>
                                <option value="">Prepare/setup machine "Stretching"</option>
                                <option value="">Prepare/setup machine "Packaging"</option>
                                <option value="">Load material</option>
                                <option value="">Unload material</option>
                                <option value="">Chaeck quality</option>
                                <option value="">Extruding</option>
                                <option value="">Twisting</option>
                                <option value="">Weaving</option>
                                <option value="">Dyeing</option>
                                <option value="">Stretching</option>
                                <option value="">Packaging</option>
                            </select>
                        </div>
                        <div class="col-xs-12 m-b">
                            <label>Number for calculate</label>
                            <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="">
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-success" data-dismiss="modal" type="button">Add New Process line</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Addnew_Master_machine_list" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header bg-success">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Add</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-6 m-b">
                            <label>Code</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Group</label>
                            <select id="" class="form-control">
                                <option value="">Extruding</option>
                                <option value="">Twisting</option>
                                <option value="">Weaving</option>
                                <option value="">Dyeing</option>
                                <option value="">Stretching</option>
                                <option value="">Packaging</option>
                            </select>
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Name</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Line</label>
                            <select id="" class="form-control">
                                <option value="">Line 1 : Fishing Net</option>
                                <option value="">Line 2 : Agriculture</option>
                                <option value="">Line 3 : Sport Net</option>
                                <option value="">Line 4 : Twine</option>
                                <option value="">Line 5 : Aquaculture Net</option>
                            </select>
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-success" data-dismiss="modal" type="button">Add List</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Edit_Master_machine_list" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Edit</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-6 m-b">
                            <label>Code</label>
                            <input class="form-control" type="text" placeholder="EX0001">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Group</label>
                            <select id="" class="form-control">
                                <option value="">Extruding</option>
                                <option value="">Twisting</option>
                                <option value="">Weaving</option>
                                <option value="">Dyeing</option>
                                <option value="">Stretching</option>
                                <option value="">Packaging</option>
                            </select>
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Name</label>
                            <input class="form-control" type="text" placeholder="Model Y2000 type">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Line</label>
                            <select id="" class="form-control">
                                <option value="">Line 1 : Fishing Net</option>
                                <option value="">Line 2 : Agriculture</option>
                                <option value="">Line 3 : Sport Net</option>
                                <option value="">Line 4 : Twine</option>
                                <option value="">Line 5 : Aquaculture Net</option>
                            </select>
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-primary" data-dismiss="modal" type="button">Save</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Addnew_Unit_Unit_of_product" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-success">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Add</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12 m-b-md">
                            <label>Name Unit</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-12 m-b text-center">
                            <label class="custom-control custom-control-primary custom-radio">
                            <input class="custom-control-input" type="radio" name="" value="none">
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-label">Process</span>
                            </label>
                            <label class="custom-control custom-control-primary custom-radio">
                            <input class="custom-control-input" type="radio" name="" value="none">
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-label">Material</span>
                            </label>
                            <label class="custom-control custom-control-primary custom-radio">
                            <input class="custom-control-input" type="radio" name="" value="none">
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-label">Product</span>
                            </label>
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-success" data-dismiss="modal" type="button">Add Unit</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Edit_Unit_Unit_of_product" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-success">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Edit</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12 m-b-md">
                            <label>Name Unit</label>
                            <input class="form-control" type="text" placeholder="Name Unit..">
                        </div>
                        <div class="col-xs-12 m-b text-center">
                            <label class="custom-control custom-control-primary custom-radio">
                            <input class="custom-control-input" type="radio" name="" value="none" checked="">
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-label">Process</span>
                            </label>
                            <label class="custom-control custom-control-primary custom-radio">
                            <input class="custom-control-input" type="radio" name="" value="none">
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-label">Material</span>
                            </label>
                            <label class="custom-control custom-control-primary custom-radio">
                            <input class="custom-control-input" type="radio" name="" value="none">
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-label">Product</span>
                            </label>
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-success" data-dismiss="modal" type="button">Save</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Addnew_status_new" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-success">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Add</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12 m-b-md">
                            <label>Name status</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-success" data-dismiss="modal" type="button">Add status</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Edit_status_new" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-success">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Edit</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12 m-b-md">
                            <label>Name status</label>
                            <input class="form-control" type="text" placeholder="Name status..">
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-success" data-dismiss="modal" type="button">Save</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Addnew_Planning_status" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Add</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12 m-b">
                            <label>Name status</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-primary" data-dismiss="modal" type="button">Add status</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Edit_Planning_status" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Edit</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12 m-b">
                            <label>Name status</label>
                            <input class="form-control" type="text" placeholder="Name status..">
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-primary" data-dismiss="modal" type="button">Save</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Addnew_Planning_Remark" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Add</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12 m-b">
                            <label>Name remark</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-primary" data-dismiss="modal" type="button">Add Remark</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Edit_Planning_Remark" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Edit</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12 m-b">
                            <label>Name remark</label>
                            <input class="form-control" type="text" placeholder="Name remark..">
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-primary" data-dismiss="modal" type="button">Save</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Addnew_Planning_Warning" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Add</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12 m-b">
                            <label>Name warning</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-primary" data-dismiss="modal" type="button">Add Warning</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Edit_Planning_Warning" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Edit</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12 m-b">
                            <label>Name warning</label>
                            <input class="form-control" type="text" placeholder="Name warning..">
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-primary" data-dismiss="modal" type="button">Save</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Addnew_Planning_Planning_list" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Add</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-6 m-b">
                            <label>Order No.</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Name</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>

                        <div class="col-xs-6 m-b">
                            <label>Color</label>
                            <select class="form-control">
                                <option>Black</option>
                                <option>Blue</option>
                                <option>Light Blue</option>
                                <option>Gray</option>
                                <option>Green</option>
                                <option>Dark green</option>
                                <option>Brown</option>
                                <option>Light brown</option>
                                <option>Orange</option>
                                <option>Pink</option>
                                <option>Red</option>
                                <option>White</option>
                                <option>Yellow</option>
                            </select>
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Quantity</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Unit</label>
                            <select class="form-control">
                                <option>Bags</option>
                                <option>fts.</option>
                                <option>Dozens</option>
                                <option>Tons</option>
                                <option>Sheets</option>
                                <option>Tanks</option>
                                <option>Kgs.</option>
                                <option>Pieces</option>
                                <option>Meters</option>
                                <option>Rolls</option>
                                <option>Packs</option>
                                <option>Pound</option>
                            </select>
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Package</label>
                            <select class="form-control">
                                <option>Roll ∅5cm.</option>
                                <option>Roll ∅10cm.</option>
                                <option>Roll ∅30cm.</option>
                                <option>Roll ∅1m.</option>
                                <option>Roll ∅2m.</option>
                                <option>Pack type1</option>
                                <option>Pack type2</option>
                                <option>Pack type3</option>
                                <option>Rell type1</option>
                                <option>Rell type2</option>
                                <option>Rell type3</option>
                            </select>
                        </div>
                        <div class="col-xs-12 m-b">
                            <label for="demo-select2-2" class="form-label">Order date / Shipping date</label>
                            <div class="input-group input-daterange" data-provide="datepicker" data-date-autoclose="true" data-date-format="dd-M-yyyy">
                                <input class="form-control" type="text" value="Order date">
                                <span class="input-group-addon">to</span>
                                <input class="form-control" type="text" value="Shipping date">
                            </div>
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-primary" data-dismiss="modal" type="button">Add order row</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Edit_Planning_Planning_list" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Edit</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-6 m-b">
                            <label>Order No.</label>
                            <input class="form-control" type="text" placeholder="201608001">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Name</label>
                            <input class="form-control" type="text" placeholder="NYLON MULTI-MONOFILAMENT NET">
                        </div>

                        <div class="col-xs-6 m-b">
                            <label>Color</label>
                            <select class="form-control">
                                <option>Black</option>
                                <option>Blue</option>
                                <option>Light Blue</option>
                                <option>Gray</option>
                                <option>Green</option>
                                <option>Dark green</option>
                                <option>Brown</option>
                                <option>Light brown</option>
                                <option>Orange</option>
                                <option>Pink</option>
                                <option>Red</option>
                                <option>White</option>
                                <option>Yellow</option>
                            </select>
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Quantity</label>
                            <input class="form-control" type="text" placeholder="10">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Unit</label>
                            <select class="form-control">
                                <option>Bags</option>
                                <option>fts.</option>
                                <option>Dozens</option>
                                <option>Tons</option>
                                <option>Sheets</option>
                                <option>Tanks</option>
                                <option>Kgs.</option>
                                <option>Pieces</option>
                                <option>Meters</option>
                                <option>Rolls</option>
                                <option>Packs</option>
                                <option>Pound</option>
                            </select>
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Package</label>
                            <select class="form-control">
                                <option>Roll ∅5cm.</option>
                                <option>Roll ∅10cm.</option>
                                <option>Roll ∅30cm.</option>
                                <option>Roll ∅1m.</option>
                                <option>Roll ∅2m.</option>
                                <option>Pack type1</option>
                                <option>Pack type2</option>
                                <option>Pack type3</option>
                                <option>Rell type1</option>
                                <option>Rell type2</option>
                                <option>Rell type3</option>
                            </select>
                        </div>
                        <div class="col-xs-12 m-b">
                            <label for="demo-select2-2" class="form-label">Order date / Shipping date</label>
                            <div class="input-group input-daterange" data-provide="datepicker" data-date-autoclose="true" data-date-format="dd-M-yyyy">
                                <input class="form-control" type="text" value="Order date">
                                <span class="input-group-addon">to</span>
                                <input class="form-control" type="text" value="Shipping date">
                            </div>
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-primary" data-dismiss="modal" type="button">Save</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Addnew_Planning_Planning_Schedule" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-md" style="width: 80%;">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Select order</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group m-b-0">
                        <!-- <div class="col-xs-12 m-b">
                            <button class="btn btn-xs btn-danger" data-toggle="modal" data-target="#" type="button"><span class="icon icon-plus"></span>&nbsp;&nbsp;Show Planned</button>
                            <button class="btn btn-xs btn-default" data-toggle="modal" data-target="#" type="button"><span class="icon icon-plus"></span>&nbsp;&nbsp;Show Order</button>
                        </div> -->
                        <div class="col-xs-12 m-b-0">
                            <div class="panel">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#Order" data-toggle="tab">Order</a></li>
                                    <li><a href="#Planet" data-toggle="tab">Planet (not fully)</a></li>
                                    <li><a href="#Planned" data-toggle="tab">Planned</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane fade active in" id="Order">
                                        <table id="" class="table table-hover table-bordered text-center m-b-0" cellspacing="0" width="100%">
                                            <thead>
                                                <tr class="at_bg_table_blue">
                                                    <th class="text-center">
                                                        <label class="custom-control custom-control-primary custom-checkbox">
                                                        <input class="custom-control-input" type="checkbox" name="mode">
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-label">
                                                            <span class="icon icon-caret-down"></span>
                                                        </span>
                                                        </label>
                                                    </th>
                                                    <th class="text-center">No.</th>
                                                    <th class="text-center">Name</th>
                                                    <th class="text-center">Color</th>
                                                    <th class="text-center">Quantity</th>
                                                    <th class="text-center">Unit</th>
                                                    <th class="text-center">Package</th>
                                                    <th class="text-center">Order date</th>
                                                    <th class="text-center">Shipping date</th>
                                                    <th class="text-center">Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <label class="custom-control custom-control-primary custom-checkbox">
                                                        <input class="custom-control-input" type="checkbox" name="mode">
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-label"></span>
                                                        </label>
                                                    </td>
                                                    <td>201608001</td>
                                                    <td class="text-left">RASCHEL NETS</td>
                                                    <td>Dark green</td>
                                                    <td>50</td>
                                                    <td>Meters</td>
                                                    <td>Roll ∅5cm.</td>
                                                    <td>01-Aug-17</td>
                                                    <td>30-Nov-17</td>
                                                    <td class="text-success">Order</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <label class="custom-control custom-control-primary custom-checkbox">
                                                        <input class="custom-control-input" type="checkbox" name="mode">
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-label"></span>
                                                        </label>
                                                    </td>
                                                    <td>201608002</td>
                                                    <td class="text-left">NYLON MULTIFILAMENT KNOTTED NETS</td>
                                                    <td>Yellow</td>
                                                    <td>1000</td>
                                                    <td>Rolls</td>
                                                    <td>Roll ∅10cm.</td>
                                                    <td>04-Aug-17</td>
                                                    <td>09-Oct-17</td>
                                                    <td class="text-success">Order</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <label class="custom-control custom-control-primary custom-checkbox">
                                                        <input class="custom-control-input" type="checkbox" name="mode">
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-label"></span>
                                                        </label>
                                                    </td>
                                                    <td>201608003</td>
                                                    <td class="text-left">NYLON MULTIFILAMENT KNOTTED NETS</td>
                                                    <td>Red</td>
                                                    <td>10</td>
                                                    <td>Meters</td>
                                                    <td>Roll ∅30cm.</td>
                                                    <td>07-Aug-17</td>
                                                    <td>21-Oct-17</td>
                                                    <td class="text-success">Order</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <label class="custom-control custom-control-primary custom-checkbox">
                                                        <input class="custom-control-input" type="checkbox" name="mode">
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-label"></span>
                                                        </label>
                                                    </td>
                                                    <td>201608004</td>
                                                    <td class="text-left">NYLON MULTIFILAMENT KNOTTED NETS</td>
                                                    <td>Brown</td>
                                                    <td>200</td>
                                                    <td>Meters</td>
                                                    <td>Roll ∅2m.</td>
                                                    <td>13-Aug-17</td>
                                                    <td>14-Nov-17</td>
                                                    <td class="text-success">Order</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <label class="custom-control custom-control-primary custom-checkbox">
                                                        <input class="custom-control-input" type="checkbox" name="mode">
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-label"></span>
                                                        </label>
                                                    </td>
                                                    <td>201608005</td>
                                                    <td class="text-left">NYLON MULTIFILAMENT KNOTTED NETS</td>
                                                    <td>White</td>
                                                    <td>70</td>
                                                    <td>Rolls</td>
                                                    <td>Pack type1</td>
                                                    <td>16-Aug-17</td>
                                                    <td>26-Nov-17</td>
                                                    <td class="text-success">Order</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <label class="custom-control custom-control-primary custom-checkbox">
                                                        <input class="custom-control-input" type="checkbox" name="mode">
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-label"></span>
                                                        </label>
                                                    </td>
                                                    <td>201608006</td>
                                                    <td class="text-left">NYLON MULTIFILAMENT KNOTTED NETS</td>
                                                    <td>Green</td>
                                                    <td>200</td>
                                                    <td>Meters</td>
                                                    <td>Pack type2</td>
                                                    <td>19-Aug-17</td>
                                                    <td>05-Oct-17</td>
                                                    <td class="text-success">Order</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="tab-pane fade" id="Planet">
                                        <table id="" class="table table-hover table-bordered text-center m-b-0" cellspacing="0" width="100%">
                                            <thead>
                                                <tr class="at_bg_table_blue">
                                                    <th class="text-center">
                                                        <label class="custom-control custom-control-primary custom-checkbox">
                                                        <input class="custom-control-input" type="checkbox" name="mode">
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-label">
                                                            <span class="icon icon-caret-down"></span>
                                                        </span>
                                                        </label>
                                                    </th>
                                                    <th class="text-center">No.</th>
                                                    <th class="text-center">Name</th>
                                                    <th class="text-center">Color</th>
                                                    <th class="text-center">Quantity</th>
                                                    <th class="text-center">Unit</th>
                                                    <th class="text-center">Package</th>
                                                    <th class="text-center">Order date</th>
                                                    <th class="text-center">Shipping date</th>
                                                    <th class="text-center">Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <label class="custom-control custom-control-primary custom-checkbox">
                                                        <input class="custom-control-input" type="checkbox" name="mode">
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-label"></span>
                                                        </label>
                                                    </td>
                                                    <td>201608007</td>
                                                    <td class="text-left">RASCHEL NETS</td>
                                                    <td>Dark green</td>
                                                    <td>50</td>
                                                    <td>Meters</td>
                                                    <td>Roll ∅5cm.</td>
                                                    <td>01-Aug-17</td>
                                                    <td>30-Nov-17</td>
                                                    <td class="text-danger">Planet (not fully)</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <label class="custom-control custom-control-primary custom-checkbox">
                                                        <input class="custom-control-input" type="checkbox" name="mode">
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-label"></span>
                                                        </label>
                                                    </td>
                                                    <td>201608008</td>
                                                    <td class="text-left">NYLON MULTIFILAMENT KNOTTED NETS</td>
                                                    <td>Yellow</td>
                                                    <td>1000</td>
                                                    <td>Rolls</td>
                                                    <td>Roll ∅10cm.</td>
                                                    <td>04-Aug-17</td>
                                                    <td>09-Oct-17</td>
                                                    <td class="text-danger">Planet (not fully)</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <label class="custom-control custom-control-primary custom-checkbox">
                                                        <input class="custom-control-input" type="checkbox" name="mode">
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-label"></span>
                                                        </label>
                                                    </td>
                                                    <td>201608009</td>
                                                    <td class="text-left">NYLON MULTIFILAMENT KNOTTED NETS</td>
                                                    <td>Red</td>
                                                    <td>10</td>
                                                    <td>Meters</td>
                                                    <td>Roll ∅30cm.</td>
                                                    <td>07-Aug-17</td>
                                                    <td>21-Oct-17</td>
                                                    <td class="text-danger">Planet (not fully)</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <label class="custom-control custom-control-primary custom-checkbox">
                                                        <input class="custom-control-input" type="checkbox" name="mode">
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-label"></span>
                                                        </label>
                                                    </td>
                                                    <td>201608010</td>
                                                    <td class="text-left">NYLON MULTIFILAMENT KNOTTED NETS</td>
                                                    <td>Brown</td>
                                                    <td>200</td>
                                                    <td>Meters</td>
                                                    <td>Roll ∅2m.</td>
                                                    <td>13-Aug-17</td>
                                                    <td>14-Nov-17</td>
                                                    <td class="text-danger">Planet (not fully)</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <label class="custom-control custom-control-primary custom-checkbox">
                                                        <input class="custom-control-input" type="checkbox" name="mode">
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-label"></span>
                                                        </label>
                                                    </td>
                                                    <td>201608011</td>
                                                    <td class="text-left">NYLON MULTIFILAMENT KNOTTED NETS</td>
                                                    <td>White</td>
                                                    <td>70</td>
                                                    <td>Rolls</td>
                                                    <td>Pack type1</td>
                                                    <td>16-Aug-17</td>
                                                    <td>26-Nov-17</td>
                                                    <td class="text-danger">Planet (not fully)</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <label class="custom-control custom-control-primary custom-checkbox">
                                                        <input class="custom-control-input" type="checkbox" name="mode">
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-label"></span>
                                                        </label>
                                                    </td>
                                                    <td>201608012</td>
                                                    <td class="text-left">NYLON MULTIFILAMENT KNOTTED NETS</td>
                                                    <td>Green</td>
                                                    <td>200</td>
                                                    <td>Meters</td>
                                                    <td>Pack type2</td>
                                                    <td>19-Aug-17</td>
                                                    <td>05-Oct-17</td>
                                                    <td class="text-danger">Planet (not fully)</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="tab-pane fade" id="Planned">
                                        <table id="" class="table table-hover table-bordered text-center m-b-0" cellspacing="0" width="100%">
                                            <thead>
                                                <tr class="at_bg_table_blue">
                                                    <th class="text-center">
                                                        <label class="custom-control custom-control-primary custom-checkbox">
                                                        <input class="custom-control-input" type="checkbox" name="mode">
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-label">
                                                            <span class="icon icon-caret-down"></span>
                                                        </span>
                                                        </label>
                                                    </th>
                                                    <th class="text-center">No.</th>
                                                    <th class="text-center">Name</th>
                                                    <th class="text-center">Color</th>
                                                    <th class="text-center">Quantity</th>
                                                    <th class="text-center">Unit</th>
                                                    <th class="text-center">Package</th>
                                                    <th class="text-center">Order date</th>
                                                    <th class="text-center">Shipping date</th>
                                                    <th class="text-center">Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <label class="custom-control custom-control-primary custom-checkbox">
                                                        <input class="custom-control-input" type="checkbox" name="mode">
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-label"></span>
                                                        </label>
                                                    </td>
                                                    <td>201608013</td>
                                                    <td class="text-left">RASCHEL NETS</td>
                                                    <td>Dark green</td>
                                                    <td>50</td>
                                                    <td>Meters</td>
                                                    <td>Roll ∅5cm.</td>
                                                    <td>01-Aug-17</td>
                                                    <td>30-Nov-17</td>
                                                    <td class="text-info">Planned</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <label class="custom-control custom-control-primary custom-checkbox">
                                                        <input class="custom-control-input" type="checkbox" name="mode">
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-label"></span>
                                                        </label>
                                                    </td>
                                                    <td>201608014</td>
                                                    <td class="text-left">NYLON MULTIFILAMENT KNOTTED NETS</td>
                                                    <td>Yellow</td>
                                                    <td>1000</td>
                                                    <td>Rolls</td>
                                                    <td>Roll ∅10cm.</td>
                                                    <td>04-Aug-17</td>
                                                    <td>09-Oct-17</td>
                                                    <td class="text-info">Planned</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <label class="custom-control custom-control-primary custom-checkbox">
                                                        <input class="custom-control-input" type="checkbox" name="mode">
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-label"></span>
                                                        </label>
                                                    </td>
                                                    <td>201608015</td>
                                                    <td class="text-left">NYLON MULTIFILAMENT KNOTTED NETS</td>
                                                    <td>Red</td>
                                                    <td>10</td>
                                                    <td>Meters</td>
                                                    <td>Roll ∅30cm.</td>
                                                    <td>07-Aug-17</td>
                                                    <td>21-Oct-17</td>
                                                    <td class="text-info">Planned</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <label class="custom-control custom-control-primary custom-checkbox">
                                                        <input class="custom-control-input" type="checkbox" name="mode">
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-label"></span>
                                                        </label>
                                                    </td>
                                                    <td>201608016</td>
                                                    <td class="text-left">NYLON MULTIFILAMENT KNOTTED NETS</td>
                                                    <td>Brown</td>
                                                    <td>200</td>
                                                    <td>Meters</td>
                                                    <td>Roll ∅2m.</td>
                                                    <td>13-Aug-17</td>
                                                    <td>14-Nov-17</td>
                                                    <td class="text-info">Planned</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <label class="custom-control custom-control-primary custom-checkbox">
                                                        <input class="custom-control-input" type="checkbox" name="mode">
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-label"></span>
                                                        </label>
                                                    </td>
                                                    <td>201608017</td>
                                                    <td class="text-left">NYLON MULTIFILAMENT KNOTTED NETS</td>
                                                    <td>White</td>
                                                    <td>70</td>
                                                    <td>Rolls</td>
                                                    <td>Pack type1</td>
                                                    <td>16-Aug-17</td>
                                                    <td>26-Nov-17</td>
                                                    <td class="text-info">Planned</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <label class="custom-control custom-control-primary custom-checkbox">
                                                        <input class="custom-control-input" type="checkbox" name="mode">
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-label"></span>
                                                        </label>
                                                    </td>
                                                    <td>201608018</td>
                                                    <td class="text-left">NYLON MULTIFILAMENT KNOTTED NETS</td>
                                                    <td>Green</td>
                                                    <td>200</td>
                                                    <td>Meters</td>
                                                    <td>Pack type2</td>
                                                    <td>19-Aug-17</td>
                                                    <td>05-Oct-17</td>
                                                    <td class="text-info">Planned</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-primary" data-dismiss="modal" type="button">Add order</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Addnew_Planning_Planning_Schedule_cancel" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title"><span class="icon icon-lg icon-object-ungroup"></span> Cancel planned</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12 m-b">
                            <table id="" class="table table-hover table-bordered at-table-small m-b" cellspacing="0" width="100%">
                                <tbody>
                                    <tr>
                                        <td width="150"><strong>Order no.</strong></td>
                                        <td>201608018</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Name</strong></td>
                                        <td>NYLON MULTIFILAMENT KNOTTED NETS</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Color</strong></td>
                                        <td>Green</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Quantity</strong></td>
                                        <td>200</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Unit</strong></td>
                                        <td>Meters</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Package</strong></td>
                                        <td>Roll ∅5cm.</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Order date</strong></td>
                                        <td>19-Aug-17</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Shipping date</strong></td>
                                        <td>05-Oct-17</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-xs-12 m-b-0">
                            <table id="" class="table table-hover table-bordered at-table-small m-b" cellspacing="0" width="100%">
                                <tbody>
                                    <tr>
                                        <td width="150"><strong>Planned on</strong></td>
                                        <td>Line 1 : Fishing Net</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Start on</strong></td>
                                        <td>9/20/17 12:35</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Finish on</strong></td>
                                        <td>9/28/17 21:40</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-xs-12 m-b m-t-md">
                            <label class="custom-control custom-control-danger custom-radio">
                                <input class="custom-control-input" type="radio" name="message" value="all">
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-label">
                                    <span class="text-danger">Keep the period for another order</span>
                                </span>
                            </label>
                            <label class="custom-control custom-control-danger custom-radio">
                                <input class="custom-control-input" type="radio" name="message" value="all">
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-label">
                                    <span class="text-danger">Move next order to start on the period</span>
                                </span>
                            </label>
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-primary" data-dismiss="modal" type="button">Confirm to cancel</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Quit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Addnew_Planning_Planning_Schedule_move" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-lg" style="width: 80%;">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title"><span class="icon icon-lg icon-random"></span> Move planned</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12 m-b">
                            <table id="" class="table table-hover table-bordered at-table-small m-b" cellspacing="0" width="100%">
                                <tbody>
                                    <tr>
                                        <td width="150"><strong>Order no.</strong></td>
                                        <td>201608018</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Name</strong></td>
                                        <td>NYLON MULTIFILAMENT KNOTTED NETS</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Color</strong></td>
                                        <td>Green</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Quantity</strong></td>
                                        <td>200</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Unit</strong></td>
                                        <td>Meters</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Package</strong></td>
                                        <td>Roll ∅5cm.</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Order date</strong></td>
                                        <td>19-Aug-17</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Shipping date</strong></td>
                                        <td>05-Oct-17</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-xs-4 m-b">
                            <table id="" class="table table-hover table-bordered at-table-small m-b" cellspacing="0" width="100%">
                                <tbody>
                                    <tr class="at_bg_table_red">
                                        <td width="150"><strong>Planned on</strong></td>
                                        <td>Line 1 : Fishing Net</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Start on</strong></td>
                                        <td>9/20/17 12:35</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Finish on</strong></td>
                                        <td>9/28/17 21:40</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-xs-4 m-b">
                            <table id="" class="table table-hover table-bordered at-table-small m-b" cellspacing="0" width="100%">
                                <tbody>
                                    <tr class="at_bg_table_red">
                                        <td width="150"><strong>Planned on</strong></td>
                                        <td>Line 2 : Agriculture</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Start on</strong></td>
                                        <td>9/20/17 12:35</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Finish on</strong></td>
                                        <td>9/28/17 21:40</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-xs-4 m-b">
                            <table id="" class="table table-hover table-bordered at-table-small m-b" cellspacing="0" width="100%">
                                <tbody>
                                    <tr class="at_bg_table_red">
                                        <td width="150"><strong>Planned on</strong></td>
                                        <td>Line 3 : Sport Net</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Start on</strong></td>
                                        <td>9/20/17 12:35</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Finish on</strong></td>
                                        <td>9/28/17 21:40</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-xs-4 m-b">
                            <table id="" class="table table-hover table-bordered at-table-small m-b" cellspacing="0" width="100%">
                                <tbody>
                                    <tr class="at_bg_table_red">
                                        <td width="150"><strong>Planned on</strong></td>
                                        <td>Line 4 : Twine</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Start on</strong></td>
                                        <td>9/20/17 12:35</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Finish on</strong></td>
                                        <td>9/28/17 21:40</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-xs-4 m-b">
                            <table id="" class="table table-hover table-bordered at-table-small m-b" cellspacing="0" width="100%">
                                <tbody>
                                    <tr class="at_bg_table_red">
                                        <td width="150"><strong>Planned on</strong></td>
                                        <td>Line 5 : Aquaculture Net</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Start on</strong></td>
                                        <td>9/20/17 12:35</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Finish on</strong></td>
                                        <td>9/28/17 21:40</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-xs-6 m-b">
                            <label>Move form</label>
                            <select id="" class="custom-select">
                                <option value="">Line 1 : Fishing Net</option>
                                <option value="">Line 2 : Agriculture</option>
                                <option value="">Line 3 : Sport Net</option>
                                <option value="">Line 4 : Twine</option>
                                <option value="">Line 5 : Aquaculture Net</option>
                            </select>
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Move to</label>
                            <select id="" class="custom-select">
                                <option value="">Line 2 : Agriculture</option>
                                <option value="">Line 3 : Sport Net</option>
                                <option value="">Line 4 : Twine</option>
                                <option value="">Line 5 : Aquaculture Net</option>
                            </select>
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Start on</label>
                            <div class="input-with-icon">
                                <input class="form-control" type="text" data-provide="datepicker" data-date-today-highlight="true" data-date-autoclose="true" data-date-format="DD, MM d, yyyy" value="Friday, jun 2, 2017">
                                <span class="icon icon-calendar input-icon"></span>
                            </div>
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Volume</label>
                            <div class="row">
                                <div class="col-xs-6">
                                    <input class="form-control text-center" type="text" placeholder="">
                                </div>
                                <div class="col-xs-6 p-l-0 p-t">
                                    Meters
                                </div>
                                <div class="col-xs-12 p-l-md p-t text-info">
                                    Estimate Finish on ...
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 m-b m-t-md text-center">
                            <label class="custom-control custom-control-danger custom-radio">
                                <input class="custom-control-input" type="radio" name="message" value="all">
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-label">
                                    <span class="text-danger">Keep the period for another order</span>
                                </span>
                            </label>
                            <label class="custom-control custom-control-danger custom-radio">
                                <input class="custom-control-input" type="radio" name="message" value="all">
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-label">
                                    <span class="text-danger">Move next order to start on the period</span>
                                </span>
                            </label>
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-danger" data-dismiss="modal" type="button">Confirm to move</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Quit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Addnew_Planning_Planning_Schedule_plan" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header bg-success">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title"><span class="icon icon-lg icon-random"></span> Move planned</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12 m-b">
                            <table id="" class="table table-hover table-bordered at-table-small m-b" cellspacing="0" width="100%">
                                <tbody>
                                    <tr>
                                        <td width="150"><strong>Order no.</strong></td>
                                        <td>201608018</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Name</strong></td>
                                        <td>NYLON MULTIFILAMENT KNOTTED NETS</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Color</strong></td>
                                        <td>Green</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Quantity</strong></td>
                                        <td>200</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Unit</strong></td>
                                        <td>Meters</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Package</strong></td>
                                        <td>Roll ∅5cm.</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Order date</strong></td>
                                        <td>19-Aug-17</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Shipping date</strong></td>
                                        <td>05-Oct-17</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- <div class="col-xs-12 m-b">
                            <table id="" class="table table-hover table-bordered at-table-small m-b" cellspacing="0" width="100%">
                                <tbody>
                                    <tr>
                                        <td width="150"><strong>Planned on</strong></td>
                                        <td>Line 1 : Fishing Net</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Start on</strong></td>
                                        <td>9/20/17 12:35</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Finish on</strong></td>
                                        <td>9/28/17 21:40</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div> -->
                        <div class="col-xs-12 m-b">
                            <label>Planned on</label>
                            <select id="" class="custom-select">
                                <option value="">Line 1 : Fishing Net</option>
                                <option value="">Line 2 : Agriculture</option>
                                <option value="">Line 3 : Sport Net</option>
                                <option value="">Line 4 : Twine</option>
                                <option value="">Line 5 : Aquaculture Net</option>
                            </select>
                        </div>
                        <!-- <div class="col-xs-12 m-b">
                            <table id="" class="table table-bordered at-table-small m-b" cellspacing="0" width="100%">
                                <tbody  class="at_bg_table_red">
                                    <tr>
                                        <td width="150">Line 1 (25%) on</td>
                                        <td>9/20/17 12:35</td>
                                    </tr>
                                    <tr>
                                        <td>Line 7 (30%) on</td>
                                        <td>9/24/17 18:15</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div> -->
                        <div class="col-xs-6 m-b">
                            <label>Start on</label>
                            <div class="input-with-icon">
                                <input class="form-control" type="text" data-provide="datepicker" data-date-today-highlight="true" data-date-autoclose="true" data-date-format="DD, MM d, yyyy" value="Friday, jun 2, 2017">
                                <span class="icon icon-calendar input-icon"></span>
                            </div>
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Volume</label>
                            <div class="row">
                                <div class="col-xs-6">
                                    <input class="form-control text-center" type="text" placeholder="100">
                                </div>
                                <div class="col-xs-6 p-l-0 p-t">
                                    Meters
                                </div>
                                <div class="col-xs-12 p-l-md p-t text-info">
                                    Estimate Finish on ...
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 m-b m-t-md">
                            <label class="custom-control custom-control-danger custom-radio">
                                <input class="custom-control-input" type="radio" name="message" value="all">
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-label">
                                    <span class="text-danger">Keep the period for another order</span>
                                </span>
                            </label>
                            <label class="custom-control custom-control-danger custom-radio">
                                <input class="custom-control-input" type="radio" name="message" value="all">
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-label">
                                    <span class="text-danger">Move next order to start on the period</span>
                                </span>
                            </label>
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-success" data-dismiss="modal" type="button">Confirm to move</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Quit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Addnew_state_material_Withdrawal" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title"><span class="icon icon-lg icon-arrow-circle-o-up"></span> Withdrawal</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12 m-b">
                            <table id="" class="table table-hover table-bordered text-center at-table-small m-b" cellspacing="0" width="100%">
                                <thead>
                                    <tr class="at_bg_table_green">
                                        <th class="text-center" width="150">Code</th>
                                        <th class="text-center">Group</th>
                                        <th class="text-center">Name</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>PM00001</td>
                                        <td>Raw nylon</td>
                                        <td>Polymide type1</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-xs-12 m-b">
                            <table id="" class="table table-hover table-borderless text-center at-table-small m-b" cellspacing="0" width="100%">
                                <tbody>
                                    <tr>
                                        <td class="fw-sb" width="150">Withdrawal for</td>
                                        <td class="text-left" width="200">
                                            <label class="custom-control custom-control-primary custom-radio m-t-sm">
                                                <input class="custom-control-input" type="radio" name="message" value="all">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-label">
                                                    <span class="text-primary">Order no.</span>
                                                </span>
                                            </label>
                                        </td>
                                        <td class="text-left">
                                            <select id="" class="custom-select custom-select-sm">
                                                <option value="">PO201608001</option>
                                                <option value="">PO201608002</option>
                                                <option value="">PO201608003</option>
                                                <option value="">PO201608004</option>
                                                <option value="">PO201608005</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td class="text-left">
                                            <label class="custom-control custom-control-primary custom-radio m-t-sm">
                                                <input class="custom-control-input" type="radio" name="message" value="all">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-label">
                                                    <span class="text-primary">Addition to order no.</span>
                                                </span>
                                            </label>
                                        </td>
                                        <td class="text-left">
                                            <select id="" class="custom-select custom-select-sm">
                                                <option value="">PO2016080112</option>
                                                <option value="">PO2016080113</option>
                                                <option value="">PO2016080114</option>
                                                <option value="">PO2016080115</option>
                                                <option value="">PO2016080116</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td class="text-left">
                                            <label class="custom-control custom-control-primary custom-radio m-t-sm">
                                                <input class="custom-control-input" type="radio" name="message" value="all">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-label">
                                                    <span class="text-primary">Another reason</span>
                                                </span>
                                            </label>
                                        </td>
                                        <td class="text-left"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-xs-12 m-b">
                            <table id="" class="table table-bordered at-table-small m-b" cellspacing="0" width="100%">
                                <tbody>
                                    <tr>
                                        <td width="150"><strong>Order no.</strong></td>
                                        <td>201608018</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Name</strong></td>
                                        <td>NYLON MULTIFILAMENT KNOTTED NETS</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Color</strong></td>
                                        <td>Green</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Quantity</strong></td>
                                        <td>200</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Unit</strong></td>
                                        <td>Meters</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Package</strong></td>
                                        <td>Roll ∅5cm.</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Order date</strong></td>
                                        <td>19-Aug-17</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Shipping date</strong></td>
                                        <td>05-Oct-17</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Planned on</strong></td>
                                        <td>Line 1 : Fishing Net</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Finish on</strong></td>
                                        <td>9/20/17 12:35</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Shipping date</strong></td>
                                        <td>9/28/17 21:40</td>
                                    </tr>
                                    <tr class="at_bg_table_green">
                                        <td><strong>Estimate usage</strong></td>
                                        <td>105.8790 kgs.</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Remark</strong></td>
                                        <td>
                                            <textarea id="form-control-8" class="form-control" rows="2"></textarea>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-xs-12 m-b-0">
                            <table id="" class="table table-bordered at-table-small m-b" cellspacing="0" width="100%">
                                <tbody>
                                    <tr class="at_bg_table_blue">
                                        <td width="150"><strong>Withdrawal</strong></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td width=""><strong>Date</strong></td>
                                        <td>
                                            <div class="input-with-icon">
                                                <input class="form-control" type="text" data-provide="datepicker" data-date-today-highlight="true" data-date-autoclose="true" data-date-format="DD, MM d, yyyy" value="Friday, jun 2, 2017">
                                                <span class="icon icon-calendar input-icon"></span>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width=""><strong>From supplier</strong></td>
                                        <td>
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <select id="" class="custom-select custom-select-sm">
                                                        <option value="">Japan</option>
                                                        <option value="">German</option>
                                                        <option value="">China</option>
                                                    </select>
                                                </div>
                                                <div class="col-xs-6 p-l-0 p-t-sm">
                                                    Japan
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width=""><strong>Quantity</strong></td>
                                        <td>
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <input class="form-control text-center at-custom-select-sm" type="text" placeholder="10">
                                                </div>
                                                <div class="col-xs-6 p-l-0 p-t-sm">
                                                    Packs. <span class="text-danger">( 110.1011 Kgs.)</span>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-primary" data-dismiss="modal" type="button">Confirm to withdrawal</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Addnew_state_material_Receive" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-lg" style="width: 90%;">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title"><span class="icon icon-lg icon-arrow-circle-o-down"></span> Receive</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group m-b-0">
                        <div class="col-xs-12 m-b-0">
                            <table id="demo-datatables-2" class="table table-striped table-bordered table-nowrap dataTable text-center" cellspacing="0" width="100%">
                                <thead>
                                    <tr class="at_bg_table_green">
                                        <th rowspan="2" class="text-center">No.</th>
                                        <th rowspan="2" class="text-center">PO No.</th>
                                        <th rowspan="2" class="text-center">Code</th>
                                        <th rowspan="2" class="text-center">Group</th>
                                        <th rowspan="2" class="text-center">Name</th>
                                        <th rowspan="2" class="text-center">Supplier</th>
                                        <th rowspan="2" class="text-center">Quantity</th>
                                        <th rowspan="2" class="text-center">Unit</th>
                                        <th rowspan="2" class="text-center">Order date</th>
                                        <th rowspan="2" class="text-center">Request date</th>
                                        <th colspan="4" class="text-center">Receive</th>
                                        <th rowspan="2" class="text-center">Modify</th>
                                    </tr>
                                    <tr class="at_bg_table_green">
                                        <th class="text-center">Receive No.</th>
                                        <th class="text-center">Date</th>
                                        <th class="text-center">Quantity</th>
                                        <th class="text-center">Unit</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>PO201708001</td>
                                        <td>PM00001</td>
                                        <td>Raw nylon</td>
                                        <td>Polymide type1</td>
                                        <td>Japan</td>
                                        <td>100</td>
                                        <td>Packs</td>
                                        <td class="text-danger">01-Aug-17</td>
                                        <td>01-Oct-17</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td class="text-center">
                                            <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Addnew_state_material_Receive_check" type="button">
                                                <span class="icon icon-lg icon-check"></span> Receive
                                            </button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>PO201708157</td>
                                        <td>PM00001</td>
                                        <td>Raw nylon</td>
                                        <td>Polymide type1</td>
                                        <td>Japan</td>
                                        <td>100</td>
                                        <td>Packs</td>
                                        <td>10-Aug-17</td>
                                        <td>20-Oct-17</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td class="text-center">
                                            <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Addnew_state_material_Receive_check" type="button">
                                                <span class="icon icon-lg icon-check"></span> Receive
                                            </button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>PO201709025</td>
                                        <td>PM00001</td>
                                        <td>Raw nylon</td>
                                        <td>Polymide type1</td>
                                        <td>China</td>
                                        <td>500</td>
                                        <td>Kgs.</td>
                                        <td>03-Sep-17</td>
                                        <td>25-Sep-17</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td class="text-center">
                                            <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Addnew_state_material_Receive_check" type="button">
                                                <span class="icon icon-lg icon-check"></span> Receive
                                            </button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Addnew_state_material_Receive_check" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title"><span class="icon icon-lg icon-check"></span> Receive</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12 m-b">
                            <table id="" class="table table-hover table-bordered at-table-small m-b-0" cellspacing="0" width="100%">
                                <tbody>
                                    <tr class="at_bg_table_red">
                                        <td width="150"><strong>Purchase</strong></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td width=""><strong>PO No.</strong></td>
                                        <td>PO201608001</td>
                                    </tr>
                                    <tr>
                                        <td width=""><strong>Name</strong></td>
                                        <td>Polymide type1</td>
                                    </tr>
                                    <tr>
                                        <td width=""><strong>Supplier</strong></td>
                                        <td>Japan</td>
                                    </tr>
                                    <tr>
                                        <td width=""><strong>Quantity</strong></td>
                                        <td>100</td>
                                    </tr>
                                    <tr>
                                        <td width=""><strong>Unit</strong></td>
                                        <td>Packs</td>
                                    </tr>
                                    <tr>
                                        <td width=""><strong>Order date</strong></td>
                                        <td>01-Aug-17</td>
                                    </tr>
                                    <tr>
                                        <td width=""><strong>Request date</strong></td>
                                        <td>01-Oct-17</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-xs-12 m-b-0">
                            <table id="" class="table table-bordered at-table-small m-b-0" cellspacing="0" width="100%">
                                <tbody>
                                    <tr class="at_bg_table_red">
                                        <td width="150"><strong>Receive</strong></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td width=""><strong>Receive No.</strong></td>
                                        <td>
                                            <input class="form-control at-custom-select-sm" type="text" placeholder="10">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width=""><strong>Date</strong></td>
                                        <td>
                                            <div class="input-with-icon">
                                                <input class="form-control" type="text" data-provide="datepicker" data-date-today-highlight="true" data-date-autoclose="true">
                                                <span class="icon icon-calendar input-icon"></span>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width=""><strong>Quantity</strong></td>
                                        <td>
                                            <input class="form-control at-custom-select-sm" type="text" placeholder="10">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width=""><strong>Unit</strong></td>
                                        <td>
                                            <select id="" class="custom-select custom-select-sm">
                                                <option value="">Bags</option>
                                                <option value="">fts.</option>
                                                <option value="">Dozens</option>
                                                <option value="">Tons</option>
                                                <option value="">Sheets</option>
                                                <option value="">Tanks</option>
                                                <option value="">Kgs.</option>
                                                <option value="">Pieces</option>
                                                <option value="">Meters</option>
                                                <option value="">Rolls</option>
                                                <option value="">Packs</option>
                                                <option value="">Pound</option>
                                            </select>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-danger" data-dismiss="modal" type="button">Confirm to Receive</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Addnew_state_machine_plan" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-success">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Add</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12 m-b">
                            <label>Machine No.</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-success" data-dismiss="modal" type="button">Add plan</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Edit_state_machine_plan" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Add</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12 m-b">
                            <table id="" class="table table-hover table-bordered m-b" cellspacing="0" width="100%">
                                <tbody>
                                    <tr>
                                        <td width=""><strong>Group of machine</strong></td>
                                        <td>Extruding</td>
                                    </tr>
                                    <tr>
                                        <td width=""><strong>Name</strong></td>
                                        <td>Model Y2000 type</td>
                                    </tr>
                                    <tr>
                                        <td width=""><strong>Line</strong></td>
                                        <td>Line 1 : Fishing Net</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Maintenance No.</label>
                            <input class="form-control" type="text" placeholder="201708001                  ">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Group</label>
                            <select class="form-control">
                                <option>Overhaul</option>
                                <option>Replacement</option>
                                <option>Repair</option>
                                <option>Other</option>
                            </select>
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>System</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Machine No.</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Maintenance date</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Estimate time</label>
                            <div class="input-with-icon">
                                <input class="form-control" type="text" data-provide="datepicker" data-date-today-highlight="true" data-date-autoclose="true">
                                <span class="icon icon-calendar input-icon"></span>
                            </div>
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Unit</label>
                            <select class="form-control">
                                <option>Mins.</option>
                                <option>Hrs.</option>
                                <option>Days</option>
                                <option>Weeks</option>
                                <option>Months</option>
                                <option>Years</option>
                            </select>
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Status</label>
                            <select class="form-control">
                                <option>Planned</option>
                                <option>Confirmed</option>
                            </select>
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-primary" data-dismiss="modal" type="button">Add plan</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Confirm_state_machine_plan" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header bg-success">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title"><span class="icon icon-lg icon-check"></span> Maintenance plan</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-12 m-b">
                            <table id="" class="table table-hover table-bordered at-table-small m-b-0" cellspacing="0" width="100%">
                                <tbody>
                                    <tr class="at_bg_table_red">
                                        <td width="150"><strong>Maintenance plan</strong></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td width=""><strong>Maintenance No.</strong></td>
                                        <td>201708001</td>
                                    </tr>
                                    <tr>
                                        <td width=""><strong>Group</strong></td>
                                        <td>Overhaul</td>
                                    </tr>
                                    <tr>
                                        <td width=""><strong>System</strong></td>
                                        <td>Machanic</td>
                                    </tr>
                                    <tr>
                                        <td width=""><strong>Machine No.</strong></td>
                                        <td>EX0001</td>
                                    </tr>
                                    <tr>
                                        <td width=""><strong>Group of machine</strong></td>
                                        <td>Extruding</td>
                                    </tr>
                                    <tr>
                                        <td width=""><strong>Name</strong></td>
                                        <td>Model Y2000 type</td>
                                    </tr>
                                    <tr>
                                        <td width=""><strong>Line</strong></td>
                                        <td>Line 1 : Fishing Net</td>
                                    </tr>
                                    <tr>
                                        <td width=""><strong>Maintenance date</strong></td>
                                        <td>9/24/17 18:15</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-xs-12 m-b-0">
                            <table id="" class="table table-bordered at-table-small m-b-0" cellspacing="0" width="100%">
                                <tbody>
                                    <tr class="at_bg_table_red">
                                        <td width="150"><strong>Confirm</strong></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td width=""><strong>Confirm Ref.# No.</strong></td>
                                        <td>
                                            <input class="form-control at-custom-select-sm" type="text" placeholder="CR201710005">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width=""><strong>Date</strong></td>
                                        <td>
                                            <div class="input-with-icon">
                                                <input class="form-control" type="text" data-provide="datepicker" data-date-today-highlight="true" data-date-autoclose="true" data-date-format="DD, MM d, yyyy" value="Friday, jun 2, 2017">
                                                <span class="icon icon-calendar input-icon"></span>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width=""><strong>Quantity</strong></td>
                                        <td>
                                            <div class="input-with-icon">
                                                <input class="form-control" type="text" data-provide="datepicker" data-date-today-highlight="true" data-date-autoclose="true" data-date-format="DD, MM d, yyyy" value="Friday, jun 2, 2017">
                                                <span class="icon icon-calendar input-icon"></span>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><strong>Remark</strong></td>
                                        <td>
                                            <textarea id="form-control-8" class="form-control" rows="2"></textarea>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-success" data-dismiss="modal" type="button">Confirm to Plan</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Addnew_site" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Add</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-6 m-b">
                            <label>Site</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Site detail</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Factory</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Factory detail</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Group machine</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Group machine detail</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-primary" data-dismiss="modal" type="button">Add site</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Edit_site" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-lg" style="width: 80%;">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Edit</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group m-b-0">
                        <div class="col-xs-12">
                            <div class="">
                                <table id="" class="table table-hover table-bordered text-center m-b-sm" cellspacing="0" width="100%" style="background-color: #FFF; white-space: normal;">
                                    <thead>
                                        <tr class="at_bg_table_blue">
                                            <th class="text-center">Site</th>
                                            <th class="text-center">Site detail</th>
                                            <th class="text-center">Factory</th>
                                            <th class="text-center">Factory detail</th>
                                            <th class="text-center">Group machine</th>
                                            <th class="text-center">Group machine detail</th>
                                            <th class="text-center">Modify</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="NR">
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="xxx">
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="A">
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="xxx">
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="A1">
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="xxx">
                                            </td>
                                            <td>
                                                <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted" type="button">
                                                    <span class="icon icon-lg icon-close"></span>
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="A2">
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="xxx">
                                            </td>
                                            <td>
                                                <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted" type="button">
                                                    <span class="icon icon-lg icon-close"></span>
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="A3">
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="xxx">
                                            </td>
                                            <td>
                                                <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted" type="button">
                                                    <span class="icon icon-lg icon-close"></span>
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="A4">
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="xxx">
                                            </td>
                                            <td>
                                                <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted" type="button">
                                                    <span class="icon icon-lg icon-close"></span>
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="A5">
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="xxx">
                                            </td>
                                            <td>
                                                <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted" type="button">
                                                    <span class="icon icon-lg icon-close"></span>
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="A6">
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="xxx">
                                            </td>
                                            <td>
                                                <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted" type="button">
                                                    <span class="icon icon-lg icon-close"></span>
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="A7">
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="xxx">
                                            </td>
                                            <td>
                                                <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted" type="button">
                                                    <span class="icon icon-lg icon-close"></span>
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="A8">
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="xxx">
                                            </td>
                                            <td>
                                                <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted" type="button">
                                                    <span class="icon icon-lg icon-close"></span>
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="A9">
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="xxx">
                                            </td>
                                            <td>
                                                <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted" type="button">
                                                    <span class="icon icon-lg icon-close"></span>
                                                </button>
                                            </td>
                                        </tr>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-primary" data-toggle="modal" data-target="#Button_Addnew_site_row" type="button">
                            <span class="icon icon-lg icon-plus"></span> Add Row
                        </button>
                        <button class="btn btn-success" data-dismiss="modal" type="button">Save</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Addnew_sitelist" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Add</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-6 m-b">
                            <label>Site</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Site detail</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Apply date</label>
                            <div class="input-with-icon">
                                <input class="form-control" type="text" data-provide="datepicker" data-date-today-highlight="true" data-date-autoclose="true" data-date-format="DD, MM d, yyyy" value="">
                                <span class="icon icon-calendar input-icon"></span>
                            </div>
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Category </label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Grade</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Grade detail</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Product type</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Product type detail</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Min. Nylon No.</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Max. Nylon No.   </label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-primary" data-dismiss="modal" type="button">Add site list</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Edit_sitelist" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-lg" style="width: 80%;">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Edit</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group m-b-0">
                        <div class="col-xs-12">
                            <div class="">
                                <table id="" class="table table-hover table-bordered text-center m-b-sm" cellspacing="0" width="100%" style="background-color: #FFF; white-space: normal;">
                                    <thead>
                                        <tr class="at_bg_table_blue">
                                            <th class="text-center" width="">Site</th>
                                            <th class="text-center" width="">Site detail</th>
                                            <th class="text-center" width="">Apply date</th>
                                            <th class="text-center" width="">Category</th>
                                            <th class="text-center" width="">Grade</th>
                                            <th class="text-center" width="">Grade detail</th>
                                            <th class="text-center" width="">Product type</th>
                                            <th class="text-center" width="">Product type detail</th>
                                            <th class="text-center" width="">Min. Nylon No.</th>
                                            <th class="text-center" width="">Max. Nylon No.</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><input class="form-control" type="text" placeholder="NR"></td>
                                            <td><input class="form-control" type="text" placeholder="xxx"></td>
                                            <td>
                                                <div class="input-with-icon">
                                                    <input class="form-control" type="text" data-provide="datepicker" data-date-today-highlight="true" data-date-autoclose="true" data-date-format="MM d, yyyy" value="June 21, 2017">
                                                    <span class="icon icon-calendar input-icon"></span>
                                                </div>
                                            </td>
                                            <td><input class="form-control" type="text" placeholder="1"></td>
                                            <td><input class="form-control" type="text" placeholder="P"></td>
                                            <td><input class="form-control" type="text" placeholder="Premium"></td>
                                            <td><input class="form-control" type="text" placeholder="M"></td>
                                            <td><input class="form-control" type="text" placeholder="Mono"></td>
                                            <td><input class="form-control" type="text" placeholder="1"></td>
                                            <td><input class="form-control" type="text" placeholder="10"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-primary" data-toggle="modal" data-target="#Button_Addnew_site_row" type="button">
                            <span class="icon icon-lg icon-plus"></span> Add Row
                        </button>
                        <button class="btn btn-success" data-dismiss="modal" type="button">Save</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Addnew_site_row" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Add</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-6 m-b">
                            <label>Site</label>
                            <input class="form-control" type="text" placeholder="NR">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Site detail</label>
                            <input class="form-control" type="text" placeholder="xxx">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Factory</label>
                            <input class="form-control" type="text" placeholder="A">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Factory detail</label>
                            <input class="form-control" type="text" placeholder="xxx">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Group machine</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Group machine detail</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-primary" data-dismiss="modal" type="button">Add Row</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Addnew_Group_Machine" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header bg-success">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Add</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-6 m-b">
                            <label>Group machine</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Group machine detail</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Machine code</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Machine code detail</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>Parameter 1</label>
                            <div class="row">
                                <div class="col-xs-12">
                                <small>Min.</small>
                                <input class="form-control" type="text" placeholder="">
                                </div>
                                <div class="col-xs-12">
                                <small>Max.</small>
                                <input class="form-control" type="text" placeholder="">
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>Parameter 2</label>
                            <div class="row">
                                <div class="col-xs-12">
                                <small>Min.</small>
                                <input class="form-control" type="text" placeholder="">
                                </div>
                                <div class="col-xs-12">
                                <small>Max.</small>
                                <input class="form-control" type="text" placeholder="">
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>Parameter 3</label>
                            <div class="row">
                                <div class="col-xs-12">
                                <small>Min.</small>
                                <input class="form-control" type="text" placeholder="">
                                </div>
                                <div class="col-xs-12">
                                <small>Max.</small>
                                <input class="form-control" type="text" placeholder="">
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Binding</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Material type</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Cost efficiencies</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Plan efficiencies</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-success" data-dismiss="modal" type="button">Add Group Machine</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Edit_Group_Machine" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-lg" style="width: 90%;">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Edit</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group m-b-0">
                        <div class="col-xs-12">
                            <div class="">
                                <table id="" class="table table-hover table-bordered text-center m-b-sm" cellspacing="0" width="100%" style="background-color: #FFF; white-space: normal;">
                                    <thead>
                                        <tr class="at_bg_table_green">
                                            <th rowspan="2" class="text-center" width="">Group machine</th>
                                            <th rowspan="2" class="text-center" width="">Group machine detail</th>
                                            <th rowspan="2" class="text-center" width="">Machine code</th>
                                            <th rowspan="2" class="text-center" width="">Machine code detail</th>
                                            <th colspan="2" class="text-center" width="">Parameter 1</th>
                                            <th colspan="2" class="text-center" width="">Parameter 2</th>
                                            <th colspan="2" class="text-center" width="">Parameter 3</th>
                                            <th rowspan="2" class="text-center" width="">Binding</th>
                                            <th rowspan="2" class="text-center" width="">Material type</th>
                                            <th rowspan="2" class="text-center" width="">Cost efficiencies</th>
                                            <th rowspan="2" class="text-center" width="">Plan efficiencies</th>
                                            <th rowspan="2" class="text-center" width="150">Modify</th>
                                        </tr>
                                        <tr class="at_bg_table_green">
                                            <th class="text-center" width="">Min.</th>
                                            <th class="text-center" width="">Max.</th>
                                            <th class="text-center" width="">Min.</th>
                                            <th class="text-center" width="">Max.</th>
                                            <th class="text-center" width="">Min.</th>
                                            <th class="text-center" width="">Max.</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="A1">
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="xxx">
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="A001">
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="xxx">
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="1&quot;">
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="5&quot;">
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="1">
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="100">
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="10">
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="1000">
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="DK">
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="T">
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="100%">
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="95%">
                                            </td>
                                            <td>
                                                <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted" type="button">
                                                    <span class="icon icon-lg icon-close"></span>
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="A002"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="xxx"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="2&quot;"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="5&quot;"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="5"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="200"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="50"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="2000"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="DK"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="T"></td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="100%">
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="95%">
                                            </td>
                                            <td>
                                                <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted" type="button">
                                                    <span class="icon icon-lg icon-close"></span>
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="A003"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="xxx"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="3&quot;"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="5&quot;"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="10"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="300"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="100"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="3000"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="DK"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="T"></td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="100%">
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="95%">
                                            </td>
                                            <td>
                                                <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted" type="button">
                                                    <span class="icon icon-lg icon-close"></span>
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="A004"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="xxx"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="4&quot;"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="10&quot;"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="15"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="400"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="150"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="4000"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="DK"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="T"></td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="100%">
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="95%">
                                            </td>
                                            <td>
                                                <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted" type="button">
                                                    <span class="icon icon-lg icon-close"></span>
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="A005"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="xxx"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="5&quot;"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="10&quot;"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="20"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="500"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="200"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="5000"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="DK"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="T"></td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="100%">
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="95%">
                                            </td>
                                            <td>
                                                <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted" type="button">
                                                    <span class="icon icon-lg icon-close"></span>
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="A006"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="xxx"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="6&quot;"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="10&quot;"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="25"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="600"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="250"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="6000"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="DK"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="T"></td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="100%">
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="95%">
                                            </td>
                                            <td>
                                                <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted" type="button">
                                                    <span class="icon icon-lg icon-close"></span>
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="A007"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="xxx"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="7&quot;"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="20&quot;"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="30"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="700"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="300"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="7000"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="DK"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="T"></td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="100%">
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="95%">
                                            </td>
                                            <td>
                                                <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted" type="button">
                                                    <span class="icon icon-lg icon-close"></span>
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="A008"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="xxx"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="8&quot;"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="20&quot;"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="35"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="800"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="350"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="8000"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="DK"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="T"></td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="100%">
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="95%">
                                            </td>
                                            <td>
                                                <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted" type="button">
                                                    <span class="icon icon-lg icon-close"></span>
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="A009"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="xxx"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="9&quot;"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="20&quot;"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="40"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="900"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="400"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="9000"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="DK"></td>
                                            <td><input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="T"></td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="100%">
                                            </td>
                                            <td>
                                                <input class="form-control at-custom-select-sm2 text-center" type="text" placeholder="95%">
                                            </td>
                                            <td>
                                                <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted" type="button">
                                                    <span class="icon icon-lg icon-close"></span>
                                                </button>
                                            </td>
                                        </tr>
                                        
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-primary" data-toggle="modal" data-target="#Button_Addnew_Group_Machine_row" type="button">
                            <span class="icon icon-lg icon-plus"></span> Add Row
                        </button>
                        <button class="btn btn-success" data-dismiss="modal" type="button">Save</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="Button_Addnew_Group_Machine_row" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header bg-success">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Add</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-6 m-b">
                            <label>Group machine</label>
                            <input class="form-control" type="text" placeholder="A1">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Group machine detail</label>
                            <input class="form-control" type="text" placeholder="xxx">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Machine code</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Machine code detail</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>Parameter 1</label>
                            <div class="row">
                                <div class="col-xs-12">
                                <small>Min.</small>
                                <input class="form-control" type="text" placeholder="">
                                </div>
                                <div class="col-xs-12">
                                <small>Max.</small>
                                <input class="form-control" type="text" placeholder="">
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>Parameter 2</label>
                            <div class="row">
                                <div class="col-xs-12">
                                <small>Min.</small>
                                <input class="form-control" type="text" placeholder="">
                                </div>
                                <div class="col-xs-12">
                                <small>Max.</small>
                                <input class="form-control" type="text" placeholder="">
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-4 m-b">
                            <label>Parameter 3</label>
                            <div class="row">
                                <div class="col-xs-12">
                                <small>Min.</small>
                                <input class="form-control" type="text" placeholder="">
                                </div>
                                <div class="col-xs-12">
                                <small>Max.</small>
                                <input class="form-control" type="text" placeholder="">
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Binding</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Material type</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Cost efficiencies</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>Plan efficiencies</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                    </div>
                    <div class="m-t text-center">
                        <button class="btn btn-success" data-dismiss="modal" type="button">Add Row</button>
                        <button class="btn btn-default" data-dismiss="modal" type="button">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--  -->
<!--  -->
<!--  -->
<!--  -->
<div id="Button_Save" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <span class="text-success icon icon-check icon-5x"></span>
                    <h3 class="text-success">Success</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                    <div class="m-t-lg">
                    <button class="btn btn-success" data-dismiss="modal" type="button">Close</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>
<div id="Button_Deleted" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <span class="text-danger icon icon-times-circle icon-5x"></span>
                    <h3 class="text-danger">Warning - Are you sure</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    <div class="m-t-lg">
                        <button class="btn btn-outline-danger" data-dismiss="modal" type="button">Leave</button>
                        <button class="btn btn-danger" data-dismiss="modal" type="button">Delete</button>
                    </div>
                </div>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>

<!-- include -->
<?php
include('inc.modal.php');
?>