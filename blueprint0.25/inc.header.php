<?php
@session_start();
$_SESSION['IS_COLLAPSE'] = false;
include('inc.function.php');
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>ITC - KKFblueprint 50%</title>
    <!-- Meta tag -->
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">
    <meta name="description" content="ITC - KKFblueprint 50%">
    <meta property="og:url" content="http://">
    <meta property="og:type" content="website">
    <meta property="og:title" content="ITC - KKFblueprint 50%">
    <meta property="og:description" content="ITC - KKFblueprint 50%">
    <meta property="og:image" content="http://">
    <meta name="twitter:card" content="">
    <meta name="twitter:site" content="">
    <meta name="twitter:creator" content="">
    <meta name="twitter:title" content="">
    <meta name="twitter:description" content="">
    <meta name="twitter:image" content="http://">
    <!-- Link css -->
    <link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon.png">
    <link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="manifest.json">
    <link rel="mask-icon" href="safari-pinned-tab.svg" color="#0288d1">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,400italic,500,700">
    <link href="https://fonts.googleapis.com/css?family=Dosis:200,300,400,500,600,700,800" rel="stylesheet">
    <link rel="stylesheet" href="scheduler/codebase/dhtmlxscheduler.css">
    <!-- <link rel='STYLESHEET' type='text/css' href='https://dhtmlx.com/docs/products/dhtmlxScheduler/codebase/dhtmlxscheduler_material.css'> -->
    <link rel="stylesheet" href="css/vendor.min.css">
    <link rel="stylesheet" href="css/elephant.css">
    <link rel="stylesheet" href="css/application.min.css">
    <link rel="stylesheet" href="css/demo.min.css">
    <link rel="stylesheet" href="css/at.css">
    <link rel="stylesheet" href="css/custom.css">
    <link rel="stylesheet" href="css/custom-dhtmlx.css">

    <script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script>
    
    <!-- <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script> -->
</head>

<!-- <body class="layout layout-header-fixed  layout-sidebar-fixed layout-footer-fixed"> -->
<body class="layout layout-header-fixed layout-sidebar-fixed layout-footer-fixed">
<!-- hidden-xs hidden-sm layout-sidebar-sticky-->