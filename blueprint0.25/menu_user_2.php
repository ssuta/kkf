<?php
   $select = 'open_menu_9';
   $select2 = 'menu_9_2';
   $select3 = 'menu_9_2';
   $select4 = 'menu_9_2';
?>
<!--  -->
<? include('inc.header.php');?>
<!--  -->
<? include('inc.navbar.php');?>
<!--  -->
<? include('inc.menu.php');?>
<!-- <div class="layout-main"> -->
<!--  -->
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <h4 class="m-t-0">
                <!-- <span class="d-ib text-danger">--</span> <span class="icon icon-angle-double-right"></span> -->
                <!-- <span class="d-ib">ข้อมูลหลัก</span> -->
                <span class="d-ib text-primary"> ผู้ดูแลระบบ</span>  <span class="icon icon-angle-double-right"> จัดการผู้ใช้งานระบบ</span>  
                <!-- <span class="d-ib">
                    <a class="title-bar-shortcut" href="#" title="Add to shortcut list" data-container="body" data-toggle-text="Remove from shortcut list" data-trigger="hover" data-placement="right" data-toggle="tooltip">
                        <span class="sr-only">Add to shortcut list</span>
                    </a>
                </span> -->
            </h4>
            <!-- <p class="title-bar-description">
                <small>Latest update on 01/01/2017 - 10.57 am.</small>
            </p> -->
        </div>

        <div class="row gutter-xs">
            <div class="col-xs-12">
                <!--  -->
                <div class="card">
                    <!-- <div class="card-header">
                        <a href="javascript:window.history.back(-1);" class="btn btn-sm btn-outline-primary"><span class="icon icon-long-arrow-left"></span>&nbsp;&nbsp;Back</a>&nbsp;&nbsp;
                        <strong class="">Latest update on 01/01/2017 - 10.57 am.</strong>
                    </div> -->
                    <div class="card-body">
                        <!--  -->
<div class="at_add_box">
    <div class="row">
        <div class="col-sm-12 text-right">
            <button class="btn btn-success" type="button"><span class="icon icon-lg icon-plus"></span>&nbsp;&nbsp;เพิ่มใหม่</button>
            <button onClick="window.print()" class="btn btn-default" type="button"><span class="icon icon-lg icon-print"></span>&nbsp;&nbsp;พิมพ์</button>
            <a href="images/SampleXLSFile_212kb.xls" class="btn btn-warning" type="button"><span class="icon icon-lg icon-file-text-o"></span>&nbsp;&nbsp;ส่งออกไฟล์</a>
        </div>
    </div>
</div>
                        <!--  -->
                        <div class="table-responsive">
                            <table id="demo-datatables-2" class="table table-striped table-bordered table-nowrap dataTable text-center no-footer" cellspacing="0" width="100%">
                                <thead>
                                    <tr class="at_bg_table_blue">
										<th class="text-center">รหัสพนักงาน</th>
                                        <th class="text-center">ชื่อ</th>
                                        <th class="text-center">นามสกุล</th>
                                        <th class="text-center">แผนก</th>
                                        <th class="text-center">ตำแหน่ง</th>
										<th class="text-center">สิทธิ</th>
										<th class="text-center">สถานะการใช้งาน</th>
										<th class="text-center">ปรับแก้ไข</th>
                                    </tr>
                                </thead>
                                <tbody>
								<?php	
								for($i = 0;$i<=15;$i++){
								?>
								<tr>
    <td>MK001</td>
	<td>นางสาวตะข่าย</td>
	<td>กว้างขวาง</td>
	<td>การตลาด</td>
    <td>ผู้จัดการ</td>
    <td>Administrator</td>
	<td>ใช้งานอยู่</td>
	<td class="text-center" style="display: table-cell;">
        <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="edit_menu_user_2.php" type="button">
<span class="icon icon-lg icon-edit"></span>
</a>
        <button class="btn btn-outline-danger btn-pill btn-xs" data-toggle="modal" data-target="#Button_Deleted_New" type="button">
            <span class="icon icon-lg icon-close"></span>
        </button>
    </td>
</tr>
								<?php
								}
								?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
            </div>
        </div>










    </div>
</div>
<!--  -->
<? include('inc.footer.php');?>
<!--  -->
<? include('inc.footer.script.php');?>
<!--  -->
<!--  -->
<div id="menu_7_2_edit" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">แก้ไข</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-xs-6 m-b">
                            <label>ชื่อ</label>
                            <input class="form-control" type="text" placeholder="นางสาวตะข่าย">
                        </div>
                        <div class="col-xs-6 m-b">
                            <label>นามสกุล</label>
                            <input class="form-control" type="text" placeholder="กว้างขวาง">
                        </div>
						 <div class="col-xs-6 m-b">
                            <label>แผนก</label>
                            <input class="form-control" type="text" placeholder="การตลาด">
                        </div>
						 <div class="col-xs-6 m-b">
                            <label>ตำแหน่ง</label>
                            <input class="form-control" type="text" placeholder="ผู้จัดการ">
                        </div>

						 <div class="col-xs-6 m-b">
                            <label>สิทธิการใช้งาน</label>
                           <select class="custom-select">
                        <option value="" selected="">Administrator</option>
                        <option value="c-plus-plus">Manager</option>
                        <option value="css">Customer</option>
                      </select>
                        </div>

						 <div class="col-xs-6 m-b">
                            <label>สถานะการใช้งาน</label>
							<div class="custom-controls-stacked m-t">
                      <label class="switch switch-primary">
                        <input class="switch-input" type="checkbox" checked="checked">
                        <span class="switch-track"></span>
                        <span class="switch-thumb"></span>
                      </label>
                      </div>
                        </div>

                    </div>
                    <div class="m-t text-center">
						<button onclick="myFunction()" class="btn btn-lg btn-primary" data-dismiss="modal" type="button">บันทึก</button>
                        <button class="btn btn-lg btn-default" data-dismiss="modal" type="button">ยกเลิก</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--  -->
