<?php
    $select = 'open_menu_4';
    $select2 = 'menu_mf_current_production_plan';

	 $productType = array('NYLON TWINE', 'MONO NET','MONO LINE','KNOTLESS NET','TRAMMEL');
     $productNo = array('V163030D2', 'V163030D2','M130PA');

     $statusText = array('ผลิตเสร็จแล้ว', 'กำลังผลิต', 'รอการผลิต');
     $statusClass = array('success', 'info', 'warning');
?>
<!--  -->
<? include('inc.header.php');?>
<!--  -->
<? include('inc.navbar.php');?>
<!--  -->
<? include('inc.menu.php');?>
<!-- <div class="layout-main"> -->
<!--  -->
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <h4 class="m-t-0">
                <!-- <span class="d-ib text-danger">--</span> <span class="icon icon-angle-double-right"></span> -->
                <!-- <span class="d-ib">ข้อมูลหลัก</span> -->
                <span class="d-ib text-primary">การผลิต</span> <span class="icon icon-angle-double-right"></span> แผนการผลิตปัจจุบัน
                <!-- <span class="d-ib">
                    <a class="title-bar-shortcut" href="#" title="Add to shortcut list" data-container="body" data-toggle-text="Remove from shortcut list" data-trigger="hover" data-placement="right" data-toggle="tooltip">
                        <span class="sr-only">Add to shortcut list</span>
                    </a>
                </span> -->
            </h4>
            <!-- <p class="title-bar-description">
                <small>Latest update on 01/01/2017 - 10.57 am.</small>
            </p> -->
        </div>

        <div class="row gutter-xs">
            <div class="col-xs-12">
                <!--  -->
                <div class="card">
                    <!-- <div class="card-header">
                        <a href="javascript:window.history.back(-1);" class="btn btn-sm btn-outline-primary"><span class="icon icon-long-arrow-left"></span>&nbsp;&nbsp;Back</a>&nbsp;&nbsp;
                        <strong class="">Latest update on 01/01/2017 - 10.57 am.</strong>
                    </div> -->
                    <div class="card-body">
                        <!--  -->

						
                        <div class="at_add_box">
    <div class="card" aria-expanded="true">
        <div class="card-header at_bg_table_light_gray">
            <div class="card-actions">
                    <button type="button" class="card-action card-toggler" title="Collapse" aria-expanded="true"></button>
                    <!-- <button type="button" class="card-action card-reload" title="Reload"></button>
                    <button type="button" class="card-action card-remove" title="Remove"></button> -->
            </div>
            <strong>ตัวเลือกการค้นหา</strong>
        </div>
        <div class="card-body" style="display: block;">
            <div class="row">
                <div class="col-md-3">
                    <form class="row form form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-3 control-label text-right" for="form-control-14">สาขา</label>
                            <div class="col-sm-9 has-feedback">
                                <select id="demo-select2-1" class="form-control">
                                    <option value="0">ทั้งหมด</option>
									<option value="B&S : สารคาม">B&S : สารคาม</option>
    <option value="BWC : ศูนย์บวร">BWC : ศูนย์บวร</option>
    <option value="CY : เชียงยืน">CY : เชียงยืน</option>
    <option value="EXT : ภายนอก">EXT : ภายนอก</option>
    <option value="XFF : สาขาเมืองจีน">XFF : สาขาเมืองจีน</option>
                                </select>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-sm-4">
                    <form class="row form form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-5 control-label text-right" for="form-control-14">กลุ่มเครื่องจักร</label>
                            <div class="col-sm-7 has-feedback">
                                <select class="form-control">
                                    <option value="0">C1 : 	9.0DK 033-040 012-015*8,10,12 210/9</option>
                                    <option value="1">C5 : 	DK12.7 045-055 210/15-18</option>
                                    <option value="2">C6 : 	DK 19.0 070-117 210/36-48</option>
                                </select>
                            </div>
                        </div>
                    </form>
                </div>
                 <div class="col-sm-4">
                    <form class="row form form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-5 control-label text-right" for="form-control-14">หมายเลขเครื่องจักร</label>
                            <div class="col-sm-7 has-feedback">
                                <select id="demo-select2-2" class="form-control">
                                    <option value="0">ทั้งหมด</option>
                                    <option value="1">B02 : เครื่อง B02</option>
                                    <option value="2">B03 : เครื่อง B03</option>
									 <option value="3">B04 : เครื่อง B04</option>
                                </select>
                            </div>
                        </div>
                    </form>
                </div>
                
                <div class="col-sm-1">
                    <form class="form form-horizontal">
                        <div class="text-right">
                            <a class="btn btn-md btn-primary" href="menu_2_1.php" type="submit">ค้นหา</a>
                        </div>
                    </form>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-3">
                    <form class="row form form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-3 control-label text-right" for="form-control-14">สถานะ</label>
                            <div class="col-sm-9 has-feedback">
                                <select id="demo-select2-3" class="form-control">
                                    <option value="0">ทั้งหมด</option>
                                    <option value="0">ผลิตเสร็จแล้ว</option>
                                    <option value="0">กำลังผลิต</option>
                                    <option value="0">มีการแก้ไขแผน รออนุมัติ</option>
                                    <option value="0">มีการเปลี่ยนแปลง รอร่างแผนใหม่</option>
                                    <option value="0">วางแผนผลิตและอนุมัติแล้ว</option>
                                </select>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-sm-4">
                    <form class="row form form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-5 control-label text-right" for="form-control-14">ตามช่วงเวลา</label>
                            <div class="col-sm-7 has-feedback">
                                <select class="form-control">
                                    <option value="0">ผลิตส่วนหน้า (ทอ)</option>
                                    <option value="1">ผลิตส่วนหลัง (วงจร)</option>
                                    <option value="2">ผลิตทั้งหมด (ทอ+วงจร)</option>
                                </select>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-sm-5">
                    <div class="input-group input-daterange" data-provide="datepicker" data-date-autoclose="true" data-date-format="dd-M-yyyy">
                        <input class="form-control" type="text" value="เริ่มต้น">
                        <span class="input-group-addon">ถึง</span>
                        <input class="form-control" type="text" value="สิ้นสุด">
                    </div>
                </div>
            </div>
		</div>
	</div>
</div>

<div class="at_add_box extra">
    <div class="row">
        <div class="col-sm-12 text-right">
            <button onClick="window.print()" class="btn btn-default" type="button"><span class="icon icon-lg icon-print"></span>&nbsp;&nbsp;พิมพ์</button>
            <a href="images/SampleXLSFile_212kb.xls" class="btn btn-warning" type="button"><span class="icon icon-lg icon-file-text-o"></span>&nbsp;&nbsp;ส่งออกข้อมูล</a>
        </div>
    </div>
</div>
						
                        <!--  -->
                        <div class="table-responsive">
                            <table id="datatables-custom" class="table table-hover table-bordered text-center" cellspacing="0" width="100%">
                                <thead>
									<tr class="at_bg_table_blue">
                                        <th class="text-center middle" rowspan="3">กลุ่มเครื่องจักร</th>
                                        <th class="text-center middle" rowspan="3">หมายเลขเครื่องจักร</th>
                                        <th class="text-center middle" rowspan="3">ประเภทสินค้า</th>
                                        <th class="text-center middle" rowspan="3">หมายเลขสินค้า</th>
                                        <th class="text-center middle" colspan="4">วันที่ผลิต</th>
                                        <th class="text-center middle" colspan="2" rowspan="2">วันที่ส่ง<br>ฝ่ายคลังสินค้า</th>
                                        <th class="text-center middle" rowspan="3">วันที่<br>จัดส่งสินค้า</th>
                                        <th class="text-center middle" rowspan="3">สถานะ</th>
                                        <th class="text-center middle" rowspan="3">แสดง</th>
                                    </tr>
                                    <tr class="at_bg_table_blue">
                                        <th class="text-center" colspan="2">ทอ</th>
                                        <th class="text-center" colspan="2">วงจรส่วนหลัง</th>
                                    </tr>
                                    <tr class="at_bg_table_blue">
                                        <th class="text-center">เริ่ม</th>
                                        <th class="text-center">เสร็จ</th>
                                        <th class="text-center">เริ่ม</th>
                                        <th class="text-center">เสร็จ</th>
                                        <th class="text-center">เริ่ม</th>
                                        <th class="text-center">เสร็จ</th>
                                    </tr>
                                </thead>
                               <tbody>
                                    <?php for($i=0; $i<10; $i++){ ?>
                                        <?php $statusRand = rand(0,2); ?>
                                        <tr data-toggle="collapse" data-target="#item-<?=$i+1?>" class="accordion-toggle">
                                            <td>
                                                C<?=$i+1?>
                                            </td>
                                            <td>B0<?=$i+1?></td>
                                            <td><?=$productType[rand(0,1)]?></td>
                                            <td><?=$productNo[rand(0,1)]?></td>
                                            <!-- <td>100</td> -->
                                            <td>20/07/2555</td>
                                            <td>20/07/2555</td>
                                            <td>20/07/2555</td>
                                            <td>20/07/2555</td>
                                            <td>20/07/2555</td>
                                            <td>20/07/2555</td>
                                            <td class="top">20/07/2555</td>
                                            <td class="left"><button class="btn btn-<?=$statusClass[$statusRand]?> status " type="button"><?=$statusText[$statusRand]?></button></td>
                                            <td class="text-center" style="display: table-cell;">
                                                <a class="btn btn-lg btn-outline-primary btn-pill btn-xs" href="menu_mk_3.php" type="button">
                                                    <span class="icon icon-lg icon-file-text-o"></span>
                                                </a>
                                            </td>
                                        </tr>
                                        
                                        
                                        <tr class="cl">
                                            <td colspan="14" class="bg_item_in n-p-i">
                                                <div class="accordian-body collapse" id="item-<?=$i+1?>"> 
                                                    <table class="table table-bordered text-center" cellspacing="0" width="100%">
                                                        <thead>
                                                        <tr class="bg_table_in">
                                                            <th class="text-center middle" rowspan="3">รหัสสินค้า</th>
                                                            <th class="text-center middle" rowspan="3">ประเภทสินค้า</th>
                                                            <th class="text-center middle" colspan="2" rowspan="2">สั่งซื้อ</th>
                                                            <th class="text-center middle" colspan="4">วันที่ผลิต</th>
                                                            <th class="text-center middle" rowspan="3">จำนวนชุดทอ</th>
                                                            <th class="text-center middle" rowspan="3">จำนวนผืนทอ</th>
                                                            <th class="text-center middle" colspan="2" rowspan="2">วันที่ส่งฝ่ายการผลิต</th>
                                                            <th class="text-center middle" rowspan="3">วันที่<br>จัดส่งสินค้า</th>
                                                            <th class="text-center middle" rowspan="3">สถานะในคลัง<br>สินค้าปัจจุบัน</th>
                                                        </tr>
                                                        <tr class="bg_table_in">
                                                            <th class="text-center" colspan="2">ทอ</th>
                                                            <th class="text-center" colspan="2">วงจรส่วนหลัง</th>
                                                        </tr>
                                                        <tr class="bg_table_in">
                                                            <th class="text-center">จำนวน</th>
                                                            <th class="text-center">หน่วย</th>
                                                            <th class="text-center">เริ่ม</th>
                                                            <th class="text-center">เสร็จ</th>
                                                            <th class="text-center">เริ่ม</th>
                                                            <th class="text-center">เสร็จ</th>
                                                            <th class="text-center">เริ่ม</th>
                                                            <th class="text-center">เสร็จ</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php $itemRand = rand(4,7) ?>
                                                            <?php for($j=0; $j<$itemRand; $j++){ ?>
                                                                <?php 
                                                                    $amountItem = rand(100, 300);
                                                                    $amountWovenDress = rand(10,20);
                                                                    $amountWeave = rand(300, 500);
                                                                ?>
                                                                <tr>
                                                                    <td>P000<?=$j+1?></td>
                                                                    <td>ข่ายเอ็น</td>
                                                                    <td><?=$amountItem?></td>
                                                                    <td>PC</td>
                                                                    <td>05/08/2556</td>
                                                                    <td>09/08/2556</td>
                                                                    <td>10/08/2556</td>
                                                                    <td>13/08/2556</td>
                                                                    <td><?=$amountWovenDress?></td>
                                                                    <td><?=$amountWeave?></td>
                                                                    <td>17/08/2556</td>
                                                                    <td>20/08/2556</td>
                                                                    <td>24/08/2556</td>
                                                                    <td><?=$amountWeave?></td>
                                                                </tr>
                                                            <?php } ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
            </div>
        </div>

    </div>
</div>
<!--  -->
<? include('inc.footer.php');?>
<!--  -->
<? include('inc.footer.script.php');?>
<!--  -->
