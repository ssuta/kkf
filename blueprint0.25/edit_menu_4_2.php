<?php
    $select = 'open_menu_1';
    $select2 = 'menu_4';
    $select3 = 'menu_4';
    $select4 = 'menu_4_2';
?>
<!--  -->
<? include('inc.header.php');?>
<!--  -->
<? include('inc.navbar.php');?>
<!--  -->
<? include('inc.menu.php');?>
<!-- <div class="layout-main"> -->
<!--  -->
<div class="layout-content">
    <div class="layout-content-body">
        <div class="title-bar">
            <h4 class="m-t-0">
                <!-- <span class="d-ib text-danger">--</span> <span class="icon icon-angle-double-right"></span> -->
                <!-- <span class="d-ib">ข้อมูลหลัก</span> -->
                <span class="d-ib text-primary">ข้อมูลหลัก</span> <span class="icon icon-angle-double-right"></span> เครื่องทอ <span class="icon icon-angle-double-right"></span> คุณสมบัติกลุ่มเครื่องทอ <span class="icon icon-angle-double-right"></span> แก้ไข
                <!-- <span class="d-ib">
                    <a class="title-bar-shortcut" href="#" title="Add to shortcut list" data-container="body" data-toggle-text="Remove from shortcut list" data-trigger="hover" data-placement="right" data-toggle="tooltip">
                        <span class="sr-only">Add to shortcut list</span>
                    </a>
                </span> -->
            </h4>
            <!-- <p class="title-bar-description">
                <small>Latest update on 01/01/2017 - 10.57 am.</small>
            </p> -->
        </div>

        <div class="row gutter-xs">
            <div class="col-xs-12">
                <!--  -->
                <div class="card">
                    <!-- <div class="card-header">
                        <a href="javascript:window.history.back(-1);" class="btn btn-sm btn-outline-primary"><span class="icon icon-long-arrow-left"></span>&nbsp;&nbsp;Back</a>&nbsp;&nbsp;
                        <strong class="">Latest update on 01/01/2017 - 10.57 am.</strong>
                    </div> -->
                    <div class="card-body">
                        <!--  -->
						  <form class="form form-horizontal">
                    <div class="form-group">
                        <div class="col-sm-4 m-b">
                            <label>รหัสกลุ่มเครื่องทอ</label>
                            <input class="form-control" type="text" placeholder="C1">
                        </div>
                        <div class="col-sm-4 m-b">
                            <label>เบอร์ใยต่ำสุด</label>
                            <input class="form-control" type="text" placeholder="012*8">
                        </div>
						 <div class="col-sm-4 m-b">
                            <label>เบอร์ใยสูงสุด</label>
                            <input class="form-control" type="text" placeholder="012*12">
                        </div>
						 <div class="col-sm-4 m-b">
                            <label>รหัสเงื่อน</label>
                            <input class="form-control" type="text" placeholder="DK">
                        </div>
						 <div class="col-sm-4 m-b">
                            <label>ขนาดตาต่ำสุด</label>
                            <input class="form-control" type="text" placeholder="3">
                        </div>
						 <div class="col-sm-4 m-b">
                            <label>ขนาดตาสูงสุด</label>
                            <input class="form-control" type="text" placeholder="120">
                        </div>
						 <div class="col-sm-4 m-b">
                            <label>จน. ตา ต่ำสุด</label>
                            <input class="form-control" type="text" placeholder="1.5">
                        </div>
						 <div class="col-sm-4 m-b">
                            <label>จน. ตา สูงสุด</label>
                            <input class="form-control" type="text" placeholder="520">
                        </div>
						 <div class="col-sm-4 m-b">
                            <label>ความยาวสูงสุด</label>
                            <input class="form-control" type="text" placeholder="999">
                        </div>
						 <div class="col-sm-4 m-b">
                            <label>ปภ.กลุ่มวัตถุดิบ</label>
                            <input class="form-control" type="text" placeholder="T">
                        </div>
						 <div class="col-sm-4 m-b">
                            <label>(MIN_STDCOST)</label>
                            <input class="form-control" type="text" placeholder="1">
                        </div>
						 <div class="col-sm-4 m-b">
                            <label>(ADD_COST)</label>
                            <input class="form-control" type="text" placeholder="50">
                        </div>
						 <div class="col-sm-4 m-b">
                            <label>(ADD_EFF)</label>
                            <input class="form-control" type="text" placeholder="-40">
                        </div>
						 <div class="col-sm-4 m-b">
                            <label>ขนาดใยจัดเรียงต่ำสุด</label>
                            <input class="form-control" type="text" placeholder="0.12">
                        </div>
						 <div class="col-sm-4 m-b">
                            <label>จน. เส้นจัดเรียงต่ำสุด</label>
                            <input class="form-control" type="text" placeholder="8">
                        </div>
						 <div class="col-sm-4 m-b">
                            <label>ประเภทใยต่ำสุด</label>
                            <input class="form-control" type="text" placeholder="2">
                        </div>
						 <div class="col-sm-4 m-b">
                            <label>อักขระพิเศษต่ำสุด</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
						 <div class="col-sm-4 m-b">
                            <label>ขนาดใยจัดเรียงสูงสุด</label>
                            <input class="form-control" type="text" placeholder="0.12">
                        </div>
						 <div class="col-sm-4 m-b">
                            <label>จน. เส้นจัดเรียงงสูงสุด</label>
                            <input class="form-control" type="text" placeholder="12">
                        </div>
						 <div class="col-sm-4 m-b">
                            <label>ประเภทใยสูงสุด</label>
                            <input class="form-control" type="text" placeholder="2">
                        </div>
						 <div class="col-sm-4 m-b">
                            <label>อักขระพิเศษสูงสุด</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
						 <div class="col-sm-4 m-b">
                            <label>เริ่มใช้งาน</label>
							<div class="input-with-icon">
                  <input class="form-control" type="text" data-provide="datepicker" placeholder="11/13/2009  5:09:08 PM">
                  <span class="icon icon-calendar input-icon"></span>
                </div>
                        </div>
                    </div>
                    <div class="m-t text-center">
						<a class="btn btn-lg btn-primary" href="menu_4_2.php" type="submit">บันทึก</a>
                        <a class="btn btn-lg btn-default" href="menu_4_2.php" type="button>">ยกเลิก</a>
                    </div>
                </form>
                        <!--  -->
                    </div>
                </div>
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
                <!--  -->
            </div>
        </div>










    </div>
</div>
<!--  -->
<? include('inc.footer.php');?>
<!--  -->
<? include('inc.footer.script.php');?>
<!--  -->